# CONTRIBUTING

## Compilation

- Download and install the open source version of Qt 6.6.0 or higher from www.qt.io
- Open "F1Telemetry.pro" with Qt Creator and compile the project


## Code Formatting
- Download and install clang-format from https://releases.llvm.org/download.html
- In Qt Creator: 
 - Activate the "Beautifier plugin" in the "About Plugin ..." dialog
 - Open the "Beatutifier" preferences
 - In the "General" tab, select "enable auto format on save" and "ClangFormat"
 - In the "Clang Format" tab, select "Use predefined style: File" and select "LLVM" as fallback style

## Deployment

### macOS
- Add a custom build step in Qt Creator:
 - Command: $QTDIR/bin/macdeployqt
 - Arguments: App/PXG\ F1\ Telemetry.app 
  
### Windows
- Add a custom build step in Qt Creator:
 - Command: %QTDIR%/bin/windeployqt.exe
 - Arguments: "App\release\PXG F1 Telemetry.exe" --dir package
- Copy "PXG F1 Telemetry.exe" in the package directory
- Install and copy the OpenSSL dll inside the package directory
