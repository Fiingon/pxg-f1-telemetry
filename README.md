
PXG F1 Telemetry is a tool to record, review and analyse UDP telemetry data from Codemasters F1 23.

## Features

F1 Telemetry is able to analyse laps, stints (multiple consecutive laps) and races of every drivers in the session (human or a.i.).

It allows you to :

- Compare your laps with the A.I. or other drivers to improve your driving
- Refine your setups
- Prepare your own practice programmes 
- Analyse what happened during a race

Data of the selected drivers is recorded on the hard drive during the sessions and can be reviewed later.

![Tracked drivers sélection](Screenshots/tracking.png)

### Analysis available for laps

- Speed
- Throttle input
- Brake input
- Steering input
- Gear
- Time differences
- Engine RPM
- Tyre wear
- Tyre temperatures
- Tyre stress
- Suspension
- ERS balance
- Tyres lockup
- Car balance i.e. oversteer / understeer
- Coasting
- Multiple laps comparison
- Car trajectory comparison

![Comparaison of laps](Screenshots/Laps.png)

![Comparaison of Trajectories](Screenshots/Trajectories.png)


### Analysis available for stints

- Lap times
- Average tyre wear per lap
- Avegrage tyre temperature
- Fuel consumption
- ERS energy
- Multiple stints comparison

![Comparaison of stints](Screenshots/Stints.png)

### Analysis available for races

- Race position
- Race time
- Lap times
- Tyre life
- Fuel consumption
- ERS energy
- Weather changes
- Air / track temperature
- Front wing damage
- Pitstop duration
- Penalties received
- Flashback count

![Analysis of races](Screenshots/Race.png)

## Author
Thibaud Rabillard (pxgf1telemetry@laposte.net)

## Contributors
- Nuvolarix (Testings)
- Sotirios Karvounis (Devlopment)
