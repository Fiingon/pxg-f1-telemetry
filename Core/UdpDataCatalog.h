#ifndef UDPDATACATALOG_H
#define UDPDATACATALOG_H

#include <QHash>
#include <QString>
#include <memory>

class TrackInfo;

using UdpData = QHash<int, QStringList>;

class UdpDataCatalog
{
  public:
	static QString formulaType(int type);
	static QString team(int team);
	static QString nationality(int nationality);

	static QString tyre(int tyre);
	static QString visualTyre(int visualTyre);
	static QString tyreWorkingRange(int tyre);
	static QString surface(int surface);

	static QString ersMode(int ers);
	static QString fuelMix(int fuel);

	static std::shared_ptr<TrackInfo> trackInfo(int track, bool useCache = true);
	static QString session(int session);
	static QString pitStatus(int status);
	static QString driverStatus(int status);
	static QString resultStatus(int status);
	static QString safetyCarStatus(int status);
	static QString weather(int weather);

	static QString buttonNamePs4(int button);
	static QString buttonNameXbox(int button);

	static QString penalty(int penalty);
	static QString infrigement(int infrigement);

	static QString ersModesDescription();
	static QString fuelMixesDescription();
	static QString weathersDescription();

	static QStringList ersModesValues();
	static QStringList fuelMixesValues();
	static QStringList weathersValues();

    static QString speedUnit(int unit);
    static QString temperatureUnit(int unit);
    static QString platform(int platform);

  private:
	static UdpDataCatalog *instance();
	static QString getValue(const QString &dataName, int value, int fieldIndex = 0);
	static QStringList getValues(const QString &dataName, int fieldIndex = 0);
	static QString getDescription(const QString &dataName, int fieldIndex = 0);

	QHash<QString, UdpData> _availableData;
	QHash<int, std::shared_ptr<TrackInfo>> _loadedTrackInfo;

	void loadCatalogs();
	UdpData readCsv(const QString &filename) const;

	std::shared_ptr<TrackInfo> getTrackInfo(int trackIndex, bool useCache);
};

#endif // UDPDATACATALOG_H
