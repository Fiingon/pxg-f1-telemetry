#include "TrackInfo.h"

#include <QFile>
#include <QSettings>

TrackInfo::TrackInfo() {}

bool TrackInfo::loadFromFile(const QString &filename)
{
	if(!QFile::exists(filename)) {
		return false;
	}

	QSettings settings(filename, QSettings::IniFormat);
	name = settings.value("name").toString();
	nbLaps = settings.value("laps", 0).toInt();
	layout = settings.value("layout").toString();

	settings.beginGroup("Turns");
	const auto keys = settings.allKeys();
	for(const auto &key : keys) {
		bool ok = false;
		auto turn = key.toInt(&ok);
		if(ok) {
			auto distance = settings.value(key).toDouble(&ok);
			if(ok) {
				turns << qMakePair(turn, distance);
			}
		}
	}
	settings.endGroup();
	std::sort(turns.begin(), turns.end(), [](auto t1, auto t2) { return t1.second < t2.second; });

	settings.beginGroup("Lines");
	auto leftLineFile = settings.value("left").toString();
	if(!leftLineFile.isEmpty())
		leftLine = loadLineFromFile(leftLineFile);

	auto rightLineFile = settings.value("right").toString();
	if(!rightLineFile.isEmpty())
		rightLine = loadLineFromFile(rightLineFile);

	auto centerLineFile = settings.value("center").toString();
	if(!centerLineFile.isEmpty())
		centerLine = loadLineFromFile(centerLineFile);
	settings.endGroup();

	return true;
}

QVector<MapPoint> TrackInfo::loadLineFromFile(const QString &filename)
{
	QVector<MapPoint> results;

	QFile file(filename);
	if(file.open(QIODevice::ReadOnly)) {
		QDataStream inStream(&file);
		inStream >> results;
	}

	return results;
}

bool TrackInfo::writeLineFromLap(const QString &outputFilename, const Lap *lap, double offset)
{
	auto data = lap->mapsData();
	if(offset != 0.0) {
		for(auto &point : data) {
			auto rightDirection = QPointF(point.direction.y(), -point.direction.x());
			auto correctedPosition = point.position + rightDirection * offset;
			point.position.setX(correctedPosition.x());
			point.position.setY(correctedPosition.y());
		}
	}

	QFile file(outputFilename);
	if(file.open(QIODevice::WriteOnly)) {
		QDataStream outStream(&file);
		outStream << data;

		return true;
	}

	return false;
}
