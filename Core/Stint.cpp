#include "Stint.h"
#include "UdpSpecification.h"

#include <QDataStream>
#include <QDir>
#include <QFile>
#include <QFileInfo>
#include <QtDebug>
#include <UdpDataCatalog.h>


Stint::Stint(const QVector<TelemetryInfo> &dataInfo) : Lap(dataInfo) { calculatedTyreWear = {0, 0, 0, 0}; }

QString Stint::description() const
{
    auto nbLap = QString::number(nbLaps());
    auto tyre = UdpDataCatalog::visualTyre(visualTyreCompound);
    return driver.driverFullName(isPlayer) + " - " + tyre + " - " + nbLap + "Laps";
}

void Stint::removeLastData()
{
    Lap::removeLastData();
    removeLastLinkedFile();
}

void Stint::resetData()
{
    Lap::resetData();
    averageSpeed = 0;
}

int Stint::nbLaps() const { return countData(); }

Stint *Stint::fromFile(const QString &filename)
{
    auto stint = new Stint;
    stint->load(filename);

    return stint;
}

void Stint::saveData(QDataStream &out) const
{
    QByteArray lapData;
    QDataStream outLap(&lapData, QIODevice::WriteOnly);
    Lap::saveData(outLap);
    out << lapData << lapTimes << calculatedTyreWear << lapsAdditionalInfo << averageSpeed;
}

void Stint::loadData(QDataStream &in)
{
    QByteArray lapData;
    in >> lapData;
    QDataStream inLap(&lapData, QIODevice::ReadOnly);
    Lap::loadData(inLap);

    in >> lapTimes >> calculatedTyreWear >> lapsAdditionalInfo >> averageSpeed;
}

QVariantMap Stint::exportData() const
{
    auto lapMap = Lap::exportData();
    QVariantMap map;

    lapMap["stint"] = map;
    lapMap["averageSpeed"] = QString::number(averageSpeed);
    return lapMap;
}
