#ifndef FLASHBACKMANAGER_H
#define FLASHBACKMANAGER_H

#include "UdpSpecification.h"
#include <QDateTime>
#include <QMap>
#include <QStringList>

enum class FlashbackType { Crash, Penalty, Echo, Other };


class FlashbackManager
{
  public:
	static QStringList typeNames;

	FlashbackManager();

	void clear();

	void setFlashbackEvent(float sessionTime);
	void setLap(const LapData &lapData);
	void setStatus(const CarStatusData &data);
	void setDamage(const CarDamageData &data);
	void setSession(const PacketSessionData &data);

	bool isOnFinishLine() const;

	QMap<FlashbackType, int> counts;

	bool isFirstStatusSinceFlashback() const;
	bool isFirstLapSinceFlashback() const;
	bool isFirstSessionSinceFlashback() const;
	bool isFirstDamageSinceFlashback() const;

	int totalCount() const;

  private:
	LapData _prevLapData;
	PacketSessionData _prevSessionData;
	CarStatusData _prevStatusData;
	CarDamageData _prevDamageData;

	float _lastFlashbackEventSessionTime = 0;
	bool _lastFlashbackCategorized = true;
	bool _isFirstLapSinceFlashback = false;
	bool _isFirstStatusSinceFlashback = false;
	bool _isFirstDamageSinceFlashback = false;
	bool _isFirstSessionSinceFlashback = false;
	bool _isOnFinishLine = false;

	int _lastFlashbackSessionTime = 0;

	void addFlashback(FlashbackType type);
	bool flashbackDetected(const LapData &data) const;
};

#endif // FLASHBACKMANAGER_H
