#include "UdpDataCatalog.h"

#include <QFile>
#include <QFileInfo>
#include <QTextStream>

#include <TrackInfo.h>

const constexpr char *BUTTONS = "buttons";
const constexpr char *ERS = "ers";
const constexpr char *FORMULA_TYPES = "formulaTypes";
const constexpr char *FUEL = "fuel";
const constexpr char *INFRINGEMENTS = "infringements";
const constexpr char *NATIONALITIES = "nationalities";
const constexpr char *PENALTIES = "penalties";
const constexpr char *SESSIONS = "sessions";
const constexpr char *SURFACES = "surfaces";
const constexpr char *TEAMS = "teams";
const constexpr char *TRACKS = "tracks";
const constexpr char *TYRES = "tyres";
const constexpr char *VISUAL_TYRES = "visualTyres";
const constexpr char *WEATHERS = "weathers";
const constexpr char *PIT_STATUS = "pitStatus";
const constexpr char *DRIVER_STATUS = "driverStatus";
const constexpr char *RESULT_STATUS = "resultStatus";
const constexpr char *SAFETY_CAR_STATUS = "safetyCarStatus";
const constexpr char *UNITS_SPEED = "unitsSpeed";
const constexpr char *UNITS_TEMP = "unitsTemp";
const constexpr char *PLATFORM = "platform";


const auto DATA_NAMES = {BUTTONS,      ERS,      FORMULA_TYPES, FUEL,          INFRINGEMENTS, NATIONALITIES,
						 PENALTIES,    SESSIONS, SURFACES,      TEAMS,         TRACKS,        TYRES,
                         VISUAL_TYRES, WEATHERS, PIT_STATUS,    DRIVER_STATUS, RESULT_STATUS, SAFETY_CAR_STATUS,
                         UNITS_SPEED, UNITS_TEMP, PLATFORM};


QString UdpDataCatalog::formulaType(int type) { return getValue(FORMULA_TYPES, type); }

QString UdpDataCatalog::team(int team) { return getValue(TEAMS, team); }

QString UdpDataCatalog::nationality(int nationality) { return getValue(NATIONALITIES, nationality); }

std::shared_ptr<TrackInfo> UdpDataCatalog::trackInfo(int track, bool useCache)
{
	return instance()->getTrackInfo(track, useCache);
}

QString UdpDataCatalog::tyre(int tyre) { return getValue(TYRES, tyre); }

QString UdpDataCatalog::visualTyre(int visualTyre)
{
	auto vt = getValue(VISUAL_TYRES, visualTyre);
	if(vt.isEmpty())
		vt = tyre(visualTyre);
	return vt;
}

QString UdpDataCatalog::tyreWorkingRange(int tyre) { return getValue(TYRES, tyre, 1); }

QString UdpDataCatalog::surface(int surface) { return getValue(SURFACES, surface); }

QString UdpDataCatalog::ersMode(int ers) { return getValue(ERS, ers); }

QString UdpDataCatalog::fuelMix(int fuel) { return getValue(FUEL, fuel); }

QString UdpDataCatalog::session(int session) { return getValue(SESSIONS, session); }

QString UdpDataCatalog::pitStatus(int status) { return getValue(PIT_STATUS, status); }

QString UdpDataCatalog::driverStatus(int status) { return getValue(DRIVER_STATUS, status); }

QString UdpDataCatalog::resultStatus(int status) { return getValue(RESULT_STATUS, status); }

QString UdpDataCatalog::safetyCarStatus(int status) { return getValue(SAFETY_CAR_STATUS, status); }

QString UdpDataCatalog::weather(int weather) { return getValue(WEATHERS, weather); }

QString UdpDataCatalog::buttonNamePs4(int button) { return getValue(BUTTONS, button, 0); }

QString UdpDataCatalog::buttonNameXbox(int button) { return getValue(BUTTONS, button, 1); }

QString UdpDataCatalog::penalty(int penalty) { return getValue(PENALTIES, penalty); }

QString UdpDataCatalog::infrigement(int infrigement) { return getValue(INFRINGEMENTS, infrigement); }

QString UdpDataCatalog::ersModesDescription() { return getDescription(ERS); }

QString UdpDataCatalog::fuelMixesDescription() { return getDescription(FUEL); }

QString UdpDataCatalog::weathersDescription() { return getDescription(WEATHERS); }

QStringList UdpDataCatalog::ersModesValues() { return getValues(ERS); }

QStringList UdpDataCatalog::fuelMixesValues() { return getValues(FUEL); }

QStringList UdpDataCatalog::weathersValues() { return getValues(WEATHERS); }

QString UdpDataCatalog::speedUnit(int unit) { return getValue(UNITS_SPEED, unit); };

QString UdpDataCatalog::temperatureUnit(int unit) { return getValue(UNITS_TEMP, unit); };

QString UdpDataCatalog::platform(int platform) { return getValue(PLATFORM, platform); };


// ------------------------------------------------------------------------------------------------------------

UdpDataCatalog *UdpDataCatalog::instance()
{
	static auto instance = UdpDataCatalog();
	if(instance._availableData.isEmpty()) {
		instance.loadCatalogs();
	}
	return &instance;
}

QString UdpDataCatalog::getValue(const QString &dataName, int value, int fieldIndex)
{
	return instance()->_availableData.value(dataName).value(value).value(fieldIndex);
}

QStringList UdpDataCatalog::getValues(const QString &dataName, int fieldIndex)
{
	QStringList values;
	const auto data = instance()->_availableData.value(dataName);
	for(auto it = data.constBegin(); it != data.constEnd(); ++it) {
		values << it.value().value(fieldIndex);
	}

	return values;
}

QString UdpDataCatalog::getDescription(const QString &dataName, int fieldIndex)
{
	QString desc;
	const auto data = instance()->_availableData.value(dataName);
	for(auto it = data.constBegin(); it != data.constEnd(); ++it) {
		if(!desc.isEmpty()) {
			desc += ", ";
		}
		desc += QString::number(it.key());
		desc += ": ";
		desc += it.value().value(fieldIndex);
	}

	return desc;
}

void UdpDataCatalog::loadCatalogs()
{
    Q_INIT_RESOURCE(F1Telemetry);

	_availableData.clear();
	for(const auto &name : DATA_NAMES) {

		auto filename = QString(":/catalogs/").append(name);
		auto data = readCsv(filename);
		_availableData[name] = data;
	}
}


UdpData UdpDataCatalog::readCsv(const QString &filename) const
{
	UdpData data;

	QFile file(filename);
	if(file.open(QIODevice::ReadOnly)) {
		QTextStream in(&file);
		auto lineNum = 0;
		while(!in.atEnd()) {
			auto line = in.readLine().trimmed();
			if(line.isEmpty()) {
				continue;
			}

			if(line.startsWith("//")) {
				continue;
			}

			auto fields = line.split('\t');

			if(fields.count() >= 2) {
				bool indexValid = false;
				auto index = fields.value(0).toInt(&indexValid);
				if(indexValid) {
					fields.removeFirst();
					data[index] = fields;
				}
			}

			++lineNum;
		}
	}

	return data;
}

std::shared_ptr<TrackInfo> UdpDataCatalog::getTrackInfo(int trackIndex, bool useCache)
{
	auto filename = getValue(TRACKS, trackIndex);

	if(useCache && _loadedTrackInfo.contains(trackIndex)) {
		return _loadedTrackInfo[trackIndex];
	}

	auto track = std::make_shared<TrackInfo>();
	bool ok = track->loadFromFile(filename);

	if(ok && useCache) {
		_loadedTrackInfo[trackIndex] = track;
	}

	return track;
}
