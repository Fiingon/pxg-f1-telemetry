#include "TyreSets.h"
#include "UdpSpecification.h"

TyreSets::TyreSets() {}

void TyreSets::resetData()
{
    _compoundsTimeDiff.clear();
    _tyresTimeDiff.clear();
}

void TyreSets::update(const PacketTyreSetsData &tyreSetData)
{
    if(isEmpty()) {
        for(auto set : std::as_const(tyreSetData.m_tyreSetData)) {
            _tyresTimeDiff << 0.0;
        }
    }

    _currentTyreIndex = tyreSetData.m_fittedIdx;

    auto fittedSet = tyreSetData.fittedSet();

    int refSetIndex = -1;
    int minWearDiff = 1000;
    for(int i = 0; i < tyreSetData.m_tyreSetData.count(); ++i) {
        const auto &set = tyreSetData.m_tyreSetData[i];
        if(fittedSet.m_actualTyreCompound == set.m_actualTyreCompound && i != _currentTyreIndex) {
            if(set.m_wear == 0) {
                refSetIndex = i;
                break;
            }
            auto wearDiff = abs(fittedSet.m_wear - set.m_wear);
            if(wearDiff < minWearDiff) {
                minWearDiff = wearDiff;
                refSetIndex = i;
            }
        }
    }

    if(refSetIndex != _currentTyreIndex && refSetIndex >= 0) {
        auto refSet = tyreSetData.m_tyreSetData[refSetIndex];
        auto delta = -refSet.m_lapDeltaTime + _tyresTimeDiff[refSetIndex];
        _tyresTimeDiff[_currentTyreIndex] = delta;
    }

    for(int i = 0; i < tyreSetData.m_tyreSetData.count(); ++i) {
        const auto &set = tyreSetData.m_tyreSetData[i];
        if(set.m_actualTyreCompound != fittedSet.m_actualTyreCompound) {
            if(set.m_wear == 0 && fittedSet.m_wear == 0) {
                if(!_compoundsTimeDiff.contains(set.m_actualTyreCompound) ||
                   !_compoundsTimeDiff[set.m_actualTyreCompound].contains(fittedSet.m_actualTyreCompound)) {
                    auto diff = set.m_lapDeltaTime;
                    _compoundsTimeDiff[set.m_actualTyreCompound][fittedSet.m_actualTyreCompound] = diff;
                    _compoundsTimeDiff[fittedSet.m_actualTyreCompound][set.m_actualTyreCompound] = -diff;
                }
            }
        }
    }
}

double TyreSets::currentTimeLossInSec() const { return _tyresTimeDiff.value(_currentTyreIndex, 0.0) / 1000.0; }
