#include "TelemetryData.h"

#include <QDataStream>
#include <QDir>
#include <QFile>
#include <QFileInfo>
#include <QJsonDocument>
#include <QJsonObject>
#include <QVariantList>
#include <QVariantMap>
#include <QtDebug>

const constexpr int GLOBAL_EVENT_VARINDEX = -1;
const constexpr int LOCAL_EVENT_VARINDEX = -2;


TelemetryData::TelemetryData(const QVector<TelemetryInfo> &dataInfo) : _telemetryInfo(dataInfo) {}

QVariant TelemetryData::autoSortData() const { return description(); }

void TelemetryData::addData(float x, const QVector<float> &dataValues)
{
	_xValues.append(x);
	_data.append(dataValues);
}

void TelemetryData::clearData()
{
	_xValues.clear();
	_data.clear();
	_telemetryEvents.clear();
	_linkedFiles.clear();
}

bool TelemetryData::hasData() const { return !_data.isEmpty(); }

int TelemetryData::countData() const { return _data.count(); }

void TelemetryData::removeLastData()
{
	if(hasData()) {
		_xValues.removeLast();
		_data.removeLast();
	}
}

void TelemetryData::removeTelemetryFrom(float x)
{
	if(!_xValues.isEmpty()) {
		auto d = _xValues.last();
		while(d > x) {
			removeLastData();
			if(_xValues.isEmpty())
				break;
			d = _xValues.last();
		}

		for(auto it = _telemetryEvents.begin(); it != _telemetryEvents.end(); ++it) {
			for(auto itEvent = it->begin(); itEvent != it->end();) {
				if(d > itEvent->x) {
					itEvent = it->erase(itEvent);
				} else {
					++itEvent;
				}
			}
		}
	}
}

QVector<float> TelemetryData::xValues() const { return _xValues; }

QVector<float> TelemetryData::smoothData(int index) const
{
	auto dataValues = data(index);

	QVector<float> originalValues = dataValues;
	for(int i = 0; i < _data.count(); ++i) {
		dataValues[i] = smoothAtIndex(originalValues, i, 2);
	}

	return dataValues;
}

QVector<float> TelemetryData::derivativeData(int index) const
{
	auto dataValues = data(index);

	QVector<float> originalValues = dataValues;
	for(int i = 0; i < _data.count(); ++i) {
		dataValues[i] = derivativeAtIndex(originalValues, _xValues, i);
	}

	return dataValues;
}

QVector<float> TelemetryData::data(int index) const
{
	QVector<float> dataValues;
	for(const auto &dataPoint : _data) {
		if(index >= dataPoint.count())
			break;
		dataValues << dataPoint[index];
	}

	return dataValues;
}

float TelemetryData::data(float x, int index) const
{
	if(_data.isEmpty()) {
		return std::nan("");
	}

	if(_xValues.contains(x)) {
		auto xIndex = _xValues.indexOf(x);
		return _data.value(xIndex).value(index, 0.0f);
	}

	auto upperBoundIt = std::upper_bound(_xValues.begin(), _xValues.end(), x);
	if(upperBoundIt != _xValues.end() && upperBoundIt != _xValues.begin()) {
		auto x1Index = upperBoundIt - _xValues.begin();
		auto x0 = _xValues.value(x1Index - 1);
		auto x1 = _xValues.value(x1Index);
		auto y0 = _data.value(x1Index - 1).value(index, 0.0f);
		auto y1 = _data.value(x1Index).value(index, 0.0f);
		return interpolate(x, x0, y0, x1, y1);
	} else if(upperBoundIt == _xValues.end()) {
		return std::nan("");
	}

	return std::nan("");
}

QVector<float> TelemetryData::lastRecordedData() const { return _data.last(); }

void TelemetryData::setTelemetryInfo(const QVector<TelemetryInfo> &dataNames) { _telemetryInfo = dataNames; }

void TelemetryData::addTelemetryEvent(int telemetryIndex,
									  float x,
									  const QString &description,
									  float y,
									  const QString &category)
{
	_telemetryEvents[telemetryIndex].append(TelemetryEvent{x, y, description, category});
}

void TelemetryData::addGlobalTelemetryEvent(float x, const QString &description, const QString &category)
{
	addTelemetryEvent(GLOBAL_EVENT_VARINDEX, x, description, NAN, category);
}

void TelemetryData::addLocalEvent(float x, const QString &description, const QString &category)
{
	addTelemetryEvent(LOCAL_EVENT_VARINDEX, x, description, NAN, category);
}

const QHash<int, QVector<TelemetryEvent>> &TelemetryData::telemetryEvents() const { return _telemetryEvents; }

QVector<TelemetryEvent> TelemetryData::telemetryEvents(int varIndex, bool includeGlobalEvents) const
{
	QVector<TelemetryEvent> events = _telemetryEvents.value(varIndex);
	if(includeGlobalEvents) {
		events.append(_telemetryEvents.value(GLOBAL_EVENT_VARINDEX));
	}

	return events;
}

double TelemetryData::integrateTelemetry(int index, const std::function<float(float)> &preprocess) const
{
	if(_xValues.isEmpty()) {
		return 0.0;
	}

	auto sum = 0.0;
	auto prevx = 0.0f;
	auto xit = _xValues.constBegin();
	for(const auto &alldata : _data) {
		auto value = preprocess(alldata.value(index));
		auto x = *xit;

		sum += double(value) * double(x - prevx);

		prevx = x;
		++xit;
	}

	auto lapDistance = xValues().last();

	return sum / lapDistance;
}

void TelemetryData::save(const QString &filename)
{
	QFile file(filename);
	if(file.open(QIODevice::WriteOnly)) {
		_filename = filename;
		QDataStream out(&file);
		saveData(out);
	} else {
		qDebug() << "Data saving failed in " << filename;
	}
}

void TelemetryData::load(const QString &filename)
{
	QFile file(filename);
	if(file.open(QIODevice::ReadOnly)) {
		_filename = filename;
		QDataStream in(&file);
		loadData(in);
	}
}

void TelemetryData::toJson(const QString &filename) const
{
	QJsonDocument doc;
	auto obj = QJsonObject::fromVariantMap(exportData());
	doc.setObject(obj);

	QFile file(filename);
	if(file.open(QIODevice::WriteOnly)) {
		file.write(doc.toJson());
	} else {
		qDebug() << "Export to JSON failed in " << filename;
	}
}

QString TelemetryData::savedFilename() const { return _filename; }

void TelemetryData::addLinkedFile(const QString &filename) { _linkedFiles << filename; }

void TelemetryData::removeLastLinkedFile()
{
	if(!_linkedFiles.isEmpty()) {
		_linkedFiles.removeLast();
	}
}

const QStringList &TelemetryData::linkedFiles() const { return _linkedFiles; }

TelemetryInfo &TelemetryData::info(int varIndex) { return _telemetryInfo[varIndex]; }

QStringList TelemetryData::relativeLinkedFiles() const
{
	if(!_filename.isEmpty()) {
		QFileInfo info(_filename);
		auto dir = info.absoluteDir();
		QStringList relativeFiles;
		for(const auto &file : std::as_const(_linkedFiles)) {
			relativeFiles << dir.relativeFilePath(file);
		}

		return relativeFiles;
	}

	return _linkedFiles;
}

void TelemetryData::saveData(QDataStream &out) const
{
	out << saveGenericData(_telemetryInfo) << _xValues << _data << saveGenericData(_telemetryEvents)
		<< relativeLinkedFiles();
}

void TelemetryData::loadData(QDataStream &in)
{
	QByteArray telemetryEvents;
	QStringList relativeLinkedFiles;
	QByteArray telemetryInfoData;
	in >> telemetryInfoData >> _xValues >> _data >> telemetryEvents >> relativeLinkedFiles;
	loadGenericData(_telemetryEvents, telemetryEvents);
	loadGenericData(_telemetryInfo, telemetryInfoData);

	if(!_filename.isEmpty()) {
		QFileInfo info(_filename);
		auto dir = info.absoluteDir();
		for(const auto &file : std::as_const(relativeLinkedFiles)) {
			_linkedFiles << dir.absoluteFilePath(file);
		}
	}
}

QVariantMap TelemetryData::exportData() const
{
	auto map = QVariantMap();
	auto xs = QVariantList();
	for(const auto &x : _xValues) {
		xs << x;
	}
	map["xValues"] = xs;

	auto yMap = QVariantMap();

	for(int i = 0; i < _telemetryInfo.count(); ++i) {
		const auto &values = data(i);
		auto vs = QVariantList();
		for(const auto &v : values) {
			vs << v;
		}
		const auto &info = _telemetryInfo[i];
		auto infoMap = info.toMap();
		infoMap["values"] = vs;
		yMap[info.name] = infoMap;
	}
	map["yValues"] = yMap;

	return map;
}

float TelemetryData::smoothAtIndex(const QVector<float> &values, int index, int nb) const
{
	float sum = 0.0;
	int nbValues = 0;
	for(int i = index - nb; i <= index + nb; i++) {
		if(i >= 0 && i < values.count()) {
			sum += values[i];
			++nbValues;
		}
	}

	if(nbValues == 0 || std::isnan(sum)) {
		return values.value(index);
	}

	return sum / float(nbValues);
}

float TelemetryData::derivativeAtIndex(const QVector<float> &values, const QVector<float> &xvalues, int index) const
{
	if(index == 0)
		return 0.0;

	float v1 = values[index];
	float v0 = values[index - 1];

	float x1 = xvalues[index];
	float x0 = xvalues[index - 1];

	if(qFuzzyIsNull(x1 - x0)) {
		return 0.0;
	}

	return (v1 - v0) / (x1 - x0);
}

QDataStream &operator>>(QDataStream &in, TelemetryInfo &data)
{
	in >> data.name >> data.description >> data.unit;
	return in;
}

QDataStream &operator<<(QDataStream &out, const TelemetryInfo &data)
{
	out << data.name << data.description << data.unit;
	return out;
}

QVariantMap TelemetryInfo::toMap() const
{
	QVariantMap map;
	map["name"] = name;
	map["description"] = description;
	map["unit"] = unit;
	return map;
}

QDataStream &operator>>(QDataStream &in, TelemetryEvent &data)
{
	in >> data.x >> data.y >> data.description >> data.category;
	return in;
}

QDataStream &operator<<(QDataStream &out, const TelemetryEvent &data)
{
	out << data.x << data.y << data.description << data.category;
	return out;
}

double TelemetryEvent::yValue(int varIndex, const TelemetryData *data) const
{
	if(std::isnan(y)) {
		return data->data(x, varIndex);
	}

	return y;
}

QVariantMap TelemetryEvent::toMap() const
{
	QVariantMap map;
	map["x"] = x;
	map["y"] = y;
	map["category"] = category;
	map["description"] = description;

	return map;
}
