#ifndef TELEMETRYDATA_H
#define TELEMETRYDATA_H

#include <QDataStream>
#include <QHash>
#include <QStringList>
#include <QVector>
#include <QIODevice>
#include <cmath>

struct TelemetryInfo {
	QString name;
	QString description;
	QString unit;

	QVariantMap toMap() const;

	bool isValid() const { return !name.isEmpty(); }
};

class TelemetryData;

struct TelemetryEvent {
	float x;
	float y = NAN;
	QString description;
	QString category;

	double yValue(int varIndex, const TelemetryData *data) const;

	QVariantMap toMap() const;
};


class TelemetryData
{
  public:
	TelemetryData(const QVector<TelemetryInfo> &dataInfo = {});
	virtual ~TelemetryData() = default;

	virtual QString description() const = 0;

	virtual QVariant autoSortData() const;

	void addData(float x, const QVector<float> &dataValues);
	void clearData();
	virtual void removeLastData();
	void removeTelemetryFrom(float x);

	bool hasData() const;
	int countData() const;

	QVector<float> xValues() const;
	QVector<float> data(int index) const;
	QVector<float> smoothData(int index) const;
	QVector<float> derivativeData(int index) const;
	float data(float x, int index) const;

	QVector<float> lastRecordedData() const;

	void setTelemetryInfo(const QVector<TelemetryInfo> &dataNames);
	virtual QVector<TelemetryInfo> availableData() const { return _telemetryInfo; }

	void addTelemetryEvent(int telemetryIndex,
						   float x,
						   const QString &description = QString(),
						   float y = NAN,
						   const QString &category = QString());
	void addGlobalTelemetryEvent(float x, const QString &description = QString(), const QString &category = QString());
	void addLocalEvent(float x, const QString &description = QString(), const QString &category = QString());

	const QHash<int, QVector<TelemetryEvent>> &telemetryEvents() const;
	QVector<TelemetryEvent> telemetryEvents(int varIndex, bool includeGlobalEvents) const;

	double integrateTelemetry(
		int index,
		const std::function<float(float)> &preprocess = [](auto value) { return value; }) const;

	void save(const QString &filename);
	void load(const QString &filename);

	void toJson(const QString &filename) const;

	virtual int trackIndex() const { return -1; };

	QString savedFilename() const;

	void addLinkedFile(const QString &filename);
	void removeLastLinkedFile();

	QStringList relativeLinkedFiles() const;
	const QStringList &linkedFiles() const;

	TelemetryInfo &info(int varIndex);

  protected:
	QVector<float> _xValues;
	QVector<QVector<float>> _data;
	QVector<TelemetryInfo> _telemetryInfo;
	QHash<int, QVector<TelemetryEvent>> _telemetryEvents;

	QString _filename;

	QStringList _linkedFiles;

	// Saving - Loading
	virtual void saveData(QDataStream &out) const;
	virtual void loadData(QDataStream &in);

	virtual QVariantMap exportData() const;


	template <class T> QByteArray saveGenericData(const T &data) const
	{
		QByteArray outData;
		QDataStream outStream(&outData, QIODevice::WriteOnly);
		outStream << data;
		return outData;
	}

	template <class T> void loadGenericData(T &data, QByteArray &inData) const
	{
		QDataStream inStream(&inData, QIODevice::ReadOnly);
		inStream >> data;
	}

	template <class T> T interpolate(float x, float x0, const T &y0, float x1, const T &y1) const
	{
		return y0 + ((y1 - y0) / (x1 - x0)) * (x - x0); // Linnear interpolation
	}

	float smoothAtIndex(const QVector<float> &values, int index, int nb) const;
	float derivativeAtIndex(const QVector<float> &values, const QVector<float> &xvalues, int index) const;
};

QDataStream &operator>>(QDataStream &in, TelemetryInfo &data);
QDataStream &operator<<(QDataStream &out, const TelemetryInfo &data);
QDataStream &operator>>(QDataStream &in, TelemetryEvent &data);
QDataStream &operator<<(QDataStream &out, const TelemetryEvent &data);
#endif // TELEMETRYDATA_H
