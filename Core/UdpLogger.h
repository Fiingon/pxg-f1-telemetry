#ifndef UDPLOGGER_H
#define UDPLOGGER_H

#include "F1Listener.h"

#include <QDir>

enum LoggableVariable {
    Speed,
    Acceleration,
    Braking,
    LapDistance,
    SessionDistance,
    LapNumber,
    LapTime,
    CarPosition,
    PitStatus,
    DriverStatus,
    ResultStatus,
    SurfaceType,
    Penalties,
    SafetyCarDelta,
    SafetyCarStatus,
    TrackPosition,
    PlayerWheelsSpeed,
    PlayerWheelsSlip,
    PlayerWheelsSlipAngle,
    PlayerCogHeight,
    PlayerButtonStatus,
    NetworkId
};


class UdpLogger : public QObject, public F1PacketInterface
{
    Q_OBJECT

  signals:
    void updated();

  public:
    UdpLogger();

    void setTrackedIndexes(const QVector<int> &trackedIndexes);
    QVector<int> trackedIndexes() const;
    QStringList trackedNames() const;


    QVector<LoggableVariable> variables() const;
    void setVariables(const QVector<LoggableVariable> &variables);
    void setVariable(LoggableVariable variable, bool visible);
    QString variableName(LoggableVariable variable) const;
    QList<LoggableVariable> availableVariables() const;

    void telemetryData(const PacketHeader &header, const PacketCarTelemetryData &data) override;
    void lapData(const PacketHeader &header, const PacketLapData &data) override;
    void sessionData(const PacketHeader &header, const PacketSessionData &data) override;
    void setupData(const PacketHeader &header, const PacketCarSetupData &data) override;
    void statusData(const PacketHeader &header, const PacketCarStatusData &data) override;
    void participant(const PacketHeader &header, const PacketParticipantsData &data) override;
    void motionData(const PacketHeader &header, const PacketMotionData &data) override;
    void motionExData(const PacketHeader &header, const PacketMotionExData &data) override;
    void eventData(const PacketHeader &header, const PacketEventData &event) override;
    void finalClassificationData(const PacketHeader &header, const PacketFinalClassificationData &data) override;
    void carDamageData(const PacketHeader &header, const PacketCarDamageData &data) override;
    void sessionHistoryData(const PacketHeader &header, const PacketSessionHistoryData &data) override;
    void tyreSetData(const PacketHeader &header, const PacketTyreSetsData &data) override;


    QString loggedData(int driver, LoggableVariable variable) const;

    QDir logDirectory() const;
    void setLogDirectory(const QDir &logDirectory);

    bool writeOnDisk() const;
    void setWriteOnDisk(bool writeOnDisk);

    void writeBuffers();

  private:
    QVector<int> _trackedIndexes;
    QVector<LoggableVariable> _variables;

    QHash<int, QMap<LoggableVariable, QString>> _loggedData;
    QStringList _driverNames;

    bool _writeOnDisk = false;
    QDir _logDirectory;
    bool _logDirectorySet = false;
    QList<QStringList> _buffers;
    int _filenum = -1;

    bool _telemetryReceived = false;
    bool _lapDataReceived = false;

    void checkUpdated();

    void fillBuffers();

    void log(int carIndex, LoggableVariable variable, const QString &value);

    template <class T>
    void logTyreNumbers(int carIndex, LoggableVariable variable, T value[4], const QString &unit = QString())
    {
        if(_variables.contains(variable)) {
            QString text = "[";
            for(int i = 0; i < 4; ++i) {
                if(i > 0) {
                    text += ", ";
                }
                text += QString::number(value[0]).append(unit);
            }
            text += "]";
            log(carIndex, variable, text);
        }
    }

    template <class T> void logNumber(int carIndex, LoggableVariable variable, T value, const QString &unit = QString())
    {
        log(carIndex, variable, QString::number(value).append(unit));
    }
};

#endif // UDPLOGGER_H
