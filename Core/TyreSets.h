
#ifndef TYRESETS_H
#define TYRESETS_H

#include <QHash>
#include <QVector>

struct PacketTyreSetsData;

class TyreSets
{
  public:
    TyreSets();

    bool isEmpty() const { return _tyresTimeDiff.isEmpty(); }
    void resetData();

    void update(const PacketTyreSetsData &tyreSetData);
    double currentTimeLossInSec() const;

  private:
    QHash<int, QHash<int, double>> _compoundsTimeDiff; // ms
    QVector<double> _tyresTimeDiff;                    // ms
    int _currentTyreIndex = -1;
};

#endif // TYRESETS_H
