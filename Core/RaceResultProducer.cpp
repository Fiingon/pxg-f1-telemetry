#include "RaceResultProducer.h"

RaceResultProducer::RaceResultProducer(const QString &filename)
{

	file.setFileName(filename);
	if(!file.exists()) {
		if(file.open(QIODevice::Append | QIODevice::Text)) {

			QTextStream out(&file);
			out << "Pos.,Driver,Grid Position,Stops,Points,Penalty(in s),Best Lap,Total Time (wo penalties), Total "
				   "Time\n";
			file.close();
		}
	} else {
		qInfo() << "Error with existing file.";
    }
}

RaceResultProducer::~RaceResultProducer() { file.close(); }

void RaceResultProducer::setRaceResult(const QVector<Race *> &rcs)
{
	if(file.open(QIODevice::Append | QIODevice::Text)) {
		QTextStream out(&file);
		double finalTotalTimeRef = -1.0;
		for(const auto &r : rcs) {
			auto bestLap = timeToStr(r->classification.m_bestLapTimeInMS);
			auto finalTotalTime = r->classification.m_totalRaceTime + r->classification.m_penaltiesTime;
			QString finalTotalTimeStr;
			if(finalTotalTimeRef < 0.0) {
				finalTotalTimeStr = raceTimeToStr(finalTotalTime);
				finalTotalTimeRef = finalTotalTime;
			} else {
				finalTotalTime = finalTotalTime - finalTotalTimeRef;
				finalTotalTimeStr = timeToStr(finalTotalTime * 1000.0, true);
			}

			out << r->classification.m_position << "," << r->driver.driverFullName(false) << ","
				<< r->classification.m_gridPosition << "," << r->pitstops.size() << "," << r->classification.m_points
				<< "," << r->classification.m_penaltiesTime << ',' << bestLap << ','
				<< raceTimeToStr(r->classification.m_totalRaceTime) << ',' << finalTotalTimeStr << '\n';
		}
		file.close();
    }
	directory.setPath(datadirectory.dirName());
}

QString RaceResultProducer::raceTimeToStr(double valueSec)
{
	auto time = QTime(0, 0).addSecs(int(floor(valueSec)));
	time = time.addMSecs(int((valueSec - floor(valueSec)) * 1000.0));
	return time.toString("h:mm:ss.zzz");
}

QString RaceResultProducer::timeToStr(double valueMS, bool forceSign)
{
	auto time = QTime(0, 0).addMSecs(abs(int(valueMS)));
	if(time.minute() >= 1) {
		return time.toString("m:ss.zzz");
	}
	auto text = time.toString("ss.zzz");
	if(valueMS < 0) {
		text.prepend("-");
	} else if(forceSign && valueMS > 0) {
		text.prepend("+");
	}
	return text;
}
