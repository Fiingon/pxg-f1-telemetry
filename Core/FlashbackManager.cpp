#include "FlashbackManager.h"
#include "UdpSpecification.h"

#include <QtDebug>

const constexpr int ECHO_DURATION_S = 18;

QStringList FlashbackManager::typeNames = {"Damage", "Penalty", "Retry", "Other"};

FlashbackManager::FlashbackManager() {}

void FlashbackManager::clear()
{
	counts.clear();
	_isFirstStatusSinceFlashback = false;
	_isFirstDamageSinceFlashback = false;
	_isFirstSessionSinceFlashback = false;
	_lastFlashbackCategorized = true;
}

void FlashbackManager::setFlashbackEvent(float sessionTime)
{
	_lastFlashbackEventSessionTime = sessionTime;
	_lastFlashbackCategorized = false;

	_isFirstStatusSinceFlashback = true;
	_isFirstDamageSinceFlashback = true;
	_isFirstSessionSinceFlashback = true;
	_isFirstLapSinceFlashback = true;
}

void FlashbackManager::setLap(const LapData &lapData)
{
	if(_isFirstLapSinceFlashback) {
		_isFirstLapSinceFlashback = false;
		_isOnFinishLine = lapData.m_lapDistance > 0 &&
						  (lapData.m_lapDistance > _prevLapData.m_lapDistance + 50 || _prevLapData.m_lapDistance < 0);

	} else if(!_lastFlashbackCategorized && !_isFirstStatusSinceFlashback && !_isFirstSessionSinceFlashback &&
			  !_isFirstDamageSinceFlashback) {
		addFlashback(FlashbackType::Other);
	}

	_prevLapData = lapData;
}

void FlashbackManager::setStatus(const CarStatusData &data)
{
	if(_isFirstStatusSinceFlashback) {
		_isFirstStatusSinceFlashback = false;
	}

	_prevStatusData = data;
}

void FlashbackManager::setDamage(const CarDamageData &data)
{
	if(isFirstDamageSinceFlashback() && !_lastFlashbackCategorized) {
		if(data.m_frontLeftWingDamage < _prevDamageData.m_frontLeftWingDamage ||
		   data.m_frontRightWingDamage < _prevDamageData.m_frontRightWingDamage) {
			addFlashback(FlashbackType::Crash);
		}
	}

	if(_isFirstDamageSinceFlashback) {
		_isFirstDamageSinceFlashback = false;
	}

	_prevDamageData = data;
}

void FlashbackManager::setSession(const PacketSessionData &data)
{
	if(isFirstSessionSinceFlashback() && !_lastFlashbackCategorized) {
		auto echoDiff = data.m_sessionTimeLeft - _lastFlashbackSessionTime;
		if(_lastFlashbackSessionTime > 0 && qAbs(echoDiff) < ECHO_DURATION_S) {
			addFlashback(FlashbackType::Echo);
		}

		_lastFlashbackSessionTime = data.m_sessionTimeLeft;
	}

	if(_isFirstSessionSinceFlashback) {
		_isFirstSessionSinceFlashback = false;
	}
	_prevSessionData = data;
}

bool FlashbackManager::flashbackDetected(const LapData &data) const
{
	return _prevLapData.isValid() && data.m_totalDistance + 0.5 < _prevLapData.m_totalDistance &&
		   data.m_driverStatus > 0;
}

bool FlashbackManager::isOnFinishLine() const { return _isOnFinishLine; }

bool FlashbackManager::isFirstStatusSinceFlashback() const { return _isFirstStatusSinceFlashback; }

bool FlashbackManager::isFirstLapSinceFlashback() const { return _isFirstLapSinceFlashback; }

bool FlashbackManager::isFirstSessionSinceFlashback() const { return _isFirstSessionSinceFlashback; }

bool FlashbackManager::isFirstDamageSinceFlashback() const { return _isFirstDamageSinceFlashback; }

int FlashbackManager::totalCount() const
{
	int total = 0;
	for(const auto value : counts) {
		total += value;
	}

	return total;
}

void FlashbackManager::addFlashback(FlashbackType type)
{
	if(_lastFlashbackCategorized) {
		return;
	}

	if(!counts.contains(type)) {
		counts[type] = 0;
	}

	counts[type] += 1;
	_lastFlashbackCategorized = true;
}
