#include "TelemetryDefinitions.h"
#include "UdpSpecification.h"

#include <UdpDataCatalog.h>

QHash<QString, TelemetryStaticInfo> TelemetryDefinitions::info = {};

void TelemetryDefinitions::initInfo()
{
    if(info.isEmpty()) {
        info[ERS_MODE_INFO_NAME] = TelemetryStaticInfo(UdpDataCatalog::ersModesValues());
        info[FUEL_MIX_INFO_NAME] = TelemetryStaticInfo(UdpDataCatalog::fuelMixesValues());
        info[WEATHER_INFO_NAME] = TelemetryStaticInfo(UdpDataCatalog::weathersValues());
    }
}

QStringList TelemetryDefinitions::intValue(int from, int to)
{
    QStringList values;
    for(int i = from; i < to; ++i) {
        values << QString::number(i);
    }

    return values;
}

int TelemetryDefinitions::indexOfLapTelemetry(const QString &name)
{
    return indexOfTelemetryIn(name, TELEMETRY_INFO + EXTENDED_TELEMETRY_INFO);
}

int TelemetryDefinitions::indexOfStintTelemetry(const QString &name)
{
    return indexOfTelemetryIn(name, TELEMETRY_STINT_INFO);
}

TelemetryStaticInfo TelemetryDefinitions::staticInfo(const QString &name)
{
    initInfo();
    return info.value(name);
}

int TelemetryDefinitions::indexOfTelemetryIn(const QString &name, const QVector<TelemetryInfo> &telemetryInfo)
{
    int i = 0;
    for(const auto &info : telemetryInfo) {
        if(info.name == name) {
            return i;
        }

        ++i;
    }

    return -1;
}

QVector<TelemetryInfo> TelemetryDefinitions::TELEMETRY_INFO = {
    TelemetryInfo{SPEED_INFO_NAME, "Speed of the car", "km/h"},
    TelemetryInfo{"Throttle", "Percentage of throttle pressed by the driver", "%"},
    TelemetryInfo{"Brake", "Percentage of brake pressed by the driver", "%"},
    TelemetryInfo{"Steering",
                  "Percentage of steering applied by the driver (>0: toward the right, <0: roward the left)", "%"},
    TelemetryInfo{"Gear", "", ""},
    TelemetryInfo{"Time", "", "s"},
    TelemetryInfo{"Tyres Time Loss", "Time lost by degradation of the tyres during this lap", "s"},
    TelemetryInfo{"Max Tyre Surface Temp.", "Surface temperature of the hotter tyre", "°C"},
    TelemetryInfo{"ERS Balance", "Energy harvested - energy deployed", "kJ"},
    TelemetryInfo{ERS_MODE_INFO_NAME, UdpDataCatalog::ersModesDescription(), ""},
    TelemetryInfo{FUEL_MIX_INFO_NAME, UdpDataCatalog::fuelMixesDescription(), ""},
    TelemetryInfo{"Lateral G-Force", "", "g"},
    TelemetryInfo{"Longitudinal G-Force", "", "g"},
    TelemetryInfo{"Engine RPM", "", "RPM"},
    TelemetryInfo{"Coasting", "1 when brake & acceleration < 5%", ""}};


const QVector<TelemetryInfo> TelemetryDefinitions::EXTENDED_TELEMETRY_INFO = {
    TelemetryInfo{"Front Locking", "Tyre locking and severity during the lap", "%"},
    TelemetryInfo{"Rear Locking", "Tyre locking and severity during the lap", "%"},
    TelemetryInfo{
        "Balance (Slip Angle)",
        "General balance of the car: = front slip angle - rear slip angle (>0: understeering, <0: oversteering)", ""},
    TelemetryInfo{TYRE_DEG_INFO_NAME, "Estimated tyre stress", ""},
    TelemetryInfo{TRACTION_INFO_NAME, "Estimated traction lost", "%"},
    TelemetryInfo{
        "Suspension F/R",
        "Front / Rear suspension balance (>0: the car tilt toward the front, <0: the car tilt toward the rear)", "mm"},
    TelemetryInfo{
        "Suspension R/L",
        "Right / Left suspension balance (>0: the car tilt toward the right, <0: the car tilt toward the left)", "mm"},
    TelemetryInfo{
                  "Front Height",
                  "Front plank edge height above road surface", "mm"},
    TelemetryInfo{
                  "Rear Height",
                  "Rear plank edge height above road surface", "mm"},
    TelemetryInfo{"Center Of Gravity", "Height of centre of gravity above ground.", "mm"}};

const QVector<TelemetryInfo> TelemetryDefinitions::TELEMETRY_STINT_INFO = {
    TelemetryInfo{"Lap Times", "", "s"},
    TelemetryInfo{"Tyres Wear", "Cumul of the average tyre wear by lap", "%"},
    TelemetryInfo{"Tyres Life", "Remaining average life of the tyres", "%"},
    TelemetryInfo{"Tyre Set Time Loss", "Time lost by the current tyre set by lap", "s"},
    TelemetryInfo{"Calculated Total Lost Traction", "Cumulated estimated total traction lost over each lap", "%"},
    TelemetryInfo{"Fuel", "Remaining fuel in the car", "kg"},
    TelemetryInfo{"Stored Energy", "Energy remaining in the battery", "kJ"},
    TelemetryInfo{"Front Right Tyre Temperature", "", "°C"},
    TelemetryInfo{"Front Left Tyre Temperature", "", "°C"},
    TelemetryInfo{"Rear Right Tyre Temperature", "", "°C"},
    TelemetryInfo{"Rear Left Tyre Temperature", "", "°C"},
};

const QVector<TelemetryInfo> TelemetryDefinitions::TELEMETRY_RACE_INFO = {
    TelemetryInfo{"Position", "", ""},
    TelemetryInfo{"Race Time", "", "s"},
    TelemetryInfo{"Lap Times", "", "s"},
    TelemetryInfo{"Tyres Life", "Average remaing life of the tyres", "%"},
    TelemetryInfo{"Fuel", "Remaining fuel in the car", "kg"},
    TelemetryInfo{"Stored Energy", "Energy remaining in the battery", "kJ"},
    TelemetryInfo{WEATHER_INFO_NAME, "", ""},
    TelemetryInfo{"Track Temperature", "", "°C"},
    TelemetryInfo{"Air Temperature", "", "°C"},
    TelemetryInfo{"Car Damage", "", "%"},
    TelemetryInfo{"Flashbacks", "", ""}};
