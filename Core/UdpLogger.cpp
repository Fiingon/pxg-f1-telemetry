#include "UdpLogger.h"

#include <QMap>
#include <UdpDataCatalog.h>

#include <QDateTime>
#include <QtDebug>

const auto VARIABLE_NAMES =
    QMap<LoggableVariable, QString>({{LoggableVariable::Speed, "Speed"},
                                     {LoggableVariable::SurfaceType, "SurfaceType"},
                                     {LoggableVariable::PlayerButtonStatus, "PlayerButtonStatus"},
                                     {LoggableVariable::Acceleration, "Acceleration"},
                                     {LoggableVariable::Braking, "Braking"},
                                     {LoggableVariable::LapDistance, "LapDistance"},
                                     {LoggableVariable::LapNumber, "LapNumber"},
                                     {LoggableVariable::LapTime, "LapTime"},
                                     {LoggableVariable::SafetyCarDelta, "SafetyCarDelta"},
                                     {LoggableVariable::SafetyCarStatus, "SafetyCarStatus"},
                                     {LoggableVariable::SessionDistance, "SessionDistance"},
                                     {LoggableVariable::CarPosition, "CarPosition"},
                                     {LoggableVariable::PitStatus, "PitStatus"},
                                     {LoggableVariable::DriverStatus, "DriverStatus"},
                                     {LoggableVariable::ResultStatus, "ResultStatus"},
                                     {LoggableVariable::TrackPosition, "TrackPosition"},
                                     {LoggableVariable::PlayerWheelsSlip, "PlayerWheelsSlipRatio"},
                                     {LoggableVariable::PlayerWheelsSlipAngle, "PlayerWheelsSlipAngle"},
                                     {LoggableVariable::PlayerWheelsSpeed, "PlayerWheelsSpeed"},
                                     {LoggableVariable::Penalties, "Penalties"}});

const int BUFFERS_MAX_LINES = 10000;

const char FIELDS_SEPARATOR = ';';


UdpLogger::UdpLogger()
{
    setVariables({LoggableVariable::Speed, LoggableVariable::LapDistance, LoggableVariable::LapTime,
                  LoggableVariable::PitStatus, LoggableVariable::DriverStatus, LoggableVariable::ResultStatus});
}

void UdpLogger::setTrackedIndexes(const QVector<int> &trackedIndexes)
{
    _loggedData.clear();
    _trackedIndexes = trackedIndexes;
}

QVector<int> UdpLogger::trackedIndexes() const { return _trackedIndexes; }

QStringList UdpLogger::trackedNames() const { return _driverNames; }

QVector<LoggableVariable> UdpLogger::variables() const { return _variables; }

void UdpLogger::setVariables(const QVector<LoggableVariable> &variables)
{
    _loggedData.clear();
    _variables = variables;
    std::sort(_variables.begin(), _variables.end());
}

void UdpLogger::setVariable(LoggableVariable variable, bool visible)
{
    if(visible && !_variables.contains(variable)) {
        _variables.append(variable);
        std::sort(_variables.begin(), _variables.end());
    } else if(!visible && _variables.contains(variable)) {
        _variables.removeAll(variable);
    }
}

QString UdpLogger::variableName(LoggableVariable variable) const { return VARIABLE_NAMES.value(variable); }

QList<LoggableVariable> UdpLogger::availableVariables() const { return VARIABLE_NAMES.keys(); }


void UdpLogger::telemetryData(const PacketHeader &header, const PacketCarTelemetryData &data)
{
    Q_UNUSED(header)

    for(int index : std::as_const(_trackedIndexes)) {
        logNumber(index, LoggableVariable::Speed, data.m_carTelemetryData[index].m_speed, "km/s");
        logNumber(index, LoggableVariable::Acceleration, data.m_carTelemetryData[index].m_throttle);
        logNumber(index, LoggableVariable::Braking, data.m_carTelemetryData[index].m_brake);

        if(_variables.contains(LoggableVariable::SurfaceType)) {
            auto surface = QString("[%1, %2, %3, %4]")
                               .arg(UdpDataCatalog::surface(data.m_carTelemetryData[index].m_surfaceType[0]),
                                    UdpDataCatalog::surface(data.m_carTelemetryData[index].m_surfaceType[1]),
                                    UdpDataCatalog::surface(data.m_carTelemetryData[index].m_surfaceType[2]),
                                    UdpDataCatalog::surface(data.m_carTelemetryData[index].m_surfaceType[3]));
            log(index, LoggableVariable::SurfaceType, surface);
        }
    }

    _telemetryReceived = true;
    checkUpdated();
}

void UdpLogger::lapData(const PacketHeader &header, const PacketLapData &data)
{
    Q_UNUSED(header)

    for(int index : std::as_const(_trackedIndexes)) {
        logNumber(index, LoggableVariable::LapDistance, data.m_lapData[index].m_lapDistance, "m");
        logNumber(index, LoggableVariable::SessionDistance, data.m_lapData[index].m_totalDistance, "m");
        logNumber(index, LoggableVariable::CarPosition, data.m_lapData[index].m_carPosition);
        logNumber(index, LoggableVariable::LapNumber, data.m_lapData[index].m_currentLapNum);
        logNumber(index, LoggableVariable::LapTime, data.m_lapData[index].m_currentLapTimeInMS / 1000.0, "s");
        logNumber(index, LoggableVariable::Penalties, data.m_lapData[index].m_penalties, "s");
        logNumber(index, LoggableVariable::SafetyCarDelta, data.m_lapData[index].m_safetyCarDelta, "s");
        log(index, LoggableVariable::PitStatus, UdpDataCatalog::pitStatus(data.m_lapData[index].m_pitStatus));
        log(index, LoggableVariable::DriverStatus, UdpDataCatalog::driverStatus(data.m_lapData[index].m_driverStatus));
        log(index, LoggableVariable::ResultStatus, UdpDataCatalog::resultStatus(data.m_lapData[index].m_resultStatus));
    }

    _lapDataReceived = true;
    checkUpdated();
}

void UdpLogger::sessionData(const PacketHeader &header, const PacketSessionData &data)
{
    Q_UNUSED(header)

    for(int index : std::as_const(_trackedIndexes)) {
        log(index, LoggableVariable::SafetyCarStatus, UdpDataCatalog::resultStatus(data.m_safetyCarStatus));
    }
}

void UdpLogger::setupData(const PacketHeader &header, const PacketCarSetupData &data)
{
    Q_UNUSED(header)
    Q_UNUSED(data)
}

void UdpLogger::statusData(const PacketHeader &header, const PacketCarStatusData &data)
{
    Q_UNUSED(header)
    Q_UNUSED(data)
}

void UdpLogger::participant(const PacketHeader &header, const PacketParticipantsData &data)
{
    Q_UNUSED(header)

    _driverNames.clear();
    for(int index : std::as_const(_trackedIndexes)) {
        _driverNames << data.m_participants[index].driverFullName(index == header.m_playerCarIndex);
        logNumber(index, LoggableVariable::NetworkId, data.m_participants[index].m_networkId);
    }
}

void UdpLogger::motionData(const PacketHeader &header, const PacketMotionData &data)
{
    Q_UNUSED(header)

    for(int index : std::as_const(_trackedIndexes)) {

        if(_variables.contains(LoggableVariable::TrackPosition)) {
            auto position = QString("[%1, %2, %3]")
                                .arg(data.m_carMotionData[index].m_worldPositionX)
                                .arg(data.m_carMotionData[index].m_worldPositionY)
                                .arg(data.m_carMotionData[index].m_worldPositionZ);
            log(index, LoggableVariable::TrackPosition, position);
        }
    }
}

void UdpLogger::motionExData(const PacketHeader &header, const PacketMotionExData &data)
{
    Q_UNUSED(header)

    for(int index : std::as_const(_trackedIndexes)) {
        logTyreNumbers(index, LoggableVariable::PlayerWheelsSpeed, data.m_wheelSpeed);
        logTyreNumbers(index, LoggableVariable::PlayerWheelsSlip, data.m_wheelSlipRatio);
        logTyreNumbers(index, LoggableVariable::PlayerWheelsSlipAngle, data.m_wheelSlipRatio);
        logNumber(index, LoggableVariable::PlayerCogHeight, data.m_heightOfCOGAboveGround);
    }
}

void UdpLogger::eventData(const PacketHeader &header, const PacketEventData &event)
{
    if(event.event == Event::ButtonStatus) {
        logNumber(header.m_playerCarIndex, LoggableVariable::PlayerButtonStatus, event.details.Buttons.m_buttonStatus);
    }
}

void UdpLogger::finalClassificationData(const PacketHeader &header, const PacketFinalClassificationData &data)
{
    Q_UNUSED(header)
    Q_UNUSED(data)
}

void UdpLogger::carDamageData(const PacketHeader &header, const PacketCarDamageData &data)
{
    Q_UNUSED(header)
    Q_UNUSED(data)
}

void UdpLogger::sessionHistoryData(const PacketHeader &header, const PacketSessionHistoryData &data)
{
    Q_UNUSED(header)
    Q_UNUSED(data)
}

void UdpLogger::tyreSetData(const PacketHeader &header, const PacketTyreSetsData &data)
{
    Q_UNUSED(header)
    Q_UNUSED(data)
}

QString UdpLogger::loggedData(int driver, LoggableVariable variable) const
{
    return _loggedData.value(driver).value(variable);
}

QDir UdpLogger::logDirectory() const { return _logDirectory; }

void UdpLogger::setLogDirectory(const QDir &logDirectory)
{
    _logDirectory = logDirectory;
    _logDirectory.mkdir("Logs");
    _logDirectory.cd("Logs");
    _filenum = 0;
}

bool UdpLogger::writeOnDisk() const { return _writeOnDisk; }

void UdpLogger::setWriteOnDisk(bool writeOnDisk) { _writeOnDisk = writeOnDisk; }

void UdpLogger::checkUpdated()
{
    if(_telemetryReceived && _lapDataReceived) {
        _telemetryReceived = false;
        _lapDataReceived = false;

        emit updated();

        if(_writeOnDisk) {
            fillBuffers();
        }
    }
}

void UdpLogger::fillBuffers()
{
    if(_buffers.count() < _trackedIndexes.count()) {
        for(int i = _buffers.count(); i < _trackedIndexes.count(); ++i) {
            _buffers << QStringList();
        }
    }

    int index = 0;
    for(int carIndex : std::as_const(_trackedIndexes)) {
        QString text;
        auto data = _loggedData[carIndex];
        text += QDateTime::currentDateTime().toString("hh:mm:ss.zzz");
        text += FIELDS_SEPARATOR;
        for(auto it = data.cbegin(); it != data.cend(); ++it) {
            text += it.value();
            text += FIELDS_SEPARATOR;
        }
        _buffers[index] << text;
        ++index;
    }

    if(_buffers.value(0).count() > BUFFERS_MAX_LINES) {
        writeBuffers();
    }
}

void UdpLogger::writeBuffers()
{
    if(_filenum < 0 || !_writeOnDisk || _buffers.count() < _trackedIndexes.count()) {
        return;
    }


    auto index = 0;
    for(int carIndex : std::as_const(_trackedIndexes)) {
        auto name = _driverNames.value(index, QString::number(carIndex));
        name += ".";
        name += QString::number(_filenum);
        auto filename = _logDirectory.absoluteFilePath(name);

        QFile file(filename);
        if(file.open(QIODevice::WriteOnly)) {
            QTextStream out(&file);
            out << "Record Time" << FIELDS_SEPARATOR;
            for(const auto &var : std::as_const(_variables)) {
                out << variableName(var) << FIELDS_SEPARATOR;
            }
            out << '\n';

            for(const auto &line : std::as_const(_buffers[index])) {
                out << line << '\n';
            }

            file.close();
            qInfo() << "Debug file written: " << filename;
        }

        ++index;
    }

    _buffers.clear();
    ++_filenum;
}


void UdpLogger::log(int carIndex, LoggableVariable variable, const QString &value)
{
    if(_variables.contains(variable))
        _loggedData[carIndex][variable] = value;
}
