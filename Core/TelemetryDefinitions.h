#ifndef TELEMETRYDEFINITIONS_H
#define TELEMETRYDEFINITIONS_H

#include "TelemetryData.h"

#include <QHash>
#include <QVector>

struct TelemetryStaticInfo {
	TelemetryStaticInfo() {}
	TelemetryStaticInfo(const QStringList &values) : customValues(values) {}
	TelemetryStaticInfo(const QString &format) : valueFormat(format) {}

	bool isValid() const { return !customValues.isEmpty() || !valueFormat.isEmpty(); }

	QStringList customValues;
	QString valueFormat;
};

class TelemetryDefinitions
{
  public:
	static const constexpr char *SPEED_INFO_NAME = "Speed";

	static const constexpr char *TRACTION_INFO_NAME = "Traction";
	static const constexpr char *TYRE_DEG_INFO_NAME = "Tyre stress";

	static const constexpr char *ERS_MODE_INFO_NAME = "ERS Mode";
	static const constexpr char *FUEL_MIX_INFO_NAME = "Fuel Mix";


	static const constexpr char *WEATHER_INFO_NAME = "Weather";

	static QVector<TelemetryInfo> TELEMETRY_INFO;
	static const QVector<TelemetryInfo> EXTENDED_TELEMETRY_INFO;
	static const QVector<TelemetryInfo> TELEMETRY_STINT_INFO;
	static const QVector<TelemetryInfo> TELEMETRY_RACE_INFO;

	static int indexOfLapTelemetry(const QString &name);
	static int indexOfStintTelemetry(const QString &name);

	static TelemetryStaticInfo staticInfo(const QString &name);

  private:
	static int indexOfTelemetryIn(const QString &name, const QVector<TelemetryInfo> &telemetryInfo);

	static QHash<QString, TelemetryStaticInfo> info;
	static void initInfo();
	static QStringList intValue(int from, int to);
};


#endif // TELEMETRYDEFINITIONS_H
