#include "F1Listener.h"
#include "Logger.h"
#include <QDataStream>
#include <QNetworkDatagram>
#include <QtDebug>


F1Listener::F1Listener(F1PacketInterface *interface, const QString &address, int port, QObject *parent)
: QObject(parent), _listener(new QUdpSocket(this)), _interfaces({interface})
{
    // bind to listening port
    auto host = address.isEmpty() ? QHostAddress(QHostAddress::Any) : QHostAddress(address);
    if(host.isNull()) {
        Logger::instance()->log(QString("Error: The listened address %1 is invalid!").arg(host.toString()));
    } else if(!_listener->bind(host, port)) {
        Logger::instance()->log(
            QString("Error: The connection to %1 (port %2) failed!").arg(host.toString()).arg(port));
    } else if(!address.isEmpty()) {
        Logger::instance()->log(QString("Listening to %1 ... (port %2)").arg(host.toString()).arg(port));
    } else {
        Logger::instance()->log(QString("Listening ... (port %1)").arg(port));
    }
    connect(_listener, &QUdpSocket::readyRead, this, &F1Listener::readData);
}

void F1Listener::addInterface(F1PacketInterface *interface) { _interfaces << interface; }

void F1Listener::removeInterface(F1PacketInterface *interface) { _interfaces.removeAll(interface); }

bool F1Listener::isConnected() const
{
    return _listener->isValid() &&
           (_listener->state() == QAbstractSocket::ConnectedState || _listener->state() == QAbstractSocket::BoundState);
}

void F1Listener::forwardTo(const QString &address, int port)
{
    delete _forwarder;
    _forwarder = nullptr;

    if(!address.isEmpty()) {
        _forwardAddress = QHostAddress(address);
        if(!_forwardAddress.isNull()) {
            _forwarder = new QUdpSocket(this);
            _forwardPort = port;
        } else {
            Logger::instance()->log(QString("Error: The forwading address %1 is invalid!").arg(address));
        }
    }
}

#include <QtDebug>

bool F1Listener::tryRead()
{
    auto expectedLength = UdpSpecification::instance()->expectedPacketLength(_expectedDataType);
    if(_buffer.size() >= expectedLength) {
        auto dataToRead = _buffer.left(expectedLength);
        _buffer.remove(0, expectedLength);

        QDataStream stream(&dataToRead, QIODevice::ReadOnly);
        stream.setByteOrder(QDataStream::LittleEndian);
        stream.setFloatingPointPrecision(QDataStream::SinglePrecision);

        switch(_expectedDataType) {
            case UdpSpecification::PacketType::Header: {
                stream >> _lastHeader;
                break;
            }
            case UdpSpecification::PacketType::Participants: {
                auto packet = PacketParticipantsData();
                stream >> packet;
                for(auto interface : std::as_const(_interfaces))
                    interface->participant(_lastHeader, packet);
                break;
            }
            case UdpSpecification::PacketType::LapData: {
                auto packet = PacketLapData();
                stream >> packet;
                for(auto interface : std::as_const(_interfaces))
                    interface->lapData(_lastHeader, packet);
                break;
            }
            case UdpSpecification::PacketType::CarTelemetry: {
                auto packet = PacketCarTelemetryData();
                stream >> packet;
                for(auto interface : std::as_const(_interfaces))
                    interface->telemetryData(_lastHeader, packet);
                break;
            }
            case UdpSpecification::PacketType::CarSetup: {
                auto packet = PacketCarSetupData();
                stream >> packet;
                for(auto interface : std::as_const(_interfaces))
                    interface->setupData(_lastHeader, packet);
                break;
            }
            case UdpSpecification::PacketType::Session: {
                auto packet = PacketSessionData();
                stream >> packet;
                for(auto interface : std::as_const(_interfaces))
                    interface->sessionData(_lastHeader, packet);
                break;
            }
            case UdpSpecification::PacketType::CarStatus: {
                auto packet = PacketCarStatusData();
                stream >> packet;
                for(auto interface : std::as_const(_interfaces))
                    interface->statusData(_lastHeader, packet);
                break;
            }
            case UdpSpecification::PacketType::Motion: {
                auto packet = PacketMotionData();
                stream >> packet;
                for(auto interface : std::as_const(_interfaces))
                    interface->motionData(_lastHeader, packet);
                break;
            }
            case UdpSpecification::PacketType::Event: {
                auto packet = PacketEventData();
                stream >> packet;
                for(auto interface : std::as_const(_interfaces))
                    interface->eventData(_lastHeader, packet);
                break;
            }
            case UdpSpecification::PacketType::FinalClassification: {
                auto packet = PacketFinalClassificationData();
                stream >> packet;
                for(auto interface : std::as_const(_interfaces))
                    interface->finalClassificationData(_lastHeader, packet);
                break;
            }
            case UdpSpecification::PacketType::CarDamage: {
                auto packet = PacketCarDamageData();
                stream >> packet;
                for(auto interface : std::as_const(_interfaces))
                    interface->carDamageData(_lastHeader, packet);
                break;
            }
            case UdpSpecification::PacketType::SessionHistory: {
                auto packet = PacketSessionHistoryData();
                stream >> packet;
                for(auto interface : std::as_const(_interfaces))
                    interface->sessionHistoryData(_lastHeader, packet);
                break;
            }
            case UdpSpecification::PacketType::MotionEx: {
                auto packet = PacketMotionExData();
                stream >> packet;
                for(auto interface : std::as_const(_interfaces))
                    interface->motionExData(_lastHeader, packet);
                break;
            }
            case UdpSpecification::PacketType::TyreSet: {
                auto packet = PacketTyreSetsData();
                stream >> packet;
                for(auto interface : std::as_const(_interfaces))
                    interface->tyreSetData(_lastHeader, packet);
                break;
            }
            default:
                break;
        }

        if(_expectedDataType == UdpSpecification::PacketType::Header)
            _expectedDataType = static_cast<UdpSpecification::PacketType>(_lastHeader.m_packetId);
        else
            _expectedDataType = UdpSpecification::PacketType::Header;

        return true;
    }

    return false;
}

void F1Listener::readData()
{
    while(_listener->hasPendingDatagrams()) {
        auto datagram = _listener->receiveDatagram();
        _buffer += datagram.data();
        while(tryRead()) {
        }

        if(_forwarder != nullptr) {
            _forwarder->writeDatagram(datagram.data(), _forwardAddress, _forwardPort);
        }
    }
}

F1PacketInterface::~F1PacketInterface() {}
