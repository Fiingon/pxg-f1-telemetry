#ifndef RACERESULTPRODUCER_H
#define RACERESULTPRODUCER_H
#include "DriverTracker.h"
#include "Race.h"
#include <QDebug>
#include <QFile>
#include <QString>
#include <QTextStream>


class RaceResultProducer
{

  public:
	RaceResultProducer(const QString &filename);
	~RaceResultProducer();

	void setRaceResult(const QVector<Race *> &rcs);

  private:
	QFile file;
	QTextStream out;
	QDir directory, datadirectory;
	QString filename;

	QString raceTimeToStr(double value);
	QString timeToStr(double valueMS, bool forceSign = false);
};


#endif // RACERESULTPRODUCER_H
