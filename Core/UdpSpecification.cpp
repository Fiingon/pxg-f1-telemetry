#include "UdpSpecification.h"

#include <QtDebug>
#include <TrackInfo.h>
#include <UdpDataCatalog.h>

QString UdpSpecification::myTeamName = "My Team";

template <typename T> void readDataList(QDataStream &in, QVector<T> &list, int nb = 22)
{
    list.reserve(nb);
    for(auto i = 0; i < nb; ++i) {
        T data;
        in >> data;
        list.append(data);
    }
}

void readDouble(QDataStream &in, double &value)
{
    in.setFloatingPointPrecision(QDataStream::DoublePrecision);
    in >> value;
    in.setFloatingPointPrecision(QDataStream::SinglePrecision);
}

void writeDouble(QDataStream &out, double value)
{
    out.setFloatingPointPrecision(QDataStream::DoublePrecision);
    out << value;
    out.setFloatingPointPrecision(QDataStream::SinglePrecision);
}

int computeTimeInMs(int minutesPart, int msPart)
{
    return minutesPart * 60 * 1000 + msPart;
}


int UdpSpecification::expectedPacketLength(UdpSpecification::PacketType type) const
{
    return packetExpectedLengths[type];
}

UdpSpecification::UdpSpecification()
{
    // Note: lenghths here doesn't count the header
    packetExpectedLengths[PacketType::Header] = 29;
    packetExpectedLengths[PacketType::Motion] = 1320;
    packetExpectedLengths[PacketType::Session] = 724;
    packetExpectedLengths[PacketType::LapData] = 1256;
    packetExpectedLengths[PacketType::Event] = 16;
    packetExpectedLengths[PacketType::Participants] = 1321;
    packetExpectedLengths[PacketType::CarSetup] = 1104;
    packetExpectedLengths[PacketType::CarTelemetry] = 1323;
    packetExpectedLengths[PacketType::CarStatus] = 1210;
    packetExpectedLengths[PacketType::FinalClassification] = 991;
    packetExpectedLengths[PacketType::LobbyInfo] = 1277;
    packetExpectedLengths[PacketType::CarDamage] = 924;
    packetExpectedLengths[PacketType::SessionHistory] = 1431;
    packetExpectedLengths[PacketType::TyreSet] = 202;
    packetExpectedLengths[PacketType::MotionEx] = 208;
    packetExpectedLengths[PacketType::TimeTrial] = 72;
}

QDataStream &operator>>(QDataStream &in, PacketHeader &packet)
{
    in >> packet.m_packetFormat >> packet.m_gameYear >> packet.m_gameMajorVersion >> packet.m_gameMinorVersion >>
        packet.m_packetVersion >> packet.m_packetId >> packet.m_sessionUID >> packet.m_sessionTime >>
        packet.m_frameIdentifier >> packet.m_overallFrameIdentifier >> packet.m_playerCarIndex >>
        packet.m_secondaryPlayerCarIndex;

    return in;
}

QDataStream &operator>>(QDataStream &in, ParticipantData &packet)
{
    in >> packet.m_aiControlled >> packet.m_driverId >> packet.m_networkId >> packet.m_teamId >> packet.m_myTeam >>
        packet.m_raceNumber >> packet.m_nationality;
    packet.m_name.clear();
    char name[48];
    for(auto i = 0; i < 48; ++i) {
        qint8 c;
        in >> c;
        name[i] = c;
    }

    packet.m_name = QString::fromUtf8(name);
    in >> packet.m_yourTelemetry >> packet.m_showOnlineNames >> packet.m_techLevel >> packet.m_platform;

    return in;
}

QDataStream &operator<<(QDataStream &out, const ParticipantData &packet)
{
    out << packet.m_aiControlled << packet.m_driverId << packet.m_networkId << packet.m_teamId << packet.m_myTeam
        << packet.m_raceNumber << packet.m_nationality;
    auto codedName = packet.m_name.toUtf8();
    for(auto i = 0; i < 48; ++i) {
        if(i < codedName.size()) {
            out << quint8(codedName[i]);
        } else {
            out << quint8(0);
        }
    }

    out << packet.m_yourTelemetry << packet.m_showOnlineNames << packet.m_platform;

    return out;
}

QDataStream &operator>>(QDataStream &in, PacketParticipantsData &packet)
{
    in >> packet.m_numActiveCars;
    readDataList<ParticipantData>(in, packet.m_participants);

    return in;
}


QString ParticipantData::driverFullName(bool isPlayer) const
{
    if(m_name.isEmpty())
        return QString();
    QString name;
    name += "(";
    name += QString::number(m_raceNumber);
    name += ") ";
    name += m_name;

    if(m_driverId == 255 && !m_aiControlled && !isPlayer) {
        name += QString::number(m_networkId);
    }

    auto team = this->team();
    if(!m_name.isEmpty() && !team.isEmpty()) {
        name += " ";
        name += team;
    }

    return name;
}

QString ParticipantData::team() const
{
    auto team = UdpDataCatalog::team(m_teamId);
    if(m_myTeam)
        team = UdpSpecification::myTeamName;
    return team;
}

QStringList PacketParticipantsData::availableDrivers(const PacketHeader &header) const
{
    QStringList names;
    int index = 0;
    for(auto &driver : m_participants) {
        names << driver.driverFullName(header.m_playerCarIndex == index);
        ++index;
    }

    return names;
}

QDataStream &operator>>(QDataStream &in, LapData &packet)
{
    in >> packet.m_lastLapTimeInMS >> packet.m_currentLapTimeInMS >>
        packet.m_sector1TimeMSPart >> packet.m_sector1TimeMinutesPart >>
        packet.m_sector2TimeMSPart >> packet.m_sector2TimeMinutesPart >>
        packet.m_deltaToCarInFrontMSPart >> packet.m_deltaToCarInFrontMinutesPart >>
        packet.m_deltaToRaceLeaderMSPart >> packet.m_deltaToRaceLeaderMinutesPart >>
        packet.m_lapDistance >> packet.m_totalDistance >> packet.m_safetyCarDelta >> packet.m_carPosition >> packet.m_currentLapNum >>
        packet.m_pitStatus >> packet.m_numPitStops >> packet.m_sector >> packet.m_currentLapInvalid >>
        packet.m_penalties >> packet.m_totalWarnings >> packet.m_cornerCuttingWarnings >>
        packet.m_numUnservedDriveThroughPens >> packet.m_numUnservedStopGoPens >> packet.m_gridPosition >>
        packet.m_driverStatus >> packet.m_resultStatus >> packet.m_pitLaneTimerActive >>
        packet.m_pitLaneTimeInLaneInMS >> packet.m_pitStopTimerInMS >> packet.m_pitStopShouldServePen >>
        packet.m_speedTrapFastestSpeed >> packet.m_speedTrapFastestLap;

    return in;
}

QDataStream &operator>>(QDataStream &in, PacketLapData &packet)
{
    readDataList<LapData>(in, packet.m_lapData);
    in >> packet.m_timeTrialPBCarIdx >> packet.m_timeTrialRivalCarIdx;
    return in;
}

QDataStream &operator>>(QDataStream &in, CarTelemetryData &packet)
{
    in >> packet.m_speed >> packet.m_throttle >> packet.m_steer >> packet.m_brake >> packet.m_clutch >> packet.m_gear >>
        packet.m_engineRPM >> packet.m_drs >> packet.m_revLightsPercent >> packet.m_revLightsBitValue;
    for(auto i = 0; i < 4; ++i)
        in >> packet.m_brakesTemperature[i];
    for(auto i = 0; i < 4; ++i)
        in >> packet.m_tyresSurfaceTemperature[i];
    for(auto i = 0; i < 4; ++i)
        in >> packet.m_tyresInnerTemperature[i];
    in >> packet.m_engineTemperature;
    for(auto i = 0; i < 4; ++i)
        in >> packet.m_tyresPressure[i];
    for(auto i = 0; i < 4; ++i)
        in >> packet.m_surfaceType[i];

    return in;
}

QDataStream &operator>>(QDataStream &in, PacketCarTelemetryData &packet)
{
    readDataList<CarTelemetryData>(in, packet.m_carTelemetryData);
    in >> packet.m_mfdPanelIndex >> packet.m_mfdPanelIndexSecondaryPlayer >> packet.m_suggestedGear;
    return in;
}

QDataStream &operator>>(QDataStream &in, CarSetupData &packet)
{
    in >> packet.m_frontWing >> packet.m_rearWing >> packet.m_onThrottle >> packet.m_offThrottle >>
        packet.m_frontCamber >> packet.m_rearCamber >> packet.m_frontToe >> packet.m_rearToe >>
        packet.m_frontSuspension >> packet.m_rearSuspension >> packet.m_frontAntiRollBar >> packet.m_rearAntiRollBar >>
        packet.m_frontSuspensionHeight >> packet.m_rearSuspensionHeight >> packet.m_brakePressure >>
        packet.m_brakeBias >> packet.m_engineBraking >> packet.m_rearLeftTyrePressure >> packet.m_rearRightTyrePressure >>
        packet.m_frontLeftTyrePressure >> packet.m_frontRightTyrePressure >> packet.m_ballast >> packet.m_fuelLoad;
    return in;
}

QDataStream &operator<<(QDataStream &out, const CarSetupData &packet)
{
    out << packet.m_frontWing << packet.m_rearWing << packet.m_onThrottle << packet.m_offThrottle
        << packet.m_frontCamber << packet.m_rearCamber << packet.m_frontToe << packet.m_rearToe
        << packet.m_frontSuspension << packet.m_rearSuspension << packet.m_frontAntiRollBar << packet.m_rearAntiRollBar
        << packet.m_frontSuspensionHeight << packet.m_rearSuspensionHeight << packet.m_brakePressure
        << packet.m_brakeBias << packet.m_engineBraking << packet.m_rearLeftTyrePressure << packet.m_rearRightTyrePressure
        << packet.m_frontLeftTyrePressure << packet.m_frontRightTyrePressure << packet.m_ballast << packet.m_fuelLoad;
    return out;
}

QDataStream &operator>>(QDataStream &in, PacketCarSetupData &packet)
{
    readDataList<CarSetupData>(in, packet.m_carSetups);
    in >> packet.m_nextFrontWingValue;
    return in;
}


QDataStream &operator>>(QDataStream &in, MarshalZone &packet)
{
    in >> packet.m_zoneStart >> packet.m_zoneFlag;
    return in;
}

QDataStream &operator>>(QDataStream &in, WeatherForecastSample &packet)
{
    in >> packet.m_sessionType >> packet.m_timeOffset >> packet.m_weather >> packet.m_trackTemperature >>
        packet.m_trackTemperatureChange >> packet.m_airTemperature >> packet.m_airTemperatureChange >>
        packet.m_rainPercentage;

    return in;
}


QDataStream &operator>>(QDataStream &in, PacketSessionData &packet)
{
    in >> packet.m_weather >> packet.m_trackTemperature >> packet.m_airTemperature >> packet.m_totalLaps >>
        packet.m_trackLength >> packet.m_sessionType >> packet.m_trackId >> packet.m_formula >>
        packet.m_sessionTimeLeft >> packet.m_sessionDuration >> packet.m_pitSpeedLimit >> packet.m_gamePaused >>
        packet.m_isSpectating >> packet.m_spectatorCarIndex >> packet.m_sliProNativeSupport >> packet.m_numMarshalZones;
    readDataList<MarshalZone>(in, packet.m_marshalZones, 21);

    in >> packet.m_safetyCarStatus >> packet.m_networkGame >> packet.m_numWeatherForecastSamples;
    readDataList<WeatherForecastSample>(in, packet.m_weatherForecastSamples, 64);

    in >> packet.m_forecastAccuracy >> packet.m_aiDifficulty >> packet.m_seasonLinkIdentifier >>
        packet.m_weekendLinkIdentifier >> packet.m_sessionLinkIdentifier >> packet.m_pitStopWindowIdealLap >>
        packet.m_pitStopWindowLatestLap >> packet.m_pitStopRejoinPosition >> packet.m_steeringAssist >>
        packet.m_brakingAssist >> packet.m_gearboxAssist >> packet.m_pitAssist >> packet.m_pitReleaseAssist >>
        packet.m_ERSAssist >> packet.m_DRSAssist >> packet.m_dynamicRacingLine >> packet.m_dynamicRacingLineType >>
        packet.m_gameMode >> packet.m_ruleSet >> packet.m_timeOfDay >> packet.m_sessionLength >>
        packet.m_speedUnitsLeadPlayer >> packet.m_temperatureUnitsLeadPlayer >> packet.m_speedUnitsSecondaryPlayer >>
        packet.m_temperatureUnitsSecondaryPlayer >> packet.m_numSafetyCarPeriods >>
        packet.m_numVirtualSafetyCarPeriods >> packet.m_numRedFlagPeriods;

    in >> packet.m_equalCarPerformance >> packet.m_recoveryMode >> packet.m_flashbackLimit >> packet.m_surfaceType >>
        packet.m_lowFuelMode >> packet.m_raceStarts >> packet.m_tyreTemperature >> packet.m_pitLaneTyreSim >>
        packet.m_carDamage >> packet.m_carDamageRate >> packet.m_collisions >> packet.m_collisionsOffForFirstLapOnly >>
        packet.m_mpUnsafePitRelease >> packet.m_mpOffForGriefing >> packet.m_cornerCuttingStringency >> packet.m_parcFermeRules >>
        packet.m_pitStopExperience >> packet.m_safetyCar >> packet.m_safetyCarExperience >> packet.m_formationLap >>
        packet.m_formationLapExperience >> packet.m_redFlags >> packet.m_affectsLicenceLevelSolo >> packet.m_affectsLicenceLevelMP >>
        packet.m_numSessionsInWeekend;

    readDataList(in, packet.m_weekendStructure, 12);

    in >> packet.m_sector2LapDistanceStart >> packet.m_sector3LapDistanceStart;

    return in;
}

QDataStream &operator>>(QDataStream &in, CarStatusData &packet)
{
    in >> packet.m_tractionControl >> packet.m_antiLockBrakes >> packet.m_fuelMix >> packet.m_frontBrakeBias >>
        packet.m_pitLimiterStatus >> packet.m_fuelInTank >> packet.m_fuelCapacity >> packet.m_fuelRemainingLaps >>
        packet.m_maxRPM >> packet.m_idleRPM >> packet.m_maxGears >> packet.m_drsAllowed >>
        packet.m_drsActivationDistance;

    in >> packet.m_actualTyreCompound >> packet.m_tyreVisualCompound >> packet.m_tyresAgeLaps;
    in >> packet.m_vehicleFiaFlags >> packet.m_enginePowerICE >> packet.m_enginePowerMGUK >> packet.m_ersStoreEnergy >>
        packet.m_ersDeployMode >> packet.m_ersHarvestedThisLapMGUK >> packet.m_ersHarvestedThisLapMGUH >>
        packet.m_ersDeployedThisLap >> packet.m_networkPaused;
    return in;
}

QDataStream &operator>>(QDataStream &in, PacketCarStatusData &packet)
{
    readDataList<CarStatusData>(in, packet.m_carStatusData);
    return in;
}

QDataStream &operator>>(QDataStream &in, PacketMotionData &packet)
{
    readDataList<CarMotionData>(in, packet.m_carMotionData);

    return in;
}

QDataStream &operator>>(QDataStream &in, PacketMotionExData &packet)
{
    for(auto i = 0; i < 4; ++i)
        in >> packet.m_suspensionPosition[i];
    for(auto i = 0; i < 4; ++i)
        in >> packet.m_suspensionVelocity[i];
    for(auto i = 0; i < 4; ++i)
        in >> packet.m_suspensionAcceleration[i];
    for(auto i = 0; i < 4; ++i)
        in >> packet.m_wheelSpeed[i];
    for(auto i = 0; i < 4; ++i)
        in >> packet.m_wheelSlipRatio[i];
    for(auto i = 0; i < 4; ++i)
        in >> packet.m_wheelSlipAngle[i];
    for(auto i = 0; i < 4; ++i)
        in >> packet.m_wheelLatForce[i];
    for(auto i = 0; i < 4; ++i)
        in >> packet.m_wheelLongForce[i];

    in >> packet.m_heightOfCOGAboveGround >> packet.m_localVelocityX >> packet.m_localVelocityY >>
        packet.m_localVelocityZ >> packet.m_angularVelocityX >> packet.m_angularVelocityY >>
        packet.m_angularVelocityZ >> packet.m_angularAccelerationX >> packet.m_angularAccelerationY >>
        packet.m_angularAccelerationZ >> packet.m_frontWheelsAngle;

    for(auto i = 0; i < 4; ++i)
        in >> packet.m_wheelVertForce[i];

    in >> packet.m_frontAeroHeight >> packet.m_rearAeroHeight >> packet.m_frontRollAngle >>
        packet.m_rearRollAngle >> packet.m_chassisYaw;

    return in;
}

QDataStream &operator>>(QDataStream &in, CarMotionData &packet)
{
    in >> packet.m_worldPositionX >> packet.m_worldPositionY >> packet.m_worldPositionZ >> packet.m_worldVelocityX >>
        packet.m_worldVelocityY >> packet.m_worldVelocityZ >> packet.m_worldForwardDirX >> packet.m_worldForwardDirY >>
        packet.m_worldForwardDirZ >> packet.m_worldRightDirX >> packet.m_worldRightDirY >> packet.m_worldRightDirZ >>
        packet.m_gForceLateral >> packet.m_gForceLongitudinal >> packet.m_gForceVertical >> packet.m_yaw >>
        packet.m_pitch >> packet.m_pitch;

    return in;
}

QDataStream &operator>>(QDataStream &in, PacketEventData &packet)
{
    packet.m_eventStringCode.clear();
    char name[4];
    for(auto i = 0; i < 4; ++i) {
        qint8 c;
        in >> c;
        name[i] = c;
    }

    packet.m_eventStringCode = QString::fromUtf8(name);
    packet.event = stringToEvent(packet.m_eventStringCode);
    loadEventDetails(in, packet.details, packet.event);
    return in;
}

void loadEventDetails(QDataStream &in, EventDataDetails &data, Event event)
{
    switch(event) {
        case Event::FastestLap:
            in >> data.FastestLap.vehicleIdx >> data.FastestLap.lapTime;
            break;
        case Event::Retirement:
            in >> data.Retirement.vehicleIdx;
            break;
        case Event::TeammateInPits:
            in >> data.TeamMateInPits.vehicleIdx;
            break;
        case Event::RaceWinner:
            in >> data.RaceWinner.vehicleIdx;
            break;
        case Event::PenaltyIssued:
            in >> data.Penalty.penaltyType >> data.Penalty.infringementType >> data.Penalty.vehicleIdx >>
                data.Penalty.otherVehicleIdx >> data.Penalty.time >> data.Penalty.lapNum >> data.Penalty.placesGained;
            break;
        case Event::SpeedTrapTrigged:
            in >> data.SpeedTrap.vehicleIdx >> data.SpeedTrap.speed >> data.SpeedTrap.isOverallFastestInSession >>
                data.SpeedTrap.isDriverFastestInSession >> data.SpeedTrap.fastestVehicleIdxInSession >>
                data.SpeedTrap.fastestSpeedInSession;
            break;
        case Event::StartLights:
            in >> data.StartLights.numLights;
            break;
        case Event::DriveThroughServed:
            in >> data.DriveThroughPenaltyServed.vehicleIdx;
            break;
        case Event::StopGoServed:
            in >> data.StopGoPenaltyServed.vehicleIdx;
            break;
        case Event::Flashback:
            in >> data.Flashback.flashbackFrameIdentifier >> data.Flashback.flashbackSessionTime;
            break;
        case Event::ButtonStatus:
            in >> data.Buttons.m_buttonStatus;
            break;
        case Event::Overtake:
            in >> data.Overtake.overtakingVehicleIdx >> data.Overtake.beingOvertakenVehicleIdx;
            break;
        case Event::SafetyCar:
            in >> data.SafetyCar.safetyCarType >> data.SafetyCar.eventType;
            break;
        case Event::Collision:
            in >> data.Collision.vehicle1Idx >> data.Collision.vehicle2Idx;
            break;
        default:
            break;
    }
}


Event stringToEvent(const QString str)
{
    if(str.startsWith("SSTA"))
        return Event::SessionStarted;
    if(str.startsWith("SEND"))
        return Event::SessionEnded;
    if(str.startsWith("FTLP"))
        return Event::FastestLap;
    if(str.startsWith("RTMT"))
        return Event::Retirement;
    if(str.startsWith("DRSE"))
        return Event::DrsEnabled;
    if(str.startsWith("DRSD"))
        return Event::DrsDisabled;
    if(str.startsWith("TMPT"))
        return Event::TeammateInPits;
    if(str.startsWith("CHQF"))
        return Event::ChequeredFlag;
    if(str.startsWith("RCWN"))
        return Event::RaceWinner;
    if(str.startsWith("PENA"))
        return Event::PenaltyIssued;
    if(str.startsWith("SPTP"))
        return Event::SpeedTrapTrigged;

    if(str.startsWith("STLG"))
        return Event::StartLights;
    if(str.startsWith("LGOT"))
        return Event::LightsOut;
    if(str.startsWith("DTSV"))
        return Event::DriveThroughServed;
    if(str.startsWith("SGSV"))
        return Event::StopGoServed;
    if(str.startsWith("FLBK"))
        return Event::Flashback;
    if(str.startsWith("BUTN"))
        return Event::ButtonStatus;
    if(str.startsWith("RDFL"))
        return Event::RedFlag;
    if(str.startsWith("OVTK"))
        return Event::Overtake;
    if(str.startsWith("SCAR"))
        return Event::SafetyCar;
    if(str.startsWith("COLL"))
        return Event::Collision;

    return Event::Unknown;
}

bool PacketSessionData::isRace() const
{
    // Formation lap is excluded
    return (m_sessionType == 15 || m_sessionType == 16 || m_sessionType == 17) && m_safetyCarStatus != 3;
}

bool PacketSessionData::isTimeTrial() const { return m_sessionType == 18; }

QString PacketSessionData::sessionName() const
{
    auto trackName = UdpDataCatalog::trackInfo(m_trackId)->name;
    auto sessionType = UdpDataCatalog::session(m_sessionType);

    return trackName + " " + sessionType;
}

QVector<WeatherForecastSample> PacketSessionData::currentSessionWeatherForecastSamples() const
{
    QVector<WeatherForecastSample> results;
    std::copy_if(m_weatherForecastSamples.begin(), m_weatherForecastSamples.end(), std::back_inserter(results),
                 [this](auto forecast) { return forecast.m_sessionType == m_sessionType; });

    return results;
}

QDataStream &operator>>(QDataStream &in, FinalClassificationData &packet)
{
    in >> packet.m_position >> packet.m_numLaps >> packet.m_gridPosition >> packet.m_points >> packet.m_numPitStops >>
        packet.m_resultStatus >> packet.m_bestLapTimeInMS;
    readDouble(in, packet.m_totalRaceTime);
    in >> packet.m_penaltiesTime >> packet.m_numPenalties >> packet.m_numTyreStints;
    readDataList<quint8>(in, packet.m_tyreStintsActual, 8);
    readDataList<quint8>(in, packet.m_tyreStintsVisual, 8);
    readDataList<quint8>(in, packet.m_tyreStintsEndLaps, 8);
    return in;
}

QDataStream &operator<<(QDataStream &out, const FinalClassificationData &packet)
{
    out << packet.m_position << packet.m_numLaps << packet.m_gridPosition << packet.m_points << packet.m_numPitStops
        << packet.m_resultStatus << packet.m_bestLapTimeInMS;
    writeDouble(out, packet.m_totalRaceTime);
    out << packet.m_penaltiesTime << packet.m_numPenalties << packet.m_numTyreStints;
    for(const auto &value : std::as_const(packet.m_tyreStintsActual))
        out << value;
    for(const auto &value : std::as_const(packet.m_tyreStintsVisual))
        out << value;
    for(const auto &value : std::as_const(packet.m_tyreStintsEndLaps))
        out << value;
    return out;
}

QDataStream &operator>>(QDataStream &in, PacketFinalClassificationData &packet)
{
    in >> packet.m_numCars;
    readDataList<FinalClassificationData>(in, packet.m_classificationData);
    return in;
}

int PacketLapData::lapIndexForCarPosition(int pos) const
{
    int index = 0;
    for(auto &lap : m_lapData) {
        if(lap.m_carPosition == pos) {
            return index;
        }

        ++index;
    }

    return -1;
}

bool WeatherForecastSample::operator==(const WeatherForecastSample &other) const
{
    return m_sessionType == other.m_sessionType && m_airTemperature == other.m_airTemperature &&
           m_timeOffset == other.m_timeOffset && m_trackTemperature == other.m_trackTemperature &&
           m_weather == other.m_weather;
}

QDataStream &operator>>(QDataStream &in, CarDamageData &packet)
{
    for(auto i = 0; i < 4; ++i)
        in >> packet.m_tyresWear[i];
    for(auto i = 0; i < 4; ++i)
        in >> packet.m_tyresDamage[i];
    for(auto i = 0; i < 4; ++i)
        in >> packet.m_brakesDamage[i];
    in >> packet.m_frontLeftWingDamage >> packet.m_frontRightWingDamage >> packet.m_rearWingDamage >>
        packet.m_floorDamage >> packet.m_diffuserDamage >> packet.m_sidepodDamage >> packet.m_drsFault >>
        packet.m_ersFault >> packet.m_gearBoxDamage >> packet.m_engineDamage >> packet.m_engineMGUHWear >>
        packet.m_engineESWear >> packet.m_engineCEWear >> packet.m_engineICEWear >> packet.m_engineMGUKWear >>
        packet.m_engineTCWear >> packet.m_engineBlown >> packet.m_engineSeized;
    return in;
}

QDataStream &operator<<(QDataStream &out, const CarDamageData &packet)
{
    for(auto i = 0; i < 4; ++i)
        out << packet.m_tyresWear[i];
    for(auto i = 0; i < 4; ++i)
        out << packet.m_tyresDamage[i];
    for(auto i = 0; i < 4; ++i)
        out << packet.m_brakesDamage[i];
    out << packet.m_frontLeftWingDamage << packet.m_frontRightWingDamage << packet.m_rearWingDamage
        << packet.m_floorDamage << packet.m_diffuserDamage << packet.m_sidepodDamage << packet.m_drsFault
        << packet.m_ersFault << packet.m_gearBoxDamage << packet.m_engineDamage << packet.m_engineMGUHWear
        << packet.m_engineESWear << packet.m_engineCEWear << packet.m_engineICEWear << packet.m_engineMGUKWear
        << packet.m_engineTCWear << packet.m_engineBlown << packet.m_engineSeized;
    return out;
}

QDataStream &operator>>(QDataStream &in, PacketCarDamageData &packet)
{
    readDataList<CarDamageData>(in, packet.m_carDamageData);
    return in;
}

QDataStream &operator>>(QDataStream &in, LapHistoryData &packet)
{
    in >> packet.m_lapTimeInMS >> packet.m_sector1TimeInMS >> packet.m_sector1TimeMinutes >> packet.m_sector2TimeInMS >>
        packet.m_sector2TimeMinutes >> packet.m_sector3TimeInMS >> packet.m_sector3TimeMinutes >>
        packet.m_lapValidBitFlags;
    return in;
}
QDataStream &operator>>(QDataStream &in, TyreStintHistoryData &packet)
{
    in >> packet.m_endLap >> packet.m_tyreActualCompound >> packet.m_tyreVisualCompound;
    return in;
}
QDataStream &operator>>(QDataStream &in, PacketSessionHistoryData &packet)
{
    in >> packet.m_carIdx >> packet.m_numLaps >> packet.m_numTyreStints >> packet.m_bestLapTimeLapNum >>
        packet.m_bestSector1LapNum >> packet.m_bestSector2LapNum >> packet.m_bestSector3LapNum;
    readDataList<LapHistoryData>(in, packet.m_lapHistoryData, 100);
    readDataList<TyreStintHistoryData>(in, packet.m_tyreStintsHistoryData, 8);
    return in;
}

QDataStream &operator>>(QDataStream &in, TyreSetData &packet)
{
    in >> packet.m_actualTyreCompound >> packet.m_visualTyreCompound >> packet.m_wear >> packet.m_available >>
        packet.m_recommendedSession >> packet.m_lifeSpan >> packet.m_usableLife >> packet.m_lapDeltaTime >>
        packet.m_fitted;
    return in;
}

QDataStream &operator>>(QDataStream &in, PacketTyreSetsData &packet)
{
    in >> packet.m_carIdx;
    readDataList<TyreSetData>(in, packet.m_tyreSetData, 20);
    in >> packet.m_fittedIdx;

    return in;
}

double CarDamageData::averageCarDamage() const
{
    return (m_frontLeftWingDamage + m_frontRightWingDamage + m_floorDamage + m_diffuserDamage + m_sidepodDamage) / 5.0;
}

double CarDamageData::averageEngineDamage() const
{
    return (m_gearBoxDamage + m_engineMGUHWear + m_engineESWear + m_engineCEWear + m_engineICEWear + m_engineMGUKWear +
            m_engineTCWear) /
           7.0;
}

double CarDamageData::averageFrontWingDamage() const { return (m_frontLeftWingDamage + m_frontRightWingDamage) / 2.0; }

QString PacketHeader::gameVersion() const
{
    QString gameVersion = QString::number(m_gameYear);
    gameVersion += " (v";
    gameVersion += QString::number(m_gameMajorVersion);
    gameVersion += ".";
    gameVersion += QString::number(m_gameMinorVersion);
    gameVersion += ")";

    return gameVersion;
}

TyreSetData PacketTyreSetsData::fittedSet() const { return m_tyreSetData.value(m_fittedIdx); }

int LapData::sector1TimeInMs() const
{
    return computeTimeInMs(m_sector1TimeMinutesPart, m_sector1TimeMSPart);
}

int LapData::sector2TimeInMs() const
{
    return computeTimeInMs(m_sector2TimeMinutesPart, m_sector2TimeMSPart);
}

int LapData::deltaToCarInFrontInMs() const
{
    return computeTimeInMs(m_deltaToCarInFrontMinutesPart, m_deltaToRaceLeaderMSPart);
}

int LapData::deltaToLeaderInMs() const
{
    return computeTimeInMs(m_deltaToRaceLeaderMinutesPart, m_deltaToRaceLeaderMSPart);
}
