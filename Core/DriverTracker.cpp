#include "DriverTracker.h"
#include "Lap.h"
#include "Logger.h"
#include "Race.h"
#include "Stint.h"
#include "TelemetryDefinitions.h"

#include <QCoreApplication>
#include <QTime>
#include <QTimer>
#include <QtDebug>
#include <QtGlobal>
#include <UdpDataCatalog.h>
#include <cmath>


DriverTracker::DriverTracker(int driverIndex, bool raceOnly) : _driverIndex(driverIndex), _raceOnly(raceOnly)
{
    _currentLap = new Lap(TelemetryDefinitions::TELEMETRY_INFO);
    _currentStint = new Stint(TelemetryDefinitions::TELEMETRY_STINT_INFO);
    _currentRace = new Race(TelemetryDefinitions::TELEMETRY_RACE_INFO);

    _buttons.setPressed_callback([this](auto button) { buttonPressed(button); });
}

void DriverTracker::init(const QDir &directory)
{
    dataDirectory = directory;
    driverDirDefined = false;
    _extendedPlayerTelemetry = false;
    _currentLap->setTelemetryInfo(TelemetryDefinitions::TELEMETRY_INFO);
    _currentRace->resetData();
    _isLapRecorded = false;
    _raceFinished = false;
    _tyres.resetData();
}

void DriverTracker::setMarkButton(Button button)
{
    _buttons.clearTrackedButtons();
    _buttons.trackButton(button);
}

void DriverTracker::telemetryData(const PacketHeader &header, const PacketCarTelemetryData &data)
{
    Q_UNUSED(header)
    if(_currentMotionData.m_carMotionData.count() <= _driverIndex) {
        qWarning() << "INVALID MOTION DATA ...." << driverDataDirectory.dirName();
        return;
    }

    const auto &driverData = data.m_carTelemetryData[_driverIndex];
    const auto &motionData = _currentMotionData.m_carMotionData[_driverIndex];

    if(driverData.m_gear < 0 && !_acquisitionMode) {
        _isLapRecorded = false; // Rear gear
        qWarning() << "Rear gear engaged " << driverDataDirectory.dirName();
        Logger::instance()->log("Lap canceled (rear gear engaged)");
    }

    TyresData<float> tyreTemp;
    tyreTemp.setArray(driverData.m_tyresSurfaceTemperature);
    const auto &tyreTempValues = tyreTemp.asList();
    auto maxTyreTemp = qAbs(**(std::max_element(tyreTempValues.begin(), tyreTempValues.end(),
                                                [](auto v1, auto v2) { return qAbs(*v1) < qAbs(*v2); })));

    auto ersBalance = (_currentStatusData.m_ersHarvestedThisLapMGUH + _currentStatusData.m_ersHarvestedThisLapMGUK) -
                      _currentStatusData.m_ersDeployedThisLap;
    ersBalance = round(ersBalance / 1000.0);

    int coasting = driverData.m_throttle < 0.05f && driverData.m_brake < 0.05f ? 1 : 0;
    auto values = QVector<float>({float(driverData.m_speed), float(driverData.m_throttle * 100.0),
                                  float(driverData.m_brake * 100.0), float(driverData.m_steer * 100.0),
                                  float(driverData.m_gear), float(_previousLapData.m_currentLapTimeInMS) / 1000.0f,
                                  maxTyreTemp, ersBalance, float(_currentStatusData.m_ersDeployMode),
                                  float(_currentStatusData.m_fuelMix), motionData.m_gForceLateral,
                                  motionData.m_gForceLongitudinal, float(driverData.m_engineRPM), float(coasting)});

    _currentLap->coasting.addValue(coasting, double(_previousLapData.m_lapDistance));

    if(_extendedPlayerTelemetry) {
        TyresData<float> slip;
        slip.setArray(_currentMotionExData.m_wheelSlipRatio);

        // locking
        if(driverData.m_brake > 0.05f) {
            values << qMax(abs(slip.frontLeft), abs(slip.frontRight)) * 100.0;
            values << qMax(abs(slip.rearLeft), abs(slip.rearRight)) * 100.0;
        } else {
            values << 0.0 << 0.0;
        }

        // Balance
        TyresData<float> slipAngle;
        slipAngle.setArray(_currentMotionExData.m_wheelSlipAngle);

        auto frontSlipAngle = (slipAngle.frontRight + slipAngle.frontLeft) / 2.0;
        auto rearSlipAngle = (slipAngle.rearRight + slipAngle.rearLeft) / 2.0;
        auto balance = (frontSlipAngle - rearSlipAngle);

        values << float(balance);
        _currentLap->meanBalance = addMean(_currentLap->meanBalance, balance, _currentLap->xValues().count() + 1);

        // degradation
        TyresData<float> wheelSpeed;
        wheelSpeed.setArray(_currentMotionExData.m_wheelSpeed);

        //		auto velocity = sqrt(motionData.m_worldVelocityX * motionData.m_worldVelocityX +
        //							 motionData.m_worldVelocityY * motionData.m_worldVelocityY);

        TyresData<float> tyreDegradationLat =
            (slip * 0.01f) + 1.0f * driverData.m_speed * qAbs(motionData.m_gForceLateral);
        tyreDegradationLat.abs();

        TyresData<float> tyreDegradationLon = slip * 10.0 * driverData.m_speed * qAbs(motionData.m_gForceLongitudinal);
        tyreDegradationLon.abs();

        auto meanDegradation = tyreDegradationLat.mean() + tyreDegradationLon.mean();
        values << meanDegradation;

        // Traction
        auto tractionRight = 100.0f - abs(slip.rearRight) * 100.0f;
        auto tractionLeft = 100.0f - abs(slip.rearLeft) * 100.0f;
        auto traction = driverData.m_throttle > 0.0f ? std::min(tractionRight, tractionLeft) : 100.0f;
        values << traction;

        // Suspension
        TyresData<float> suspension;
        suspension.setArray(_currentMotionExData.m_suspensionPosition);

        auto suspFR = ((suspension.frontRight + suspension.frontLeft) / 2.0) -
                      ((suspension.rearRight + suspension.rearLeft) / 2.0);
        auto suspRL = ((suspension.frontRight + suspension.rearRight) / 2.0) -
                      ((suspension.frontLeft + suspension.rearLeft) / 2.0);

        values << suspFR << suspRL;

        // Heights
        values << _currentMotionExData.m_frontAeroHeight << _currentMotionExData.m_rearAeroHeight;

        // COG
        values << _currentMotionExData.m_heightOfCOGAboveGround;
    }

    _currentLap->addData(_previousLapData.m_lapDistance, values);
    _currentLap->addMapPoint(
        MapPoint{QPointF(motionData.m_worldPositionX, motionData.m_worldPositionZ),
                 _previousLapData.m_currentLapTimeInMS / 1000.0,
                 QPointF(motionData.m_worldForwardDirX / 32767.0f, motionData.m_worldForwardDirZ / 32767.0f)});

    //	qInfo() << "POS" << motionData.m_worldPositionX << motionData.m_worldPositionY << motionData.m_worldPositionZ;
    //	qInfo() << "DIR" << motionData.m_worldForwardDirX / 32767.0f << motionData.m_worldForwardDirY / 32767.0f
    //			<< motionData.m_worldForwardDirZ / 32767.0f;

    _currentLap->innerTemperatures.apply(
        [driverData](auto index, auto &temp) { temp.addValue(driverData.m_tyresInnerTemperature[index]); });
    _currentStint->innerTemperatures.apply(
        [driverData](auto index, auto &temp) { temp.addValue(driverData.m_tyresInnerTemperature[index]); });

    if(_currentLap->maxSpeed < int(driverData.m_speed)) {
        _currentLap->maxSpeed = int(driverData.m_speed);
        _currentLap->maxSpeedErsMode = _currentStatusData.m_ersDeployMode;
        _currentLap->maxSpeedFuelMix = _currentStatusData.m_fuelMix;
    }
}

void DriverTracker::lapData(const PacketHeader &header, const PacketLapData &data)
{
    _header = header;
    const auto &lapData = data.m_lapData[_driverIndex];

    _currentLap->flashbackManager.setLap(lapData);
    _currentRace->flashbackManager.setLap(lapData);
    _currentStint->flashbackManager.setLap(lapData);

    if(isRace()) {
        auto leaderData = leaderLapData(data);
        if(leaderData.m_resultStatus == 3 && _currentRace->hasData()) {
            _raceFinished = true;
        }
        recordRaceLapEvents(lapData);
    }

    // Out lap recording - désactivated because telemetry during this lap is not working properly
    //	if(!_isLapRecorded && lapData.m_driverStatus == 3 && lapData.m_pitStatus == 0 &&
    //	   ((lapData.m_lapDistance > 0 && lapData.m_lapDistance < _currentSessionData.m_trackLength / 4.0) ||
    //		(lapData.m_lapDistance < 0 && lapData.m_lapDistance < -3.0 * _currentSessionData.m_trackLength / 4.0)))
    //	{
    //		qInfo() << "In Lap Started, TimeDif = " << lapData.m_currentLapTime;
    //		startLap(lapData);
    //		_timeDiff = lapData.m_currentLapTime;
    //		_currentLap->isOutLap = true;
    //	}

    if((_isLapRecorded || isRace()) && _currentLap->flashbackManager.isFirstStatusSinceFlashback()) {
        // Flashback
        if(!_currentLap->flashbackManager.isOnFinishLine()) {
            _currentLap->removeTelemetryFrom(lapData.m_lapDistance);
            qInfo() << "Flashback" << driverDataDirectory.dirName() << _previousLapData.m_lapDistance
                    << lapData.m_lapDistance;
            if(!lapData.m_currentLapInvalid)
                _currentLap->invalid = false;
        } else {
            // Flashback on the start line
            if(!_acquisitionMode) {
                _isLapRecorded = false;
                _currentLap->resetData();
            }
            _currentRace->removeLastData();
            qInfo() << "Flashback on start line" << driverDataDirectory.dirName() << _previousLapData.m_lapDistance
                    << lapData.m_lapDistance;
        }
    } else if(finishLineCrossed(lapData)) {
        // A tracked lap ended

        if(lapData.m_pitStatus != 0 && !_acquisitionMode) {
            _isLapRecorded = false;
            qWarning() << "Crossing line in pits " << driverDataDirectory.dirName();
        }

        saveCurrentLap(lapData);

        if(isRace() && _previousLapData.m_lapDistance >= 0.0f) {
            addLapToRace(_currentLap, lapData);
        }

        _pendingStartLap = true;
    } else if(lapData.m_lapDistance - _previousLapData.m_lapDistance >= 200) {
        // Teleported ?
        _currentLap->partial = true;
    }

    if(_pendingStartLap && lapData.m_pitStatus == 0) {
        startLap(lapData);
        _pendingStartLap = false;
    }

    if(lapData.m_currentLapInvalid) {
        _currentLap->invalid = true;
    }

    //	qInfo() << "Driver:" << _driverIndex << _currentLap->driver.m_name << "Distance:" << lapData.m_lapDistance
    //			<< "pitStatus:" << lapData.m_pitStatus << "driverStatus: " << lapData.m_driverStatus;

    if(((lapData.m_pitStatus > 0 && lapData.m_driverStatus != 3) || lapData.m_driverStatus == 0) &&
       _currentStint->hasData()) {
        if(lapData.m_driverStatus == 2) { // In lap
            _currentLap->isInLap = true;
            saveCurrentLap(lapData);
        }
        saveCurrentStint();
    }

    _currentLap->ers.addValue(_currentStatusData.m_ersDeployMode, double(lapData.m_lapDistance));
    _currentLap->fuelMix.addValue(_currentStatusData.m_fuelMix, double(lapData.m_lapDistance));

    if(_isLapRecorded && lapData.m_pitStatus != 0 && !isRace() && !_acquisitionMode) {
        _isLapRecorded = false;
        qWarning() << "Pitting " << driverDataDirectory.dirName() << "S:" << lapData.m_pitStatus
                   << lapData.m_lapDistance;
    }


    _previousLapData = LapData(lapData);
}


void DriverTracker::saveCurrentStint()
{
    if(_raceOnly) {
        return;
    }

    if(_currentStint->hasData()) {
        // A stint ended
        makeDriverDir();

        auto tyre = UdpDataCatalog::visualTyre(_currentStint->visualTyreCompound).remove(' ');
        auto fileName = "Stint" + QString::number(_currentStintNum) + '_' + QString::number(_currentStint->nbLaps()) +
                        "Laps_" + tyre + ".f1stint";
        auto filePath = driverDataDirectory.absoluteFilePath(fileName);
        _currentStint->isPlayer = _isPlayer;
        _currentStint->save(filePath);
        ++_currentStintNum;
        Logger::instance()->log(QString("Stint recorded: ").append(driverDataDirectory.dirName()));
        qWarning() << "Stint recorded " << driverDataDirectory.dirName() << fileName;

        if(isRace()) {
            _currentRace->addLinkedFile(_currentStint->savedFilename());
        }
    }
    _currentStint->resetData();
}

void DriverTracker::saveCurrentLap(const LapData &lapData)
{
    if(_currentLap->xValues().count() <= 1) {
        qWarning() << "Save Empty Lap " << driverDataDirectory.dirName();
        return;
    }

    _currentLap->averageEndTyreWear = averageTyreWear(_currentDamageData);
    _currentLap->endTyreWear.setArray(_currentDamageData.m_tyresWear);
    _currentLap->fuelOnEnd = double(_currentStatusData.m_fuelInTank);
    _currentLap->energyBalance = _currentStatusData.m_ersStoreEnergy - _currentLap->energyBalance;
    _currentLap->endCarDamage = _currentDamageData;
    _currentLap->tyreTimeLossEnd = _tyres.currentTimeLossInSec();
    _currentLap->tyreRemaingLifeEnd = _tyreSet.m_lifeSpan;

    auto degradationIndex = TelemetryDefinitions::indexOfLapTelemetry(TelemetryDefinitions::TYRE_DEG_INFO_NAME);
    _currentLap->calculatedTyreDegradation = _currentLap->integrateTelemetry(degradationIndex);

    auto tractionIndex = TelemetryDefinitions::indexOfLapTelemetry(TelemetryDefinitions::TRACTION_INFO_NAME);
    _currentLap->calculatedTotalLostTraction =
        _currentLap->integrateTelemetry(tractionIndex, [](auto value) { return 100.0f - value; });

    QString lapType;
    if(_currentLap->isInLap && !isRace()) {
        _currentLap->lapTime = lapData.m_currentLapTimeInMS / 1000.0f;
        _currentLap->sector1Time = lapData.sector1TimeInMs() / 1000.0f;
        _currentLap->sector2Time = lapData.sector2TimeInMs() / 1000.0f;
        _currentLap->sector3Time =
            lapData.m_currentLapTimeInMS / 1000.0f - _currentLap->sector2Time - _currentLap->sector1Time;
        lapType = "_(In)";
    } else if(_currentLap->isOutLap && !isRace()) {
        _currentLap->lapTime = _previousLapData.m_currentLapTimeInMS / 1000.0 - _timeDiff;
        _currentLap->sector1Time = _previousLapData.sector1TimeInMs() / 1000.0;
        _currentLap->sector2Time = _previousLapData.sector2TimeInMs() / 1000.0;
        _currentLap->sector3Time =
            _previousLapData.m_currentLapTimeInMS - _timeDiff - _currentLap->sector2Time - _currentLap->sector1Time;
        lapType = "_(Out)";
    } else {
        if(lapData.m_lapDistance > _currentSessionData.m_trackLength - 5) {
            _currentLap->lapTime = lapData.m_currentLapTimeInMS / 1000.0f;
        } else {
            _currentLap->lapTime = lapData.m_lastLapTimeInMS / 1000.0f;
        }
        _currentLap->sector1Time = _previousLapData.sector1TimeInMs() / 1000.0f;
        _currentLap->sector2Time = _previousLapData.sector2TimeInMs() / 1000.0f;
        _currentLap->sector3Time = _currentLap->lapTime - _currentLap->sector2Time - _currentLap->sector1Time;
    }

    if(std::isnan(_currentLap->lapTime)) {
        _currentLap->lapTime = 0;
    }

    _currentLap->energy = double(_currentStatusData.m_ersStoreEnergy);
    _currentLap->harvestedEnergy =
        double(_currentStatusData.m_ersHarvestedThisLapMGUH + _currentStatusData.m_ersHarvestedThisLapMGUK);
    _currentLap->deployedEnergy = double(_currentStatusData.m_ersDeployedThisLap);
    _currentLap->trackDistance = _currentSessionData.m_trackLength;
    _currentLap->aiDifficulty = _currentSessionData.m_aiDifficulty;

    _currentLap->ers.finalize(double(_currentSessionData.m_trackLength));
    _currentLap->fuelMix.finalize(double(_currentSessionData.m_trackLength));
    _currentLap->coasting.finalize(double(_currentSessionData.m_trackLength));

    if(_isLapRecorded && !_raceOnly && _currentLap->lapTime > 0) {
        makeDriverDir();
        auto lapTime = QTime(0, 0).addMSecs(int(_currentLap->lapTime * 1000.0)).toString("m.ss.zzz");
        auto invalidText = _currentLap->invalid ? "_invalid" : "";
        auto partialText = _currentLap->partial ? "_partial" : "";
        auto fileName =
            "Lap" + QString::number(_currentLapNum) + "_" + lapTime + invalidText + partialText + lapType + ".f1lap";
        auto filePath = driverDataDirectory.absoluteFilePath(fileName);
        _currentLap->save(filePath);
        ++_currentLapNum;
        Logger::instance()->log(QString("Lap recorded: ").append(driverDataDirectory.dirName()));
        qWarning() << "Lap recorded " << driverDataDirectory.dirName() << fileName;

        _timeDiff = 0;
        addLapToStint(_currentLap);
    }
}

void DriverTracker::addLapToStint(Lap *lap)
{
    if(!_currentStint->hasData()) {
        _currentStint->resetData();
        _currentStint->startTyreWear.frontLeft = lap->startTyreWear.frontLeft;
        _currentStint->startTyreWear.frontRight = lap->startTyreWear.frontRight;
        _currentStint->startTyreWear.rearLeft = lap->startTyreWear.rearLeft;
        _currentStint->startTyreWear.rearRight = lap->startTyreWear.rearRight;
        _currentStint->averageStartTyreWear = lap->averageStartTyreWear;
        _currentStint->fuelOnStart = lap->fuelOnStart;
        _currentStint->startCarDamage = lap->startCarDamage;
        _currentStint->aiDifficulty = lap->aiDifficulty;
        _currentStint->tyreTimeLossStart = lap->tyreTimeLossStart;
        _currentStint->tyreRemaingLifeStart = lap->tyreRemaingLifeStart;
    }

    auto values = {lap->lapTime,
                   float(lap->averageEndTyreWear - _currentStint->averageStartTyreWear),
                   float(lap->averageEndTyreWear),
                   float(lap->tyreTimeLossEnd),
                   float(lap->calculatedTotalLostTraction),
                   float(lap->fuelOnEnd),
                   float(lap->energy / 1000.0),
                   float(lap->innerTemperatures.frontRight.mean),
                   float(lap->innerTemperatures.frontLeft.mean),
                   float(lap->innerTemperatures.rearRight.mean),
                   float(lap->innerTemperatures.rearLeft.mean)};
    _currentStint->addData(float(_currentStint->countData() + 1), values);
    _currentStint->recordDate = QDateTime::currentDateTime();
    _currentStint->recordVersion = qApp->applicationVersion();
    _currentStint->tyreCompound = lap->tyreCompound;
    _currentStint->visualTyreCompound = lap->visualTyreCompound;
    _currentStint->endTyreWear = lap->endTyreWear;
    _currentStint->averageEndTyreWear = lap->averageEndTyreWear;
    _currentStint->fuelOnEnd = lap->fuelOnEnd;
    _currentStint->lapTime = addMean(_currentStint->lapTime, lap->lapTime, _currentStint->nbLaps());
    _currentStint->sector1Time = addMean(_currentStint->sector1Time, lap->sector1Time, _currentStint->nbLaps());
    _currentStint->sector2Time = addMean(_currentStint->sector2Time, lap->sector2Time, _currentStint->nbLaps());
    _currentStint->sector3Time = addMean(_currentStint->sector3Time, lap->sector3Time, _currentStint->nbLaps());
    _currentStint->meanBalance = addMean(_currentStint->meanBalance, lap->meanBalance, _currentStint->nbLaps());
    _currentStint->averageSpeed = addMean(_currentStint->averageSpeed, lap->maxSpeed, _currentStint->nbLaps());
    _currentStint->endCarDamage = lap->endCarDamage;
    _currentStint->coasting += lap->coasting;
    _currentStint->calculatedTyreDegradation =
        addMean(_currentStint->calculatedTyreDegradation, lap->calculatedTyreDegradation, _currentStint->nbLaps());
    _currentStint->calculatedTotalLostTraction =
        addMean(_currentStint->calculatedTotalLostTraction, lap->calculatedTotalLostTraction, _currentStint->nbLaps());
    _currentStint->lapTimes.append(lap->lapTime);
    _currentStint->tyreTimeLossEnd = lap->tyreTimeLossEnd;
    _currentStint->tyreRemaingLifeEnd = lap->tyreRemaingLifeEnd;

    QStringList infos;
    if(lap->invalid)
        infos << "invalid";
    if(lap->partial)
        infos << "partial";

    _currentStint->lapsAdditionalInfo.append(infos);
    if(lap->isOutLap)
        _currentStint->isOutLap = true;
    if(lap->isInLap)
        _currentStint->isInLap = true;
    if(_currentStint->maxSpeed < lap->maxSpeed) {
        _currentStint->maxSpeed = lap->maxSpeed;
        _currentStint->maxSpeedErsMode = lap->maxSpeedErsMode;
        _currentStint->maxSpeedFuelMix = lap->maxSpeedFuelMix;
    }

    _currentStint->addLinkedFile(lap->savedFilename());
}

void DriverTracker::addLapToRace(Lap *lap, const LapData &lapData)
{
    if(!_currentRace->hasData()) {
        makeDriverDir();
        qDebug() << "RACE Started : " << driverDataDirectory.dirName();

        initLap(_currentRace, lapData);
        _currentRace->setup = lap->setup;
        _currentRace->aiDifficulty = lap->aiDifficulty;
    }

    float raceTime = sessionTimePassed();
    if(_raceFinished && _currentRace->hasData()) {
        raceTime = _currentRace->lastRecordedData().value(1);
        raceTime += lap->lapTime;
    }

    auto values = {float(lapData.m_carPosition),
                   raceTime,
                   lap->lapTime,
                   float(_tyres.currentTimeLossInSec() - lap->tyreTimeLossStart),
                   float(lap->averageEndTyreWear),
                   float(lap->fuelOnEnd),
                   float(lap->energy / 1000.0),
                   float(_currentSessionData.m_weather),
                   float(_currentSessionData.m_trackTemperature),
                   float(_currentSessionData.m_airTemperature),
                   float(_currentDamageData.averageCarDamage()),
                   float(lap->flashbackManager.totalCount())};

    lastRaceSessionPassedTime = sessionTimePassed();

    _currentRace->addData(_currentRace->countData() + 1, values);
    _currentRace->recordDate = QDateTime::currentDateTime();
    _currentRace->recordVersion = qApp->applicationVersion();
    _currentRace->recordGameVersion = _header.gameVersion();
    _currentRace->endTyreWear = lap->endTyreWear;
    _currentRace->averageEndTyreWear = lap->averageEndTyreWear;
    _currentRace->fuelOnEnd = lap->fuelOnEnd;
    _currentRace->lapTime = addMean(_currentRace->lapTime, lap->lapTime, _currentRace->nbLaps());
    _currentRace->sector1Time = addMean(_currentRace->sector1Time, lap->sector1Time, _currentRace->nbLaps());
    _currentRace->sector2Time = addMean(_currentRace->sector2Time, lap->sector2Time, _currentRace->nbLaps());
    _currentRace->sector3Time = addMean(_currentRace->sector3Time, lap->sector3Time, _currentRace->nbLaps());
    _currentRace->meanBalance = addMean(_currentRace->meanBalance, lap->meanBalance, _currentRace->nbLaps());
    _currentRace->endCarDamage = lap->endCarDamage;
    _currentRace->coasting += lap->coasting;

    if(_currentRace->maxSpeed >= lap->maxSpeed) {
        _currentRace->maxSpeed = lap->maxSpeed;
        _currentRace->maxSpeedErsMode = lap->maxSpeedErsMode;
        _currentRace->maxSpeedFuelMix = lap->maxSpeedFuelMix;
    }

    qInfo() << "ADD LAP TO RACE" << _previousLapData.m_lapDistance << lapData.m_lapDistance
            << driverDataDirectory.dirName() << "NB LAPS" << _currentRace->nbLaps();
}

void DriverTracker::recordRaceStint(const LapData &lapData, bool isLastStint)
{
    _currentRace->stintsVisualTyre << _currentStatusData.m_tyreVisualCompound;

    auto previousStintsLaps = 0;
    for(const auto &num : _currentRace->stintsLaps) {
        previousStintsLaps += num;
    }
    int nb = lapData.m_currentLapNum - previousStintsLaps;
    if(isLastStint)
        nb -= 1;
    _currentRace->stintsLaps << nb;
}

void DriverTracker::recordRaceLapEvents(const LapData &lapData)
{
    if(!_currentRace->flashbackManager.isFirstStatusSinceFlashback()) {
        if(_previousLapData.m_pitStatus == 0 && lapData.m_pitStatus > 0) {
            recordRaceStint(lapData);
        }

        if(_previousLapData.m_pitStatus > 0 && lapData.m_pitStatus == 0) {
            _currentRace->pitstops << PitStop{_previousLapData.m_pitLaneTimeInLaneInMS / 1000.0,
                                              _previousLapData.m_pitStopTimerInMS / 1000.0};
        }
    } else {
        if(_previousLapData.m_pitStatus == 0 && lapData.m_pitStatus > 0 && !_currentRace->pitstops.isEmpty()) {
            _currentRace->pitstops.removeLast();
        }
        if(_previousLapData.m_pitStatus > 0 && lapData.m_pitStatus == 0 && !_currentRace->stintsLaps.isEmpty()) {
            _currentRace->stintsVisualTyre.removeLast();
            _currentRace->stintsLaps.removeLast();
        }
    }
}

void DriverTracker::saveCurrentRace()
{
    if(isRace() && _currentRace->hasData()) {
        // A race ended
        qInfo() << "RACE FINISHED: " << driverDataDirectory.dirName();

        recordRaceStint(_previousLapData, true);

        auto fileName = driverDataDirectory.dirName() + ".f1race";
        auto raceDir = dataDirectory;
        auto sessionType = UdpDataCatalog::session(_currentRace->session_type);
        auto raceDirName = QString("_Race Data (").append(sessionType).append(")");
        raceDir.mkdir(raceDirName);
        raceDir.cd(raceDirName);
        auto filePath = raceDir.absoluteFilePath(fileName);
        _currentRace->save(filePath);
        Logger::instance()->log(QString("Race recorded: ").append(driverDataDirectory.dirName()));
    } else {
        qWarning() << "Save empty race !";
    }
}

void DriverTracker::initLap(Lap *lap, const LapData &lapData)
{

    lap->resetData();
    lap->lapTime = std::numeric_limits<float>::quiet_NaN();
    lap->track = _currentSessionData.m_trackId;
    lap->session_type = _currentSessionData.m_sessionType;
    lap->trackTemp = _currentSessionData.m_trackTemperature;
    lap->airTemp = _currentSessionData.m_airTemperature;
    lap->weather = _currentSessionData.m_weather;
    lap->recordDate = QDateTime::currentDateTime();
    lap->recordVersion = qApp->applicationVersion();
    lap->recordGameVersion = _header.gameVersion();
    lap->averageStartTyreWear = averageTyreWear(_currentDamageData);
    lap->startTyreWear.setArray(_currentDamageData.m_tyresWear);
    lap->tyreCompound = _currentStatusData.m_actualTyreCompound;
    lap->visualTyreCompound = _currentStatusData.m_tyreVisualCompound;
    lap->fuelOnStart = double(_currentStatusData.m_fuelInTank);
    lap->maxSpeed = 0;
    lap->energyBalance = _currentStatusData.m_ersStoreEnergy;
    lap->startCarDamage = _currentDamageData;
    lap->isPlayer = _isPlayer;
    lap->tyreTimeLossStart = _tyres.currentTimeLossInSec();
    lap->tyreRemaingLifeStart = _tyreSet.m_lifeSpan;
}

void DriverTracker::startLap(const LapData &lapData)
{
    // A new lap started
    makeDriverDir();
    if(!_raceOnly)
        qDebug() << "LAP Started : " << driverDataDirectory.dirName();

    initLap(_currentLap, lapData);
    if(!_currentStint->hasData() && driverDirDefined) {
        if(!_raceOnly)
            qDebug() << "STINT Started : " << driverDataDirectory.dirName();

        // A new stint started
        _currentStint->track = _currentLap->track;
        _currentStint->driver = _currentLap->driver;
        _currentStint->session_type = _currentLap->session_type;
        _currentStint->trackTemp = _currentLap->trackTemp;
        _currentStint->airTemp = _currentLap->airTemp;
        _currentStint->weather = _currentLap->weather;
        _currentStint->setup = _currentLap->setup;
        _currentStint->recordDate = QDateTime::currentDateTime();
        _currentStint->recordVersion = qApp->applicationVersion();
        _currentStint->recordGameVersion = _header.gameVersion();
        _currentStint->trackDistance = _currentLap->trackDistance;
        _currentStint->lapTimes.clear();
        _currentStint->lapsAdditionalInfo.clear();
    }

    _isLapRecorded = true;
}

void DriverTracker::sessionData(const PacketHeader &header, const PacketSessionData &data)
{
    Q_UNUSED(header)

    _currentLap->flashbackManager.setSession(data);
    _currentRace->flashbackManager.setSession(data);
    _currentStint->flashbackManager.setSession(data);

    if(isRace()) {
        if(!_currentLap->flashbackManager.isFirstSessionSinceFlashback() &&
           _currentSessionData.m_safetyCarStatus == 0 && data.m_safetyCarStatus > 0) {
            // The safety car appears
            switch(data.m_safetyCarStatus) {
                case 1:
                    _currentRace->nbSafetyCars += 1;
                    break;
                case 2:
                    _currentRace->nbVirtualSafetyCars += 1;
                    break;
                default:
                    break;
            }
        } else if(_currentLap->flashbackManager.isFirstSessionSinceFlashback() &&
                  _currentSessionData.m_safetyCarStatus > 0 && data.m_safetyCarStatus == 0) {
            switch(_currentSessionData.m_safetyCarStatus) {
                case 1:
                    _currentRace->nbSafetyCars -= 1;
                    break;
                case 2:
                    _currentRace->nbVirtualSafetyCars -= 1;
                    break;
                default:
                    break;
            }
        }
    }

    _currentSessionData = data;
}

void DriverTracker::setupData(const PacketHeader &header, const PacketCarSetupData &data)
{
    Q_UNUSED(header)
    _currentLap->setup = data.m_carSetups[_driverIndex];
}

void DriverTracker::statusData(const PacketHeader &header, const PacketCarStatusData &data)
{
    Q_UNUSED(header)
    _currentStatusData = data.m_carStatusData[_driverIndex];
    _currentLap->flashbackManager.setStatus(_currentStatusData);
    _currentRace->flashbackManager.setStatus(_currentStatusData);
    _currentStint->flashbackManager.setStatus(_currentStatusData);
}

void DriverTracker::participant(const PacketHeader &header, const PacketParticipantsData &data)
{
    Q_UNUSED(header)
    const auto &driverData = data.m_participants[_driverIndex];
    _currentLap->driver = driverData;
    //	fixDriverNameForMultiplayer(_currentLap);
    _currentRace->driver = driverData;
    //	fixDriverNameForMultiplayer(_currentRace);
    _isPlayer = header.m_playerCarIndex == _driverIndex;

    _currentParticipant = data;
}

void DriverTracker::makeDriverDir()
{
    if(!driverDirDefined) {
        auto driverData = _currentLap->driver;
        auto subDirName = driverData.driverFullName(_isPlayer);
        if(subDirName.isEmpty()) {
            subDirName = QString::number(_driverIndex);
        }

        if(dataDirectory.mkdir(subDirName)) {
            // Reset the counter if a new directory is created
            _currentLapNum = 1;
            _currentStintNum = 1;
        }
        driverDataDirectory = dataDirectory;
        driverDataDirectory.cd(subDirName);
        qDebug() << driverDataDirectory.absolutePath();
        driverDirDefined = true;

        if(_isPlayer) {
            _extendedPlayerTelemetry = true;
            _currentLap->setTelemetryInfo(TelemetryDefinitions::TELEMETRY_INFO +
                                          TelemetryDefinitions::EXTENDED_TELEMETRY_INFO);
        }
    }
}

void DriverTracker::fixDriverNameForMultiplayer(Lap *lap)
{
    if(_currentSessionData.m_networkGame == 1) {
        lap->fixDriverNameForMultiplayer(_driverIndex);
    }
}

int DriverTracker::sessionTimePassed()
{
    return _currentSessionData.m_sessionDuration - _currentSessionData.m_sessionTimeLeft;
}

LapData DriverTracker::leaderLapData(const PacketLapData &lapsData) const
{
    for(int i = 0; i < lapsData.m_lapData.count(); ++i) {
        const auto &data = lapsData.m_lapData[i];
        if(lapsData.m_lapData.value(i).m_carPosition == 1) {
            return data;
        }
    }

    return LapData();
}

void DriverTracker::motionData(const PacketHeader &header, const PacketMotionData &data)
{
    Q_UNUSED(header)
    _currentMotionData = data;
}

void DriverTracker::motionExData(const PacketHeader &header, const PacketMotionExData &data)

{
    Q_UNUSED(header)
    _currentMotionExData = data;
}

void DriverTracker::tyreSetData(const PacketHeader &header, const PacketTyreSetsData &data)
{

    Q_UNUSED(header)
    if(_driverIndex == data.m_carIdx) {
        _tyreSet = data.fittedSet();
        _tyres.update(data);
    }
}

void DriverTracker::eventData(const PacketHeader &header, const PacketEventData &data)
{
    Q_UNUSED(header)
    switch(data.event) {
        case Event::SessionEnded:
            if(!isRace()) {
                onSessionEnd();
            }
            break;
        case Event::ButtonStatus:
            if(_isPlayer && _isLapRecorded) {
                _buttons.setButtonsStatus(data.details.Buttons.m_buttonStatus);
            }
            break;
        case Event::Flashback:
            _currentLap->flashbackManager.setFlashbackEvent(data.details.Flashback.flashbackSessionTime);
            _currentRace->flashbackManager.setFlashbackEvent(data.details.Flashback.flashbackSessionTime);
            _currentStint->flashbackManager.setFlashbackEvent(data.details.Flashback.flashbackSessionTime);
            break;
        case Event::Overtake:
            if(_driverIndex == data.details.Overtake.overtakingVehicleIdx) {
                auto driverName =
                    _currentParticipant.m_participants[data.details.Overtake.beingOvertakenVehicleIdx].m_name;
                auto text = QString("Overtaking %1").arg(driverName);
                _currentLap->addGlobalTelemetryEvent(_previousLapData.m_lapDistance, text, "Overtaking");
            } else if(_driverIndex == data.details.Overtake.beingOvertakenVehicleIdx) {
                auto driverName = _currentParticipant.m_participants[data.details.Overtake.overtakingVehicleIdx].m_name;
                auto text = QString("Overtaken by %1").arg(driverName);
                _currentLap->addGlobalTelemetryEvent(_previousLapData.m_lapDistance, text, "Overtaking");
            }
            break;
        case Event::Collision:
            if(_driverIndex == data.details.Collision.vehicle1Idx || _driverIndex == data.details.Collision.vehicle2Idx) {
                auto driverName1 =
                    _currentParticipant.m_participants[data.details.Collision.vehicle1Idx].m_name;
                auto driverName2 =
                    _currentParticipant.m_participants[data.details.Collision.vehicle2Idx].m_name;
                auto text = QString("Collision between %1 and %2").arg(driverName1, driverName2);
                _currentLap->addGlobalTelemetryEvent(_previousLapData.m_lapDistance, text, "Collision");
            }
            break;
        default:
            break;
    }

    //	qDebug() << "Event Received : " << data.m_eventStringCode << int(data.event);
}

void DriverTracker::finalClassificationData(const PacketHeader &header, const PacketFinalClassificationData &data)
{
    Q_UNUSED(header)
    Q_UNUSED(data)

    if(isRace()) {
        auto driverData = data.m_classificationData[_driverIndex];
        _currentRace->classification = driverData;
    }

    onSessionEnd();
    _raceFinished = true;
}

void DriverTracker::carDamageData(const PacketHeader &header, const PacketCarDamageData &data)
{
    Q_UNUSED(header)

    auto carData = data.m_carDamageData[_driverIndex];

    if(carData.m_floorDamage > _currentDamageData.m_floorDamage) {
        _currentLap->addGlobalTelemetryEvent(_previousLapData.m_lapDistance,
                                             QString("%1% floor damage").arg(carData.m_floorDamage), "Damage");
    }
    if(carData.m_sidepodDamage > _currentDamageData.m_sidepodDamage) {
        _currentLap->addGlobalTelemetryEvent(_previousLapData.m_lapDistance,
                                             QString("%1% sidepod damage").arg(carData.m_sidepodDamage), "Damage");
    }
    if(carData.m_diffuserDamage > _currentDamageData.m_diffuserDamage) {
        _currentLap->addGlobalTelemetryEvent(_previousLapData.m_lapDistance,
                                             QString("%1% diffuser damage").arg(carData.m_diffuserDamage), "Damage");
    }
    if(carData.averageFrontWingDamage() > _currentDamageData.averageFrontWingDamage()) {
        _currentLap->addGlobalTelemetryEvent(_previousLapData.m_lapDistance,
                                             QString("%1% front wing damage").arg(carData.averageFrontWingDamage()),
                                             "Damage");
    }
    if(carData.m_rearWingDamage > _currentDamageData.m_rearWingDamage) {
        _currentLap->addGlobalTelemetryEvent(_previousLapData.m_lapDistance,
                                             QString("%1% rear wing damage").arg(carData.m_rearWingDamage), "Damage");
    }

    _currentDamageData = carData;

    _currentLap->flashbackManager.setDamage(_currentDamageData);
    _currentRace->flashbackManager.setDamage(_currentDamageData);
    _currentStint->flashbackManager.setDamage(_currentDamageData);
}

void DriverTracker::sessionHistoryData(const PacketHeader &header, const PacketSessionHistoryData &data)
{
    Q_UNUSED(header)
    Q_UNUSED(data)
}

void DriverTracker::setAcquisitionMode(bool acquisitionMode) { _acquisitionMode = acquisitionMode; }

void DriverTracker::buttonPressed(Button button)
{
    Q_UNUSED(button)
    _currentLap->addGlobalTelemetryEvent(_previousLapData.m_lapDistance, "Mark");
    Logger::instance()->log("Mark Added");
}

void DriverTracker::onSessionEnd()
{
    if(_previousLapData.m_lapDistance > _currentSessionData.m_trackLength - 5 && _previousLapData.m_pitStatus == 0) {
        saveCurrentLap(_previousLapData);
        _currentLap->lapTime = _previousLapData.m_currentLapTimeInMS / 1000.0f;

        if(isRace() && _currentLap->hasData()) {
            addLapToRace(_currentLap, _previousLapData);
        }
    }
    saveCurrentStint();
    saveCurrentRace();
    _currentLap->resetData();

    if(_raceOnly && driverDataDirectory != QDir::current()) {
        driverDataDirectory.removeRecursively();
    }
}

bool DriverTracker::finishLineCrossed(const LapData &data) const
{
    auto cross = _previousLapData.isValid() &&
                 (_previousLapData.m_lapDistance < 0 ||
                  _previousLapData.m_lapDistance > (_currentSessionData.m_trackLength - 200)) &&
                 ((data.m_lapDistance < 200 && data.m_lapDistance > 0) ||
                  ((data.m_lapDistance > _currentSessionData.m_trackLength - 5) &&
                   _currentSessionData.isTimeTrial() && !_isPlayer));

    //	if(cross)
    //		qInfo() << "CR - DIST :" << _previousLapData.m_lapDistance << data.m_lapDistance
    //				<< _currentSessionData.m_trackLength << "- STATUS :" << data.m_pitStatus << data.m_driverStatus
    //				<< "- REC :" << _isLapRecorded << cross << driverDataDirectory.dirName();

    return cross && (data.m_driverStatus > 0 || isRace());
}

double DriverTracker::averageTyreWear(const CarDamageData &carDamage) const
{
    return (carDamage.m_tyresWear[0] + carDamage.m_tyresWear[1] + carDamage.m_tyresWear[2] + carDamage.m_tyresWear[3]) /
           4.0;
}

bool DriverTracker::isRace() const { return _currentSessionData.isRace(); }

bool DriverTracker::isLastRaceLap(const LapData &data) const
{
    return isRace() && data.m_currentLapNum == _currentSessionData.m_totalLaps;
}

Race *DriverTracker::currentRace()
{
    if(isRace())
        return _currentRace;
    return nullptr;
}
