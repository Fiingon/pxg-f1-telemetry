#include "Units.h"

#include <QMap>

namespace Units
{
const QMap<QString, double> FACTORS_TO_KPH = {{Units::MPH, 1.609344}, {Units::KPH, 1.0}};


double factor(const QString &fromUnit, const QString &toUnit)
{
	auto stdFactor = FACTORS_TO_KPH.value(fromUnit, 1.0);
	return stdFactor / FACTORS_TO_KPH.value(toUnit, 1.0);
}

double convert(double value, const QString &fromUnit, const QString &toUnit)
{
	return value * factor(fromUnit, toUnit);
}

QStringList speedUnits() { return {Units::KPH, Units::MPH}; }

} // namespace Units

double UnitsHandler::convert(double value, const QString &fromUnit)
{
	auto toUnit = displayedUnit(fromUnit);
	if(toUnit != fromUnit) {
		return Units::convert(value, fromUnit, toUnit);
	}

	return value;
}

QString UnitsHandler::displayedUnit(const QString &fromUnit)
{
	if(Units::speedUnits().contains(fromUnit)) {
		return speed;
	}

	return fromUnit;
}
