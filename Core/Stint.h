#ifndef STINT_H
#define STINT_H

#include <QDateTime>
#include <QStringList>
#include <QVector>

#include "Lap.h"
#include "Tyres.h"
#include "UdpSpecification.h"


class Stint : public Lap
{
  public:
    Stint(const QVector<TelemetryInfo> &dataInfo = {});

    QString description() const override;
    void removeLastData() override;
    void resetData() override;

    int nbLaps() const;

    // Saving - Loading
    static Stint *fromFile(const QString &filename);

    QVector<float> lapTimes;
    QVector<QStringList> lapsAdditionalInfo;
    int averageSpeed = 0;

    // Deprecated, kept here for backward compatibility
    TyresData<double> calculatedTyreWear;

  protected:
    void saveData(QDataStream &out) const override;
    void loadData(QDataStream &in) override;
    QVariantMap exportData() const override;
};

#endif // STINT_H
