#ifndef F1LISTENER_H
#define F1LISTENER_H

#include "UdpSpecification.h"
#include <QObject>
#include <QUdpSocket>


class F1PacketInterface
{
  public:
    virtual ~F1PacketInterface();
    virtual void telemetryData(const PacketHeader &header, const PacketCarTelemetryData &data) = 0;
    virtual void lapData(const PacketHeader &header, const PacketLapData &data) = 0;
    virtual void sessionData(const PacketHeader &header, const PacketSessionData &data) = 0;
    virtual void setupData(const PacketHeader &header, const PacketCarSetupData &data) = 0;
    virtual void statusData(const PacketHeader &header, const PacketCarStatusData &data) = 0;
    virtual void participant(const PacketHeader &header, const PacketParticipantsData &data) = 0;
    virtual void motionData(const PacketHeader &header, const PacketMotionData &data) = 0;
    virtual void motionExData(const PacketHeader &header, const PacketMotionExData &data) = 0;
    virtual void eventData(const PacketHeader &header, const PacketEventData &event) = 0;
    virtual void finalClassificationData(const PacketHeader &header, const PacketFinalClassificationData &data) = 0;
    virtual void carDamageData(const PacketHeader &header, const PacketCarDamageData &data) = 0;
    virtual void sessionHistoryData(const PacketHeader &header, const PacketSessionHistoryData &data) = 0;
    virtual void tyreSetData(const PacketHeader &header, const PacketTyreSetsData &data) = 0;
};

class F1Listener : public QObject
{
    Q_OBJECT

  signals:
    void dataReceived(QString &);

  public:
    explicit F1Listener(F1PacketInterface *i, const QString &address, int port, QObject *parent = nullptr);

    void addInterface(F1PacketInterface *i);
    void removeInterface(F1PacketInterface *i);
    bool isConnected() const;

    void forwardTo(const QString &address, int port);

  private:
    QUdpSocket *_listener = nullptr;
    QByteArray _buffer;
    QVector<F1PacketInterface *> _interfaces;
    UdpSpecification::PacketType _expectedDataType = UdpSpecification::PacketType::Header;
    PacketHeader _lastHeader;
    QUdpSocket *_forwarder = nullptr;
    QHostAddress _forwardAddress;
    int _forwardPort;


    bool tryRead();
    void readHeader(QByteArray &data);

  private slots:
    void readData();
};

#endif // F1LISTENER_H
