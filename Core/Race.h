#ifndef RACE_H
#define RACE_H

#include "DriverTracker.h"
#include "Lap.h"

struct PitStop {
    double pitlaneTime = 0;
    double pitStopTime = 0;
};


class Race : public Lap
{
  public:
    Race(const QVector<TelemetryInfo> &dataInfo = {});

    QString description() const override;
    void resetData() override;
    QVariant autoSortData() const override;
    void removeLastData() override;

    int nbLaps() const;


    // Saving - Loading
    static Race *fromFile(const QString &filename);

    int nbSafetyCars = 0;
    int nbVirtualSafetyCars = 0;
    QVector<PitStop> pitstops;
    QVector<int> stintsLaps;
    QVector<int> stintsVisualTyre;
    FinalClassificationData classification;

  protected:
    void saveData(QDataStream &out) const override;
    void loadData(QDataStream &in) override;
    QVariantMap exportData() const override;
};

QDataStream &operator>>(QDataStream &in, PitStop &data);
QDataStream &operator<<(QDataStream &out, const PitStop &data);

#endif // RACE_H
