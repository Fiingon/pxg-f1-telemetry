#include "Race.h"
#include "TrackInfo.h"

#include <QVariant>
#include <UdpDataCatalog.h>

Race::Race(const QVector<TelemetryInfo> &dataInfo) : Lap(dataInfo) {}

QString Race::description() const
{
	auto nbLap = QString::number(nbLaps());
	auto trackName = UdpDataCatalog::trackInfo(track)->name;
	return driver.driverFullName(isPlayer) + " - " + trackName + " - " + nbLap + "Laps";
}

void Race::resetData()
{
	Lap::resetData();

	pitstops.clear();
	stintsLaps.clear();
	stintsVisualTyre.clear();

	nbSafetyCars = 0;
	nbVirtualSafetyCars = 0;
}

QVariant Race::autoSortData() const { return classification.m_position; }

void Race::removeLastData() { Lap::removeLastData(); }

int Race::nbLaps() const { return countData(); }

Race *Race::fromFile(const QString &filename)
{
	auto race = new Race;
	race->load(filename);

	return race;
}

void Race::saveData(QDataStream &out) const
{
	QByteArray lapData;
	QDataStream outLap(&lapData, QIODevice::WriteOnly);
	Lap::saveData(outLap);
	out << lapData << 0 << nbSafetyCars << nbVirtualSafetyCars << stintsLaps << stintsVisualTyre << 0 << 0 << 0 << 0
		<< saveGenericData(pitstops) << saveGenericData(classification);
}

void Race::loadData(QDataStream &in)
{
	QByteArray lapData;
	in >> lapData;
	QDataStream inLap(&lapData, QIODevice::ReadOnly);
	Lap::loadData(inLap);

	QByteArray pitStopData, classificationData;
	int startedGridPosition = -1;
	int endPosition = -1;
	int pointScored = -1;
	int raceStatus = -1;
	int penalties = -1;
	in >> penalties >> nbSafetyCars >> nbVirtualSafetyCars >> stintsLaps >> stintsVisualTyre >> startedGridPosition >>
		endPosition >> pointScored >> raceStatus >> pitStopData >> classificationData;
	loadGenericData(pitstops, pitStopData);
	loadGenericData(classification, classificationData);

	if(startedGridPosition >= 0) {
		classification.m_gridPosition = quint8(startedGridPosition);
	}
	if(endPosition >= 0) {
		classification.m_position = quint8(endPosition);
	}
	if(pointScored >= 0) {
		classification.m_points = quint8(pointScored);
	}
	if(raceStatus >= 0) {
		classification.m_resultStatus = quint8(raceStatus);
	}
	if(penalties >= 0) {
		classification.m_penaltiesTime = quint8(penalties);
	}
}

QVariantMap Race::exportData() const
{
	auto lapMap = Lap::exportData();

	QVariantMap map;
	map["nbSafetyCars"] = nbSafetyCars;
	map["nbVirtualSafetyCars"] = nbVirtualSafetyCars;

	QVariantMap classificationMap;
	classificationMap["position"] = classification.m_position;
	classificationMap["numLap"] = classification.m_numLaps;
	classificationMap["gridPosition"] = classification.m_gridPosition;
	classificationMap["points"] = classification.m_points;
	classificationMap["numPitStops"] = classification.m_numPitStops;
	classificationMap["raceStatus"] = classification.m_resultStatus;
	classificationMap["bestLapTime"] = classification.m_bestLapTimeInMS / 1000.0;
	classificationMap["totalRaceTime"] = classification.m_totalRaceTime;
	classificationMap["penaltiesTime"] = classification.m_penaltiesTime;
	classificationMap["numPenalties"] = classification.m_numPenalties;
	classificationMap["numTyreStints"] = classification.m_numTyreStints;
	map["finalClassification"] = classificationMap;

	QVariantList pitstopsValues;
	for(const auto &ps : pitstops) {
		QVariantMap pitStopMap;
		pitStopMap["pitlaneTime"] = ps.pitlaneTime;
		pitStopMap["pitStopTime"] = ps.pitStopTime;
		pitstopsValues << pitStopMap;
	}
	map["pitstops"] = pitstopsValues;

	QVariantList stintsLapsValues;
	for(const auto &sl : stintsLaps)
		stintsLapsValues << sl;
	map["stintsLaps"] = stintsLapsValues;

	QVariantList stintsTyreValues;
	for(const auto &st : stintsVisualTyre)
		stintsTyreValues << st;
	map["stintsTyres"] = stintsTyreValues;

	lapMap["Race"] = map;
	return lapMap;
}


QDataStream &operator>>(QDataStream &in, PitStop &data)
{
	in >> data.pitlaneTime >> data.pitStopTime;
	return in;
}
QDataStream &operator<<(QDataStream &out, const PitStop &data)
{
	out << data.pitlaneTime << data.pitStopTime;
	return out;
}
