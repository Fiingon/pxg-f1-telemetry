#ifndef UNITS_H
#define UNITS_H

#include <QHash>
#include <QString>

namespace Units
{

const constexpr char *MPH = "mph";
const constexpr char *KPH = "km/h";

double factor(const QString &fromUnit, const QString &toUnit);
double convert(double value, const QString &fromUnit, const QString &toUnit);

QStringList speedUnits();

} // namespace Units

struct UnitsHandler {
	QString speed;

	double convert(double value, const QString &fromUnit);
	QString displayedUnit(const QString &fromUnit);
};


#endif // UNITS_H
