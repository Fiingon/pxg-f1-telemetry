#ifndef UDPSPECIFICATION_H
#define UDPSPECIFICATION_H

#include <QDataStream>
#include <QMap>
#include <QVector>

#include <memory>

enum Button {
    NoButton = 0x0,
    CrossA = 0x0001,
    TriangleY = 0x0002,
    CircleB = 0x0004,
    SquareX = 0x0008,
    Left = 0x0010,
    Right = 0x0020,
    Up = 0x0040,
    Down = 0x0080,
    OptionMenu = 0x0100,
    L1 = 0x0200,
    R1 = 0x0400,
    L2 = 0x0800,
    R2 = 0x1000,
    LeftStickClick = 0x2000,
    RightStickCLick = 0x4000,
    RightStickLeft = 0x8000,
    RightStickRight = 0x10000,
    RightStickUp = 0x20000,
    RightStickDown = 0x40000,
    Special = 0x00080000,
    Action1 = 0x00100000,
    Action2 = 0x00200000,
    Action3 = 0x00400000,
    Action4 = 0x00800000,
    Action5 = 0x01000000,
    Action6 = 0x02000000,
    Action7 = 0x04000000,
    Action8 = 0x08000000,
    Action9 = 0x10000000,
    Action10 = 0x20000000,
    Action11 = 0x40000000,
    Action12 = 0x80000000
};


class UdpSpecification
{
  public:
    enum class PacketType {
        Header = -1,
        Motion = 0,
        Session = 1,
        LapData = 2,
        Event = 3,
        Participants = 4,
        CarSetup = 5,
        CarTelemetry = 6,
        CarStatus = 7,
        FinalClassification = 8,
        LobbyInfo = 9,
        CarDamage = 10,
        SessionHistory = 11,
        TyreSet = 12,
        MotionEx = 13,
        TimeTrial = 14
    };


    static UdpSpecification *instance()
    {
        static auto instance = UdpSpecification();
        return &instance;
    }

    int expectedPacketLength(PacketType type) const;

    static QString myTeamName;

  private:
    UdpSpecification();

    QMap<PacketType, int> packetExpectedLengths;
};

enum class Event {
    SessionStarted,
    SessionEnded,
    FastestLap,
    Retirement,
    DrsEnabled,
    DrsDisabled,
    TeammateInPits,
    ChequeredFlag,
    RaceWinner,
    PenaltyIssued,
    SpeedTrapTrigged,
    StartLights,
    LightsOut,
    DriveThroughServed,
    StopGoServed,
    Flashback,
    ButtonStatus,
    RedFlag,
    Overtake,
    SafetyCar,
    Collision,
    Unknown
};

Event stringToEvent(const QString str);

struct PacketHeader {
    quint16 m_packetFormat;                 // 2023
    quint8 m_gameYear;                      // Game year - last two digits e.g. 23
    quint8 m_gameMajorVersion;              // Game major version - "X.00"
    quint8 m_gameMinorVersion;              // Game minor version - "1.XX"
    quint8 m_packetVersion;                 // Version of this packet type, all start from 1
    quint8 m_packetId;                      // Identifier for the packet type, see below
    quint64 m_sessionUID;                   // Unique identifier for the session
    float m_sessionTime;                    // Session timestamp
    quint32 m_frameIdentifier;              // Identifier for the frame the data was retrieved on
    quint32 m_overallFrameIdentifier;       // Overall identifier for the frame the data was retrieved
    quint8 m_playerCarIndex = 254;          // Index of player's car in the array
    quint8 m_secondaryPlayerCarIndex = 255; // Index of secondary player's car in the array (splitscreen)
                                            // 255 if no second player

    bool isValid() const { return m_playerCarIndex < 254; }
    bool hasSecondaryPlayer() { return m_secondaryPlayerCarIndex < 255; }
    QString gameVersion() const;
};

struct ParticipantData {
    quint8 m_aiControlled = 0; // Whether the vehicle is AI (1) or Human (0) controlled
    quint8 m_driverId = 0;     // Driver id - see appendix
    quint8 m_networkId = 0;    // Network id – unique identifier for network players
    quint8 m_teamId = 0;       // Team id - see appendix
    quint8 m_myTeam = 0;       // My team flag – 1 = My Team, 0 = otherwise
    quint8 m_raceNumber = 0;   // Race number of the car
    quint8 m_nationality = 0;  // Nationality of the driver
    QString m_name;            // Name of participant in UTF-8 format – null terminated
                               // Will be truncated with … (U+2026) if too long
    quint8 m_yourTelemetry;    // The player's UDP setting, 0 = restricted, 1 = public
    quint8 m_showOnlineNames;  // The player's show online names setting, 0 = off, 1 = on
    quint16 m_techLevel;         // F1 World tech level
    quint8 m_platform;         // 1 = Steam, 3 = PlayStation, 4 = Xbox, 6 = Origin, 255 = unknown


    QString driverFullName(bool isPlayer) const;
    QString team() const;
};
struct PacketParticipantsData {
    quint8 m_numActiveCars; // Number of active cars in the data – should match number of
                            // cars on HUD
    QVector<ParticipantData> m_participants;

    QStringList availableDrivers(const PacketHeader &header) const;
};

class LapData
{
  public:
    quint32 m_lastLapTimeInMS = 0;    // Last lap time in milliseconds
    quint32 m_currentLapTimeInMS = 0; // Current time around the lap in milliseconds
    quint16 m_sector1TimeMSPart;         // Sector 1 time milliseconds part
    quint8  m_sector1TimeMinutesPart;    // Sector 1 whole minute part
    quint16 m_sector2TimeMSPart;         // Sector 2 time milliseconds part
    quint8  m_sector2TimeMinutesPart;    // Sector 2 whole minute part
    quint16 m_deltaToCarInFrontMSPart;   // Time delta to car in front milliseconds part
    quint8  m_deltaToCarInFrontMinutesPart; // Time delta to car in front whole minute part
    quint16 m_deltaToRaceLeaderMSPart;      // Time delta to race leader milliseconds part
    quint8  m_deltaToRaceLeaderMinutesPart; // Time delta to race leader whole minute part

    float m_lapDistance = -999999;        // Distance vehicle is around current lap in metres – could
                                          // be negative if line hasn’t been crossed yet
    float m_totalDistance = -999999;      // Total distance travelled in session in metres – could
                                          // be negative if line hasn’t been crossed yet
    float m_safetyCarDelta;               // Delta in seconds for safety car
    quint8 m_carPosition = 0;             // Car race position
    quint8 m_currentLapNum;               // Current lap number
    quint8 m_pitStatus;                   // 0 = none, 1 = pitting, 2 = in pit area
    quint8 m_numPitStops;                 // Number of pit stops taken in this race
    quint8 m_sector;                      // 0 = sector1, 1 = sector2, 2 = sector3
    quint8 m_currentLapInvalid;           // Current lap invalid - 0 = valid, 1 = invalid
    quint8 m_penalties;                   // Accumulated time penalties in seconds to be added
    quint8 m_totalWarnings;               // Accumulated number of warnings issued
    quint8 m_cornerCuttingWarnings;       // Accumulated number of corner cutting warnings issued
    quint8 m_numUnservedDriveThroughPens; // Num drive through pens left to serve
    quint8 m_numUnservedStopGoPens;       // Num stop go pens left to serve
    quint8 m_gridPosition;                // Grid position the vehicle started the race in
    quint8 m_driverStatus;                // Status of driver - 0 = in garage, 1 = flying lap
                                          // 2 = in lap, 3 = out lap, 4 = on track
    quint8 m_resultStatus;                // Result status - 0 = invalid, 1 = inactive, 2 = active
                                          // 3 = finished, 4 = didnotfinish, 5 = disqualified
                                          // 6 = not classified, 7 = retired

    quint8 m_pitLaneTimerActive;     // Pit lane timing, 0 = inactive, 1 = active
    quint16 m_pitLaneTimeInLaneInMS; // If active, the current time spent in the pit lane in ms
    quint16 m_pitStopTimerInMS;      // Time of the actual pit stop in ms
    quint8 m_pitStopShouldServePen;  // Whether the car should serve a penalty at this stop
    float m_speedTrapFastestSpeed;     // Fastest speed through speed trap for this car in kmph
    quint8 m_speedTrapFastestLap;       // Lap no the fastest speed was achieved, 255 = not set


    bool isValid() const { return m_lapDistance != -999999; }
    int sector1TimeInMs() const;
    int sector2TimeInMs() const;
    int deltaToCarInFrontInMs() const;
    int deltaToLeaderInMs() const;
};

struct PacketLapData {
    QVector<LapData> m_lapData; // Lap data for all cars on track

    int lapIndexForCarPosition(int pos) const;
    quint8 m_timeTrialPBCarIdx;    // Index of Personal Best car in time trial (255 if invalid)
    quint8 m_timeTrialRivalCarIdx; // Index of Rival car in time trial (255 if invalid)
};

struct CarTelemetryData {
    quint16 m_speed;                     // Speed of car in kilometres per hour
    float m_throttle;                    // Amount of throttle applied (0.0 to 1.0)
    float m_steer;                       // Steering (-1.0 (full lock left) to 1.0 (full lock right))
    float m_brake;                       // Amount of brake applied (0.0 to 1.0)
    quint8 m_clutch;                     // Amount of clutch applied (0 to 100)
    qint8 m_gear;                        // Gear selected (1-8, N=0, R=-1)
    quint16 m_engineRPM;                 // Engine RPM
    quint8 m_drs;                        // 0 = off, 1 = on
    quint8 m_revLightsPercent;           // Rev lights indicator (percentage)
    quint16 m_revLightsBitValue;         // Rev lights (bit 0 = leftmost LED, bit 14 = rightmost LED)
    quint16 m_brakesTemperature[4];      // Brakes temperature (celsius)
    quint8 m_tyresSurfaceTemperature[4]; // Tyres surface temperature (celsius)
    quint8 m_tyresInnerTemperature[4];   // Tyres inner temperature (celsius)
    quint16 m_engineTemperature;         // Engine temperature (celsius)
    float m_tyresPressure[4];            // Tyres pressure (PSI)
    quint8 m_surfaceType[4];             // Driving surface, see appendices
};

struct PacketCarTelemetryData {
    QVector<CarTelemetryData> m_carTelemetryData;

    quint8 m_mfdPanelIndex;                // Index of MFD panel open - 255 = MFD closed
                                           // Single player, race – 0 = Car setup, 1 = Pits
                                           // 2 = Damage, 3 =  Engine, 4 = Temperatures
                                           // May vary depending on game mode
    quint8 m_mfdPanelIndexSecondaryPlayer; // See above
    qint8 m_suggestedGear;                 // Suggested gear for the player (1-8)
                                           // 0 if no gear suggested
};

struct CarSetupData {
    quint8 m_frontWing = 0;             // Front wing aero
    quint8 m_rearWing = 0;              // Rear wing aero
    quint8 m_onThrottle = 0;            // Differential adjustment on throttle (percentage)
    quint8 m_offThrottle = 0;           // Differential adjustment off throttle (percentage)
    float m_frontCamber = 0.0;          // Front camber angle (suspension geometry)
    float m_rearCamber = 0.0;           // Rear camber angle (suspension geometry)
    float m_frontToe = 0.0;             // Front toe angle (suspension geometry)
    float m_rearToe = 0.0;              // Rear toe angle (suspension geometry)
    quint8 m_frontSuspension = 0;       // Front suspension
    quint8 m_rearSuspension = 0;        // Rear suspension
    quint8 m_frontAntiRollBar = 0;      // Front anti-roll bar
    quint8 m_rearAntiRollBar = 0;       // Front anti-roll bar
    quint8 m_frontSuspensionHeight = 0; // Front ride height
    quint8 m_rearSuspensionHeight = 0;  // Rear ride height
    quint8 m_brakePressure = 0;         // Brake pressure (percentage)
    quint8 m_brakeBias = 0;             // Brake bias (percentage)
    quint8 m_engineBraking = 0;            // Engine braking (percentage)
    float m_rearLeftTyrePressure = 0;       // Rear left tyre pressure (PSI)
    float m_rearRightTyrePressure = 0;      // Rear right tyre pressure (PSI)
    float m_frontLeftTyrePressure = 0;      // Front left tyre pressure (PSI)
    float m_frontRightTyrePressure = 0;     // Front right tyre pressure (PSI)
    quint8 m_ballast = 0;               // Ballast
    float m_fuelLoad = 0.0;             // Fuel load
};

struct PacketCarSetupData {
    QVector<CarSetupData> m_carSetups;
    float m_nextFrontWingValue;	// Value of front wing after next pit stop - player only
};

struct MarshalZone {
    float m_zoneStart; // Fraction (0..1) of way through the lap the marshal zone starts
    qint8 m_zoneFlag;  // -1 = invalid/unknown, 0 = none, 1 = green, 2 = blue, 3 = yellow, 4 = red
};

struct WeatherForecastSample {
    quint8 m_sessionType;           // 0 = unknown, 1 = P1, 2 = P2, 3 = P3, 4 = Short P, 5 = Q1
                                    // 6 = Q2, 7 = Q3, 8 = Short Q, 9 = OSQ, 10 = R, 11 = R2
                                    // 12 = Time Trial
    quint8 m_timeOffset;            // Time in minutes the forecast is for
    quint8 m_weather;               // Weather - 0 = clear, 1 = light cloud, 2 = overcast
                                    // 3 = light rain, 4 = heavy rain, 5 = storm
    qint8 m_trackTemperature;       // Track temp. in degrees celsius
    qint8 m_trackTemperatureChange; // Track temp. change – 0 = up, 1 = down, 2 = no changed
    qint8 m_airTemperature;         // Air temp. in degrees celsius
    qint8 m_airTemperatureChange;   // Air temp. change – 0 = up, 1 = down, 2 = no change
    quint8 m_rainPercentage;        // Rain percentage (0-100)

    bool operator==(const WeatherForecastSample &other) const;
};

struct PacketSessionData {
    quint8 m_weather;                    // Weather - 0 = clear, 1 = light cloud, 2 = overcast
                                         // 3 = light rain, 4 = heavy rain, 5 = storm
    qint8 m_trackTemperature;            // Track temp. in degrees celsius
    qint8 m_airTemperature;              // Air temp. in degrees celsius
    quint8 m_totalLaps;                  // Total number of laps in this race
    quint16 m_trackLength;               // Track length in metres
    quint8 m_sessionType;                // 0 = unknown, 1 = P1, 2 = P2, 3 = P3, 4 = Short P
                                         // 5 = Q1, 6 = Q2, 7 = Q3, 8 = Short Q, 9 = OSQ
                                         // 10 = R, 11 = R2, 12 = Time Trial
    qint8 m_trackId;                     // -1 for unknown, 0-21 for tracks, see appendix
    quint8 m_formula;                    // Formula, 0 = F1 Modern, 1 = F1 Classic, 2 = F2, 3 = F1 Generic
    quint16 m_sessionTimeLeft;           // Time left in session in seconds
    quint16 m_sessionDuration;           // Session duration in seconds
    quint8 m_pitSpeedLimit;              // Pit speed limit in kilometres per hour
    quint8 m_gamePaused;                 // Whether the game is paused
    quint8 m_isSpectating;               // Whether the player is spectating
    quint8 m_spectatorCarIndex;          // Index of the car being spectated
    quint8 m_sliProNativeSupport;        // SLI Pro support, 0 = inactive, 1 = active
    quint8 m_numMarshalZones;            // Number of marshal zones to follow
    QVector<MarshalZone> m_marshalZones; // List of marshal zones – max 21
    quint8 m_safetyCarStatus;            // 0 = no safety car, 1 = full safety car
                                         // 2 = virtual safety car
    quint8 m_networkGame;                // 0 = offline, 1 = online
    quint8 m_numWeatherForecastSamples;  // Number of weather samples to follow
    QVector<WeatherForecastSample> m_weatherForecastSamples; // Array of weather forecast samples
    quint8 m_forecastAccuracy;                               // 0 = Perfect, 1 = Approximate

    quint8 m_aiDifficulty;                    // AI Difficulty rating – 0-110
    quint32 m_seasonLinkIdentifier;           // Identifier for season - persists across saves
    quint32 m_weekendLinkIdentifier;          // Identifier for weekend - persists across saves
    quint32 m_sessionLinkIdentifier;          // Identifier for session - persists across saves
    quint8 m_pitStopWindowIdealLap;           // Ideal lap to pit on for current strategy (player)
    quint8 m_pitStopWindowLatestLap;          // Latest lap to pit on for current strategy (player)
    quint8 m_pitStopRejoinPosition;           // Predicted position to rejoin at (player)
    quint8 m_steeringAssist;                  // 0 = off, 1 = on
    quint8 m_brakingAssist;                   // 0 = off, 1 = low, 2 = medium, 3 = high
    quint8 m_gearboxAssist;                   // 1 = manual, 2 = manual & suggested gear, 3 = auto
    quint8 m_pitAssist;                       // 0 = off, 1 = on
    quint8 m_pitReleaseAssist;                // 0 = off, 1 = on
    quint8 m_ERSAssist;                       // 0 = off, 1 = on
    quint8 m_DRSAssist;                       // 0 = off, 1 = on
    quint8 m_dynamicRacingLine;               // 0 = off, 1 = corners only, 2 = full
    quint8 m_dynamicRacingLineType;           // 0 = 2D, 1 = 3D
    quint8 m_gameMode;                        // Game mode id - see appendix
    quint8 m_ruleSet;                         // Ruleset - see appendix
    quint32 m_timeOfDay;                      // Local time of day - minutes since midnight
    quint8 m_sessionLength;                   // 0 = None, 2 = Very Short, 3 = Short, 4 = Medium
                                              // 5 = Medium Long, 6 = Long, 7 = Full
    quint8 m_speedUnitsLeadPlayer;            // 0 = MPH, 1 = KPH
    quint8 m_temperatureUnitsLeadPlayer;      // 0 = Celsius, 1 = Fahrenheit
    quint8 m_speedUnitsSecondaryPlayer;       // 0 = MPH, 1 = KPH
    quint8 m_temperatureUnitsSecondaryPlayer; // 0 = Celsius, 1 = Fahrenheit
    quint8 m_numSafetyCarPeriods;             // Number of safety cars called during session
    quint8 m_numVirtualSafetyCarPeriods;      // Number of virtual safety cars called
    quint8 m_numRedFlagPeriods;               // Number of red flags called during session

    quint8 m_equalCarPerformance;              // 0 = Off, 1 = On
    quint8 m_recoveryMode;              	// 0 = None, 1 = Flashbacks, 2 = Auto-recovery
    quint8 m_flashbackLimit;            	// 0 = Low, 1 = Medium, 2 = High, 3 = Unlimited
    quint8 m_surfaceType;               	// 0 = Simplified, 1 = Realistic
    quint8 m_lowFuelMode;               	// 0 = Easy, 1 = Hard
    quint8 m_raceStarts;			// 0 = Manual, 1 = Assisted
    quint8 m_tyreTemperature;           	// 0 = Surface only, 1 = Surface & Carcass
    quint8 m_pitLaneTyreSim;            	// 0 = On, 1 = Off
    quint8 m_carDamage;                 	// 0 = Off, 1 = Reduced, 2 = Standard, 3 = Simulation
    quint8 m_carDamageRate;                    // 0 = Reduced, 1 = Standard, 2 = Simulation
    quint8 m_collisions;                       // 0 = Off, 1 = Player-to-Player Off, 2 = On
    quint8 m_collisionsOffForFirstLapOnly;     // 0 = Disabled, 1 = Enabled
    quint8 m_mpUnsafePitRelease;               // 0 = On, 1 = Off (Multiplayer)
    quint8 m_mpOffForGriefing;                 // 0 = Disabled, 1 = Enabled (Multiplayer)
    quint8 m_cornerCuttingStringency;          // 0 = Regular, 1 = Strict
    quint8 m_parcFermeRules;                   // 0 = Off, 1 = On
    quint8 m_pitStopExperience;                // 0 = Automatic, 1 = Broadcast, 2 = Immersive
    quint8 m_safetyCar;                        // 0 = Off, 1 = Reduced, 2 = Standard, 3 = Increased
    quint8 m_safetyCarExperience;              // 0 = Broadcast, 1 = Immersive
    quint8 m_formationLap;                     // 0 = Off, 1 = On
    quint8 m_formationLapExperience;           // 0 = Broadcast, 1 = Immersive
    quint8 m_redFlags;                         // 0 = Off, 1 = Reduced, 2 = Standard, 3 = Increased
    quint8 m_affectsLicenceLevelSolo;          // 0 = Off, 1 = On
    quint8 m_affectsLicenceLevelMP;            // 0 = Off, 1 = On
    quint8 m_numSessionsInWeekend;             // Number of session in following array
    QVector<quint8> m_weekendStructure;    		   // List of session types to show weekend
                                  // structure - see appendix for types
    float    m_sector2LapDistanceStart;          // Distance in m around track where sector 2 starts
    float    m_sector3LapDistanceStart;          // Distance in m around track where sector 3 starts



    bool isRace() const;
    bool isTimeTrial() const;

    QString sessionName() const;
    QVector<WeatherForecastSample> currentSessionWeatherForecastSamples() const;
};

struct CarStatusData {
    quint8 m_tractionControl;  // 0 (off) - 2 (high)
    quint8 m_antiLockBrakes;   // 0 (off) - 1 (on)
    quint8 m_fuelMix;          // Fuel mix - 0 = lean, 1 = standard, 2 = rich, 3 = max
    quint8 m_frontBrakeBias;   // Front brake bias (percentage)
    quint8 m_pitLimiterStatus; // Pit limiter status - 0 = off, 1 = on
    float m_fuelInTank;        // Current fuel mass
    float m_fuelCapacity;      // Fuel capacity
    float m_fuelRemainingLaps; // Fuel remaining in terms of laps (value on MFD)
    quint16 m_maxRPM;          // Cars max RPM, point of rev limiter
    quint16 m_idleRPM;         // Cars idle RPM
    quint8 m_maxGears;         // Maximum number of gears
    quint8 m_drsAllowed;       // 0 = not allowed, 1 = allowed, -1 = unknown

    quint16 m_drsActivationDistance; // 0 = DRS not available, non-zero - DRS will be available
                                     // in [X] metres

    quint8 m_actualTyreCompound; // F1 Modern - 16 = C5, 17 = C4, 18 = C3, 19 = C2, 20 = C1
                                 // 7 = inter, 8 = wet
                                 // F1 Classic - 9 = dry, 10 = wet
                                 // F2 – 11 = super soft, 12 = soft, 13 = medium, 14 = hard
                                 // 15 = wet
    quint8 m_tyreVisualCompound; // F1 visual (can be different from actual compound)
                                 // 16 = soft, 17 = medium, 18 = hard, 7 = inter, 8 = wet
                                 // F1 Classic – same as above
                                 // F2 – same as above
    quint8 m_tyresAgeLaps;       // Age in laps of the current set of tyres
    qint8 m_vehicleFiaFlags;     // -1 = invalid/unknown, 0 = none, 1 = green
                                 // 2 = blue, 3 = yellow, 4 = red
    float m_enginePowerICE;      // Engine power output of ICE (W)
    float m_enginePowerMGUK;     // Engine power output of MGU-K (W)

    float m_ersStoreEnergy;          // ERS energy store in Joules
    quint8 m_ersDeployMode;          // ERS deployment mode, 0 = none, 1 = low, 2 = medium
                                     // 3 = high, 4 = overtake, 5 = hotlap
    float m_ersHarvestedThisLapMGUK; // ERS energy harvested this lap by MGU-K
    float m_ersHarvestedThisLapMGUH; // ERS energy harvested this lap by MGU-H
    float m_ersDeployedThisLap;      // ERS energy deployed this lap
    quint8 m_networkPaused;          // Whether the car is paused in a network game
};

struct PacketCarStatusData {
    QVector<CarStatusData> m_carStatusData;
};

struct CarMotionData {
    float m_worldPositionX;     // World space X position
    float m_worldPositionY;     // World space Y position
    float m_worldPositionZ;     // World space Z position
    float m_worldVelocityX;     // Velocity in world space X
    float m_worldVelocityY;     // Velocity in world space Y
    float m_worldVelocityZ;     // Velocity in world space Z
    qint16 m_worldForwardDirX;  // World space forward X direction (normalised)
    qint16 m_worldForwardDirY;  // World space forward Y direction (normalised)
    qint16 m_worldForwardDirZ;  // World space forward Z direction (normalised)
    qint16 m_worldRightDirX;    // World space right X direction (normalised)
    qint16 m_worldRightDirY;    // World space right Y direction (normalised)
    qint16 m_worldRightDirZ;    // World space right Z direction (normalised)
    float m_gForceLateral;      // Lateral G-Force component
    float m_gForceLongitudinal; // Longitudinal G-Force component
    float m_gForceVertical;     // Vertical G-Force component
    float m_yaw;                // Yaw angle in radians
    float m_pitch;              // Pitch angle in radians
    float m_roll;               // Roll angle in radians
};
struct PacketMotionData {
    PacketHeader m_header; // Header

    QVector<CarMotionData> m_carMotionData; // Data for all cars on track
};

struct PacketMotionExData {
    PacketHeader m_header; // Header

    // Extra player car ONLY data
    float m_suspensionPosition[4];     // Note: All wheel arrays have the following order:
    float m_suspensionVelocity[4];     // RL, RR, FL, FR
    float m_suspensionAcceleration[4]; // RL, RR, FL, FR
    float m_wheelSpeed[4];             // Speed of each wheel
    float m_wheelSlipRatio[4];         // Slip ratio for each wheel
    float m_wheelSlipAngle[4];         // Slip angle for each wheel
    float m_wheelLatForce[4];          // Lateral forces for each wheel
    float m_wheelLongForce[4];         // Longitudinal forces for each wheel
    float m_heightOfCOGAboveGround;    // Height of centre of gravity above ground
    float m_localVelocityX;            // Velocity in local space
    float m_localVelocityY;            // Velocity in local space
    float m_localVelocityZ;            // Velocity in local space
    float m_angularVelocityX;          // Angular velocity x-component
    float m_angularVelocityY;          // Angular velocity y-component
    float m_angularVelocityZ;          // Angular velocity z-component
    float m_angularAccelerationX;      // Angular velocity x-component
    float m_angularAccelerationY;      // Angular velocity y-component
    float m_angularAccelerationZ;      // Angular velocity z-component
    float m_frontWheelsAngle;          // Current front wheels angle in radians
    float m_wheelVertForce[4];         // Vertical forces for each wheel
    float m_frontAeroHeight;             // Front plank edge height above road surface
    float m_rearAeroHeight;              // Rear plank edge height above road surface
    float m_frontRollAngle;              // Roll angle of the front suspension
    float m_rearRollAngle;               // Roll angle of the rear suspension
    float m_chassisYaw;                  // Yaw angle of the chassis relative to the direction
    // of motion - radians

};

// The event details packet is different for each type of event.
// Make sure only the correct type is interpreted.
union EventDataDetails {
    struct {
        quint8 vehicleIdx; // Vehicle index of car achieving fastest lap
        float lapTime;     // Lap time is in seconds
    } FastestLap;

    struct {
        quint8 vehicleIdx; // Vehicle index of car retiring
    } Retirement;

    struct {
        quint8 vehicleIdx; // Vehicle index of team mate
    } TeamMateInPits;

    struct {
        quint8 vehicleIdx; // Vehicle index of the race winner
    } RaceWinner;

    struct {
        quint8 penaltyType;      // Penalty type – see Appendices
        quint8 infringementType; // Infringement type – see Appendices
        quint8 vehicleIdx;       // Vehicle index of the car the penalty is applied to
        quint8 otherVehicleIdx;  // Vehicle index of the other car involved
        quint8 time;             // Time gained, or time spent doing action in seconds
        quint8 lapNum;           // Lap the penalty occurred on
        quint8 placesGained;     // Number of places gained by this
    } Penalty;

    struct {
        quint8 vehicleIdx;                 // Vehicle index of the vehicle triggering speed trap
        float speed;                       // Top speed achieved in kilometres per hour
        quint8 isOverallFastestInSession;  // Overall fastest speed in session = 1, otherwise 0
        quint8 isDriverFastestInSession;   // Fastest speed for driver in session = 1, otherwise 0
        quint8 fastestVehicleIdxInSession; // Vehicle index of the vehicle that is the fastest in this session
        float fastestSpeedInSession;       // Speed of the vehicle that is the fastest in this session
    } SpeedTrap;

    struct {
        quint8 numLights; // Number of lights showing
    } StartLights;

    struct {
        quint8 vehicleIdx; // Vehicle index of the vehicle serving drive through
    } DriveThroughPenaltyServed;

    struct {
        quint8 vehicleIdx; // Vehicle index of the vehicle serving stop go
    } StopGoPenaltyServed;

    struct {
        quint32 flashbackFrameIdentifier; // Frame identifier flashed back to
        float flashbackSessionTime;       // Session time flashed back to
    } Flashback;

    struct {
        quint32 m_buttonStatus; // Bit flags specifying which buttons are being pressed
                                // currently - see appendices
    } Buttons;

    struct {
        quint8 overtakingVehicleIdx;     // Vehicle index of the vehicle overtaking
        quint8 beingOvertakenVehicleIdx; // Vehicle index of the vehicle being overtaken
    } Overtake;

    struct
    {
        quint8 safetyCarType;              // 0 = No Safety Car, 1 = Full Safety Car
                             // 2 = Virtual Safety Car, 3 = Formation Lap Safety Car
        quint8 eventType;                  // 0 = Deployed, 1 = Returning, 2 = Returned
                         // 3 = Resume Race
    } SafetyCar;

    struct
    {
        quint8 vehicle1Idx;            // Vehicle index of the first vehicle involved in the collision
        quint8 vehicle2Idx;            // Vehicle index of the second vehicle involved in the collision
    } Collision;
};

struct PacketEventData {
    QString m_eventStringCode; // Event string code, see below
    EventDataDetails details;
    Event event;
};

struct FinalClassificationData {
    quint8 m_position;                   // Finishing position
    quint8 m_numLaps;                    // Number of laps completed
    quint8 m_gridPosition;               // Grid position of the car
    quint8 m_points;                     // Number of points scored
    quint8 m_numPitStops;                // Number of pit stops made
    quint8 m_resultStatus;               // Result status - 0 = invalid, 1 = inactive, 2 = active
                                         // 3 = finished, 4 = didnotfinish, 5 = disqualified
                                         // 6 = not classified, 7 = retired
    quint32 m_bestLapTimeInMS;           // Best lap time of the session in seconds
    double m_totalRaceTime;              // Total race time in seconds without penalties
    quint8 m_penaltiesTime;              // Total penalties accumulated in seconds
    quint8 m_numPenalties;               // Number of penalties applied to this driver
    quint8 m_numTyreStints;              // Number of tyres stints up to maximum
    QVector<quint8> m_tyreStintsActual;  // Actual tyres used by this driver
    QVector<quint8> m_tyreStintsVisual;  // Visual tyres used by this driver
    QVector<quint8> m_tyreStintsEndLaps; // The lap number stints end on
};

struct PacketFinalClassificationData {
    PacketHeader m_header; // Header

    quint8 m_numCars; // Number of cars in the final classification
    QVector<FinalClassificationData> m_classificationData;
};

struct CarDamageData {
    float m_tyresWear[4];              // Tyre wear (percentage)
    quint8 m_tyresDamage[4];           // Tyre damage (percentage)
    quint8 m_brakesDamage[4];          // Brakes damage (percentage)
    quint8 m_frontLeftWingDamage = 0;  // Front left wing damage (percentage)
    quint8 m_frontRightWingDamage = 0; // Front right wing damage (percentage)
    quint8 m_rearWingDamage = 0;       // Rear wing damage (percentage)
    quint8 m_floorDamage = 0;          // Floor damage (percentage)
    quint8 m_diffuserDamage = 0;       // Diffuser damage (percentage)
    quint8 m_sidepodDamage = 0;        // Sidepod damage (percentage)
    quint8 m_drsFault = 0;             // Indicator for DRS fault, 0 = OK, 1 = fault
    quint8 m_ersFault = 0;             // Indicator for ERS fault, 0 = OK, 1 = fault
    quint8 m_gearBoxDamage = 0;        // Gear box damage (percentage)
    quint8 m_engineDamage = 0;         // Engine damage (percentage)
    quint8 m_engineMGUHWear = 0;       // Engine wear MGU-H (percentage)
    quint8 m_engineESWear = 0;         // Engine wear ES (percentage)
    quint8 m_engineCEWear = 0;         // Engine wear CE (percentage)
    quint8 m_engineICEWear = 0;        // Engine wear ICE (percentage)
    quint8 m_engineMGUKWear = 0;       // Engine wear MGU-K (percentage)
    quint8 m_engineTCWear = 0;         // Engine wear TC (percentage)
    quint8 m_engineBlown = 0;          // Engine blown, 0 = OK, 1 = fault
    quint8 m_engineSeized = 0;         // Engine seized, 0 = OK, 1 = fault

    double averageCarDamage() const;
    double averageEngineDamage() const;
    double averageFrontWingDamage() const;
};

struct PacketCarDamageData {
    PacketHeader m_header; // Header

    QVector<CarDamageData> m_carDamageData;
};

struct LapHistoryData {
    quint32 m_lapTimeInMS;       // Lap time in milliseconds
    quint16 m_sector1TimeInMS;   // Sector 1 time in milliseconds
    quint8 m_sector1TimeMinutes; // Sector 1 whole minute part
    quint16 m_sector2TimeInMS;   // Sector 2 time in milliseconds
    quint8 m_sector2TimeMinutes; // Sector 2 whole minute part
    quint16 m_sector3TimeInMS;   // Sector 3 time in milliseconds
    quint8 m_sector3TimeMinutes; // Sector 3 whole minute part
    quint8 m_lapValidBitFlags;   // 0x01 bit set-lap valid,      0x02 bit set-sector 1 valid
                                 // 0x04 bit set-sector 2 valid, 0x08 bit set-sector 3 valid
};

struct TyreStintHistoryData {
    quint8 m_endLap;             // Lap the tyre usage ends on (255 of current tyre)
    quint8 m_tyreActualCompound; // Actual tyres used by this driver
    quint8 m_tyreVisualCompound; // Visual tyres used by this driver
};

struct PacketSessionHistoryData {
    PacketHeader m_header; // Header

    quint8 m_carIdx;        // Index of the car this lap data relates to
    quint8 m_numLaps;       // Num laps in the data (including current partial lap)
    quint8 m_numTyreStints; // Number of tyre stints in the data

    quint8 m_bestLapTimeLapNum; // Lap the best lap time was achieved on
    quint8 m_bestSector1LapNum; // Lap the best Sector 1 time was achieved on
    quint8 m_bestSector2LapNum; // Lap the best Sector 2 time was achieved on
    quint8 m_bestSector3LapNum; // Lap the best Sector 3 time was achieved on

    QVector<LapHistoryData> m_lapHistoryData;              // 100 laps of data max
    QVector<TyreStintHistoryData> m_tyreStintsHistoryData; // 8 stints of data max
};

struct TyreSetData {
    quint8 m_actualTyreCompound; // Actual tyre compound used
    quint8 m_visualTyreCompound; // Visual tyre compound used
    quint8 m_wear;               // Tyre wear (percentage)
    quint8 m_available;          // Whether this set is currently available
    quint8 m_recommendedSession; // Recommended session for tyre set
    quint8 m_lifeSpan;           // Laps left in this tyre set
    quint8 m_usableLife;         // Max number of laps recommended for this compound
    qint16 m_lapDeltaTime;       // Lap delta time in milliseconds compared to fitted set
    quint8 m_fitted;             // Whether the set is fitted or not
};

struct PacketTyreSetsData {
    PacketHeader m_header; // Header

    quint8 m_carIdx;                    // Index of the car this data relates to
    QVector<TyreSetData> m_tyreSetData; // 13 (dry) + 7 (wet)
    quint8 m_fittedIdx;                 // Index into array of fitted tyre

    TyreSetData fittedSet() const;
};

QDataStream &operator>>(QDataStream &in, PacketHeader &packet);
QDataStream &operator>>(QDataStream &in, ParticipantData &packet);
QDataStream &operator>>(QDataStream &in, PacketParticipantsData &packet);
QDataStream &operator>>(QDataStream &in, LapData &packet);
QDataStream &operator>>(QDataStream &in, PacketLapData &packet);
QDataStream &operator>>(QDataStream &in, CarTelemetryData &packet);
QDataStream &operator>>(QDataStream &in, PacketCarTelemetryData &packet);
QDataStream &operator>>(QDataStream &in, CarSetupData &packet);
QDataStream &operator>>(QDataStream &in, PacketCarSetupData &packet);
QDataStream &operator>>(QDataStream &in, MarshalZone &packet);
QDataStream &operator>>(QDataStream &in, PacketSessionData &packet);
QDataStream &operator>>(QDataStream &in, WeatherForecastSample &packet);
QDataStream &operator>>(QDataStream &in, CarStatusData &packet);
QDataStream &operator>>(QDataStream &in, PacketCarStatusData &packet);
QDataStream &operator>>(QDataStream &in, PacketMotionData &packet);
QDataStream &operator>>(QDataStream &in, PacketMotionExData &packet);
QDataStream &operator>>(QDataStream &in, CarMotionData &packet);
QDataStream &operator>>(QDataStream &in, PacketEventData &packet);
QDataStream &operator>>(QDataStream &in, FinalClassificationData &packet);
QDataStream &operator>>(QDataStream &in, PacketFinalClassificationData &packet);
QDataStream &operator>>(QDataStream &in, CarDamageData &packet);
QDataStream &operator>>(QDataStream &in, PacketCarDamageData &packet);
QDataStream &operator>>(QDataStream &in, LapHistoryData &packet);
QDataStream &operator>>(QDataStream &in, TyreStintHistoryData &packet);
QDataStream &operator>>(QDataStream &in, PacketSessionHistoryData &packet);
QDataStream &operator>>(QDataStream &in, TyreSetData &packet);
QDataStream &operator>>(QDataStream &in, PacketTyreSetsData &packet);

QDataStream &operator<<(QDataStream &out, const CarSetupData &packet);
QDataStream &operator<<(QDataStream &out, const ParticipantData &packet);
QDataStream &operator<<(QDataStream &out, const CarDamageData &packet);
QDataStream &operator<<(QDataStream &out, const FinalClassificationData &packet);


void loadEventDetails(QDataStream &in, EventDataDetails &data, Event event);


#endif // UDPSPECIFICATION_H
