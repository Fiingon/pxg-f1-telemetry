#ifndef LAP_H
#define LAP_H

#include "FlashbackManager.h"
#include "ModeData.h"
#include "TelemetryData.h"
#include "Tyres.h"
#include "UdpSpecification.h"

#include <QDateTime>
#include <QPointF>
#include <QStringList>
#include <QVector>


struct MapPoint {
    QPointF position;
    double t = 0.0;
    QPointF direction;
};

MapPoint operator+(const MapPoint &p1, const MapPoint &p2);
MapPoint operator-(const MapPoint &p1, const MapPoint &p2);
MapPoint operator*(const MapPoint &p, double value);
MapPoint operator/(const MapPoint &p, double value);


class Lap : public TelemetryData
{
  public:
    Lap(const QVector<TelemetryInfo> &dataInfo = {});

    QString description() const override;
    virtual void resetData();
    QVariant autoSortData() const override;

    void removeLastData() override;

    int trackIndex() const override { return track; }

    void fixDriverNameForMultiplayer(int driverIndex);

    // Metadata
    QDateTime recordDate;
    QString recordVersion;
    QString recordGameVersion;

    int track = -1;
    int session_type = -1;
    int trackTemp = 0;
    int airTemp = 0;
    int weather = 0;
    bool invalid = false;
    bool partial = false;
    ParticipantData driver;
    bool isPlayer = false;
    double averageStartTyreWear = 0;
    double averageEndTyreWear = 0;
    TyresData<double> startTyreWear;
    TyresData<double> endTyreWear;
    CarSetupData setup;
    QString comment;
    float lapTime = 0;
    float sector1Time = 0;
    float sector2Time = 0;
    float sector3Time = 0;
    int maxSpeed = 0;
    int maxSpeedErsMode = -1;
    int maxSpeedFuelMix = -1;
    int tyreCompound = -1;
    int visualTyreCompound = -1;
    double fuelOnStart = 0;
    double fuelOnEnd = 0;
    ModeData ers;
    ModeData fuelMix;
    double energy = 0;
    double harvestedEnergy = 0;
    double deployedEnergy = 0;
    double energyBalance = 0;
    TyresData<TemperatureData> innerTemperatures;
    double trackDistance = 0;
    FlashbackManager flashbackManager;
    ModeData coasting;
    int aiDifficulty = -1;

    bool isOutLap = false;
    bool isInLap = false;

    double meanBalance = 0.0;
    double calculatedTyreDegradation = 0.0;
    double calculatedTotalLostTraction = 0.0;

    //	int engineDamageStart = 0;
    //	int gearboxDamageStart = 0;
    //	int engineDamageEnd = 0;
    //	int gearboxDamageEnd = 0;

    CarDamageData startCarDamage;
    CarDamageData endCarDamage;

    double tyreTimeLossStart = 0;
    double tyreTimeLossEnd = 0;
    double tyreRemaingLifeStart = 0;
    double tyreRemaingLifeEnd = 0;


    const QVector<MapPoint> &mapsData() const;
    void addMapPoint(const MapPoint &point);
    MapPoint mapPointAt(float x) const;

    // Saving - Loading
    static Lap *fromFile(const QString &filename);


  protected:
    void saveData(QDataStream &out) const override;
    void loadData(QDataStream &in) override;
    QVariantMap exportData() const override;

    QVariantMap exportCarDamageData(const CarDamageData &carDamage) const;

    QVector<MapPoint> _mapsData;
};

QDataStream &operator>>(QDataStream &in, MapPoint &data);
QDataStream &operator<<(QDataStream &out, const MapPoint &data);

#endif // LAP_H
