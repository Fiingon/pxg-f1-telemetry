## PXG F1 Telemetry Changelog

**Note:** The app is not actively developed anymore. If you want to take over the development, contact me. Knowledge of C++ and Qt are required.

### Version 4.1

- FIRST VERSION FOR F1 24
- Records tyre sets theorical lost time
- Records tyre sets remaning life
- Adds Las Vegas, Miami and Losail tracks
- Shows damage and overtaking events in the laps

#### Big shoutout to Nuvolarix. Without him this version would not have been possible.


### Version 3.0

- FIRST VERSION FOR F1 2021
- Records and displays complete car damage
- Records and displays ai difficulty
- Adds the possibility to track ghosts in time trial (note: some telemetry data are not available for ghosts)
- Adds the possibility to forward the UDP data to another ip address

#### Notes 
- Data recorded with previous versions cannot be loaded in this version


### Version 2.5

- Adds button to select how to zoom in the graphs
- Adds a debug tool, available in the "Tools" menu, to show and record some UDP values from the game in real time
- Adds buttons to open laps from a stint and stints from a race
- Shows if a lap is invalid in its name and filename
- Fixes the saving of users parameters on Windows

#### Known issues:

- Ghosts are not recorded in time trial (this is an issue with F1 2020)
- A crash might occur in multiplayer when someone leave the session

### Version 2.4

- Reworks how graphs are organized for a greater flexibility
	- Telemetry graphs are now organized in pages
	- Pages can be displayed in tabs or detached in order to exploit multiple screens
	- Tabs can be reordered
	- Graphs can be reordered within a page
	- Pages can be saved
	- A new section "Saved Pages" has been added on the right side below the telemetry variables
	- Buttons on each graph have been added to zoom or unzoom vertically
- Computes "Coasting" data over a lap, a stint and a race (when brake and acceleration are less than 5%) 
- Displays the car numbers of the drivers next to their name
- Displays the working temperature range of tyres in a stint
- Adds an option to export all loaded laps, stints or races in JSON
- Fixes the displayed player names in multiplayer games
- Fixes various bugs

### Version 2.3

- Displays track borders on the "Track Position" view
- Displays remaining session time in the "Tracking" tab
- Displays current session weather forecast in the "Tracking" tab
- Adds an help button to explain the different ways of navigating in the graphs
- Fixes the export of JSON files
- Fixes various bugs


### Version 2.2

- Displays the telemetry graphs in tabs
- Adds a cursor displayed across all graphs at a choosen position
- Adds a table to show the values of the displayed graph for the cursor value
- Adds a "Track Position" tab to view and compare trajectories
- Allows panning the graphs with the middle mouse button
- Allows zooming the graphs with a pinch gesture on macOS
- Differentiates the player filenames in a multiplayer session
- Fixes various bugs


### Version 2.1

- Adds RPM telemetry data on a lap
- Adds flashback categories (Damage, Penalty, Retry, Other)
- Adds the possibility to add custom marks on a lap while driving by pressing a button which can be configured in the settings
- Adds a new dialog to test the pressed buttons
- Records and displays engine and gearbox wear
- Adds an option to export a data (lap, stint or race) in JSON
- Fixes various bugs


### Version 2.0

- FIRST VERSION FOR F1 2020
- Adds tracking of the second player for splitscreen
- Records the number of points scored and the final status of a race
- Adds a preference dialog to select the displayed name of "My Team"


### Version 1.5

- Adds a new "Race Analysis" tab. New files are produced for each driver during a race for each driver. These files can be opened in the race analysis tab to display some statistic on the race
- Adds a new option to record race data for all drivers, even those which are not tracked
- Adds fuel mix and ers telemetry data on a lap
- Adds the possibility to change the theme of the telemetry charts from the menu "View > Theme"
- Highlights the line of the selected data
- Adds the possibility to change the color of a single line in the chart

### Version 1.4

- Allows settings the listened port and IP address.
- Adds a new "track all drivers" option (recommended for time trial)
- Differentiate front and read tyre locking telemetry data
- Adds lateral and longitudinal G-Forces telemetry data
- Adds traction telemetry data
- Displays the mouse position within the telemetry graphs
- Improves the readability of the turn names in the graphs
- Adds a feedback dialog in the help menu
- Fixes the recording of the player data in time trial


### Version 1.3

- Adds suspension position telemetry data
- Adds a "data distrubution" display for each graph
- Adds the support for the F2 2019 teams
- Adds tooltips explaining most telemetry data
- Adds an automatic update check
- Fixes the recording of the last lap in qualification


### Version 1.2

- Adds an option to records all time trial ghosts
- Adds the recording of the "in lap" of a stint
