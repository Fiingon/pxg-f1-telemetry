#include "Tools.h"

#include <QDateTime>
#include <QIcon>
#include <QPixmap>
#include <QVariant>

const Tools::Version Tools::LAST_SUPPORTED_VERSION = {3, 0};


QImage Tools::loadImageWithColor(const QString &file, const QColor &color)
{
    auto image = QImage(file);

    if(image.isNull() == false) {

        // Colorize if needed
        if(color.isValid() == true) {
            unsigned char R = color.red();
            unsigned char G = color.green();
            unsigned char B = color.blue();
            image = image.convertToFormat(QImage::Format_ARGB32);
            int pixelsCount = image.width() * image.height();
            unsigned char *imageData = image.bits();
            for(int i = 0; i < pixelsCount; i++) {
                imageData[i * 4 + 2] = R; // Use specified color components
                imageData[i * 4 + 1] = G; // Use specified color components
                imageData[i * 4 + 0] = B; // Use specified color components
                                          // imageData[i*4 + 3] = srcAlpha; // Keep alpha value
            }
        }
    }

    return image;
}

QIcon Tools::loadIconWithColor(const QString &file, const QColor &color)
{
    auto pixmap = QPixmap::fromImage(loadImageWithColor(file, color));
    return QIcon(pixmap);
}

bool Tools::compareQVariants(const QVariant &v1, const QVariant &v2)
{
    if(v1.metaType() != v2.metaType()) {
        return false;
    }

    if(v1.canConvert<QDateTime>()) {
        return v1.toDateTime() < v2.toDateTime();
    } else if(v1.canConvert<double>()) {
        return v1.toDouble() < v2.toDouble();
    } else if(v1.canConvert<QString>()) {
        return v1.toString() < v2.toString();
    }

    return false;
}

Tools::Version Tools::parseVersion(const QString &versionStr)
{
    Version version;
    auto values = versionStr.split('.', Qt::SkipEmptyParts);
    for(const auto &value : std::as_const(values)) {
        bool ok = false;
        int num = value.toInt(&ok);
        if(ok) {
            version << num;
        }
    }

    return version;
}

bool Tools::versionIsOlderThan(const QString &versionStr, const Version &thanVersion)
{
    if(versionStr.isEmpty()) {
        return false;
    }
    auto version = parseVersion(versionStr);
    auto nbValues = qMax(version.count(), thanVersion.count());
    for(int i = 0; i < nbValues; ++i) {
        auto v1 = version.value(i, 0);
        auto v2 = thanVersion.value(i, 0);

        if(v1 > v2)
            return false;
        if(v1 < v2)
            return true;
    }

    return false;
}
