#ifndef UDPDEBUGGER_H
#define UDPDEBUGGER_H

#include <QAbstractTableModel>
#include <QDialog>

#include <Core/UdpLogger.h>

namespace Ui
{
class UdpDebugger;
}

class DebuggerTableModel : public QAbstractTableModel
{
	Q_OBJECT

  public:
	DebuggerTableModel(UdpLogger *logger, QObject *parent = nullptr);

	int rowCount(const QModelIndex &parent = QModelIndex()) const override;
	int columnCount(const QModelIndex &parent = QModelIndex()) const override;
	QVariant data(const QModelIndex &index, int role) const override;
	QVariant headerData(int section, Qt::Orientation orientation, int role) const override;

	UdpLogger *logger() const;

  public slots:
	void refresh();

  private:
	UdpLogger *_logger;
	int _nbLoadedRows = 0;
	int _nbLoadedCols = 0;
};

class UdpDebugger : public QDialog
{
	Q_OBJECT

  public:
	explicit UdpDebugger(UdpLogger *logger, QWidget *parent = nullptr);
	~UdpDebugger() override;

  private:
	Ui::UdpDebugger *ui;
	DebuggerTableModel *_model;

	void startAutoRefresh(int timeoutInMs);
	void stopAutoRefresh();

	QTimer *_timer = nullptr;
	bool _isStarted = false;

	void updateStartStopText();

  private slots:
	void startStop();
	void writeToDisk(bool value);
	void changeRefreshRate(int value);
	void variableContextMenuRequested(const QPoint &pos);
	void variableChecked(bool value);
};

#endif // UDPDEBUGGER_H
