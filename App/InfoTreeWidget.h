#ifndef INFOTREEWIDGET_H
#define INFOTREEWIDGET_H

#include <functional>

#include <QHeaderView>
#include <QToolButton>
#include <QTreeWidget>

class InfoTreeWidget : public QTreeWidget
{
  public:
	InfoTreeWidget(QWidget *parent = nullptr);

	QTreeWidgetItem *makeTopLevelItem(const QString &property, const QString &value);
	QTreeWidgetItem *makeItem(QTreeWidgetItem *parentItem, const QString &property, const QString &value);

	void setItemAction(QTreeWidgetItem *item, const QString &name, std::function<void(void)> actionCallback);

  private:
	QIcon actionIcon;

  private slots:
	void showContextMenu(const QPoint &pos);
};

#endif // INFOTREEWIDGET_H
