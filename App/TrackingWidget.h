#ifndef TRACKINGWIDGET_H
#define TRACKINGWIDGET_H

#include <QCheckBox>
#include <QTimer>
#include <QWidget>

#include "Core/Logger.h"
#include <Core/F1Listener.h>

namespace Ui
{
class TrackingWidget;
}

class F1TelemetrySettings;
class MeteoTableModel;

struct TrackedCars {
    bool me;
    bool teammate;
    bool player2;
    bool all;
    bool allRace;
    bool allGhosts;
    const QVector<int> &ids;
};

class TrackingWidget : public QWidget, public LogInterface, public F1PacketInterface
{
    Q_OBJECT

  signals:
    void startTracking(const TrackedCars &cars);
    void stopStracking();
    void networkInfoChanged();

  public:
    explicit TrackingWidget(QWidget *parent = nullptr);
    virtual ~TrackingWidget() override;

    void saveSettings(F1TelemetrySettings *settings);
    void loadSettings(F1TelemetrySettings *settings);

    QString getDataDirectory() const;

  public slots:
    void setSession(const QString &sessionName);
    void setTime(int remainingTimeInSec);
    void setRaceLaps(int nbLaps, int totalLaps);
    void setDrivers(const QStringList &drivers);
    void setStatus(const QString &status, bool trackingInProgress);
    void setConnectionStatus(bool connected);
    virtual void log(const QString &text) override;

    void showQuickInstructions();

  private:
    Ui::TrackingWidget *ui;

    QList<QCheckBox *> _driverCheckBoxes;
    bool _trackingInProgress = false;
    int _nbLogLines = 0;

    QTimer *_sessionTimer;
    int currentTime = -1;
    int _timerCount = 0;

    bool _isRace = false;
    int currentLap = -1;
    int totalLaps = -1;

    PacketHeader _header;
    QStringList _driverList;

    MeteoTableModel *_meteoModel;

    QString getLocalIpAddress() const;
    void updateNetworkData();

  private slots:
    void startStop();
    void browseDataDirectory();
    void allCarsChecked(bool checked);
    void editPort();
    void editServer();
    void onSessionTimer();
    void editForwardPort();
    void editForwardIp();

  protected:
    void telemetryData(const PacketHeader &header, const PacketCarTelemetryData &data) override;
    void lapData(const PacketHeader &header, const PacketLapData &data) override;
    void sessionData(const PacketHeader &header, const PacketSessionData &data) override;
    void setupData(const PacketHeader &header, const PacketCarSetupData &data) override;
    void statusData(const PacketHeader &header, const PacketCarStatusData &data) override;
    void participant(const PacketHeader &header, const PacketParticipantsData &data) override;
    void motionData(const PacketHeader &header, const PacketMotionData &data) override;
    void motionExData(const PacketHeader &header, const PacketMotionExData &data) override;
    void eventData(const PacketHeader &header, const PacketEventData &data) override;
    void finalClassificationData(const PacketHeader &header, const PacketFinalClassificationData &data) override;
    void carDamageData(const PacketHeader &header, const PacketCarDamageData &data) override;
    void sessionHistoryData(const PacketHeader &header, const PacketSessionHistoryData &data) override;
    void tyreSetData(const PacketHeader &header, const PacketTyreSetsData &data) override;
};

#endif // TRACKINGWIDGET_H
