#include "CompareTelemetryWidget.h"
#include "CompareTelemetryPage.h"
#include "CompareWidgetInterface.h"
#include "F1TelemetrySettings.h"
#include "TelemetryChart.h"
#include "TelemetryChartView.h"
#include "TelemetryDataTableModel.h"
#include "ui_CompareTelemetryWidget.h"

#include "Core/TelemetryDefinitions.h"
#include "Core/TrackInfo.h"

#include <F1Telemetry.h>
#include <QBarCategoryAxis>
#include <QBoxPlotSeries>
#include <QCategoryAxis>
#include <QColorDialog>
#include <QFileDialog>
#include <QGraphicsProxyWidget>
#include <QInputDialog>
#include <QLineSeries>
#include <QMenu>
#include <QRadioButton>
#include <QScatterSeries>
#include <QTimer>
#include <QToolButton>
#include <QValueAxis>
#include <QtDebug>

#include <Tools.h>
#include <algorithm>
#include <cmath>

#include <Core/UdpDataCatalog.h>


const int LEFT_PANEL_DEFAULT_WIDTH = 250;

const int CURSOR_SLIDER_HANDLE_WIDTH = 8;

CompareTelemetryWidget::CompareTelemetryWidget(const QString &unitX, const QString &xLabelFormat, QWidget *parent)
: QWidget(parent), ui(new Ui::CompareTelemetryWidget), _unitX(unitX), _xLabelFormat(xLabelFormat)
{
    ui->setupUi(this);

    _toolbar = new QToolBar(this);
    ui->graphLayout->insertWidget(0, _toolbar);
    initActions();

    _telemetryDataModel = new TelemetryDataTableModel(themeColors(_theme));
    ui->lapsTableView->setModel(_telemetryDataModel);
    ui->lapsTableView->horizontalHeader()->setSectionResizeMode(QHeaderView::ResizeToContents);
    ui->lapsTableView->horizontalHeader()->setSectionResizeMode(0, QHeaderView::Stretch);
    connect(ui->btnAddLaps, &QPushButton::clicked, this, &CompareTelemetryWidget::browseData);
    connect(ui->btnClear, &QPushButton::clicked, this, &CompareTelemetryWidget::clearData);
    connect(_telemetryDataModel, &TelemetryDataTableModel::lapsChanged, this, &CompareTelemetryWidget::updateData);
    connect(_telemetryDataModel, &TelemetryDataTableModel::visibilityChanged, this,
            &CompareTelemetryWidget::updateDataVisibilities);
    connect(ui->lapsTableView->selectionModel(), &QItemSelectionModel::currentRowChanged, this,
            &CompareTelemetryWidget::telemetryDataSelected);
    connect(ui->checkTrackLayout, &QCheckBox::toggled, this, &CompareTelemetryWidget::showTrackLayout);
    connect(ui->checkDataName, &QCheckBox::toggled, this, &CompareTelemetryWidget::showTelemetryInfo);
    connect(ui->checkVariables, &QCheckBox::toggled, ui->variablesWidget, &QWidget::setVisible);
    connect(ui->checkPages, &QCheckBox::toggled, ui->pagesWidget, &QWidget::setVisible);

    connect(ui->btnUncheckAllVariables, &QToolButton::clicked, this, &CompareTelemetryWidget::uncheckAllVariables);
    connect(ui->btnUncheckAllPages, &QToolButton::clicked, this, &CompareTelemetryWidget::uncheckAllVariables);

    connect(ui->cursor, &QSlider::valueChanged, this, &CompareTelemetryWidget::updateCursors);
    // ui->cursor->setStyleSheet(cursorSliderStylesheet());
    ui->cursorWidget->hide();

    ui->splitter->setSizes({size().width() - LEFT_PANEL_DEFAULT_WIDTH, LEFT_PANEL_DEFAULT_WIDTH});

    ui->trackWidget->hide();
    showTelemetryInfo(ui->checkDataName->isChecked());
    ui->variablesWidget->setVisible(ui->checkVariables->isChecked());
}

void CompareTelemetryWidget::initActions()
{
    auto iconWidth = QFontMetrics(font()).height();

    auto homeBtn = new QPushButton("Home", this);
    homeBtn->setShortcut(Qt::Key_Escape);
    homeBtn->setToolTip("Displays to the default view (esc)");
    connect(homeBtn, &QPushButton::clicked, this, &CompareTelemetryWidget::home);
    _toolbar->addWidget(homeBtn);

    auto expandWidget = new QWidget();
    expandWidget->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Preferred);
    _toolbar->addWidget(expandWidget);

    _cursorVisibleCheckbox = new QCheckBox("Cursor");
    connect(_cursorVisibleCheckbox, &QCheckBox::toggled, this, &CompareTelemetryWidget::updateCursorWidget);
    _toolbar->addWidget(_cursorVisibleCheckbox);

    auto prevValueButton = new QToolButton(this);
    prevValueButton->setArrowType(Qt::LeftArrow);
    prevValueButton->setAutoRepeat(true);
#ifdef Q_OS_MACOS
    prevValueButton->setMaximumSize(QSize(iconWidth, iconWidth));
#endif
    connect(prevValueButton, &QToolButton::clicked, this,
            [this]() { ui->cursor->setValue(ui->cursor->value() - ui->cursor->pageStep()); });
    _toolbar->addWidget(prevValueButton);

    auto nextValueButton = new QToolButton(this);
    nextValueButton->setArrowType(Qt::RightArrow);
    nextValueButton->setAutoRepeat(true);
#ifdef Q_OS_MACOS
    nextValueButton->setMaximumSize(QSize(iconWidth, iconWidth));
#endif
    connect(nextValueButton, &QToolButton::clicked, this,
            [this]() { ui->cursor->setValue(ui->cursor->value() + ui->cursor->pageStep()); });
    _toolbar->addWidget(nextValueButton);

    auto expandWidget2 = new QWidget();
    expandWidget2->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Preferred);
    _toolbar->addWidget(expandWidget2);

    auto helpButton = new QToolButton(this);
    helpButton->setIcon(QIcon(":/help/icon"));
    homeBtn->setShortcut(QKeySequence::HelpContents);
    helpButton->setToolTip("Displays help to control the graphs");
    helpButton->setAutoRaise(true);
    connect(helpButton, &QPushButton::clicked, this, &CompareTelemetryWidget::help);
#ifdef Q_OS_MACOS
    helpButton->setMaximumSize(QSize(iconWidth, iconWidth));
#endif
    _toolbar->addWidget(helpButton);


    ui->lapsTableView->setContextMenuPolicy(Qt::CustomContextMenu);
    connect(ui->lapsTableView, &QTableView::customContextMenuRequested, this,
            &CompareTelemetryWidget::telemetryTableContextMenu);

    _telemetryContextMenu = new QMenu(this);
    auto setRefAction = _telemetryContextMenu->addAction("Define as reference (R)");
    setRefAction->setShortcut(Qt::Key_R);
    connect(setRefAction, &QAction::triggered, this, &CompareTelemetryWidget::changeReferenceData);
    addAction(setRefAction);

    auto setColorAction = _telemetryContextMenu->addAction("Change color...");
    connect(setColorAction, &QAction::triggered, this, &CompareTelemetryWidget::changeColor);
    addAction(setColorAction);

    auto exportAction = _telemetryContextMenu->addAction("Export to JSON");
    connect(exportAction, &QAction::triggered, this, &CompareTelemetryWidget::exportData);
    addAction(exportAction);

    auto exportAllAction = _telemetryContextMenu->addAction("Export all to JSON");
    connect(exportAllAction, &QAction::triggered, this, &CompareTelemetryWidget::exportAllData);
    addAction(exportAllAction);

    addAction(_telemetryContextMenu->addSeparator());

    auto checkAllAction = _telemetryContextMenu->addAction("Check All");
    connect(checkAllAction, &QAction::triggered, this, [this]() { _telemetryDataModel->setVisibleAll(true); });
    addAction(checkAllAction);

    auto checkOthersAction = _telemetryContextMenu->addAction("Check Others");
    connect(checkOthersAction, &QAction::triggered, this,
            [this]() { _telemetryDataModel->setVisibleAllExcept(ui->lapsTableView->currentIndex().row(), true); });
    addAction(checkOthersAction);

    addAction(_telemetryContextMenu->addSeparator());

    auto uncheckAllAction = _telemetryContextMenu->addAction("Uncheck All");
    connect(uncheckAllAction, &QAction::triggered, this, [this]() { _telemetryDataModel->setVisibleAll(false); });
    addAction(uncheckAllAction);

    auto uncheckOthersAction = _telemetryContextMenu->addAction("Uncheck Others");
    connect(uncheckOthersAction, &QAction::triggered, this,
            [this]() { _telemetryDataModel->setVisibleAllExcept(ui->lapsTableView->currentIndex().row(), false); });
    addAction(uncheckOthersAction);

    addAction(_telemetryContextMenu->addSeparator());

    auto removeAction = _telemetryContextMenu->addAction("Rem");
    removeAction->setShortcut(QKeySequence::Delete);
    removeAction->setText("Remove (" + removeAction->shortcut().toString() + ")");
    connect(removeAction, &QAction::triggered, this, &CompareTelemetryWidget::removeData);
    addAction(removeAction);
}

CompareTelemetryWidget::~CompareTelemetryWidget() { delete ui; }

void CompareTelemetryWidget::addTelemetryData(QVector<TelemetryData *> telemetry)
{
    if(!telemetry.isEmpty()) {
        setTrackIndex(telemetry.first()->trackIndex());
    }

    std::sort(telemetry.begin(), telemetry.end(),
              [](auto t1, auto t2) { return Tools::compareQVariants(t1->autoSortData(), t2->autoSortData()); });
    _telemetryDataModel->addTelemetryData(telemetry);
    ui->lapsTableView->setCurrentIndex(
        _telemetryDataModel->index(_telemetryDataModel->rowCount() - telemetry.count(), 0));

    for(auto customWidget : _allVariablesWidgets) {
        customWidget->addTelemetryData(telemetry);
        customWidget->setColors(_telemetryDataModel->colors());
        customWidget->setReference(_telemetryDataModel->getReferenceData());
    }

    ui->table->addTelemetryData(telemetry);
    ui->table->setColors(_telemetryDataModel->colors());
    ui->table->setReference(_telemetryDataModel->getReferenceData());

    updateWidgetsVisibilities();
}

void CompareTelemetryWidget::setTelemetry(const QVector<TelemetryData *> &telemetry)
{
    if(!telemetry.isEmpty()) {
        for(const auto &telemetryData : telemetry) {
            createVariables(telemetryData->availableData());
            createPages();
        }

        setTrackIndex(_trackIndex);
        QList<QColor> colors = _telemetryDataModel->colors();
        int varIndex = 0;
        for(auto chartView : _variablesCharts) {
            auto isDiff = chartView->isDisplayModeActivated(TelemetryChartView::DisplayMode::Diff);
            auto isStats = chartView->isDisplayModeActivated(TelemetryChartView::DisplayMode::Stats);
            auto isSmooth = chartView->isDisplayModeActivated(TelemetryChartView::DisplayMode::Smoothing);
            chartView->reloadVariableSeries(telemetry, varIndex, isDiff, isStats, isSmooth, colors);
            chartView->setHomeZoom();

            if(_customTheme.isValid())
                chartView->setCustomTheme(_customTheme);

            ++varIndex;
        }

        alignCharts();
        setColors(colors);
        updateCursorWidget();
    } else {
        clearVariables();
    }

    setUnits();
}

void CompareTelemetryWidget::alignCharts()
{
    for(const auto &page : std::as_const(_pages)) {
        page->alignCharts();
    }
}

void CompareTelemetryWidget::highlight(int lapIndex)
{
    for(const auto &page : std::as_const(_pages)) {
        page->highlight(lapIndex);
    }
}

void CompareTelemetryWidget::setTelemetryVisibility(const QVector<bool> &visibility)
{
    for(auto customWidget : _allVariablesWidgets) {
        customWidget->setTelemetryVisibility(visibility);
    }

    ui->table->setTelemetryVisibility(visibility);
}

void CompareTelemetryWidget::createVariables(const QVector<TelemetryInfo> &variables)
{
    if(variables.count() <= _variables.count())
        return;

    F1TelemetrySettings settings;
    int nbCheckboxes = variables.count() + _customCompareWidgets.count();
    int maxVarRows = ceil(nbCheckboxes / 2.0);
    int varCurrentCol = 0;
    int varCurrentRow = 0;
    auto oldCheckboxes = _variableCheckboxes;
    for(auto checkbox : std::as_const(oldCheckboxes))
        ui->variableLayout->removeWidget(checkbox);
    _allVariablesWidgets.clear();
    _variableCheckboxes.clear();

    for(int varIndex = 0; varIndex < nbCheckboxes; ++varIndex) {
        bool isCustom = varIndex >= variables.count();
        auto customWidget = _customCompareWidgets.value(varIndex - variables.count());
        auto var = variables.value(varIndex);
        auto name = isCustom ? customWidget->name() : var.name;

        auto existingCheckboxIt = std::find_if(oldCheckboxes.begin(), oldCheckboxes.end(),
                                               [&name](auto checkbox) { return name == checkbox->text(); });
        if(existingCheckboxIt != oldCheckboxes.end()) {
            ui->variableLayout->addWidget(*existingCheckboxIt, varCurrentRow, varCurrentCol);
            if(isCustom) {
                _allVariablesWidgets << customWidget;
            } else {
                _allVariablesWidgets << _variablesCharts.value(varIndex);
            }
            _variableCheckboxes << *existingCheckboxIt;
        } else {
            auto checkbox = new QCheckBox(name, this);
            connect(checkbox, &QCheckBox::toggled, this, &CompareTelemetryWidget::variableChecked);
            _variableCheckboxes << checkbox;
            ui->variableLayout->addWidget(checkbox, varCurrentRow, varCurrentCol);

            CompareWidgetInterface *compareWidget = nullptr;
            if(!isCustom) {
                auto chart = new TelemetryChart();
                chart->setMargins(QMargins());
                chart->setContentsMargins(0, 0, 0, 0);
                chart->legend()->hide();
                chart->setCursorColor(settings.cursorColor());
                connect(chart, &TelemetryChart::xAxisChanged, this, &CompareTelemetryWidget::distanceZoomChanged);


                QSizePolicy pol(QSizePolicy::Expanding, QSizePolicy::Expanding);
                pol.setVerticalStretch(1);

                auto view = new TelemetryChartView(chart, var.name, showTrackTurns(), this);
                view->viewport()->installEventFilter(this);
                view->setUnitX(_unitX);
                chart->setMargins(QMargins());
                view->setSizePolicy(pol);
                _variablesCharts << view;
                view->setVisible(checkbox->isChecked());
                compareWidget = view;

                connect(view, &TelemetryChartView::displayModeChanged, this,
                        &CompareTelemetryWidget::chartDisplayModeChanged);
            } else {
                compareWidget = _customCompareWidgets.value(varIndex - variables.count());
            }

            compareWidget->widget()->setContextMenuPolicy(Qt::CustomContextMenu);
            connect(compareWidget->widget(), &QWidget::customContextMenuRequested, this,
                    &CompareTelemetryWidget::chartMenuRequested);

            _allVariablesWidgets << compareWidget;
            compareWidget->setTheme(_theme);
            if(_customTheme.isValid()) {
                compareWidget->setCustomTheme(_customTheme);
            }
        }

        ++varCurrentRow;
        if(varCurrentRow >= maxVarRows) {
            varCurrentRow = 0;
            varCurrentCol += 1;
        }
    }

    _variables = variables;
}

void CompareTelemetryWidget::createPages()
{
    int maxVarRows = ceil(_pageSettings.count() / 2.0);
    int varCurrentCol = 0;
    int varCurrentRow = 0;
    for(auto checkbox : _pageCheckboxes)
        ui->pagesLayout->removeWidget(checkbox);

    for(int pgIndex = 0; pgIndex < _pageSettings.count(); ++pgIndex) {
        auto page = _pageSettings[pgIndex];
        auto existingCheckboxIt = std::find_if(_pageCheckboxes.begin(), _pageCheckboxes.end(),
                                               [page](auto checkbox) { return page->name == checkbox->text(); });
        if(existingCheckboxIt != _pageCheckboxes.end()) {
            ui->pagesLayout->addWidget(*existingCheckboxIt, varCurrentRow, varCurrentCol);
        } else {
            auto checkbox = new QCheckBox(page->name, this);
            connect(checkbox, &QCheckBox::toggled, this, &CompareTelemetryWidget::pageChecked);
            _pageCheckboxes << checkbox;
            ui->pagesLayout->addWidget(checkbox, varCurrentRow, varCurrentCol);
        }

        ++varCurrentRow;
        if(varCurrentRow >= maxVarRows) {
            varCurrentRow = 0;
            varCurrentCol += 1;
        }
    }

    ui->lblPagesText->setVisible(_pageCheckboxes.isEmpty());
}

void CompareTelemetryWidget::saveSettings(F1TelemetrySettings *settings)
{
    settings->beginGroup(_dataName);
    settings->setValue("splitterState", ui->splitter->saveState());
    settings->setValue("showTrack", ui->checkTrackLayout->isChecked());
    settings->setValue("showInfo", ui->checkDataName->isChecked());
    settings->setValue("showVariables", ui->checkVariables->isChecked());
    settings->setValue("cursorVisible", _cursorVisibleCheckbox->isChecked());
    settings->setValue("showPages", ui->checkPages->isChecked());
    settings->endGroup();
}

void CompareTelemetryWidget::loadSettings(F1TelemetrySettings *settings)
{
    settings->beginGroup(_dataName);
    ui->splitter->restoreState(settings->value("splitterState").toByteArray());
    ui->checkTrackLayout->setChecked(settings->value("showTrack", true).toBool());
    ui->checkDataName->setChecked(settings->value("showInfo", true).toBool());
    ui->checkVariables->setChecked(settings->value("showVariables", true).toBool());
    _cursorVisibleCheckbox->setChecked(settings->value("cursorVisible", true).toBool());
    ui->checkPages->setChecked(settings->value("showPages", false).toBool());
    settings->endGroup();

    if(!_dataName.isEmpty()) {
        _pageSettings = settings->pageSettings(_dataName);
    }
}

void CompareTelemetryWidget::setDataName(const QString &name)
{
    _dataName = name;
    ui->checkDataName->setText(name + " Data");
    ui->btnAddLaps->setText("Add " + name + "s");
}

void CompareTelemetryWidget::setTrackIndex(int trackIndex)
{
    _trackIndex = trackIndex;

    for(auto widget : _allVariablesWidgets) {
        widget->setTrackIndex(trackIndex);
    }
    ui->table->setTrackIndex(trackIndex);

    ui->lblTrackMap->clear();
    auto trackImage = UdpDataCatalog::trackInfo(trackIndex)->layout;
    ui->trackWidget->setVisible(!trackImage.isEmpty());
    if(!trackImage.isEmpty()) {
        ui->lblTrackMap->setPixmap(QPixmap(trackImage)
                                       .scaled(QSize(ui->lblTrackMap->width(), ui->lblTrackMap->height()),
                                               Qt::KeepAspectRatio, Qt::SmoothTransformation));
    }
}

void CompareTelemetryWidget::setTheme(QChart::ChartTheme theme)
{
    if(_theme != theme) {
        _theme = theme;
        _customTheme = CustomTheme();
        _telemetryDataModel->setBaseColors(themeColors(theme));

        for(auto widget : _allVariablesWidgets) {
            widget->setTheme(theme);
        }

        ui->table->setTheme(theme);

        updateData();
    }

    refreshHighlighting();
    updateCursorsColor();
}

void CompareTelemetryWidget::setCustomTheme(const CustomTheme &theme)
{
    if(theme != _customTheme && theme.isValid()) {
        _customTheme = theme;
        _telemetryDataModel->setBaseColors(theme.seriesColors);

        updateData();

        for(const auto &view : std::as_const(_variablesCharts)) {
            theme.apply(view->chart());
        }

        for(auto customWidget : _allVariablesWidgets) {
            customWidget->setCustomTheme(theme);
        }

        ui->table->setCustomTheme(theme);
    }

    refreshHighlighting();
    updateCursorsColor();
}

void CompareTelemetryWidget::refreshHighlighting()
{
    for(auto page : std::as_const(_pages)) {
        page->refreshHighlighting();
    }
}

QList<QColor> CompareTelemetryWidget::themeColors(QChart::ChartTheme theme) const
{
    QList<QColor> colors;

    QChart chart;
    chart.setTheme(theme);

    for(int i = 0; i < 5; ++i) {
        auto serie = new QLineSeries(&chart);
        chart.addSeries(serie);
        colors << serie->color();
    }

    return colors;
}

void CompareTelemetryWidget::updateTabVisibility()
{
    auto visible = ui->tabs->count() > 0 || _pages.isEmpty();
    ui->tabs->setVisible(visible);
    ui->table->setLimitedViewMode(visible);
}

void CompareTelemetryWidget::addToPage(CompareWidgetInterface *widget, bool forceNewPage)
{
    auto pageSettings = pageSettingsForVariable(widget->name());

    CompareTelemetryPage *page = nullptr;

    if(pageSettings == nullptr) {
        if(!forceNewPage && !_pages.isEmpty()) {
            auto lastPage = _pages.last();
            auto lastPageSettings = pageSettingsForPageName(lastPage->name());
            auto defaultVarPerPages = F1TelemetrySettings().nbVariablesPerPage();
            if(!lastPageSettings && _pages.last()->totalStretch() + widget->strecthFactor() <= defaultVarPerPages) {
                page = _pages.last();
                pageSettings = pageSettingsForPageName(page->name());
            }
        }
        if(!page) {
            page = createPage("Page " + QString::number(_pages.count() + 1), false);
        }
    } else {
        page = pageFromName(pageSettings->name);
        if(!page) {
            page = createPage(pageSettings->name, pageSettings->detached);
            page->setPageSettings(pageSettings);
        }

        if(pageSettings) {
            auto pageIndex = _pageSettings.indexOf(pageSettings);
            if(pageIndex >= 0) {
                auto pageCheckbox = _pageCheckboxes.value(pageIndex);
                if(pageCheckbox) {
                    pageCheckbox->blockSignals(true);
                    pageCheckbox->setChecked(true);
                    pageCheckbox->blockSignals(false);
                }
            }
        }
    }

    page->addWidget(widget, pageSettings ? pageSettings->variables : QStringList());
    if(!page->isDetached())
        ui->tabs->setCurrentWidget(page);
    else
        page->show();

    updateTabVisibility();
}

void CompareTelemetryWidget::removeFromPage(CompareWidgetInterface *widget)
{
    for(auto page : std::as_const(_pages)) {
        if(page->widgets().contains(widget)) {
            page->removeWidget(widget);
            widget->widget()->setParent(this);

            if(page->widgets().isEmpty()) {
                if(!page->isDetached()) {
                    auto tabIndex = ui->tabs->indexOf(page);
                    ui->tabs->removeTab(tabIndex);
                }
                _pages.removeAll(page);
                auto settings = pageSettingsForPageName(page->name());
                if(settings) {
                    auto pageIndex = _pageSettings.indexOf(settings);
                    if(pageIndex >= 0) {
                        auto pageCheckbox = _pageCheckboxes.value(pageIndex);
                        if(pageCheckbox) {
                            pageCheckbox->blockSignals(true);
                            pageCheckbox->setChecked(false);
                            pageCheckbox->blockSignals(false);
                        }
                    }
                }
                page->deleteLater();
            }

            break;
        }
    }

    updateTabVisibility();
}

std::shared_ptr<PageSettings> CompareTelemetryWidget::pageSettingsForVariable(const QString &varName) const
{
    auto it = std::find_if(_pageSettings.begin(), _pageSettings.end(),
                           [&varName](const auto &ps) { return ps->variables.contains(varName); });
    if(it != _pageSettings.end()) {
        return *it;
    }

    return nullptr;
}

std::shared_ptr<PageSettings> CompareTelemetryWidget::pageSettingsForPageName(const QString &pageName) const
{
    auto it = std::find_if(_pageSettings.begin(), _pageSettings.end(),
                           [&pageName](const auto &ps) { return ps->name == pageName; });
    if(it != _pageSettings.end()) {
        return *it;
    }

    return nullptr;
}

QString CompareTelemetryWidget::findUniquePageName(const QString &name) const
{
    auto existing_settings = pageSettingsForPageName(name);
    if(existing_settings) {
        int index = 1;
        QString checkName = name;
        do {
            checkName = QString(name).append(" (").append(QString::number(index)).append(")");
            existing_settings = pageSettingsForPageName(checkName);
            ++index;
        } while(existing_settings);
        return checkName;
    }

    return name;
}

CompareTelemetryPage *CompareTelemetryWidget::createPage(const QString &name, bool detached)
{
    auto page = new CompareTelemetryPage(name, _dataName, this);
    connect(page, &CompareTelemetryPage::detachAsked, this, &CompareTelemetryWidget::detachPage);
    connect(page, &CompareTelemetryPage::renameAsked, this, &CompareTelemetryWidget::renamePage);
    connect(page, &CompareTelemetryPage::saveAsked, this, &CompareTelemetryWidget::savePage);
    connect(page, &CompareTelemetryPage::deleteAsked, this, &CompareTelemetryWidget::deleteSavedPage);
    connect(page, &CompareTelemetryPage::chartDataChanged, this, &CompareTelemetryWidget::updateData);
    connect(page, &CompareTelemetryPage::closed, this, &CompareTelemetryWidget::pageClosed);
    connect(page, &CompareTelemetryPage::zoomModeChanged, this, &CompareTelemetryWidget::updatePagesZoomMode);

    ui->tabs->insertTab(_pages.count(), page, name);
    _pages << page;

    if(detached) {
        QTimer::singleShot(0, page, &CompareTelemetryPage::detach);
    }

    return page;
}

CompareTelemetryPage *CompareTelemetryWidget::pageFromCompareWidget(CompareWidgetInterface *widget)
{
    auto it =
        std::find_if(_pages.begin(), _pages.end(), [widget](auto page) { return page->widgets().contains(widget); });
    if(it != _pages.end()) {
        return *it;
    }

    return nullptr;
}

CompareTelemetryPage *CompareTelemetryWidget::pageFromName(const QString &name)
{
    auto pageIt = std::find_if(_pages.begin(), _pages.end(), [&name](auto page) { return page->name() == name; });
    if(pageIt != _pages.end()) {
        return *pageIt;
    }

    return nullptr;
}

std::shared_ptr<PageSettings> CompareTelemetryWidget::createPageSettings(CompareTelemetryPage *page)
{
    auto pageSettings = std::make_shared<PageSettings>();
    saveSettingsFromPage(pageSettings, page);
    _pageSettings << pageSettings;

    return pageSettings;
}

void CompareTelemetryWidget::saveSettingsFromPage(const std::shared_ptr<PageSettings> &pageSettings,
                                                  CompareTelemetryPage *page)
{
    pageSettings->name = page->name();
    pageSettings->detached = page->isDetached();
    pageSettings->variables = page->variables();
}

void CompareTelemetryWidget::detachPage(bool value)
{
    auto page = qobject_cast<CompareTelemetryPage *>(sender());
    if(value) {
        auto tabIndex = ui->tabs->indexOf(page);
        ui->tabs->removeTab(tabIndex);

        page->setDetached(true);
        page->show();
    } else {
        page->setDetached(false);
        ui->tabs->addTab(page, page->name());
    }

    auto settings = pageSettingsForPageName(page->name());
    if(settings)
        settings->detached = value;

    updateTabVisibility();
}

void CompareTelemetryWidget::renamePage()
{
    auto page = qobject_cast<CompareTelemetryPage *>(sender());

    auto settings = pageSettingsForPageName(page->name());
    if(!settings)
        return;

    QString text = QInputDialog::getText(this, "Rename the page", "Please enter a new name for the page",
                                         QLineEdit::Normal, page->name());
    if(!text.isEmpty() && settings->name != text) {
        text = findUniquePageName(text);
        settings->name = text;
        if(!page->isDetached()) {
            auto index = ui->tabs->indexOf(page);
            ui->tabs->setTabText(index, text);
        }
        page->setName(text);

        auto pageIndex = _pageSettings.indexOf(settings);
        Q_ASSERT(pageIndex >= 0);
        auto pageCheckbox = _pageCheckboxes.value(pageIndex);
        pageCheckbox->setText(text);
        savePageSettingsToDisk();
    }
}

void CompareTelemetryWidget::savePage()
{
    auto page = qobject_cast<CompareTelemetryPage *>(sender());

    auto pageSettings = pageSettingsForPageName(page->name());
    if(!pageSettings || !page->pageSettings()) {
        QString text = QInputDialog::getText(this, "New page name", "Please enter a name to save the page",
                                             QLineEdit::Normal, page->name());
        if(!text.isEmpty()) {
            text = findUniquePageName(text);
            page->setName(text);
            if(!page->isDetached()) {
                auto index = ui->tabs->indexOf(page);
                ui->tabs->setTabText(index, text);
            }
            pageSettings = createPageSettings(page);
            page->setPageSettings(pageSettings);
            createPages();
            auto pageCheckbox = _pageCheckboxes.last();
            pageCheckbox->setChecked(true);
            savePageSettingsToDisk();
        }
    } else {
        saveSettingsFromPage(pageSettings, page);
        savePageSettingsToDisk();
    }
}

void CompareTelemetryWidget::deleteSavedPage()
{
    auto page = qobject_cast<CompareTelemetryPage *>(sender());

    auto settings = pageSettingsForPageName(page->name());
    if(!settings)
        return;

    if(QMessageBox::question(this, "Remove a page",
                             QString("Are you sure you want to remove the page '%1' ?").arg(page->name()),
                             QMessageBox::Yes | QMessageBox::No, QMessageBox::Yes) == QMessageBox::No)
        return;

    page->setPageSettings(nullptr);
    auto pageIndex = _pageSettings.indexOf(settings);
    Q_ASSERT(pageIndex >= 0);
    auto pageCheckbox = _pageCheckboxes.value(pageIndex);
    _pageCheckboxes.removeAll(pageCheckbox);
    delete pageCheckbox;

    _pageSettings.removeAll(settings);

    savePageSettingsToDisk();
}

void CompareTelemetryWidget::pageClosed()
{
    auto page = qobject_cast<CompareTelemetryPage *>(sender());

    for(auto widget : page->widgets()) {
        auto varIndex = _allVariablesWidgets.indexOf(widget);
        auto checkbox = _variableCheckboxes.value(varIndex);
        if(checkbox)
            checkbox->setChecked(false);
    }
}

void CompareTelemetryWidget::chartMenuRequested(const QPoint &pos)
{
    auto senderWidget = qobject_cast<QWidget *>(sender());
    if(!senderWidget)
        return;

    auto compareWidgetIt =
        std::find_if(_allVariablesWidgets.begin(), _allVariablesWidgets.end(),
                     [senderWidget](auto compareWidget) { return compareWidget->widget() == senderWidget; });
    if(compareWidgetIt == _allVariablesWidgets.end())
        return;

    auto compareWidget = *compareWidgetIt;

    QMenu menu;

    auto action = menu.addAction(
        "Move First", this, [this, compareWidget]() { moveGraph(compareWidget, std::numeric_limits<int>::min()); });
    action->setEnabled(canMoveGraph(compareWidget, std::numeric_limits<int>::min()));
    action = menu.addAction("Move Up", this, [this, compareWidget]() { moveGraph(compareWidget, -1); });
    action->setEnabled(canMoveGraph(compareWidget, -1));
    action = menu.addAction("Move Down", this, [this, compareWidget]() { moveGraph(compareWidget, 1); });
    action->setEnabled(canMoveGraph(compareWidget, 1));
    action = menu.addAction("Move Last", this,
                            [this, compareWidget]() { moveGraph(compareWidget, std::numeric_limits<int>::max()); });
    action->setEnabled(canMoveGraph(compareWidget, std::numeric_limits<int>::max()));
    menu.addSeparator();
    auto sendMenu = menu.addMenu("Send to...");
    for(auto page : std::as_const(_pages)) {
        action =
            sendMenu->addAction(page->name(), this, [this, compareWidget, page]() { sendToPage(compareWidget, page); });
        action->setEnabled(!page->widgets().contains(compareWidget));
    }
    sendMenu->addAction("New page...", this, [this, compareWidget]() { sendToPage(compareWidget); });
    menu.addSeparator();
    action = menu.addAction("Close", this, [this, compareWidget]() { removeFromPageSettings(compareWidget); });

    if(!menu.isEmpty()) {
        menu.exec(compareWidget->widget()->mapToGlobal(pos));
    }
}

void CompareTelemetryWidget::savePageSettingsToDisk()
{
    F1TelemetrySettings settings;
    if(!_dataName.isEmpty()) {
        settings.setPageSettings(_dataName, _pageSettings);
    }
}

void CompareTelemetryWidget::updatePagesZoomMode(QChartView::RubberBand zoomMode)
{
    for(const auto &page : std::as_const(_pages)) {
        page->setZoomMode(zoomMode);
    }
}

void CompareTelemetryWidget::moveGraph(CompareWidgetInterface *widget, int nb)
{
    auto page = pageFromCompareWidget(widget);
    if(!page)
        return;

    page->moveGraph(widget, nb);
}

bool CompareTelemetryWidget::canMoveGraph(CompareWidgetInterface *widget, int nb)
{
    auto page = pageFromCompareWidget(widget);
    if(!page)
        return false;

    return page->canMoveGraph(widget, nb);
}

void CompareTelemetryWidget::sendToPage(CompareWidgetInterface *widget, CompareTelemetryPage *page)
{
    if(page) {
        removeFromPage(widget);
        page->addWidget(widget);
        if(!page->isDetached())
            ui->tabs->setCurrentWidget(page);
        else
            page->show();

        updateTabVisibility();
    } else {
        removeFromPage(widget);
        addToPage(widget, true);
    }
}

void CompareTelemetryWidget::removeFromPageSettings(CompareWidgetInterface *widget)
{
    auto page = pageFromCompareWidget(widget);
    if(!page)
        return;

    auto pageSettings = pageSettingsForPageName(page->name());
    if(pageSettings) {
        pageSettings->variables.removeAll(widget->name());
    }

    auto varIndex = _allVariablesWidgets.indexOf(widget);
    auto checkbox = _variableCheckboxes.value(varIndex);
    if(checkbox)
        checkbox->setChecked(false);
}

void CompareTelemetryWidget::addCustomCompareWidget(CompareWidgetInterface *compareWidgets)
{
    _customCompareWidgets.append(compareWidgets);
}

int CompareTelemetryWidget::existingFileIndex(const QString &filename) const
{
    auto it =
        std::find_if(_telemetryDataModel->getTelemetryData().begin(), _telemetryDataModel->getTelemetryData().end(),
                     [&filename](auto data) { return filename == data->savedFilename(); });
    if(it == _telemetryDataModel->getTelemetryData().end())
        return -1;
    return _telemetryDataModel->getTelemetryData().indexOf(*it);
}

void CompareTelemetryWidget::setCurrentDataIndex(int index)
{
    ui->lapsTableView->selectionModel()->select(_telemetryDataModel->index(index, 0),
                                                QItemSelectionModel::ClearAndSelect);
}

void CompareTelemetryWidget::moveViewsToCursor()
{
    auto value = ui->cursor->value();
    for(auto chartView : std::as_const(_variablesCharts)) {
        auto chart = chartView->chart();
        auto axis = static_cast<QValueAxis *>(chart->axes(Qt::Horizontal)[0]);
        auto min = axis->min();
        auto max = axis->max();
        auto length = max - min;
        auto margin = 0.0;
        if(value < min + margin) {
            min = value - margin;
            max = min + length;
        } else if(value > max - margin) {
            max = value + margin;
            min = max - length;
        }
        axis->setRange(min, max);
    }
}

void CompareTelemetryWidget::moveCursorToView()
{
    auto chart = _variablesCharts.value(0)->chart();
    auto axis = static_cast<QValueAxis *>(chart->axes(Qt::Horizontal)[0]);
    if(ui->cursor->value() < axis->min())
        ui->cursor->setValue(axis->min());
    else if(ui->cursor->value() > axis->max())
        ui->cursor->setValue(axis->max());
}

void CompareTelemetryWidget::updateCursorsColor()
{
    F1TelemetrySettings settings;
    setCursorColor(settings.cursorColor());
}

void CompareTelemetryWidget::setUnits()
{
    F1TelemetrySettings settings;
    auto units = settings.units();
    auto varIndex = 0;
    for(auto chartView : std::as_const(_variablesCharts)) {
        auto info = _variables.value(varIndex);
        auto unit = units.displayedUnit(info.unit);
        chartView->setUnitY(unit);

        auto tooltip = info.description;
        if(!unit.isEmpty()) {
            if(!info.description.isEmpty()) {
                tooltip += " (";
            }
            tooltip += unit;
            if(!info.description.isEmpty()) {
                tooltip += ')';
            }
        }
        _variableCheckboxes.value(varIndex)->setToolTip(tooltip);
        ++varIndex;
    }
}

void CompareTelemetryWidget::clearVariables()
{
    uncheckAllVariables();
    for(auto it = _pageCheckboxes.constBegin(); it != _pageCheckboxes.constEnd(); ++it) {
        delete *it;
    }
    for(auto it = _variableCheckboxes.constBegin(); it != _variableCheckboxes.constEnd(); ++it) {
        delete *it;
    }

    for(auto it = _variablesCharts.constBegin(); it != _variablesCharts.constEnd(); ++it) {
        delete *it;
    }

    for(const auto &page : std::as_const(_pages)) {
        page->clear();
    }

    _allVariablesWidgets.clear();
    _variableCheckboxes.clear();
    _pageCheckboxes.clear();
    _variablesCharts.clear();
    _variables.clear();
    ui->tabs->clear();
    qDeleteAll(_pages);
    _pages.clear();

    ui->lblPagesText->hide();
}

void CompareTelemetryWidget::chartDisplayModeChanged(TelemetryChartView::DisplayMode mode,
                                                     bool value,
                                                     TelemetryChartView *chartView)
{
    if(mode == TelemetryChartView::DisplayMode::Diff) {
        changeVariableDiff(value, chartView);
    } else if(mode == TelemetryChartView::DisplayMode::Stats) {
        changeStats(value, chartView);
    }
}

void CompareTelemetryWidget::changeVariableDiff(bool value, TelemetryChartView *chartView)
{
    auto prevAxis = qobject_cast<QValueAxis *>(chartView->chart()->axes(Qt::Horizontal)[0]);
    auto prevMin = prevAxis->min();
    auto prevMax = prevAxis->max();
    auto newAxis = qobject_cast<QValueAxis *>(chartView->chart()->axes(Qt::Horizontal)[0]);
    newAxis->setRange(prevMin, prevMax);
    updateData();
    refreshHighlighting();
    for(const auto &page : std::as_const(_pages)) {
        if(page->widgets().contains(chartView))
            page->updateChartMargin(chartView);
    }

    auto index = _variablesCharts.indexOf(chartView);
    for(auto customWidget : _allVariablesWidgets) {
        customWidget->setTelemetryDataDiff(index, value);
    }
    ui->table->setTelemetryDataDiff(index, value);
}

void CompareTelemetryWidget::changeStats(bool value, TelemetryChartView *chartView)
{
    Q_UNUSED(value)
    updateData();

    for(const auto &page : std::as_const(_pages)) {
        if(page->widgets().contains(chartView))
            page->updateChartMargin(chartView);
    }
}

void CompareTelemetryWidget::clearData()
{
    for(auto customWidget : std::as_const(_allVariablesWidgets)) {
        customWidget->clear();
    }

    _telemetryDataModel->clear();
    ui->infoTreeWidget->clear();
    ui->trackWidget->hide();
    ui->cursorWidget->hide();

    ui->table->clear();
}

void CompareTelemetryWidget::setColors(const QList<QColor> &colors)
{
    _telemetryDataModel->setColors(colors);
    for(auto customWidget : std::as_const(_allVariablesWidgets)) {
        customWidget->setColors(colors);
    }

    ui->table->setColors(colors);
}

void CompareTelemetryWidget::updateData()
{
    setTelemetry(_telemetryDataModel->getTelemetryData());
    setTelemetryVisibility(_telemetryDataModel->getVisibility());
}

QTreeWidgetItem *CompareTelemetryWidget::speedItem(QTreeWidget *tree, const Lap *lap) const
{
    auto maxSpeedItem = new QTreeWidgetItem(tree, {"Max Speed", QString::number(lap->maxSpeed) + "km/h"});
    auto ersMode = UdpDataCatalog::ersMode(lap->maxSpeedErsMode);
    new QTreeWidgetItem(maxSpeedItem, {"ERS Mode", ersMode});
    auto fuelMix = UdpDataCatalog::fuelMix(lap->maxSpeedFuelMix);
    new QTreeWidgetItem(maxSpeedItem, {"Fuel Mix", fuelMix});

    return maxSpeedItem;
}

void CompareTelemetryWidget::updateDataVisibilities() { setTelemetryVisibility(_telemetryDataModel->getVisibility()); }

void CompareTelemetryWidget::variableChecked(bool value)
{
    auto checkbox = qobject_cast<QCheckBox *>(sender());
    auto varIndex = _variableCheckboxes.indexOf(checkbox);
    auto widget = _allVariablesWidgets.value(varIndex, nullptr);
    if(widget) {
        widget->widget()->setVisible(value);
        if(value) {
            addToPage(widget);
        } else {
            removeFromPage(widget);
        }
    }

    alignCharts();

    if(varIndex < _variablesCharts.count())
        ui->table->setDisplayIndex(varIndex, value);

    QTimer::singleShot(0, this, [this]() { updateCursors(); });
    updateCursorWidget();
}

void CompareTelemetryWidget::pageChecked(bool value)
{
    auto checkbox = qobject_cast<QCheckBox *>(sender());
    auto pgIndex = _pageCheckboxes.indexOf(checkbox);
    auto pageSettings = _pageSettings.value(pgIndex);
    if(pageSettings) {
        if(value) {
            for(auto var : pageSettings->variables) {
                auto it = std::find_if(_variableCheckboxes.begin(), _variableCheckboxes.end(),
                                       [&var](auto checkbox) { return checkbox->text() == var; });
                if(it != _variableCheckboxes.end()) {
                    (*it)->setChecked(value);
                }
            }
            auto page = pageFromName(pageSettings->name);
            if(!page) {
                checkbox->blockSignals(true);
                checkbox->setChecked(false);
                checkbox->blockSignals(false);
            }
        } else {
            auto page = pageFromName(pageSettings->name);
            for(auto widget : page->widgets()) {
                auto varIndex = _allVariablesWidgets.indexOf(widget);
                auto checkBox = _variableCheckboxes.value(varIndex);
                checkBox->setChecked(false);
            }
        }
    }
}

void CompareTelemetryWidget::uncheckAllVariables()
{
    for(auto variableCheckbox : std::as_const(_variableCheckboxes)) {
        variableCheckbox->setChecked(false);
    }
}

void CompareTelemetryWidget::home()
{
    for(auto customWidget : _allVariablesWidgets) {
        customWidget->home();
    }

    ui->table->home();
}

void CompareTelemetryWidget::help()
{
    auto css = "h3 {margin-top: 30px;}";
    F1Telemetry::displayMarkdownFile(":/help/graphs", css, "Help", this);
}

void CompareTelemetryWidget::setCursor(double value)
{
    for(auto customWidget : _allVariablesWidgets) {
        customWidget->setCursor(value);
    }

    ui->table->setCursor(value);
}

void CompareTelemetryWidget::setCursorColor(const QColor &color)
{
    for(auto customWidget : _allVariablesWidgets) {
        customWidget->setCursorColor(color);
    }

    ui->table->setCursorColor(color);
}

void CompareTelemetryWidget::setCursorVisible(bool value)
{
    for(auto customWidget : _allVariablesWidgets) {
        customWidget->setCursorVisible(value);
    }

    ui->table->setCursorVisible(value);
}

void CompareTelemetryWidget::distanceZoomChanged(qreal min, qreal max)
{
    auto senderChart = qobject_cast<QChart *>(sender());

    TelemetryChartView *senderView = nullptr;
    for(auto chartView : _variablesCharts) {
        if(senderChart != chartView->chart()) {
            chartView->chart()->axes(Qt::Horizontal)[0]->setRange(min, max);
        } else {
            senderView = chartView;
        }
    }

    auto yaxis = static_cast<QValueAxis *>(senderChart->axes(Qt::Vertical)[0]);
    auto senderHomeRect = senderView->homeView();
    auto isxHome =
        qFuzzyCompare(yaxis->min(), senderHomeRect.left()) && qFuzzyCompare(yaxis->max(), senderHomeRect.right());

    QTimer::singleShot(0, this, [this, isxHome]() { updateCursors(isxHome); });
}

void CompareTelemetryWidget::telemetryDataSelected(const QModelIndex &current, const QModelIndex &previous)
{
    Q_UNUSED(previous)
    const auto &data = _telemetryDataModel->getTelemetryData().value(current.row());
    fillInfoTree(ui->infoTreeWidget, data);

    highlight(current.row());
}

void CompareTelemetryWidget::telemetryTableContextMenu(const QPoint &pos)
{
    auto currentIndex = ui->lapsTableView->currentIndex();
    if(currentIndex.isValid()) {
        _telemetryContextMenu->exec(ui->lapsTableView->mapToGlobal(pos));
    }
}

void CompareTelemetryWidget::changeReferenceData()
{
    auto currentIndex = ui->lapsTableView->currentIndex();
    if(currentIndex.isValid()) {
        _telemetryDataModel->setReferenceLapIndex(currentIndex.row());

        for(auto customWidget : _allVariablesWidgets) {
            customWidget->setReference(_telemetryDataModel->getReferenceData());
        }
        ui->table->setReference(_telemetryDataModel->getReferenceData());

        updateData();
        highlight(currentIndex.row());
    }
}

void CompareTelemetryWidget::changeColor()
{
    auto currentIndex = ui->lapsTableView->currentIndex();
    if(currentIndex.isValid()) {
        auto colors = _telemetryDataModel->colors();
        auto color = colors.value(currentIndex.row());
        color = QColorDialog::getColor(color, this, "Select the new color");
        if(color.isValid()) {
            colors[currentIndex.row()] = color;
            setColors(colors);
            updateData();
            highlight(currentIndex.row());
        }
    }
}

void CompareTelemetryWidget::exportData()
{
    auto currentIndex = ui->lapsTableView->currentIndex();
    if(currentIndex.isValid()) {
        auto name = _telemetryDataModel->data(currentIndex).toString().replace(" ", "_").replace(":", ".") + ".json";
        auto filename = QFileDialog::getSaveFileName(this, "Export to JSON", name, "*.json");
        if(!filename.isEmpty()) {
            _telemetryDataModel->exportData(currentIndex.row(), filename);
        }
    }
}


void CompareTelemetryWidget::exportAllData()
{
    auto directory = QFileDialog::getExistingDirectory(this, "Export all data to JSON");
    if(!directory.isEmpty()) {
        QDir dir(directory);
        for(int i = 0; i < _telemetryDataModel->rowCount(); ++i) {
            auto index = _telemetryDataModel->index(i, 0);
            auto name = _telemetryDataModel->data(index).toString().replace(" ", "_").replace(":", ".") + ".json";
            _telemetryDataModel->exportData(i, dir.absoluteFilePath(name));
        }
    }
}

void CompareTelemetryWidget::updateCursors(bool allowMovingView)
{
    auto value = ui->cursor->value();
    setCursor(value);

    if(ui->cursorWidget->isVisible()) {
        _cursorVisibleCheckbox->setText(QString("Cursor: ").append(QString::number(value)).append(_unitX));
        if(allowMovingView)
            moveViewsToCursor();
        else
            moveCursorToView();
    } else {
        _cursorVisibleCheckbox->setText("Cursor");
    }
}

void CompareTelemetryWidget::updateCursorWidget()
{
    if(!_variablesCharts.isEmpty()) {
        auto view = _variablesCharts.value(0);
        auto hasCheckedVariables = std::any_of(_variableCheckboxes.cbegin(), _variableCheckboxes.cend(),
                                               [](auto checkbox) { return checkbox->isChecked(); });
        if(hasCheckedVariables) {
            auto firstChart = view->chart();
            auto haxe = static_cast<QValueAxis *>(firstChart->axes(Qt::Horizontal).value(0));
            auto min = firstChart->mapToPosition(QPointF(haxe->min(), 0));
            auto max = firstChart->mapToPosition(QPointF(haxe->max(), 0));
            auto viewMin = view->mapFromScene(min);
            auto viewMax = view->mapFromScene(max);

            auto l = viewMin.x();
            auto r = view->width() - viewMax.x();
            auto pm = CURSOR_SLIDER_HANDLE_WIDTH;

            ui->cursorWidget->layout()->setContentsMargins(l - pm + ui->tabs->tabBar()->width(), 0, r - pm, 0);
        } else {
            ui->cursorWidget->layout()->setContentsMargins(ui->tabs->tabBar()->width(), 0, 0, 0);
        }

        auto homeRect = view->homeView();
        ui->cursor->setMinimum(homeRect.left());
        ui->cursor->setMaximum(homeRect.left() + homeRect.width());
    }

    updateWidgetsVisibilities();
}

void CompareTelemetryWidget::updateWidgetsVisibilities()
{
    auto visible = _cursorVisibleCheckbox->isChecked() && !_pages.isEmpty();
    ui->cursorWidget->setVisible(visible);
    setCursorVisible(visible);
    updateCursors(false);
}

void CompareTelemetryWidget::removeData()
{
    auto currentIndex = ui->lapsTableView->currentIndex();
    if(currentIndex.isValid()) {
        _telemetryDataModel->removeTelemetryData(currentIndex.row());
        for(auto customWidget : _allVariablesWidgets) {
            customWidget->removeTelemetryData(currentIndex.row());
        }
        ui->table->removeTelemetryData(currentIndex.row());

        bool isEmpty = _variablesCharts.isEmpty() || _variablesCharts.first()->chart()->series().isEmpty();
        if(isEmpty) {
            clearData();
        }
    }
}

void CompareTelemetryWidget::showTrackLayout(bool value)
{
    ui->lblTrackMap->setVisible(value);
    if(value) {
        setTrackIndex(_trackIndex);
    }
}

void CompareTelemetryWidget::showTelemetryInfo(bool value)
{
    ui->infoTreeWidget->setVisible(value);
    ui->lapsTableView->setMaximumHeight(value ? 120 : QWIDGETSIZE_MAX);
}

QTreeWidgetItem *CompareTelemetryWidget::setupItem(QTreeWidget *tree, const Lap *lap) const
{
    auto setupItem = new QTreeWidgetItem(tree, {"Setup", ""});
    new QTreeWidgetItem(setupItem, {"Front Wing", QString::number(lap->setup.m_frontWing)});
    new QTreeWidgetItem(setupItem, {"Rear Wing", QString::number(lap->setup.m_rearWing)});
    new QTreeWidgetItem(setupItem, {"On Throttle", QString::number(lap->setup.m_onThrottle) + "%"});
    new QTreeWidgetItem(setupItem, {"Off Throttle", QString::number(lap->setup.m_offThrottle) + "%"});
    new QTreeWidgetItem(setupItem, {"Front Camber", QString::number(lap->setup.m_frontCamber)});
    new QTreeWidgetItem(setupItem, {"Rear Camber", QString::number(lap->setup.m_rearCamber)});
    new QTreeWidgetItem(setupItem, {"Front Toe", QString::number(lap->setup.m_frontToe)});
    new QTreeWidgetItem(setupItem, {"Rear Toe", QString::number(lap->setup.m_rearToe)});
    new QTreeWidgetItem(setupItem, {"Front Suspension", QString::number(lap->setup.m_frontSuspension)});
    new QTreeWidgetItem(setupItem, {"Rear Suspension", QString::number(lap->setup.m_rearSuspension)});
    new QTreeWidgetItem(setupItem, {"Front ARB", QString::number(lap->setup.m_frontAntiRollBar)});
    new QTreeWidgetItem(setupItem, {"Rear ARB", QString::number(lap->setup.m_rearAntiRollBar)});
    new QTreeWidgetItem(setupItem, {"Front Height", QString::number(lap->setup.m_frontSuspensionHeight)});
    new QTreeWidgetItem(setupItem, {"Rear height", QString::number(lap->setup.m_rearSuspensionHeight)});
    new QTreeWidgetItem(setupItem, {"Brake Pressure", QString::number(lap->setup.m_brakePressure) + "%"});
    new QTreeWidgetItem(setupItem, {"Brake Bias", QString::number(lap->setup.m_brakeBias) + "%"});
    new QTreeWidgetItem(setupItem, {"Engine Braking", QString::number(lap->setup.m_engineBraking) + "%"});
    new QTreeWidgetItem(setupItem, {"Front Right Tyre Pressure", QString::number(lap->setup.m_frontRightTyrePressure)});
    new QTreeWidgetItem(setupItem, {"Front Left Tyre Pressure", QString::number(lap->setup.m_frontLeftTyrePressure)});
    new QTreeWidgetItem(setupItem, {"Rear Right Tyre Pressure", QString::number(lap->setup.m_rearRightTyrePressure)});
    new QTreeWidgetItem(setupItem, {"Rear Left Tyre Pressure", QString::number(lap->setup.m_rearLeftTyrePressure)});
    new QTreeWidgetItem(setupItem, {"Ballast", QString::number(lap->setup.m_ballast)});
    new QTreeWidgetItem(setupItem, {"Fuel Load", QString::number(lap->setup.m_fuelLoad) + "kg"});
    return setupItem;
}

QTreeWidgetItem *CompareTelemetryWidget::tyreTempItem(QTreeWidget *tree, const Lap *lap) const
{
    auto averageTemp = (lap->innerTemperatures.frontLeft.mean + lap->innerTemperatures.frontRight.mean +
                        lap->innerTemperatures.rearLeft.mean + lap->innerTemperatures.rearRight.mean) /
                       4.0;
    auto averageDev = (lap->innerTemperatures.frontLeft.deviation + lap->innerTemperatures.frontRight.deviation +
                       lap->innerTemperatures.rearLeft.deviation + lap->innerTemperatures.rearRight.deviation) /
                      4.0;
    auto tempItem = new QTreeWidgetItem(tree, {"Tyre Temperature", QString::number(int(averageTemp)) + "°C (+/- " +
                                                                       QString::number(int(averageDev)) + "°C)"});
    new QTreeWidgetItem(tempItem,
                        {"Front Right", QString::number(int(lap->innerTemperatures.frontRight.mean)) + "°C (+/- " +
                                            QString::number(int(lap->innerTemperatures.frontRight.deviation)) + "°C)"});
    new QTreeWidgetItem(tempItem,
                        {"Front Left", QString::number(int(lap->innerTemperatures.frontLeft.mean)) + "°C (+/- " +
                                           QString::number(int(lap->innerTemperatures.frontLeft.deviation)) + "°C)"});
    new QTreeWidgetItem(tempItem,
                        {"Rear Right", QString::number(int(lap->innerTemperatures.rearRight.mean)) + "°C (+/- " +
                                           QString::number(int(lap->innerTemperatures.rearRight.deviation)) + "°C)"});
    new QTreeWidgetItem(tempItem,
                        {"Rear Left", QString::number(int(lap->innerTemperatures.rearLeft.mean)) + "°C (+/- " +
                                          QString::number(int(lap->innerTemperatures.rearLeft.deviation)) + "°C)"});
    return tempItem;
}

QTreeWidgetItem *CompareTelemetryWidget::tyreItem(QTreeWidget *tree, const Lap *lap, double divisor) const
{
    auto lapWear = (lap->averageEndTyreWear - lap->averageStartTyreWear) / divisor;
    auto tyreWearItem = new QTreeWidgetItem(
        tree, {"Tyre Wear",
               QString("%1% (%2% -> %3%)").arg(lapWear).arg(lap->averageStartTyreWear).arg(lap->averageEndTyreWear)});
    auto frontRightWear = (lap->endTyreWear.frontRight - lap->startTyreWear.frontRight) / divisor;
    new QTreeWidgetItem(tyreWearItem, {"Front Right", QString("%1% (%2% -> %3%)")
                                                          .arg(frontRightWear)
                                                          .arg(lap->startTyreWear.frontRight)
                                                          .arg(lap->endTyreWear.frontRight)});
    auto frontLeftWear = (lap->endTyreWear.frontLeft - lap->startTyreWear.frontLeft) / divisor;
    new QTreeWidgetItem(tyreWearItem, {"Front Left", QString("%1% (%2% -> %3%)")
                                                         .arg(frontLeftWear)
                                                         .arg(lap->startTyreWear.frontLeft)
                                                         .arg(lap->endTyreWear.frontLeft)});
    auto rearRightWear = (lap->endTyreWear.rearRight - lap->startTyreWear.rearRight) / divisor;
    new QTreeWidgetItem(tyreWearItem, {"Rear Right", QString("%1% (%2% -> %3%)")
                                                         .arg(rearRightWear)
                                                         .arg(lap->startTyreWear.rearRight)
                                                         .arg(lap->endTyreWear.rearRight)});
    auto rearLeftWear = (lap->endTyreWear.rearLeft - lap->startTyreWear.rearLeft) / divisor;
    new QTreeWidgetItem(tyreWearItem, {"Rear Left", QString("%1% (%2% -> %3%)")
                                                        .arg(rearLeftWear)
                                                        .arg(lap->startTyreWear.rearLeft)
                                                        .arg(lap->endTyreWear.rearLeft)});
    return tyreWearItem;
}

QTreeWidgetItem *CompareTelemetryWidget::tyreSetItem(QTreeWidget *tree, const Lap *lap) const
{
    auto tyreSet = new QTreeWidgetItem(
        tree, {"Tyre Set time loss", QString("%1s (%2s -> %3s)")
                                         .arg(lap->tyreTimeLossEnd - lap->tyreTimeLossStart, 0, 'f', 3)
                                         .arg(lap->tyreTimeLossStart, 0, 'f', 3)
                                         .arg(lap->tyreTimeLossEnd, 0, 'f', 3)});
    new QTreeWidgetItem(tyreSet, {"Remaining life", QString("%1 lap(s)").arg(lap->tyreRemaingLifeEnd)});
    new QTreeWidgetItem(tyreSet, {"Calculated lost traction", QString("%1%").arg(lap->calculatedTotalLostTraction)});

    return tyreSet;

}

QTreeWidgetItem *CompareTelemetryWidget::recordItem(QTreeWidget *tree, const Lap *lap) const
{
    auto recordItem = new QTreeWidgetItem(tree, {"Record Date", lap->recordDate.toString("dd/MM/yyyy hh:mm:ss")});
    new QTreeWidgetItem(recordItem, {"Version", lap->recordVersion});
    new QTreeWidgetItem(recordItem, {"Game Version", lap->recordGameVersion});
    return recordItem;
}

QTreeWidgetItem *CompareTelemetryWidget::flashbackItem(QTreeWidget *tree, const Lap *lap) const
{
    auto flashbackItem = new QTreeWidgetItem(tree, {"Flashbacks", ""});
    int total = 0;
    for(const auto type : lap->flashbackManager.counts.keys()) {
        auto name = FlashbackManager::typeNames.value(static_cast<int>(type));
        auto count = lap->flashbackManager.counts[type];
        total += count;
        new QTreeWidgetItem(flashbackItem, {name, QString::number(count)});
    }

    flashbackItem->setText(1, QString::number(total));

    return flashbackItem;
}

QTreeWidgetItem *CompareTelemetryWidget::driverItem(QTreeWidget *tree, const Lap *lap) const
{
    auto team = lap->driver.team();
    auto teamItem = new QTreeWidgetItem(tree, {"Driver", lap->driver.m_name + QString(" (%1)").arg(team)});
    if(lap->driver.m_aiControlled && lap->aiDifficulty > 0) {
        new QTreeWidgetItem(teamItem, {"A.I.", QString::number(lap->aiDifficulty)});
    }

    return teamItem;
}

QTreeWidgetItem *CompareTelemetryWidget::trackItem(QTreeWidget *tree, const Lap *lap) const
{
    auto track = UdpDataCatalog::trackInfo(lap->track)->name;
    auto sessionType = UdpDataCatalog::session(lap->session_type);
    return new QTreeWidgetItem(tree, {"Track", track + QString(" (%1)").arg(sessionType)});
}

QTreeWidgetItem *CompareTelemetryWidget::weatherItem(QTreeWidget *tree, const Lap *lap) const
{
    auto weather = UdpDataCatalog::weather(lap->weather);
    auto weatherItem = new QTreeWidgetItem(tree, {"Weather", weather});
    new QTreeWidgetItem(weatherItem, {"Air Temp.", QString::number(lap->airTemp) + "°C"});
    new QTreeWidgetItem(weatherItem, {"Track Temp.", QString::number(lap->trackTemp) + "°C"});
    return weatherItem;
}

QTreeWidgetItem *CompareTelemetryWidget::tyreCompoundItem(QTreeWidget *tree, const Lap *lap) const
{
    auto compound = UdpDataCatalog::tyre(lap->tyreCompound);
    auto visualCompound = UdpDataCatalog::visualTyre(lap->visualTyreCompound);
    if(compound != visualCompound && !visualCompound.isEmpty()) {
        compound += " - " + visualCompound;
    }
    return new QTreeWidgetItem(tree, {"Tyre Compound", compound});
}

QTreeWidgetItem *CompareTelemetryWidget::carDamageDiffItem(QTreeWidget *tree, const Lap *lap) const
{
    auto diff = lap->endCarDamage.averageCarDamage() - lap->startCarDamage.averageCarDamage();
    auto wear = QString("%1%").arg(diff);
    auto item = new QTreeWidgetItem(tree, {"Car Damage", wear});
    new QTreeWidgetItem(item, {"Start", QString("%1%").arg(lap->startCarDamage.averageCarDamage())});
    new QTreeWidgetItem(item, {"End", QString("%1%").arg(lap->endCarDamage.averageCarDamage())});

    return item;
}

QTreeWidgetItem *CompareTelemetryWidget::engineDamageDiffItem(QTreeWidget *tree, const Lap *lap) const
{
    if(!lap->driver.m_aiControlled) {

        auto diff = lap->endCarDamage.averageEngineDamage() - lap->startCarDamage.averageEngineDamage();
        auto wear = QString("%1%").arg(diff);
        auto item = new QTreeWidgetItem(tree, {"Engine Damage", wear});
        new QTreeWidgetItem(item, {"Start", QString("%1%").arg(lap->startCarDamage.averageEngineDamage())});
        new QTreeWidgetItem(item, {"End", QString("%1%").arg(lap->endCarDamage.averageEngineDamage())});

        return item;
    }

    return nullptr;
}

QTreeWidgetItem *CompareTelemetryWidget::eventsItem(QTreeWidget *tree, const Lap *lap) const
{
    int nbEvents = 0;
    auto item = new QTreeWidgetItem(tree, {"Events", ""});
    for(auto it = lap->telemetryEvents().constBegin(); it != lap->telemetryEvents().constEnd(); ++it) {
        auto varIndex = it.key();
        for(const auto &event : std::as_const(it.value())) {
            QString text;
            if(varIndex >= 0) {
                text = QString::number(event.yValue(varIndex, lap)) + lap->availableData().value(varIndex).unit;
                text += " - ";
            }
            text += event.description;
            new QTreeWidgetItem(item, {QString::number(event.x) + _unitX, text});

            ++nbEvents;
        }
    }

    item->setText(1, QString("%1 event(s)").arg(nbEvents));
    return item;
}

QTreeWidgetItem *CompareTelemetryWidget::coastingItem(QTreeWidget *tree, const Lap *lap) const
{
    auto distance = lap->coasting.distancesPerMode.value(1, 0.0);
    return new QTreeWidgetItem(tree, {"Coasting", QString::number(distance, 'f', 2) + "m"});
}

QTreeWidgetItem *CompareTelemetryWidget::carDamageItem(QTreeWidget *tree, const Lap *lap) const
{
    auto carDamageItem =
        new QTreeWidgetItem(tree, {"Car Damage", QString::number(lap->endCarDamage.averageCarDamage()) + "%"});
    new QTreeWidgetItem(
        carDamageItem,
        {"Front Wing",
         QString::number((lap->endCarDamage.m_frontLeftWingDamage + lap->endCarDamage.m_frontRightWingDamage) / 2.0) +
             "%"});
    new QTreeWidgetItem(carDamageItem, {"Rear Wing", QString::number(lap->endCarDamage.m_rearWingDamage) + "%"});
    new QTreeWidgetItem(carDamageItem, {"Floor", QString::number(lap->endCarDamage.m_floorDamage) + "%"});
    new QTreeWidgetItem(carDamageItem, {"Diffuser", QString::number(lap->endCarDamage.m_diffuserDamage) + "%"});
    new QTreeWidgetItem(carDamageItem, {"Sidepod", QString::number(lap->endCarDamage.m_sidepodDamage) + "%"});
    return carDamageItem;
}

QTreeWidgetItem *CompareTelemetryWidget::engineDamageItem(QTreeWidget *tree, const Lap *lap) const
{
    if(!lap->driver.m_aiControlled) {
        auto engineDamageItem = new QTreeWidgetItem(
            tree, {"Engine Damage", QString::number(lap->endCarDamage.averageEngineDamage()) + "%"});
        new QTreeWidgetItem(engineDamageItem, {"Gearbox", QString::number(lap->endCarDamage.m_gearBoxDamage) + "%"});
        new QTreeWidgetItem(engineDamageItem, {"MGU-H", QString::number(lap->endCarDamage.m_engineMGUHWear) + "%"});
        new QTreeWidgetItem(engineDamageItem, {"MGU-K", QString::number(lap->endCarDamage.m_engineMGUKWear) + "%"});
        new QTreeWidgetItem(engineDamageItem, {"ES", QString::number(lap->endCarDamage.m_engineESWear) + "%"});
        new QTreeWidgetItem(engineDamageItem, {"CE", QString::number(lap->endCarDamage.m_engineCEWear) + "%"});
        new QTreeWidgetItem(engineDamageItem, {"ICE", QString::number(lap->endCarDamage.m_engineICEWear) + "%"});
        new QTreeWidgetItem(engineDamageItem, {"TC", QString::number(lap->endCarDamage.m_engineTCWear) + "%"});
        return engineDamageItem;
    }

    return nullptr;
}

int CompareTelemetryWidget::nbDigit(int num) const { return int(floor(log10(num))) + 1; }

int CompareTelemetryWidget::ceilToDigit(int num, int roundFactor) const
{
    auto doubleNum = num * roundFactor;
    auto q = pow(10, nbDigit(num) - 1);
    return int(ceil(doubleNum / q) * q) / roundFactor;
}

int CompareTelemetryWidget::floorToDigit(int num, int roundFactor) const
{
    if(num == 0)
        return 0;

    auto doubleNum = num * roundFactor;
    auto q = pow(10, nbDigit(num) - 1);
    return int(floor(doubleNum / q) * q) / roundFactor;
}

bool CompareTelemetryWidget::eventFilter(QObject *obj, QEvent *event)
{
    if(event->type() == QEvent::MouseMove) {
        for(const auto &chart : std::as_const(_variablesCharts)) {
            chart->setPosLabelVisible(chart->viewport() == obj);
        }
    }

    return QWidget::eventFilter(obj, event);
}

QString CompareTelemetryWidget::cursorSliderStylesheet()
{
    return QString("QSlider::groove:horizontal {"
                   "border: 1px solid #999999;"
                   "height: 2px;"
                   "background: qlineargradient(x1:0, y1:0, x2:0, y2:1, stop:0 #B1B1B1, stop:1 #c4c4c4);"
                   "margin: 4px 0;"
                   "}"

                   "QSlider::handle:horizontal {"
                   "background: qlineargradient(x1:0, y1:0, x2:1, y2:1, stop:0 #b4b4b4, stop:1 #8f8f8f);"
                   "border: 1px solid #5c5c5c;"
                   "width: %1px;"
                   "margin:-%2px -1px;"
                   "border-radius: %3px;"
                   "}")
        .arg(CURSOR_SLIDER_HANDLE_WIDTH * 2)
        .arg(CURSOR_SLIDER_HANDLE_WIDTH)
        .arg(CURSOR_SLIDER_HANDLE_WIDTH + 1);
}
