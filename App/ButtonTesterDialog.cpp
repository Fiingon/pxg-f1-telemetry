#include "ButtonTesterDialog.h"
#include "F1TelemetrySettings.h"
#include "ui_ButtonTesterDialog.h"

#include "Core/UdpSpecification.h"
#include <Core/UdpDataCatalog.h>


const QVector<Button> ACCEPTED_BUTTONS = {
    Button::CrossA, Button::TriangleY, Button::CircleB, Button::SquareX,        Button::L1,
    Button::R1,     Button::L2,        Button::R2,      Button::LeftStickClick, Button::RightStickCLick,
    Button::Left,   Button::Right,     Button::Up,      Button::Down,           Button::OptionMenu};


ButtonTesterDialog::ButtonTesterDialog(QWidget *parent) : QDialog(parent), ui(new Ui::ButtonTesterDialog)
{
    ui->setupUi(this);

    F1TelemetrySettings settings;
    auto controllerType = settings.controllerType();
    if(!controllerType.isEmpty()) {
        ui->cbControllerType->setCurrentText(controllerType);
    }

    setActive(false);
    ui->lblButtons->clear();
}

ButtonTesterDialog::~ButtonTesterDialog() { delete ui; }


void ButtonTesterDialog::telemetryData(const PacketHeader &header, const PacketCarTelemetryData &data)
{
    Q_UNUSED(header)
    Q_UNUSED(data)

    setActive(true);
}

void ButtonTesterDialog::lapData(const PacketHeader &header, const PacketLapData &data)
{
    Q_UNUSED(header)
    Q_UNUSED(data)
}

void ButtonTesterDialog::sessionData(const PacketHeader &header, const PacketSessionData &data)
{
    Q_UNUSED(header)
    Q_UNUSED(data)
}

void ButtonTesterDialog::setupData(const PacketHeader &header, const PacketCarSetupData &data)
{
    Q_UNUSED(header)
    Q_UNUSED(data)
}

void ButtonTesterDialog::statusData(const PacketHeader &header, const PacketCarStatusData &data)
{
    Q_UNUSED(header)
    Q_UNUSED(data)
}

void ButtonTesterDialog::participant(const PacketHeader &header, const PacketParticipantsData &data)
{
    Q_UNUSED(header)
    Q_UNUSED(data)
}

void ButtonTesterDialog::motionData(const PacketHeader &header, const PacketMotionData &data)
{
    Q_UNUSED(header)
    Q_UNUSED(data)
}

void ButtonTesterDialog::motionExData(const PacketHeader &header, const PacketMotionExData &data)
{
    Q_UNUSED(header)
    Q_UNUSED(data)
}

void ButtonTesterDialog::eventData(const PacketHeader &header, const PacketEventData &event)
{
    Q_UNUSED(header)

    if(event.event == Event::SessionEnded) {
        setActive(false);
    } else if(event.event == Event::ButtonStatus) {
        QStringList pressed;

        auto status = event.details.Buttons.m_buttonStatus;
        auto isPs4 = ui->cbControllerType->currentText() == "PS4";

        for(const auto &button : ACCEPTED_BUTTONS) {
            if((status & button) == button) {
                if(isPs4)
                    pressed << UdpDataCatalog::buttonNamePs4(button);
                else
                    pressed << UdpDataCatalog::buttonNameXbox(button);
            }
        }

        ui->lblButtons->setText(pressed.join(", "));
    }
}

void ButtonTesterDialog::finalClassificationData(const PacketHeader &header, const PacketFinalClassificationData &data)
{
    Q_UNUSED(header)
    Q_UNUSED(data)

    setActive(false);
}

void ButtonTesterDialog::carDamageData(const PacketHeader &header, const PacketCarDamageData &data)
{
    Q_UNUSED(header)
    Q_UNUSED(data)
}

void ButtonTesterDialog::sessionHistoryData(const PacketHeader &header, const PacketSessionHistoryData &data)
{
    Q_UNUSED(header)
    Q_UNUSED(data)
}

void ButtonTesterDialog::tyreSetData(const PacketHeader &header, const PacketTyreSetsData &data)
{
    Q_UNUSED(header)
    Q_UNUSED(data)
}

void ButtonTesterDialog::setActive(bool value)
{
    if(value) {
        ui->lblSession->setText("Press some buttons...");
    } else {
        ui->lblSession->setText("Waiting for a session...");
    }
}
