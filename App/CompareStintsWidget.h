#ifndef COMPARESTINTWIDGET_H
#define COMPARESTINTWIDGET_H

#include "CompareTelemetryWidget.h"

#include <QTreeWidget>

class Stint;

class CompareStintsWidget : public CompareTelemetryWidget
{
	Q_OBJECT

  signals:
	void askOpenLaps(const QStringList &filename) const;

  public:
	CompareStintsWidget(QWidget *parent = nullptr);
	virtual ~CompareStintsWidget() override {}

  public slots:
	void browseData() override;
	void openDataFiles(const QStringList &filenames) override;

  public:
	void fillInfoTree(InfoTreeWidget *tree, const TelemetryData *data) override;

  private:
	void raceLoadItem(QTreeWidgetItem *parent, int nbLaps, int nbRaceLaps, double fuel, int percentage);
};

#endif // COMPARESTINTWIDGET_H
