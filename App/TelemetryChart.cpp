#include "TelemetryChart.h"
#include <QGraphicsScene>
#include <QValueAxis>
#include <QtDebug>

TelemetryChart::TelemetryChart()
{
	_cursor = new QGraphicsLineItem(0, 0, 0, 0, this);
	setCursorColor(Qt::red);
	_cursor->setZValue(999999);
}

void TelemetryChart::addTelemetrySeries(const QVector<QAbstractSeries *> &series)
{
	_series.append(series);
	for(const auto &serie : series) {
		addSeries(serie);
	}
}

void TelemetryChart::setTelemetrySeriesVisible(int index, bool value)
{
	for(const auto &serie : _series.value(index)) {
		serie->setVisible(value);
	}
}

void TelemetryChart::removeTelemetrySeries(int index)
{
	for(const auto &serie : _series.value(index)) {
		removeSeries(serie);
	}

	_series.removeAt(index);
}

void TelemetryChart::clearTelemetrySeries()
{
	_series.clear();
	removeAllSeries();
}

void TelemetryChart::setCursor(double value)
{
	if(!_cursorVisible) {
		_cursor->setLine(QLineF(0, 0, 0, 0));
		return;
	}

	_cursorValue = value;

	auto vaxe = static_cast<QValueAxis *>(axes(Qt::Vertical).value(0));
	auto haxe = static_cast<QValueAxis *>(axes(Qt::Horizontal).value(0));
	auto minScenePos = mapToPosition(QPointF(value, vaxe->min()));
	auto maxScenePos = mapToPosition(QPointF(value, vaxe->max()));

	auto minSceneX = mapToPosition(QPointF(haxe->min(), 0));
	auto maxSceneX = mapToPosition(QPointF(haxe->max(), 0));

	_cursor->setLine(QLineF(minScenePos, maxScenePos));
	_cursor->setVisible(minScenePos.x() >= minSceneX.x() && minScenePos.x() <= maxSceneX.x());
}

void TelemetryChart::setCursorColor(const QColor &color) { _cursor->setPen(QPen(color)); }

void TelemetryChart::setCursorVisible(bool value)
{
	_cursorVisible = value;
	setCursor(_cursorValue);
}

void TelemetryChart::connectAxis()
{
	connect(axes(Qt::Horizontal)[0], SIGNAL(rangeChanged(qreal, qreal)), this, SLOT(distanceZoomChanged(qreal, qreal)));
}
