#include "CompareTelemetryPage.h"
#include <F1TelemetrySettings.h>
#include <TelemetryChartView.h>
#include <Tools.h>

#include <QActionGroup>
#include <QCategoryAxis>
#include <QCheckBox>
#include <QValueAxis>

CompareTelemetryPage::CompareTelemetryPage(const QString &name, const QString &dataName, QWidget *parent)
: QWidget(parent), _dataName(dataName)
{
    setName(name);

    _layout = new QVBoxLayout(this);
    _layout->setContentsMargins(0, 0, 0, 0);

    _toolbar = new QToolBar(this);
    _layout->addWidget(_toolbar);

#ifdef Q_OS_MACOS
    _toolbar->setIconSize(QSize(16, 16));
#endif

    auto color = palette().color(QPalette::Active, QPalette::Text);

    auto zoomIcon = Tools::loadIconWithColor(":/icons/zoom", color);
    _zoomAction = _toolbar->addAction(zoomIcon, "Zoom", this, &CompareTelemetryPage::setZoomRect);
    _zoomAction->setCheckable(true);
    _zoomAction->setChecked(true);

    auto zoomHIcon = Tools::loadIconWithColor(":/icons/zoom_h", color);
    _zoomHAction = _toolbar->addAction(zoomHIcon, "Zoom Horizontal", this, &CompareTelemetryPage::setZoomHorizontal);
    _zoomHAction->setCheckable(true);

    auto zoomVIcon = Tools::loadIconWithColor(":/icons/zoom_v", color);
    _zoomVAction = _toolbar->addAction(zoomVIcon, "Zoom Vertical", this, &CompareTelemetryPage::setZoomVertical);
    _zoomVAction->setCheckable(true);

    auto zoomModeGroup = new QActionGroup(this);
    zoomModeGroup->addAction(_zoomAction);
    zoomModeGroup->addAction(_zoomHAction);
    zoomModeGroup->addAction(_zoomVAction);
    zoomModeGroup->setExclusive(true);

    auto expandWidget = new QWidget();
    expandWidget->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Preferred);
    _toolbar->addWidget(expandWidget);

    auto saveIcon = Tools::loadIconWithColor(":/icons/save", color);
    _saveAction = _toolbar->addAction(saveIcon, "Save Page");
    _saveAction->setToolTip("Save this page");
    connect(_saveAction, &QAction::triggered, this, &CompareTelemetryPage::saveAsked);

    auto renameIcon = Tools::loadIconWithColor(":/icons/rename", color);
    _renameAction = _toolbar->addAction(renameIcon, "Rename Page");
    _renameAction->setToolTip("Rename this page");
    connect(_renameAction, &QAction::triggered, this, &CompareTelemetryPage::renameAsked);

    auto deleteIcon = Tools::loadIconWithColor(":/icons/delete", color);
    _deleteAction = _toolbar->addAction(deleteIcon, "Delete Page");
    _deleteAction->setToolTip("Delete this page");
    connect(_deleteAction, &QAction::triggered, this, &CompareTelemetryPage::deleteAsked);

    _toolbar->addSeparator();
    auto detachedIcon = Tools::loadIconWithColor(":/icons/detach", color);
    _detachAction = _toolbar->addAction(detachedIcon, "Detach");
    _detachAction->setToolTip("Detach or reattach this page with the main window");
    _detachAction->setCheckable(true);
    connect(_detachAction, &QAction::toggled, this, &CompareTelemetryPage::detachAsked);

    setPageSettings(nullptr);
}

void CompareTelemetryPage::addWidget(CompareWidgetInterface *widget, const QStringList &varOrder)
{
    if(varOrder.isEmpty() || _widgets.isEmpty()) {
        _widgets << widget;
        _layout->addWidget(widget->widget());
    } else {
        int index = 0;
        auto itChart = _widgets.begin();
        for(const auto &variable : varOrder) {
            if(variable == widget->name() || itChart == _widgets.end()) {
                break;
            } else if(variable == (*itChart)->name()) {
                ++index;
                ++itChart;
            }
        }

        _widgets.insert(index, widget);
        _layout->insertWidget(index + 1, widget->widget()); // +1 for the toolbar
    }

    widget->setZoomMode(_currentZoomMode);

    _layout->setStretchFactor(widget->widget(), widget->strecthFactor());
    _totalStretch += widget->strecthFactor();
}

void CompareTelemetryPage::removeWidget(CompareWidgetInterface *widget)
{
    _layout->removeWidget(widget->widget());
    _widgets.removeOne(widget);
    _totalStretch -= widget->strecthFactor();
}

void CompareTelemetryPage::alignCharts()
{
    QHash<CompareWidgetInterface *, int> sizes;
    _chartMargins = 0;
    for(auto widget : _widgets) {
        auto chartYAxisWidth = widget->leftMargin();
        _chartMargins = qMax(_chartMargins, chartYAxisWidth);
        sizes[widget] = chartYAxisWidth;
    }
    for(auto widget : _widgets) {
        auto margin = _chartMargins - sizes[widget];
        widget->setLeftMargin(margin);
    }
}

void CompareTelemetryPage::updateChartMargin(CompareWidgetInterface *widget) const
{
    widget->setLeftMargin(_chartMargins - widget->leftMargin());
}

const QList<CompareWidgetInterface *> &CompareTelemetryPage::widgets() const { return _widgets; }

void CompareTelemetryPage::moveGraph(CompareWidgetInterface *widget, int nb)
{
    if(!_widgets.contains(widget))
        return;

    int currentIndex = _widgets.indexOf(widget);
    auto stretchFactor = _layout->stretch(currentIndex + 1);
    _layout->removeWidget(widget->widget());
    _widgets.removeOne(widget);
    if(nb == std::numeric_limits<int>::max()) {
        _widgets.append(widget);
        _layout->addWidget(widget->widget(), stretchFactor);
    } else if(nb == std::numeric_limits<int>::min()) {
        _widgets.prepend(widget);
        _layout->insertWidget(1, widget->widget(), stretchFactor);
    } else {
        auto newIndex = currentIndex + nb;
        _widgets.insert(newIndex, widget);
        _layout->insertWidget(newIndex + 1, widget->widget(), stretchFactor);
    }
}

bool CompareTelemetryPage::canMoveGraph(CompareWidgetInterface *widget, int nb)
{
    if(_widgets.count() <= 1)
        return false;

    int currentIndex = _widgets.indexOf(widget);
    if(currentIndex == 0 && (nb == std::numeric_limits<int>::min() || currentIndex + nb < 0))
        return false;
    if(currentIndex == _widgets.count() - 1 &&
       (nb == std::numeric_limits<int>::max() || currentIndex + nb > _widgets.count() - 1))
        return false;

    return true;
}

void CompareTelemetryPage::clear()
{
    _widgets.clear();
    _totalStretch = 0;
}

void CompareTelemetryPage::closeEvent(QCloseEvent *event) { emit closed(); }

void CompareTelemetryPage::refreshHighlighting() { highlight(_currentHighlightedLapIndex); }

void CompareTelemetryPage::setZoomRect() { changeWidgetsZoomMode(QChartView::RectangleRubberBand); }

void CompareTelemetryPage::setZoomVertical() { changeWidgetsZoomMode(QChartView::HorizontalRubberBand); }

void CompareTelemetryPage::setZoomHorizontal() { changeWidgetsZoomMode(QChartView::VerticalRubberBand); }

void CompareTelemetryPage::highlight(int lapIndex)
{
    _currentHighlightedLapIndex = lapIndex;

    for(const auto &widget : _widgets) {
        widget->highlight(lapIndex);
    }
}

QString CompareTelemetryPage::name() const { return _name; }

void CompareTelemetryPage::setDetached(bool value)
{
    setWindowFlags(value ? Qt::Dialog : Qt::Widget);
    _detached = value;
}

bool CompareTelemetryPage::isDetached() const { return _detached; }

QStringList CompareTelemetryPage::variables() const
{
    QStringList vars;

    for(const auto &widget : _widgets) {
        vars << widget->name();
    }

    return vars;
}

int CompareTelemetryPage::totalStretch() const { return _totalStretch; }

std::shared_ptr<PageSettings> CompareTelemetryPage::pageSettings() const { return _pageSettings; }

void CompareTelemetryPage::setPageSettings(const std::shared_ptr<PageSettings> &value)
{
    _pageSettings = value;
    _renameAction->setEnabled(_pageSettings != nullptr);
    _deleteAction->setEnabled(_pageSettings != nullptr);
}

void CompareTelemetryPage::setZoomMode(QChartView::RubberBand zoomMode)
{
    blockSignals(true);
    switch(zoomMode) {
        case QChartView::VerticalRubberBand:
            _zoomHAction->setChecked(true);
            break;
        case QChartView::HorizontalRubberBand:
            _zoomVAction->setChecked(true);
            break;
        case QChartView::NoRubberBand:
        case QChartView::RectangleRubberBand:
            _zoomAction->setChecked(true);
            break;
        case QChartView::ClickThroughRubberBand:
            break;
    }
    changeWidgetsZoomMode(zoomMode);
    blockSignals(false);
}

void CompareTelemetryPage::changeWidgetsZoomMode(QChartView::RubberBand zoomMode)
{
    for(const auto &widget : std::as_const(_widgets)) {
        widget->setZoomMode(zoomMode);
    }

    _currentZoomMode = zoomMode;

    emit zoomModeChanged(_currentZoomMode);
}

void CompareTelemetryPage::setName(const QString &name)
{
    _name = name;
    setWindowTitle(_dataName + "s: " + name);
}

void CompareTelemetryPage::detach() { emit detachAsked(true); }
