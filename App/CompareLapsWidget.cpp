#include "CompareLapsWidget.h"

#include "Core/Lap.h"
#include <Core/UdpDataCatalog.h>

#include <InfoTreeWidget.h>
#include <QFileDialog>
#include <QHeaderView>
#include <Tools.h>
#include <TrackView.h>

CompareLapsWidget::CompareLapsWidget(QWidget *parent) : CompareTelemetryWidget("m", QString(), parent)
{
    setDataName("Lap");
    auto track = new TrackView;
    addCustomCompareWidget(track);
}

void CompareLapsWidget::browseData()
{
    auto filenames = QFileDialog::getOpenFileNames(this, "Select some laps to compare", "", "*.f1lap", nullptr,
                                                   QFileDialog::DontUseNativeDialog);

    openDataFiles(filenames);
}

void CompareLapsWidget::openDataFiles(const QStringList &filenames)
{
    bool dataDiscarded = false;
    int existingIndex = -1;
    QVector<TelemetryData *> laps;
    for(auto file : filenames) {
        if(file.endsWith(".f1lap")) {
            auto index = existingFileIndex(file);
            if(index < 0) {
                auto lap = Lap::fromFile(file);
                if(!Tools::versionIsOlderThan(lap->recordVersion, Tools::LAST_SUPPORTED_VERSION)) {
                    laps.append(lap);
                } else {
                    delete lap;
                    dataDiscarded = true;
                }
            } else
                existingIndex = index;
        }
    }

    addTelemetryData(laps);

    if(existingIndex > 0) {
        setCurrentDataIndex(existingIndex);
    }

    if(dataDiscarded) {
        QMessageBox::warning(this, "Invalid lap data",
                             "Some laps have been discarded because they have been recorded with a too old version.");
    }
}

void CompareLapsWidget::fillInfoTree(InfoTreeWidget *tree, const TelemetryData *data)
{
    auto lap = dynamic_cast<const Lap *>(data);
    if(!lap)
        return;

    tree->clear();

    driverItem(tree, lap);
    trackItem(tree, lap);
    weatherItem(tree, lap);

    auto time = QTime(0, 0).addMSecs(int(double(lap->lapTime) * 1000.0)).toString("m:ss.zzz");
    if(lap->isOutLap)
        time += " (Out Lap)";
    if(lap->isInLap)
        time += " (In Lap)";
    auto s1time = QTime(0, 0).addMSecs(int(double(lap->sector1Time) * 1000.0)).toString("m:ss.zzz");
    auto s2time = QTime(0, 0).addMSecs(int(double(lap->sector2Time) * 1000.0)).toString("m:ss.zzz");
    auto s3time = QTime(0, 0).addMSecs(int(double(lap->sector3Time) * 1000.0)).toString("m:ss.zzz");
    auto timeItem = new QTreeWidgetItem(tree, {"Lap Time", time});
    new QTreeWidgetItem(timeItem, {"Sector 1", s1time});
    new QTreeWidgetItem(timeItem, {"Sector 2", s2time});
    new QTreeWidgetItem(timeItem, {"Sector 3", s3time});

    if(lap->invalid) {
        new QTreeWidgetItem(timeItem, {"Invalid", ""});
    }

    timeItem->setExpanded(true);

    auto maxSpeedItem = speedItem(tree, lap);
    maxSpeedItem->setExpanded(true);

    tyreCompoundItem(tree, lap);
    tyreItem(tree, lap);
    tyreSetItem(tree, lap);

    auto tempItem = tyreTempItem(tree, lap);
    tempItem->setExpanded(true);

    auto lapFuel = lap->fuelOnEnd - lap->fuelOnStart;
    auto fuelItem = new QTreeWidgetItem(tree, {"Fuel Consumption", QString::number(qAbs(lapFuel)) + "kg"});
    new QTreeWidgetItem(fuelItem, {"Start", QString::number(lap->fuelOnStart) + "kg"});
    new QTreeWidgetItem(fuelItem, {"End", QString::number(lap->fuelOnEnd) + "kg"});
    for(auto it = lap->fuelMix.distancesPerMode.constBegin(); it != lap->fuelMix.distancesPerMode.constEnd(); ++it) {
        auto percentage = (it.value() / lap->trackDistance) * 100.0;
        new QTreeWidgetItem(fuelItem, {UdpDataCatalog::fuelMix(it.key()), QString::number(percentage, 'f', 2) + "%"});
    }

    auto ersItem = new QTreeWidgetItem(tree, {"ERS Energy", QString::number(int(lap->energy / 1000.0)) + "kJ"});
    new QTreeWidgetItem(ersItem, {"Balance", QString::number(int(lap->energyBalance / 1000.0)) + "kJ"});
    new QTreeWidgetItem(ersItem, {"Deployed", QString::number(int(lap->deployedEnergy / 1000.0)) + "kJ"});
    new QTreeWidgetItem(ersItem, {"Harvested", QString::number(int(lap->harvestedEnergy / 1000.0)) + "kJ"});
    for(auto it = lap->ers.distancesPerMode.constBegin(); it != lap->ers.distancesPerMode.constEnd(); ++it) {
        auto percentage = (it.value() / lap->trackDistance) * 100.0;
        new QTreeWidgetItem(ersItem, {UdpDataCatalog::ersMode(it.key()), QString::number(percentage, 'f', 2) + "%"});
    }
    ersItem->setExpanded(true);

    carDamageItem(tree, lap);
    engineDamageItem(tree, lap);

    setupItem(tree, lap);

    coastingItem(tree, lap);

    eventsItem(tree, lap);

    flashbackItem(tree, lap);
    recordItem(tree, lap);

    tree->header()->setSectionResizeMode(0, QHeaderView::ResizeToContents);
}
