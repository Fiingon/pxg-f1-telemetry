#ifndef TELEMETRYCHART_H
#define TELEMETRYCHART_H

#include <QChart>


class TelemetryChart : public QChart
{
	Q_OBJECT

  signals:
	void xAxisChanged(qreal min, qreal max);

  public:
	TelemetryChart();

	void addTelemetrySeries(const QVector<QAbstractSeries *> &series);
	void setTelemetrySeriesVisible(int index, bool value);
	void removeTelemetrySeries(int index);
	void clearTelemetrySeries();

	void setCursor(double value);
	void setCursorColor(const QColor &color);
	void setCursorVisible(bool value);

	void connectAxis();

  private:
	QVector<QVector<QAbstractSeries *>> _series;
	QGraphicsLineItem *_cursor;
	double _cursorValue = 0.0;
	bool _cursorVisible = true;
};

#endif // TELEMETRYCHART_H
