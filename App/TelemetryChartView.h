#ifndef TELEMETRYCHARTVIEW_H
#define TELEMETRYCHARTVIEW_H

#include <CompareWidgetInterface.h>
#include <QChartView>
#include <QCheckBox>
#include <QLabel>
#include <QScatterSeries>
#include <QToolButton>

#include <Core/TelemetryData.h>
#include <Core/TelemetryDefinitions.h>

class TelemetryChart;
class TelemetryChartView : public QChartView, public CompareWidgetInterface
{
	Q_OBJECT

  public:
	enum class DisplayMode { Diff, Stats, Smoothing };

  signals:
	void displayModeChanged(DisplayMode option, bool value, TelemetryChartView *view);
	void requestContextMenu(const QPoint &pos);

  public:
    TelemetryChartView(QChart *chart, const QString &varName, bool showTrackTurns, QWidget *parent = nullptr);

	void setHomeZoom();
	QRect homeView() const;

	bool zoomEnabled() const;
	void setZoomEnabled(bool zoomEnabled);

	void setUnitX(const QString &unitX);
	void setUnitY(const QString &unit);

	void setPosLabelVisible(bool value);

	void clearEvents();
    void addEventSerie(QScatterSeries *serie, const QStringList &tooltips);

	TelemetryChart *telemetryChart() const;

	void scaleViewBy(double factor, const QPointF &zoomPoint);
	void scaleViewByOnCenter(double factor, bool onX = true, bool onY = true);

	QRectF visibleSceneRect() const;

	bool isDisplayModeActivated(DisplayMode mode) const;

	void reloadVariableSeries(const QVector<TelemetryData *> &telemetryData,
							  int varIndex,
							  bool diff,
							  bool stats,
							  bool smooth,
							  QList<QColor> colors);

	// CompareWidgetInterface
	void home() override;
	QString name() const override;
	QWidget *widget() override;
	void addTelemetryData(const QVector<TelemetryData *> &telemetry) override;
	void removeTelemetryData(int index) override;
	void clear() override;
	void setTelemetryVisibility(const QVector<bool> &visibility) override;
	void setColors(const QList<QColor> &colors) override;
	void setTheme(QChart::ChartTheme theme) override;
	void setCustomTheme(const CustomTheme &theme) override;
	void setCursor(double value) override;
	void setCursorColor(const QColor &color) override;
	void setCursorVisible(bool value) override;
	void setTelemetryDataDiff(int index, bool diff) override;
	void setReference(const TelemetryData *data) override;
	void setTrackIndex(int trackIndex) override;
	int leftMargin() override;
	void setLeftMargin(int value) override;
	int strecthFactor() override;
	void highlight(int index) override;
	void setZoomMode(QChartView::RubberBand zoomMode) override;

  protected:
	bool event(QEvent *event) override;
	void wheelEvent(QWheelEvent *event) override;
	void mousePressEvent(QMouseEvent *event) override;
	void mouseReleaseEvent(QMouseEvent *event) override;
	void mouseMoveEvent(QMouseEvent *event) override;
	void resizeEvent(QResizeEvent *event) override;

  private:
	QString _varName;
	bool _showTrackTurns = false;

	QPair<double, double> xHome;
	QPair<double, double> yHome;
	bool _zoomEnabled = true;

	QMap<DisplayMode, QCheckBox *> _displayModes;
	QButtonGroup *_displayModesGroup;

	QList<QWidget *> _configWidgets;
	QLabel *_posLabel;
	QString _unitX, _unitY;

    QVector<QScatterSeries *> _eventsSeries;
	QVector<QStringList> _eventsTooltips;
	QRectF _pickerRect;

	QPointF _panStartPos;
	QPointF _panReferenceValue;

	const TelemetryData *_reference = nullptr;
	int _trackIndex;

	void addConfigurationWidget(QWidget *widget);
	void addDisplayModeConfig(DisplayMode mode, const QString &text);

	void updateLabelsPosition();

	void initMenuButton();

	QAbstractSeries *createTelemetryStat(TelemetryData *data, int varIndex, const QColor &color);
	QScatterSeries *createEventLine(const QVector<TelemetryEvent> &events,
									TelemetryData *data,
									int varIndex,
									const QColor &color,
									const QColor &borderColor);
	QAbstractSeries *createTelemetryLine(TelemetryData *data,
										 int varIndex,
										 const TelemetryData *refData,
										 bool diff,
										 bool smooth,
										 const QColor &color);

	void createAxis(QChart *chart, bool stats, const TelemetryStaticInfo &staticInfo);
	float findMedian(int begin, int end, const QVector<float> &data);

	int labelSize(Qt::Orientation axisOrientation, double value) const;
	int labelSize(Qt::Orientation axisOrientation, const QString &value) const;

  private slots:
	void configChanged(QAbstractButton *buttonClicked);
	void zoomY();
	void unzoomY();
};

#endif // TELEMETRYCHARTVIEW_H
