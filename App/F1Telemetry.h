#ifndef F1TELEMETRY_H
#define F1TELEMETRY_H

#include "Core/F1Listener.h"

#include <QCheckBox>
#include <QMainWindow>
#include <QMessageBox>
#include <QNetworkAccessManager>

namespace Ui
{
class F1Telemetry;
}

class Tracker;
class FileDownloader;
class CheckUpdatesDialog;
class ButtonTesterDialog;
class PreferencesDialog;
struct TrackedCars;
class UdpDebugger;
class UdpLogger;

class F1Telemetry : public QMainWindow
{
	Q_OBJECT

  public:
	explicit F1Telemetry(QWidget *parent = nullptr);
	~F1Telemetry();

	static void
		displayMarkdownFile(const QString &mdFile, const QString &css, const QString &title, QWidget *parent = nullptr);

  private:
	Ui::F1Telemetry *ui;
	F1Listener *_listener = nullptr;
	Tracker *_tracker;
	FileDownloader *_downloader;
	CheckUpdatesDialog *_updateDialog;
	bool _isAutoCheckUpdates = false;
	ButtonTesterDialog *_buttonTesterDialog = nullptr;
	UdpDebugger *_debugger = nullptr;
	UdpLogger *_udpLogger = nullptr;

	PreferencesDialog *_prefDialog = nullptr;
	bool _testOpenedFromPref = false;

	enum DownloadingFileTypes { VersionFile, ChangelogFile };

	void loadSettings();
	void saveSetings();
	void initDefaultSettings();

	void initMenu();

	bool isGreaterVersion(const QString &version);
	void updateWindowTitle(const QString &status, bool tracking);

  protected:
	void closeEvent(QCloseEvent *event);

  private slots:
	void buildListener();
	void startTracking(const TrackedCars &cars);
	void stopTracking();

	void checkUpdates();
	void fileDownloaded(int type, const QByteArray &data);
	void showChangeLog();
	void changelogAutoDisplay();
	void contact();
	void editTheme();
	void updateTheme();
	void editPreferences();

	void testButtonsFromPref();
	void testButtons();

	void startDebugger();
	void stopDebugger();

	void updatedTrackersWithPreferences();

	void showLaps(const QStringList &laps);
	void showStints(const QStringList &stints);
};

#endif // F1TELEMETRY_H
