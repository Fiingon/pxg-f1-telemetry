#ifndef TELEMETRYTABLEWIDGET_H
#define TELEMETRYTABLEWIDGET_H

#include <CompareWidgetInterface.h>
#include <QAbstractTableModel>
#include <QWidget>

namespace Ui
{
class TelemetryTableWidget;
}

class TelemetryData;


class TelemetryTableModel : public QAbstractTableModel
{
  public:
	TelemetryTableModel(QObject *parent);

	void setTelemetryData(QVector<TelemetryData *> telemetry);
	void clearTelemetryData();

	void setDisplayIndexes(const QList<int> &indexes);
	void setCursorValue(double value);
	void setColors(const QList<QColor> &colors);

	// QAbstractItemModel interface
	int rowCount(const QModelIndex &parent = QModelIndex()) const override;
	int columnCount(const QModelIndex &parent = QModelIndex()) const override;
	QVariant data(const QModelIndex &index, int role) const override;
	QVariant headerData(int section, Qt::Orientation orientation, int role) const override;
	Qt::ItemFlags flags(const QModelIndex &index) const override;

	void setTelemetryDataDiff(int index, bool diff);
	void setReference(const TelemetryData *data);

  private:
	QVector<TelemetryData *> _telemetry;
	QList<int> _dataIndexes;
	const TelemetryData *_reference = nullptr;
	QHash<int, bool> _diffs;
	QList<QColor> _colors;

	double _cursorValue = 0.0f;
};

class TelemetryTableWidget : public QWidget, public CompareWidgetInterface
{
	Q_OBJECT

  public:
	explicit TelemetryTableWidget(QWidget *parent = nullptr);
	~TelemetryTableWidget() override;

	void setDisplayIndex(int index, bool visible);
	void setLimitedViewMode(bool limitedViewMode);


	// CompareWidgetInterface interface
	QString name() const override;
	QWidget *widget() override;
	void home() override;
	void addTelemetryData(const QVector<TelemetryData *> &telemetry) override;
	void removeTelemetryData(int index) override;
	void clear() override;
	void setTelemetryVisibility(const QVector<bool> &visibility) override;
	void setColors(const QList<QColor> &colors) override;
	void setTheme(QChart::ChartTheme theme) override;
	void setCustomTheme(const CustomTheme &theme) override;
	void setCursor(double value) override;
	void setCursorColor(const QColor &color) override;
	void setCursorVisible(bool value) override;
	void setTelemetryDataDiff(int index, bool diff) override;
	void setReference(const TelemetryData *data) override;
	void setTrackIndex(int trackIndex) override;
	int leftMargin() override;
	void setLeftMargin(int value) override;
	int strecthFactor() override;
	void highlight(int index) override;
	void setZoomMode(QChartView::RubberBand zoomMode) override;

  private:
	Ui::TelemetryTableWidget *ui;
	TelemetryTableModel *_model;
	QVector<TelemetryData *> _allTelemetry;

	QVector<bool> _visibilities;
	QList<int> _displayedIndexes;

	bool _limitedViewMode = true;

	void updateTelemetryModel();

  private slots:
	void updateHeight();
	void copySelection();
};

#endif // TELEMETRYTABLEWIDGET_H
