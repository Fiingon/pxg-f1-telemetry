#include "TrackingWidget.h"
#include "F1TelemetrySettings.h"
#include "MeteoTableModel.h"
#include "ui_TrackingWidget.h"

#include <QDateTime>
#include <QFileDialog>
#include <QHeaderView>
#include <QInputDialog>
#include <QMessageBox>
#include <QNetworkInterface>

const auto MAX_LOG_LINES = 100;


TrackingWidget::TrackingWidget(QWidget *parent) : QWidget(parent), ui(new Ui::TrackingWidget)
{
    ui->setupUi(this);

    updateNetworkData();
    _driverCheckBoxes << ui->driver1 << ui->driver2 << ui->driver3 << ui->driver4 << ui->driver5 << ui->driver6
                      << ui->driver7 << ui->driver8 << ui->driver9 << ui->driver10 << ui->driver11 << ui->driver12
                      << ui->driver13 << ui->driver14 << ui->driver15 << ui->driver16 << ui->driver17 << ui->driver18
                      << ui->driver19 << ui->driver20 << ui->driver21 << ui->driver22;
    connect(ui->btnTrack, &QPushButton::clicked, this, &TrackingWidget::startStop);
    connect(ui->btnBrowse, &QPushButton::clicked, this, &TrackingWidget::browseDataDirectory);
    connect(ui->btnQuickInstructions, &QPushButton::clicked, this, &TrackingWidget::showQuickInstructions);
    connect(ui->allcars, &QCheckBox::toggled, this, &TrackingWidget::allCarsChecked);
    connect(ui->btnEditPort, &QToolButton::clicked, this, &TrackingWidget::editPort);
    connect(ui->btnEditServer, &QToolButton::clicked, this, &TrackingWidget::editServer);
    connect(ui->btnEditForwardPort, &QToolButton::clicked, this, &TrackingWidget::editForwardPort);
    connect(ui->btnEditForwardIP, &QToolButton::clicked, this, &TrackingWidget::editForwardIp);
    connect(ui->btnRetry, &QPushButton::clicked, this, &TrackingWidget::networkInfoChanged);
    setStatus("", false);
    setConnectionStatus(false);
    setDrivers({});
    setSession("");

    _sessionTimer = new QTimer(this);
    _sessionTimer->setInterval(1000);
    connect(_sessionTimer, &QTimer::timeout, this, &TrackingWidget::onSessionTimer);

    _meteoModel = new MeteoTableModel(this);
    ui->meteoTable->setModel(_meteoModel);
    ui->meteoTable->verticalHeader()->setSectionResizeMode(QHeaderView::ResizeToContents);
    ui->meteoTable->verticalHeader()->setSectionResizeMode(1, QHeaderView::Stretch);

    Logger::instance()->setInterface(this);
}

TrackingWidget::~TrackingWidget() { delete ui; }

void TrackingWidget::saveSettings(F1TelemetrySettings *settings)
{
    settings->beginGroup("Tracking");
    settings->setValue("dataDirectory", ui->leDataDir->text());
    settings->setValue("trackPlayer", ui->player->isChecked());
    settings->setValue("trackTeammate", ui->teammate->isChecked());
    settings->setValue("trackAll", ui->allcars->isChecked());
    settings->setValue("trackAllRace", ui->allRace->isChecked());
    settings->setValue("trackAllGhosts", ui->allGhosts->isChecked());
    settings->endGroup();
}

void TrackingWidget::loadSettings(F1TelemetrySettings *settings)
{
    settings->beginGroup("Tracking");
    ui->leDataDir->setText(settings->value("dataDirectory").toString());
    QDir::setCurrent(settings->value("dataDirectory").toString());
    ui->player->setChecked(settings->value("trackPlayer").toBool());
    ui->teammate->setChecked(settings->value("trackTeammate").toBool());
    ui->allcars->setChecked(settings->value("trackAll").toBool());
    ui->allRace->setChecked(settings->value("trackAllRace", true).toBool());
    ui->allGhosts->setChecked(settings->value("trackAllGhosts", true).toBool());
    settings->endGroup();
}

void TrackingWidget::setSession(const QString &sessionName)
{
    ui->lblSession->setText(sessionName);
    if(sessionName.trimmed().isEmpty()) {
        setTime(-1);
        ui->meteoTable->setVisible(false);
        ui->meteoTitle->setVisible(false);
    }
}

void TrackingWidget::setTime(int remainingTimeInSec)
{
    QString text;
    if(remainingTimeInSec >= 0) {
        text += '(';
        text += QTime(0, 0).addSecs(remainingTimeInSec).toString("hh:mm:ss");
        text += ')';
    }

    ui->lblSessionTime->setText(text);
}

void TrackingWidget::setRaceLaps(int nbLaps, int totalLaps)
{
    if(nbLaps <= totalLaps) {
        QString text;
        text += QString::number(nbLaps);
        text += '/';
        text += QString::number(totalLaps);

        ui->lblSessionTime->setText(text);
    }
}

void TrackingWidget::setDrivers(const QStringList &drivers)
{
    _driverList = drivers;
    for(auto i = 0; i < _driverCheckBoxes.count(); ++i) {
        auto name = drivers.value(i, "Unknown Driver");
        _driverCheckBoxes[i]->setText(name);
    }
}

void TrackingWidget::setStatus(const QString &status, bool trackingInProgress)
{
    ui->lblStatus->setText(status);
    _trackingInProgress = trackingInProgress;
    ui->btnTrack->setText(_trackingInProgress ? "Stop" : "Start");
    ui->driverWidget->setEnabled(!trackingInProgress);
    ui->btnBrowse->setEnabled(!trackingInProgress);
}

void TrackingWidget::setConnectionStatus(bool connected) { ui->btnRetry->setVisible(!connected); }

void TrackingWidget::log(const QString &text)
{
    ui->logTextEdit->appendPlainText(QDateTime::currentDateTime().toString("dd/MM/yyyy hh:mm:ss: ").append(text));
    ++_nbLogLines;

    if(_nbLogLines > MAX_LOG_LINES) {
        QString text = ui->logTextEdit->toPlainText();
        auto firstEolIndex = text.indexOf('\n');
        ui->logTextEdit->setPlainText(text.mid(firstEolIndex + 1));
        --_nbLogLines;
    }
}

void TrackingWidget::showQuickInstructions()
{
    auto instructionText = "The PC or Console where F1 24 is running and the PC where this application is running "
                           "must be connected to the same local network.\n\n"
                           "Method 1: Broadcast mode OFF\n"
                           "   In F1 24, open Game Options > Settings > Telemetry Settings\n"
                           "      1. set UDP Telemetry to On\n"
                           "      2. set UDP Broadcast Mode to Off\n"
                           "      3. set UDP IP Address to the local IP address of the PC where this application is "
                           "running (or \"127.0.0.1\" if the game and the telemetry application run on the same PC)\n"
                           "      4. set Port to 20777 (default value)\n"
                           "      5. set UDP Send Rate to 20Hz (default value)\n"
                           "      6. set UDP Format to 2022 (default value)\n\n"
                           "Method 2: Broadcast mode ON (not recommended)\n"
                           "   In F1 24, open Game Options > Settings > Telemetry Settings\n"
                           "      1. set UDP Telemetry to On\n"
                           "      2. set UDP Broadcast Mode to On\n"
                           "      3. set Port to 20777 (default value)\n"
                           "      4. set UDP Send Rate to 20Hz (default value)\n"
                           "      5. set UDP Format to 2020 (default value)\n\n"
                           "Launch a session in F1 24, when the name of the session appear, select the drivers you "
                           "want to track and click \"Start\".";

    QMessageBox msgBox(QMessageBox::Information, "Quick Connection Instructions", instructionText, QMessageBox::Ok,
                       this);
    QSpacerItem *horizontalSpacer = new QSpacerItem(800, 0, QSizePolicy::Minimum, QSizePolicy::Expanding);
    QGridLayout *layout = (QGridLayout *)msgBox.layout();
    layout->addItem(horizontalSpacer, layout->rowCount(), 0, 1, layout->columnCount());
    msgBox.exec();
}

QString TrackingWidget::getLocalIpAddress() const
{
    for(const auto &address : QNetworkInterface::allAddresses()) {
        if(address.protocol() == QAbstractSocket::IPv4Protocol && address != QHostAddress(QHostAddress::LocalHost))
            return address.toString();
    }

    return "Unknown";
}

void TrackingWidget::updateNetworkData()
{
    F1TelemetrySettings settings;
    auto serverAddress = settings.server();
    if(serverAddress.isEmpty()) {
        serverAddress = "Any";
    }

    ui->lblIP->setText(getLocalIpAddress());
    ui->lblPort->setText(QString::number(settings.port()));
    ui->lblServer->setText(serverAddress);

    if(!settings.forwardIp().isEmpty()) {
        ui->lblForwardIP->setText(settings.forwardIp());
        ui->lblForwardPort->setText(QString::number(settings.forwardPort()));
    } else {
        ui->lblForwardIP->setText("-");
        ui->lblForwardPort->setText(QString::number(settings.forwardPort()));
    }
}

void TrackingWidget::startStop()
{
    if(!_trackingInProgress) {
        if(ui->leDataDir->text().isEmpty()) {
            browseDataDirectory();
            if(ui->leDataDir->text().isEmpty()) {
                QMessageBox::critical(this, "Missing data directory",
                                      "A data directory where the data will be stored must be selected.");
                return;
            }
        }

        QVector<int> trackedId;
        for(auto i = 0; i < _driverCheckBoxes.count(); ++i) {
            if(_driverCheckBoxes[i]->isChecked())
                trackedId << i;
        }
        TrackedCars cars{ui->player->isChecked(),
                         ui->teammate->isChecked(),
                         ui->player2->isChecked(),
                         ui->allcars->isChecked(),
                         ui->allRace->isChecked(),
                         ui->allGhosts->isChecked(),
                         trackedId};
        emit startTracking(cars);
    } else {
        emit stopStracking();
    }
}

void TrackingWidget::browseDataDirectory()
{
    auto directory = QFileDialog::getExistingDirectory(
        this, "Please select the directory where the data should be stored", ui->leDataDir->text());
    ui->leDataDir->setText(directory);
    QDir::setCurrent(directory);
}

void TrackingWidget::allCarsChecked(bool checked)
{
    for(const auto &check : _driverCheckBoxes) {
        check->setDisabled(checked);
    }

    ui->allRace->setDisabled(checked);
    ui->allGhosts->setDisabled(checked);
}

void TrackingWidget::editPort()
{
    F1TelemetrySettings settings;
    auto port = settings.port();
    bool ok = false;
    port = QInputDialog::getInt(ui->btnEditPort, "Listened port",
                                "Enter the port number configured in the game telemetry settings", port, 1, 999999, 1,
                                &ok);
    if(ok) {
        settings.setPort(port);
        updateNetworkData();
        emit networkInfoChanged();
    }
}

void TrackingWidget::editServer()
{
    F1TelemetrySettings settings;
    auto server = settings.server();
    bool ok = false;
    server = QInputDialog::getText(
        ui->btnEditServer, "Listened IP address",
        "Enter the IP address of the computer where the game is running.\n(Leave empty to listen to any IP address)",
        QLineEdit::Normal, server, &ok);
    if(ok) {
        if(!server.isEmpty() && QHostAddress(server).isNull()) {
            QMessageBox::critical(this, "Invalid IP address", QString("\"%1\" is not a valid IP address!").arg(server));
            editServer();
            return;
        }
        settings.setServer(server);
        updateNetworkData();
        emit networkInfoChanged();
    }
}

void TrackingWidget::onSessionTimer()
{
    if(_timerCount < 3) {
        setTime(currentTime);
        --currentTime;
        ++_timerCount;
    }
}

void TrackingWidget::editForwardPort()
{
    F1TelemetrySettings settings;
    auto port = settings.forwardPort();
    bool ok = false;
    port =
        QInputDialog::getInt(ui->btnEditForwardPort, "Forward port",
                             "Enter the port number used to forward the UDP telemetry data", port, 1, 999999, 1, &ok);
    if(ok) {
        settings.setForwardPort(port);
        updateNetworkData();
        emit networkInfoChanged();
    }
}

void TrackingWidget::editForwardIp()
{
    F1TelemetrySettings settings;
    auto server = settings.forwardIp();
    bool ok = false;
    server = QInputDialog::getText(
        ui->btnEditServer, "Forward IP address",
        "Enter the IP address where the UDP telemetry data will be forwarded (leave empty to disable the UDP it)",
        QLineEdit::Normal, server, &ok);
    if(ok) {
        if(!server.isEmpty() && QHostAddress(server).isNull()) {
            QMessageBox::critical(this, "Invalid IP address", QString("\"%1\" is not a valid IP address!").arg(server));
            editForwardIp();
            return;
        }
        settings.setForwardIp(server);
        updateNetworkData();
        emit networkInfoChanged();
    }
}

QString TrackingWidget::getDataDirectory() const { return ui->leDataDir->text(); }


void TrackingWidget::telemetryData(const PacketHeader &, const PacketCarTelemetryData &) {}

void TrackingWidget::lapData(const PacketHeader &, const PacketLapData &data)
{
    if(_isRace) {
        auto firstCarIndex = data.lapIndexForCarPosition(1);
        auto lapNum = data.m_lapData.value(firstCarIndex).m_currentLapNum;
        if(lapNum != currentLap) {
            currentLap = lapNum;
            setRaceLaps(currentLap, totalLaps);
        }
    }
}

void TrackingWidget::sessionData(const PacketHeader &header, const PacketSessionData &data)
{
    if(header.m_sessionUID != _header.m_sessionUID) {
        setSession(data.sessionName());
        currentTime = data.m_sessionTimeLeft;
        totalLaps = data.m_totalLaps;
        currentLap = -1;
        _isRace = data.isRace();
        if(!data.isRace())
            _sessionTimer->start();
    }

    if(_timerCount >= 3) {
        currentTime = data.m_sessionTimeLeft;
    }

    if(data.isTimeTrial()) {
        currentTime = -1;
    }

    _timerCount = 0;

    _header = header;
    _meteoModel->setForecast(data.currentSessionWeatherForecastSamples());
    ui->meteoTable->setVisible(_meteoModel->columnCount() > 0);
    ui->meteoTitle->setVisible(ui->meteoTable->isVisible());
}

void TrackingWidget::setupData(const PacketHeader &, const PacketCarSetupData &) {}

void TrackingWidget::statusData(const PacketHeader &, const PacketCarStatusData &) {}

void TrackingWidget::participant(const PacketHeader &header, const PacketParticipantsData &data)
{
    auto drivers = data.availableDrivers(header);
    if(drivers != _driverList) {
        setDrivers(drivers);
    }
}

void TrackingWidget::motionData(const PacketHeader &, const PacketMotionData &) {}

void TrackingWidget::motionExData(const PacketHeader &, const PacketMotionExData &) {}

void TrackingWidget::eventData(const PacketHeader &, const PacketEventData &data)
{
    switch(data.event) {
        case Event::SessionEnded:
            _sessionTimer->stop();
            setSession("");
            break;
        default:
            break;
    }
}

void TrackingWidget::finalClassificationData(const PacketHeader &, const PacketFinalClassificationData &) {}

void TrackingWidget::carDamageData(const PacketHeader &, const PacketCarDamageData &) {}

void TrackingWidget::sessionHistoryData(const PacketHeader &, const PacketSessionHistoryData &) {}

void TrackingWidget::tyreSetData(const PacketHeader &, const PacketTyreSetsData &) {}
