#ifndef TRACKVIEW_H
#define TRACKVIEW_H

#include <CompareWidgetInterface.h>
#include <CustomTheme.h>
#include <QGraphicsPathItem>
#include <QGraphicsView>
#include <QtCharts>

class Lap;

class LapItem : public QGraphicsPathItem
{
  public:
	LapItem(Lap *lap, const QColor &color, QGraphicsItem *parent = nullptr);

	Lap *lap() const { return _lap; }

	void setColor(const QColor &color);

  private:
	Lap *_lap;
};

class TrackItem : public QGraphicsPathItem
{
  public:
	TrackItem(const QColor &color, QGraphicsItem *parent = nullptr);

	void setTrackIndex(int trackIndex);
	int trackIndex() const;

  private:
	int track = -1;
};

class TrackView : public QGraphicsView, public CompareWidgetInterface
{
	Q_OBJECT

  public:
	explicit TrackView(QWidget *parent = nullptr);
	~TrackView() override = default;

	void zoomIn();
	void zoomOut();
	void scaleViewBy(qreal scaleFactor);
	void scaleViewTo(qreal absoluteScaleFactor);

	QRectF visibleSceneRect() const;

	qreal getScaleFactor() const;

	// CompareWidgetInterface
	QString name() const override;
	QWidget *widget() override;

	void home() override;

	void addTelemetryData(const QVector<TelemetryData *> &telemetry) override;
	void removeTelemetryData(int index) override;
	void clear() override;

	void setTelemetryVisibility(const QVector<bool> &visibility) override;
	void setColors(const QList<QColor> &colors) override;

	void setTheme(QChart::ChartTheme theme) override;
	void setCustomTheme(const CustomTheme &theme) override;

	void setCursor(double value) override;
	void setCursorColor(const QColor &color) override;
	void setCursorVisible(bool value) override;

	void setTelemetryDataDiff(int index, bool diff) override;
	void setReference(const TelemetryData *data) override;

	void setTrackIndex(int trackIndex) override;

	int leftMargin() override;
	void setLeftMargin(int value) override;
	int strecthFactor() override;

	void highlight(int index) override;

	void setZoomMode(QChartView::RubberBand zoomMode) override;

  protected:
	bool event(QEvent *event) override;
	void wheelEvent(QWheelEvent *event) override;
	void mouseMoveEvent(QMouseEvent *event) override;
	void mousePressEvent(QMouseEvent *event) override;
	void mouseReleaseEvent(QMouseEvent *event) override;
	void resizeEvent(QResizeEvent *event) override;

  private:
	qreal _zoomScale;
	QList<LapItem *> _laps;
	TrackItem *_track;
	QGraphicsEllipseItem *_cursor;
	QSize prevSize;
	bool _isHome = true;

	QPoint _panStartPos;
	QPointF _panReferenceScenePos;

	QPointF _rubberStart;
	QPointF _rubberEnd;

	qreal calculateScaleFactor(const QTransform &matrix);

  private slots:
	void rubberBandChanged(QRect rubberBandRect, QPointF fromScenePoint, QPointF toScenePoint);
};

#endif // TRACKVIEW_H
