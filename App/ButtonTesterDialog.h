#ifndef BUTTONTESTERDIALOG_H
#define BUTTONTESTERDIALOG_H

#include "Core/F1Listener.h"

#include <QDialog>

namespace Ui
{
class ButtonTesterDialog;
}

class ButtonTesterDialog : public QDialog, public F1PacketInterface
{
    Q_OBJECT

  public:
    explicit ButtonTesterDialog(QWidget *parent = nullptr);
    ~ButtonTesterDialog() override;

  protected:
    // F1PacketInterface interface
    void telemetryData(const PacketHeader &header, const PacketCarTelemetryData &data) override;
    void lapData(const PacketHeader &header, const PacketLapData &data) override;
    void sessionData(const PacketHeader &header, const PacketSessionData &data) override;
    void setupData(const PacketHeader &header, const PacketCarSetupData &data) override;
    void statusData(const PacketHeader &header, const PacketCarStatusData &data) override;
    void participant(const PacketHeader &header, const PacketParticipantsData &data) override;
    void motionData(const PacketHeader &header, const PacketMotionData &data) override;
    void motionExData(const PacketHeader &header, const PacketMotionExData &data) override;
    void eventData(const PacketHeader &header, const PacketEventData &event) override;
    void finalClassificationData(const PacketHeader &header, const PacketFinalClassificationData &data) override;
    void carDamageData(const PacketHeader &header, const PacketCarDamageData &data) override;
    void sessionHistoryData(const PacketHeader &header, const PacketSessionHistoryData &data) override;
    void tyreSetData(const PacketHeader &header, const PacketTyreSetsData &data) override;

  private:
    Ui::ButtonTesterDialog *ui;

    void setActive(bool value);
};

#endif // BUTTONTESTERDIALOG_H
