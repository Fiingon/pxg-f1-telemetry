#include "CompareStintsWidget.h"

#include "Core/Stint.h"
#include "Core/TrackInfo.h"
#include <Core/UdpDataCatalog.h>

#include <InfoTreeWidget.h>
#include <QFileDialog>
#include <QHeaderView>
#include <QtDebug>

#include <QMessageBox>
#include <Tools.h>
#include <algorithm>


CompareStintsWidget::CompareStintsWidget(QWidget *parent) : CompareTelemetryWidget(" lap", "", parent)
{
    setDataName("Stint");
}

void CompareStintsWidget::browseData()
{
    auto filenames = QFileDialog::getOpenFileNames(this, "Select some stints to compare", "", "*.f1stint", nullptr,
                                                   QFileDialog::DontUseNativeDialog);
    openDataFiles(filenames);
}

void CompareStintsWidget::openDataFiles(const QStringList &filenames)
{
    bool dataDiscarded = false;
    int existingIndex = -1;
    QVector<TelemetryData *> stints;
    for(auto file : filenames) {
        if(file.endsWith(".f1stint")) {
            auto index = existingFileIndex(file);
            if(index < 0) {
                auto stint = Stint::fromFile(file);
                if(!Tools::versionIsOlderThan(stint->recordVersion, Tools::LAST_SUPPORTED_VERSION)) {
                    stints.append(stint);
                } else {
                    delete stint;
                    dataDiscarded = true;
                }

            } else {
                existingIndex = index;
            }
        }
    }

    addTelemetryData(stints);

    if(existingIndex > 0) {
        setCurrentDataIndex(existingIndex);
    }

    if(dataDiscarded) {
        QMessageBox::warning(this, "Invalid stint data",
                             "Some stints have been discarded because they have been recorded with a too old version.");
    }
}


void CompareStintsWidget::fillInfoTree(InfoTreeWidget *tree, const TelemetryData *data)
{
    auto stint = dynamic_cast<const Stint *>(data);
    if(!stint)
        return;

    tree->clear();

    driverItem(tree, stint);
    trackItem(tree, stint);
    weatherItem(tree, stint);
    tyreCompoundItem(tree, stint);


    auto stintItem = tree->makeTopLevelItem("Stint", QString::number(stint->nbLaps()) + " Laps");
    if(std::any_of(stint->linkedFiles().begin(), stint->linkedFiles().end(),
                   [](const auto &lapFile) { return QFile::exists(lapFile); })) {
        tree->setItemAction(stintItem, "Open All Laps", [this, stint]() { emit askOpenLaps(stint->linkedFiles()); });
    }

    int lapIndex = 0;
    float averageTime = 0;
    for(auto lap : stint->lapTimes) {
        auto lapTime = QTime(0, 0).addMSecs(int(double(lap) * 1000.0)).toString("m:ss.zzz");
        auto text = QString("Lap ").append(QString::number(lapIndex + 1));
        auto infos = stint->lapsAdditionalInfo.value(lapIndex);
        for(const auto &info : std::as_const(infos)) {
            text += " - ";
            text += info;
        }

        if(stint->isOutLap && lapIndex == 0) {
            text += " (Out Lap)";
        }
        if(stint->isInLap && lapIndex == stint->lapTimes.count() - 1) {
            text += " (In Lap)";
        }
        auto lapItem = tree->makeItem(stintItem, text, lapTime);
        if(QFile::exists(stint->linkedFiles().value(lapIndex))) {
            tree->setItemAction(lapItem, "Open lap", [lapIndex, this, stint]() {
                emit askOpenLaps({stint->linkedFiles().value(lapIndex)});
            });
        }

        averageTime += lap;

        ++lapIndex;
    }

    if(stint->nbLaps() > 0)
        averageTime /= stint->nbLaps();
    else
        averageTime = 0.0;
    auto avgLapTime = QTime(0, 0).addMSecs(int(double(averageTime) * 1000.0)).toString("m:ss.zzz");
    tree->makeItem(stintItem, "Average", avgLapTime);

    auto maxSpeedItem = speedItem(tree, stint);
    tree->makeItem(maxSpeedItem, "Average Speed", QString::number(stint->averageSpeed) + "km/h");
    auto avgSpeedfactor = stint->averageSpeed > 0 ? float(stint->maxSpeed) / float(stint->averageSpeed) : 0.0;
    tree->makeItem(maxSpeedItem, "Average Speed factor", QString::number(avgSpeedfactor));

    auto wearDiff = (stint->endTyreWear - stint->startTyreWear) / double(stint->nbLaps());
    if(stint->calculatedTyreWear.max() > 0.0) {
        wearDiff = stint->calculatedTyreWear; // Backward compatibility
    }
    auto wearList = {wearDiff.frontLeft, wearDiff.frontRight, wearDiff.rearLeft, wearDiff.rearRight};
    auto maxWear = *(std::max_element(wearList.begin(), wearList.end()));

    auto avgLapWear = (wearDiff.frontLeft + wearDiff.frontRight + wearDiff.rearLeft + wearDiff.rearRight) / 4.0;
    auto calcTyreWearItem = new QTreeWidgetItem(tree, {"Tyre Wear (per lap)", QString("%1%").arg(avgLapWear)});
    new QTreeWidgetItem(calcTyreWearItem, {"Front Right", QString("%1%").arg(wearDiff.frontRight)});
    new QTreeWidgetItem(calcTyreWearItem, {"Front Left", QString("%1%").arg(wearDiff.frontLeft)});
    new QTreeWidgetItem(calcTyreWearItem, {"Rear Right", QString("%1%").arg(wearDiff.rearRight)});
    new QTreeWidgetItem(calcTyreWearItem, {"Rear Left", QString("%1%").arg(wearDiff.rearLeft)});
    new QTreeWidgetItem(calcTyreWearItem, {"Estimated Life (40%)", QString("%1 Laps").arg(40.0 / maxWear, 0, 'f', 1)});
    calcTyreWearItem->setExpanded(true);

    tyreSetItem(tree, stint);

    auto avgTemp = (stint->innerTemperatures.frontLeft.mean + stint->innerTemperatures.frontRight.mean +
                    stint->innerTemperatures.rearLeft.mean + stint->innerTemperatures.rearRight.mean) /
                   4.0;
    auto workingRange = UdpDataCatalog::tyreWorkingRange(stint->tyreCompound);
    QString tempText = QString::number(int(avgTemp)) + "°C";
    if(!workingRange.isEmpty()) {
        tempText += " (Target: ";
        tempText += workingRange;
        tempText += ")";
    }

    auto tempItem = new QTreeWidgetItem(tree, {"Avg. Tyre Temperature", tempText});
    new QTreeWidgetItem(tempItem,
                        {"Front Right", QString::number(int(stint->innerTemperatures.frontRight.mean)) + "°C "});
    new QTreeWidgetItem(tempItem, {"Front Left", QString::number(int(stint->innerTemperatures.frontLeft.mean)) + "°C"});
    new QTreeWidgetItem(tempItem, {"Rear Right", QString::number(int(stint->innerTemperatures.rearRight.mean)) + "°C"});
    new QTreeWidgetItem(tempItem, {"Rear Left", QString::number(int(stint->innerTemperatures.rearLeft.mean)) + "°C"});

    auto fuel = stint->fuelOnStart - stint->fuelOnEnd;
    auto fuelItem =
        new QTreeWidgetItem(tree, {"Average Fuel Consumption", QString::number(fuel / stint->nbLaps()) + "kg"});
    new QTreeWidgetItem(fuelItem, {"Start", QString::number(stint->fuelOnStart) + "kg"});
    new QTreeWidgetItem(fuelItem, {"End", QString::number(stint->fuelOnEnd) + "kg"});
    auto nbRaceLaps = UdpDataCatalog::trackInfo(stint->track)->nbLaps;
    if(nbRaceLaps > 0) {
        raceLoadItem(fuelItem, stint->nbLaps(), nbRaceLaps, fuel, 25);
        raceLoadItem(fuelItem, stint->nbLaps(), nbRaceLaps, fuel, 50);
        raceLoadItem(fuelItem, stint->nbLaps(), nbRaceLaps, fuel, 100);
    }
    fuelItem->setExpanded(true);

    carDamageDiffItem(tree, stint);
    engineDamageDiffItem(tree, stint);
    setupItem(tree, stint);

    auto coasting = coastingItem(tree, stint);
    auto avgCoasting = stint->coasting.distancesPerMode.value(1, 0.0) / stint->nbLaps();
    new QTreeWidgetItem(coasting, {"Avg. Coasting Per Lap", QString::number(avgCoasting, 'f', 2) + "m"});

    eventsItem(tree, stint);

    flashbackItem(tree, stint);
    recordItem(tree, stint);

    tree->header()->setSectionResizeMode(0, QHeaderView::ResizeToContents);
}

void CompareStintsWidget::raceLoadItem(QTreeWidgetItem *parent, int nbLaps, int nbRaceLaps, double fuel, int percentage)
{
    auto realRaceLaps = (nbRaceLaps * percentage) / 100;
    new QTreeWidgetItem(parent, {"Estimated Race Load", QString::number((fuel * realRaceLaps) / nbLaps) + "kg (" +
                                                            QString::number(realRaceLaps) + " Laps - " +
                                                            QString::number(percentage) + "%)"});
}
