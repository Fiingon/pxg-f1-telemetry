#include "F1TelemetrySettings.h"

#include "Core/Units.h"
#include <QTemporaryFile>

namespace SettingsKeys
{

const constexpr char *PORT = "port";
const constexpr char *SERVER = "server";

const constexpr char *FORWARD_IP = "forwardIp";
const constexpr char *FORWARD_PORT = "forwardPort";

const constexpr char *THEME = "theme";
const constexpr char *CUSTOM_THEME_BACKGROUND_COLOR = "customTheme/backgroundColor";
const constexpr char *CUSTOM_THEME_TEXT_COLOR = "customTheme/textColor";
const constexpr char *CUSTOM_THEME_GRID_COLOR = "customTheme/gridColor";
const constexpr char *CUSTOM_THEME_SERIES_COLORS = "customTheme/seriesColors";
const constexpr char *CUSTOM_THEME_USED = "customTheme/used";

const constexpr char *LINE_WIDTH = "lines/width";
const constexpr char *SELECTED_LINE_WIDTH = "lines/selectedWidth";

const constexpr char *CURSOR_COLOR = "cursor/color";

const constexpr char *MY_TEAM_NAME = "preferences/myteam";
const constexpr char *MARK_BUTTON = "preferences/markButon";
const constexpr char *CONTROLLER_TYPE = "preferences/controllerType";

const constexpr char *VARIABLES_PER_PAGES = "telemetryPages/nbVariablesPerPages";

const constexpr char *PAGE_SETTINGS = "telemetryPages/settings";
const constexpr char *PAGE_NAME = "name";
const constexpr char *PAGE_DETACHED = "detached";
const constexpr char *PAGE_VARIABLES = "variables";

const constexpr char *SPEED_UNIT = "units/speed";


} // namespace SettingsKeys

namespace SettingsDefaultValues
{
const constexpr int PORT = 20777;
const constexpr int THEME = 0;
const constexpr int FORWARD_PORT = 20778;
const CustomTheme CUSTOM_THEME = CustomTheme::defaultTheme();
const constexpr bool CUSTOM_THEME_USED = false;
const constexpr int LINE_WIDTH = 2;
const constexpr int SELECTED_LINE_WIDTH = 4;
const constexpr char *MY_TEAM_NAME = "My Team";
const constexpr unsigned int MARK_BUTTON = 0;
const QColor CURSOR_COLOR = QColor(Qt::red);
const constexpr int VARIABLES_PER_PAGES = 4;
const constexpr bool PAGE_DETACHED = false;
const constexpr char *SPEED_UNIT = Units::KPH;

} // namespace SettingsDefaultValues

F1TelemetrySettings::F1TelemetrySettings() : ApplicationSettings() {}

F1TelemetrySettings::F1TelemetrySettings(const QString &iniFile) : ApplicationSettings(iniFile) {}

std::shared_ptr<F1TelemetrySettings> F1TelemetrySettings::defaultSettings()
{
	QTemporaryFile file;
	file.open();

	auto defaultSettings = std::make_shared<F1TelemetrySettings>(file.fileName());
	defaultSettings->init();
	return defaultSettings;
}

void F1TelemetrySettings::reset()
{
	setValue(SettingsKeys::PORT, SettingsDefaultValues::PORT);
	setValue(SettingsKeys::SERVER, QString());
	setValue(SettingsKeys::THEME, SettingsDefaultValues::THEME);
	setValue(SettingsKeys::CUSTOM_THEME_USED, SettingsDefaultValues::CUSTOM_THEME_USED);
	setCustomTheme(SettingsDefaultValues::CUSTOM_THEME);

	ApplicationSettings::reset();
}

QChart::ChartTheme F1TelemetrySettings::theme() const
{
	return static_cast<QChart::ChartTheme>(value(SettingsKeys::THEME, SettingsDefaultValues::THEME).toInt());
}

void F1TelemetrySettings::setTheme(QChart::ChartTheme theme) { setValue(SettingsKeys::THEME, theme); }

int F1TelemetrySettings::port() const { return value(SettingsKeys::PORT, SettingsDefaultValues::PORT).toInt(); }
void F1TelemetrySettings::setPort(int value) { setValue(SettingsKeys::PORT, value); }

QString F1TelemetrySettings::server() const { return value(SettingsKeys::SERVER).toString(); }
void F1TelemetrySettings::setServer(const QString &address) { setValue(SettingsKeys::SERVER, address); }

int F1TelemetrySettings::forwardPort() const
{
	return value(SettingsKeys::FORWARD_PORT, SettingsDefaultValues::FORWARD_PORT).toInt();
}

void F1TelemetrySettings::setForwardPort(int value) { setValue(SettingsKeys::FORWARD_PORT, value); }

QString F1TelemetrySettings::forwardIp() const { return value(SettingsKeys::FORWARD_IP).toString(); }

void F1TelemetrySettings::setForwardIp(const QString &address) { setValue(SettingsKeys::FORWARD_IP, address); }

QString F1TelemetrySettings::myTeamName() const
{
	return value(SettingsKeys::MY_TEAM_NAME, SettingsDefaultValues::MY_TEAM_NAME).toString();
}

void F1TelemetrySettings::setMyTeamName(const QString &name) { setValue(SettingsKeys::MY_TEAM_NAME, name); }

unsigned int F1TelemetrySettings::driverMarkButton() const
{
	return value(SettingsKeys::MARK_BUTTON, SettingsDefaultValues::MARK_BUTTON).toUInt();
}

void F1TelemetrySettings::setDriverButton(unsigned int button) { setValue(SettingsKeys::MARK_BUTTON, button); }

QString F1TelemetrySettings::controllerType() const { return value(SettingsKeys::CONTROLLER_TYPE).toString(); }

void F1TelemetrySettings::setControllerType(const QString &type) { setValue(SettingsKeys::CONTROLLER_TYPE, type); }

int F1TelemetrySettings::nbVariablesPerPage()
{
	return value(SettingsKeys::VARIABLES_PER_PAGES, SettingsDefaultValues::VARIABLES_PER_PAGES).toInt();
}

void F1TelemetrySettings::setNbVariablesPerPage(int value) { setValue(SettingsKeys::VARIABLES_PER_PAGES, value); }

CustomTheme F1TelemetrySettings::customTheme() const
{
	auto theme = SettingsDefaultValues::CUSTOM_THEME;
	theme.backgroundColor = colorValue(SettingsKeys::CUSTOM_THEME_BACKGROUND_COLOR, theme.backgroundColor);
	theme.textColor = colorValue(SettingsKeys::CUSTOM_THEME_TEXT_COLOR, theme.textColor);
	theme.gridColor = colorValue(SettingsKeys::CUSTOM_THEME_GRID_COLOR, theme.gridColor);
	theme.seriesColors = colorListValue(SettingsKeys::CUSTOM_THEME_SERIES_COLORS, theme.seriesColors);
	return theme;
}

void F1TelemetrySettings::setCustomTheme(const CustomTheme &theme)
{
	setColorValue(SettingsKeys::CUSTOM_THEME_BACKGROUND_COLOR, theme.backgroundColor);
	setColorValue(SettingsKeys::CUSTOM_THEME_TEXT_COLOR, theme.textColor);
	setColorValue(SettingsKeys::CUSTOM_THEME_GRID_COLOR, theme.gridColor);
	setColorListValue(SettingsKeys::CUSTOM_THEME_SERIES_COLORS, theme.seriesColors);
}

bool F1TelemetrySettings::useCustomTheme() const
{
	return value(SettingsKeys::CUSTOM_THEME_USED, SettingsDefaultValues::CUSTOM_THEME_USED).toBool();
}

void F1TelemetrySettings::setUseCustomTheme(bool value) { setValue(SettingsKeys::CUSTOM_THEME_USED, value); }

int F1TelemetrySettings::linesWidth() const
{
	return value(SettingsKeys::LINE_WIDTH, SettingsDefaultValues::LINE_WIDTH).toInt();
}
void F1TelemetrySettings::setLinesWidth(int value) { setValue(SettingsKeys::LINE_WIDTH, value); }

int F1TelemetrySettings::selectedLinesWidth() const
{
	return value(SettingsKeys::SELECTED_LINE_WIDTH, SettingsDefaultValues::SELECTED_LINE_WIDTH).toInt();
}
void F1TelemetrySettings::setSelectedLinesWidth(int value) { setValue(SettingsKeys::SELECTED_LINE_WIDTH, value); }

QColor F1TelemetrySettings::cursorColor() const
{
	return colorValue(SettingsKeys::CURSOR_COLOR, SettingsDefaultValues::CURSOR_COLOR);
}

void F1TelemetrySettings::setCursorColor(const QColor &color) { setColorValue(SettingsKeys::CURSOR_COLOR, color); }

QVector<std::shared_ptr<PageSettings>> F1TelemetrySettings::pageSettings(const QString &type)
{
	QVector<std::shared_ptr<PageSettings>> pageSettings;

	auto size = m_settings.beginReadArray(QString(SettingsKeys::PAGE_SETTINGS) + "/" + type);
	for(auto i = 0; i < size; ++i) {
		m_settings.setArrayIndex(i);

		auto ps = std::make_shared<PageSettings>();
		ps->name = value(SettingsKeys::PAGE_NAME).toString();
		ps->detached = value(SettingsKeys::PAGE_DETACHED, SettingsDefaultValues::PAGE_DETACHED).toBool();
		ps->variables = value(SettingsKeys::PAGE_VARIABLES).toStringList();
		pageSettings << ps;
	}
	m_settings.endArray();

	return pageSettings;
}

void F1TelemetrySettings::setPageSettings(const QString &type,
										  const QVector<std::shared_ptr<PageSettings>> &pageSettings)
{
	m_settings.beginWriteArray(QString(SettingsKeys::PAGE_SETTINGS) + "/" + type);
	for(auto i = 0; i < pageSettings.count(); ++i) {
		m_settings.setArrayIndex(i);

		auto &ps = pageSettings[i];
		setValue(SettingsKeys::PAGE_NAME, ps->name);
		setValue(SettingsKeys::PAGE_DETACHED, ps->detached);
		setValue(SettingsKeys::PAGE_VARIABLES, ps->variables);
	}
	m_settings.endArray();
}

UnitsHandler F1TelemetrySettings::units() const
{
	UnitsHandler units;
	units.speed = value(SettingsKeys::SPEED_UNIT, SettingsDefaultValues::SPEED_UNIT).toString();
	return units;
}

void F1TelemetrySettings::setUnits(const UnitsHandler &units) { setValue(SettingsKeys::SPEED_UNIT, units.speed); }
