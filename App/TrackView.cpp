#include "TrackView.h"

#include <Core/Lap.h>
#include <Core/TrackInfo.h>
#include <Core/UdpDataCatalog.h>

#include <QWheelEvent>
#include <QtDebug>
#include <SideToolBar.h>

const constexpr double ZOOM_STEP = 1.05;
const constexpr double ZOOM_LOWER_LIMIT = 0.001;
const constexpr double ZOOM_UPPER_LIMIT = 100.0;

const constexpr int HOME_VIEW_MARGIN = 10;

TrackView::TrackView(QWidget *parent) : QGraphicsView(parent), _zoomScale(1.0)
{
	setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
	setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);

	setBackgroundBrush(Qt::gray);
	setResizeAnchor(QGraphicsView::AnchorUnderMouse);
	setTransformationAnchor(QGraphicsView::AnchorUnderMouse);
	setViewportUpdateMode(QGraphicsView::FullViewportUpdate);

	setDragMode(QGraphicsView::RubberBandDrag);
	setInteractive(true);

	connect(this, &QGraphicsView::rubberBandChanged, this, &TrackView::rubberBandChanged);

	setScene(new QGraphicsScene());

	_track = new TrackItem(Qt::gray);
	scene()->addItem(_track);

	_cursor = new QGraphicsEllipseItem(-1, -1, 2, 2);
	_cursor->setBrush(QBrush(Qt::red));
	QPen cursorPen;
	cursorPen.setWidth(0);
	cursorPen.setColor(Qt::red);
	_cursor->setPen(cursorPen);
	_cursor->setZValue(1000);

	auto subCursor = new QGraphicsEllipseItem(-5, -5, 10, 10, _cursor);
	subCursor->setBrush(QBrush(Qt::red));
	subCursor->setFlag(QGraphicsItem::ItemIgnoresTransformations, true);
	subCursor->setPen(cursorPen);
	subCursor->setZValue(1000);

	scene()->addItem(_cursor);

	grabGesture(Qt::PinchGesture);

	auto sizePolicy = QSizePolicy(QSizePolicy::Expanding, QSizePolicy::QSizePolicy::QSizePolicy::Expanding);
	sizePolicy.setVerticalStretch(1);
	setSizePolicy(sizePolicy);

	auto barWidget = new SideToolBar(this);
	barWidget->addContextMenuAction(this);
	barWidget->addToWidget(this);
}

bool TrackView::event(QEvent *event)
{
	if(event->type() == QEvent::Gesture) {
		auto ge = static_cast<QGestureEvent *>(event);
		if(auto pinch = static_cast<QPinchGesture *>(ge->gesture(Qt::PinchGesture))) {
			scaleViewBy(pinch->scaleFactor());
		}
	} else
		return QGraphicsView::event(event);

	return true;
}

void TrackView::wheelEvent(QWheelEvent *event)
{
	if(event->source() == Qt::MouseEventNotSynthesized) {
		if((event->angleDelta().y()) > 0)
			zoomIn();
		else
			zoomOut();
	}
}

void TrackView::mouseMoveEvent(QMouseEvent *event)
{
	if(!_panStartPos.isNull()) {
		auto panDistance = mapToScene(_panStartPos) - mapToScene(event->pos());
		centerOn(_panReferenceScenePos + panDistance);
	} else
		QGraphicsView::mouseMoveEvent(event);
}

void TrackView::mousePressEvent(QMouseEvent *event)
{
	if(event->button() == Qt::MiddleButton) {
		_panStartPos = event->pos();
		_panReferenceScenePos = visibleSceneRect().center();
	} else
		QGraphicsView::mousePressEvent(event);
}

void TrackView::mouseReleaseEvent(QMouseEvent *event)
{
	if(event->button() == Qt::MiddleButton) {
		_panStartPos = QPoint();
	}
	QGraphicsView::mouseReleaseEvent(event);
}

void TrackView::resizeEvent(QResizeEvent *event)
{
	QGraphicsView::resizeEvent(event);
	if(_isHome)
		home();
}

void TrackView::zoomIn() { scaleViewBy(ZOOM_STEP); }

void TrackView::zoomOut() { scaleViewBy(1 / ZOOM_STEP); }

void TrackView::scaleViewBy(qreal scaleFactor)
{
	qreal nextFactor = calculateScaleFactor(transform().scale(scaleFactor, scaleFactor));
	if(nextFactor < ZOOM_LOWER_LIMIT || nextFactor > ZOOM_UPPER_LIMIT)
		return;

	scale(scaleFactor, scaleFactor);
	_zoomScale = nextFactor;
	_isHome = false;
	//	qInfo() << "F" << _zoomScale;
}

void TrackView::scaleViewTo(qreal absoluteScaleFactor)
{
	qreal scale = absoluteScaleFactor / _zoomScale;
	scaleViewBy(scale);
}

QRectF TrackView::visibleSceneRect() const { return mapToScene(viewport()->geometry()).boundingRect(); }

void TrackView::home()
{
	if(!_laps.isEmpty()) {
		auto homeRect = _laps.value(0)->boundingRect();
		auto margin = HOME_VIEW_MARGIN;
		homeRect.adjust(-margin, -margin, margin, margin);
		QTimer::singleShot(0, [this, homeRect]() { fitInView(homeRect, Qt::KeepAspectRatio); });
	}

	_isHome = true;
}

qreal TrackView::calculateScaleFactor(const QTransform &matrix)
{
	qreal factor;
	matrix.map(1.0, 1.0, &factor, &factor);
	return factor;
}

void TrackView::rubberBandChanged(QRect rubberBandRect, QPointF fromScenePoint, QPointF toScenePoint)
{
	if(!fromScenePoint.isNull() && !toScenePoint.isNull() && !rubberBandRect.isNull()) {
		_rubberStart = fromScenePoint;
		_rubberEnd = toScenePoint;
	} else if(_rubberStart != _rubberEnd) {
		auto totalSize = mapToScene(width(), height()) - mapToScene(0, 0);
		auto widthFactor = totalSize.x() / qAbs(_rubberStart.x() - _rubberEnd.x());
		auto heightFactor = totalSize.y() / qAbs(_rubberStart.y() - _rubberEnd.y());
		scaleViewBy(qMin(widthFactor, heightFactor));

		auto rubberMid = (_rubberStart + _rubberEnd) / 2.0;
		centerOn(rubberMid);

		_rubberStart = QPointF();
		_rubberEnd = QPointF();
	}
}

qreal TrackView::getScaleFactor() const { return _zoomScale; }

QString TrackView::name() const { return "Track Position"; }

QWidget *TrackView::widget() { return this; }

void TrackView::addTelemetryData(const QVector<TelemetryData *> &telemetry)
{
	for(auto data : telemetry) {
		auto lap = dynamic_cast<Lap *>(data);
		auto item = new LapItem(lap, Qt::black);
		scene()->addItem(item);
		_laps << item;
	}

	auto itemsRect = scene()->itemsBoundingRect();
	auto extW = itemsRect.width() / 2.0;
	auto extH = itemsRect.height() / 2.0;
	itemsRect.adjust(-extW, -extH, extW, extH);
	setSceneRect(itemsRect);
}

void TrackView::removeTelemetryData(int index)
{
	auto lapItem = _laps.value(index);
	scene()->removeItem(lapItem);
	delete lapItem;
	_laps.removeOne(lapItem);
}

void TrackView::clear()
{
	for(auto lap : std::as_const(_laps)) {
		scene()->removeItem(lap);
	}
	_laps.clear();
	_track->setTrackIndex(-1);
}

void TrackView::setTelemetryVisibility(const QVector<bool> &visibility)
{
	int i = 0;
	for(const auto &item : _laps) {
		item->setVisible(visibility.value(i));
		++i;
	}
}

void TrackView::setColors(const QList<QColor> &colors)
{
	int i = 0;
	for(const auto &item : _laps) {
		item->setColor(colors.value(i));
		++i;
	}
}

void TrackView::setTheme(QChart::ChartTheme theme)
{
	QChart chart;
	chart.setTheme(theme);

	setBackgroundBrush(chart.backgroundBrush());
}

void TrackView::setCustomTheme(const CustomTheme &theme) { setBackgroundBrush(QBrush(theme.backgroundColor)); }

void TrackView::setCursor(double value)
{
	if(!_laps.isEmpty()) {
		auto cursorInView = visibleSceneRect().contains(_cursor->pos());
		auto lap = _laps.first();
		auto point = lap->lap()->mapPointAt(value);
		_cursor->setPos(point.position);

		if(cursorInView) {
			auto maring = _isHome ? 0 : 50;
			ensureVisible(_cursor, maring, maring);
		}
	}
}

void TrackView::setCursorColor(const QColor &color) { _cursor->setBrush(QBrush(color)); }

void TrackView::setCursorVisible(bool value) { _cursor->setVisible(value); }


LapItem::LapItem(Lap *lap, const QColor &color, QGraphicsItem *parent) : QGraphicsPathItem(parent), _lap(lap)
{
	if(!lap->mapsData().isEmpty()) {
		setColor(color);
		setBrush(QBrush());
		QPainterPath path;
		path.moveTo(lap->mapsData().first().position);
		for(const auto &point : lap->mapsData()) {
			path.lineTo(point.position);
		}
		//	path.closeSubpath();

		setPath(path);
	}
}

void LapItem::setColor(const QColor &color)
{
	QPen pen;
	pen.setWidth(2); // 2m wide
	pen.setColor(color);
	pen.setJoinStyle(Qt::RoundJoin);
	setPen(pen);
}

TrackItem::TrackItem(const QColor &color, QGraphicsItem *parent) : QGraphicsPathItem(parent)
{
	QPen pen;
	pen.setWidth(0);
	pen.setColor(color);
	setPen(pen);

	setBrush(QBrush(color.lighter()));
}

void TrackItem::setTrackIndex(int trackIndex)
{
	track = trackIndex;
	auto trackInfo = UdpDataCatalog::trackInfo(trackIndex);

	if(!trackInfo->leftLine.isEmpty() && !trackInfo->rightLine.isEmpty()) {
		QPainterPath path;

		path.moveTo(trackInfo->leftLine.first().position);
		for(const auto &point : std::as_const(trackInfo->leftLine)) {
			path.lineTo(point.position);
		}
		path.closeSubpath();

		path.moveTo(trackInfo->rightLine.first().position);
		for(const auto &point : std::as_const(trackInfo->rightLine)) {
			path.lineTo(point.position);
		}
		path.closeSubpath();

		setPath(path);
	} else {
		setPath(QPainterPath());
	}
}

int TrackItem::trackIndex() const { return track; }


void TrackView::setTelemetryDataDiff(int index, bool diff)
{
	Q_UNUSED(index)
	Q_UNUSED(diff)
}

void TrackView::setReference(const TelemetryData *data) { Q_UNUSED(data) }


void TrackView::setTrackIndex(int trackIndex) { _track->setTrackIndex(trackIndex); }

int TrackView::leftMargin() { return 0; }

void TrackView::setLeftMargin(int value) { Q_UNUSED(value) }

int TrackView::strecthFactor() { return 2; }

void TrackView::highlight(int index) { Q_UNUSED(index) }

void TrackView::setZoomMode(QChartView::RubberBand zoomMode) { Q_UNUSED(zoomMode) }
