#include "PreferencesDialog.h"
#include "F1TelemetrySettings.h"
#include "ui_PreferencesDialog.h"

#include "Core/UdpSpecification.h"
#include "Core/Units.h"

#include <Core/UdpDataCatalog.h>

const QVector<Button> ACCEPTED_BUTTONS = {
	Button::NoButton, Button::CrossA, Button::TriangleY, Button::CircleB,        Button::SquareX,        Button::L1,
	Button::R1,       Button::L2,     Button::R2,        Button::LeftStickClick, Button::RightStickCLick};

PreferencesDialog::PreferencesDialog(QWidget *parent) : QDialog(parent), ui(new Ui::PreferencesDialog)
{
	ui->setupUi(this);
	populateButtons();
	ui->cbSpeedUnits->addItems(Units::speedUnits());

	connect(ui->cbControllerType, qOverload<int>(&QComboBox::currentIndexChanged), this,
			&PreferencesDialog::populateButtons);
	connect(ui->btnTest, &QPushButton::clicked, this, &PreferencesDialog::test);

	F1TelemetrySettings settings;
	ui->leMyTeam->setText(settings.myTeamName());

	auto controllerType = settings.controllerType();
	if(!controllerType.isEmpty()) {
		ui->cbControllerType->setCurrentText(controllerType);
	}

	auto button = settings.driverMarkButton();
	auto foundIndex = ui->cbButton->findData(button);
	if(foundIndex >= 0) {
		ui->cbButton->setCurrentIndex(foundIndex);
	}

	ui->spVariablesPerPages->setValue(settings.nbVariablesPerPage());

	auto units = settings.units();
	auto speedunitIndex = ui->cbSpeedUnits->findText(units.speed);
	if(speedunitIndex >= 0) {
		ui->cbSpeedUnits->setCurrentIndex(speedunitIndex);
	}
}

PreferencesDialog::~PreferencesDialog() { delete ui; }

void PreferencesDialog::accept()
{
	F1TelemetrySettings settings;
	settings.setMyTeamName(ui->leMyTeam->text());
	settings.setDriverButton(ui->cbButton->currentData().toUInt());
	settings.setControllerType(ui->cbControllerType->currentText());
	settings.setNbVariablesPerPage(ui->spVariablesPerPages->value());

	UnitsHandler units;
	units.speed = ui->cbSpeedUnits->currentText();
	settings.setUnits(units);

	QDialog::accept();
}

void PreferencesDialog::populateButtons()
{
	auto oldIndex = ui->cbButton->currentIndex();
	ui->cbButton->clear();
	auto isPs4 = ui->cbControllerType->currentText() == "PS4";
	for(const auto &button : ACCEPTED_BUTTONS) {
		auto text = isPs4 ? UdpDataCatalog::buttonNamePs4(button) : UdpDataCatalog::buttonNameXbox(button);
		ui->cbButton->addItem(text, button);
	}
	ui->cbButton->setCurrentIndex(oldIndex);
}

void PreferencesDialog::test()
{
	accept();
	emit testButtons();
}
