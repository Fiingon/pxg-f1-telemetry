#ifndef COMPARERACEWIDGET_H
#define COMPARERACEWIDGET_H

#include "CompareTelemetryWidget.h"

#include <QTreeWidget>

class Race;

class CompareRaceWidget : public CompareTelemetryWidget
{
	Q_OBJECT

  signals:
	void askOpenStints(const QStringList &filename) const;

  public:
	CompareRaceWidget(QWidget *parent = nullptr);
	virtual ~CompareRaceWidget() override {}

  public slots:
	void browseData() override;
	void openDataFiles(const QStringList &filenames) override;

  protected:
	void fillInfoTree(InfoTreeWidget *tree, const TelemetryData *data) override;
};

#endif // COMPARERACEWIDGET_H
