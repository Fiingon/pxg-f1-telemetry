#ifndef COMPARETELEMETRYWIDGET_H
#define COMPARETELEMETRYWIDGET_H

#include "CompareTelemetryPage.h"
#include "Core/Lap.h"
#include "CustomTheme.h"

#include <QAbstractSeries>
#include <QChartView>
#include <QCheckBox>
#include <QLabel>
#include <QLineSeries>
#include <QRadioButton>
#include <QScatterSeries>
#include <QSignalMapper>
#include <QToolBar>
#include <QTreeWidget>
#include <QWidget>
#include <TelemetryChartView.h>

namespace Ui
{
class CompareTelemetryWidget;
}

class F1TelemetrySettings;
class TelemetryDataTableModel;
class TelemetryChartView;
struct TelemetryStaticInfo;
class CompareTelemetryPage;
class CompareWidgetInterface;
class InfoTreeWidget;

class CompareTelemetryWidget : public QWidget
{
    Q_OBJECT

  public:
    explicit CompareTelemetryWidget(const QString &unitX, const QString &xLabelFormat, QWidget *parent = nullptr);
    virtual ~CompareTelemetryWidget() override;

    void addTelemetryData(QVector<TelemetryData *> telemetry);

    void saveSettings(F1TelemetrySettings *settings);
    void loadSettings(F1TelemetrySettings *settings);

    void setDataName(const QString &name);

    void setTrackIndex(int trackIndex);

    void setTheme(QChart::ChartTheme theme);
    void setCustomTheme(const CustomTheme &theme);

  private:
    Ui::CompareTelemetryWidget *ui;
    QList<CompareTelemetryPage *> _pages;
    QVector<std::shared_ptr<PageSettings>> _pageSettings;
    QList<CompareWidgetInterface *> _customCompareWidgets;
    QList<TelemetryChartView *> _variablesCharts;
    QList<CompareWidgetInterface *> _allVariablesWidgets;
    QString _dataName;
    QString _unitX;
    QString _xLabelFormat;
    QChart::ChartTheme _theme = QChart::ChartThemeLight;
    CustomTheme _customTheme;
    TelemetryDataTableModel *_telemetryDataModel;
    QList<QCheckBox *> _variableCheckboxes;
    QList<QCheckBox *> _pageCheckboxes;
    QVector<TelemetryInfo> _variables;
    QToolBar *_toolbar;
    QMenu *_telemetryContextMenu;
    int _trackIndex = -1;
    bool _selectionHighlighted = true;
    int _chartMargins = 0;
    QCheckBox *_cursorVisibleCheckbox;

    void initActions();

    void setTelemetry(const QVector<TelemetryData *> &telemetry);
    void setTelemetryVisibility(const QVector<bool> &visibility);
    void clearVariables();
    void createVariables(const QVector<TelemetryInfo> &variables);
    void createPages();

    int nbDigit(int num) const;
    int ceilToDigit(int num, int roundFactor = 2) const;
    int floorToDigit(int num, int roundFactor = 2) const;

    void alignCharts();

    void highlight(int lapIndex);
    void refreshHighlighting();

    QList<QColor> themeColors(QChart::ChartTheme theme) const;

    void updateTabVisibility();

    void addToPage(CompareWidgetInterface *widget, bool forceNewPage = false);
    void removeFromPage(CompareWidgetInterface *widget);

    CompareTelemetryPage *createPage(const QString &name, bool detached);
    CompareTelemetryPage *pageFromCompareWidget(CompareWidgetInterface *widget);
    CompareTelemetryPage *pageFromName(const QString &name);

    std::shared_ptr<PageSettings> createPageSettings(CompareTelemetryPage *page);
    void saveSettingsFromPage(const std::shared_ptr<PageSettings> &pageSettings, CompareTelemetryPage *page);
    std::shared_ptr<PageSettings> pageSettingsForVariable(const QString &varName) const;
    std::shared_ptr<PageSettings> pageSettingsForPageName(const QString &pageName) const;

    QString findUniquePageName(const QString &name) const;

    void detachPage(bool value);
    void renamePage();
    void savePage();
    void deleteSavedPage();
    void pageClosed();
    void chartMenuRequested(const QPoint &pos);
    void savePageSettingsToDisk();
    void updatePagesZoomMode(QChartView::RubberBand zoomMode);

    void moveGraph(CompareWidgetInterface *widget, int nb);
    bool canMoveGraph(CompareWidgetInterface *widget, int nb);

    void sendToPage(CompareWidgetInterface *widget, CompareTelemetryPage *page = nullptr);
    void removeFromPageSettings(CompareWidgetInterface *widget);


  public slots:
    virtual void browseData() {}
    virtual void openDataFiles(const QStringList &filenames) { Q_UNUSED(filenames); }
    virtual void clearData();
    void updateData();

  protected:
    virtual void fillInfoTree(InfoTreeWidget *tree, const TelemetryData *data)
    {
        Q_UNUSED(tree)
        Q_UNUSED(data)
    }

    virtual bool showTrackTurns() const { return false; }

    QTreeWidgetItem *speedItem(QTreeWidget *tree, const Lap *lap) const;
    QTreeWidgetItem *setupItem(QTreeWidget *tree, const Lap *lap) const;
    QTreeWidgetItem *tyreTempItem(QTreeWidget *tree, const Lap *lap) const;
    QTreeWidgetItem *tyreItem(QTreeWidget *tree, const Lap *lap, double divisor = 1.0) const;
    QTreeWidgetItem *tyreSetItem(QTreeWidget *tree, const Lap *lap) const;
    QTreeWidgetItem *recordItem(QTreeWidget *tree, const Lap *lap) const;
    QTreeWidgetItem *flashbackItem(QTreeWidget *tree, const Lap *lap) const;
    QTreeWidgetItem *driverItem(QTreeWidget *tree, const Lap *lap) const;
    QTreeWidgetItem *trackItem(QTreeWidget *tree, const Lap *lap) const;
    QTreeWidgetItem *weatherItem(QTreeWidget *tree, const Lap *lap) const;
    QTreeWidgetItem *tyreCompoundItem(QTreeWidget *tree, const Lap *lap) const;
    QTreeWidgetItem *eventsItem(QTreeWidget *tree, const Lap *lap) const;
    QTreeWidgetItem *coastingItem(QTreeWidget *tree, const Lap *lap) const;
    QTreeWidgetItem *carDamageItem(QTreeWidget *tree, const Lap *lap) const;
    QTreeWidgetItem *engineDamageItem(QTreeWidget *tree, const Lap *lap) const;
    QTreeWidgetItem *carDamageDiffItem(QTreeWidget *tree, const Lap *lap) const;
    QTreeWidgetItem *engineDamageDiffItem(QTreeWidget *tree, const Lap *lap) const;


    bool eventFilter(QObject *obj, QEvent *event) override;

    static QString cursorSliderStylesheet();

    void addCustomCompareWidget(CompareWidgetInterface *compareWidgets);
    int existingFileIndex(const QString &filename) const;
    void setCurrentDataIndex(int index);

  private slots:
    void chartDisplayModeChanged(TelemetryChartView::DisplayMode mode, bool value, TelemetryChartView *chartView);
    void changeVariableDiff(bool value, TelemetryChartView *chartView);
    void changeStats(bool value, TelemetryChartView *chartView);

    void updateDataVisibilities();
    void variableChecked(bool value);
    void pageChecked(bool value);
    void uncheckAllVariables();
    void distanceZoomChanged(qreal min, qreal max);
    void telemetryDataSelected(const QModelIndex &current, const QModelIndex &previous);
    void telemetryTableContextMenu(const QPoint &pos);
    void changeReferenceData();
    void removeData();
    void showTrackLayout(bool value);
    void showTelemetryInfo(bool value);
    void changeColor();
    void exportData();
    void exportAllData();
    void updateCursors(bool allowMovingView = true);
    void updateCursorWidget();
    void updateWidgetsVisibilities();
    void moveViewsToCursor();
    void moveCursorToView();
    void updateCursorsColor();
    void setUnits();

    void setColors(const QList<QColor> &colors);
    void home();
    void help();

    void setCursor(double value);
    void setCursorColor(const QColor &color);
    void setCursorVisible(bool value);
};

#endif // COMPARETELEMETRYWIDGET_H
