#include "TrackLineBuilderDialog.h"
#include "ui_TrackLineBuilderDialog.h"

#include <QButtonGroup>
#include <QFileDialog>

#include <Core/TrackInfo.h>
#include <Core/UdpDataCatalog.h>

TrackLineBuilderDialog::TrackLineBuilderDialog(QWidget *parent) : QDialog(parent), ui(new Ui::TrackLineBuilderDialog)
{
	ui->setupUi(this);

	connect(ui->btnBrowse, &QToolButton::clicked, this, &TrackLineBuilderDialog::browse);

	defaultOffsets[ui->rbLeft] = 100.0;
	defaultOffsets[ui->rbCenter] = 0.0;
	defaultOffsets[ui->rbRight] = -100.0;

	rbButtonGroup = new QButtonGroup(this);
	rbButtonGroup->addButton(ui->rbLeft);
	rbButtonGroup->addButton(ui->rbRight);
	rbButtonGroup->addButton(ui->rbCenter);
	connect(rbButtonGroup, qOverload<QAbstractButton *>(&QButtonGroup::buttonClicked), this,
			&TrackLineBuilderDialog::buttonChanged);
}

TrackLineBuilderDialog::~TrackLineBuilderDialog() { delete ui; }

void TrackLineBuilderDialog::accept()
{
	if(!QFile::exists(ui->leLap->text())) {
		return;
	}

	auto lap = Lap::fromFile(ui->leLap->text());

	QString defaultName = UdpDataCatalog::trackInfo(lap->track)->name;
	defaultName += '_';
	defaultName += rbButtonGroup->checkedButton()->text();
	defaultName += ".line";
	auto outputFilename = QFileDialog::getSaveFileName(this, "Output line file", defaultName, "*.line", nullptr,
													   QFileDialog::DontUseNativeDialog);
	if(outputFilename.isEmpty()) {
		return;
	}

	TrackInfo::writeLineFromLap(outputFilename, lap, ui->spOffset->value() / 100.0);
	QDialog::accept();
}

void TrackLineBuilderDialog::browse()
{
	auto filename = QFileDialog::getOpenFileName(this, "Select a lap", QString(), "*.f1lap", nullptr,
												 QFileDialog::DontUseNativeDialog);
	if(!filename.isEmpty()) {
		ui->leLap->setText(filename);
	}
}

void TrackLineBuilderDialog::buttonChanged(QAbstractButton *button)
{
	ui->spOffset->setValue(defaultOffsets.value(button));
}
