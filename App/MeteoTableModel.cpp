#include "MeteoTableModel.h"

#include <QFont>
#include <QIcon>
#include <QPixmap>
#include <QPixmapCache>
#include <QtDebug>

const QSize DEFAULT_IMAGE_SIZE = QSize(100, 100);


MeteoTableModel::MeteoTableModel(QObject *parent) : QAbstractTableModel(parent)
{
	QVector<QString> meteoIcons = {":/meteo/clear",     ":/meteo/cloud",     ":/meteo/overcast",
								   ":/meteo/lightRain", ":/meteo/heavyRain", ":/meteo/storm"};

	for(const auto &icon : meteoIcons) {
		pixmaps << QPixmap(icon).scaledToWidth(DEFAULT_IMAGE_SIZE.width(), Qt::SmoothTransformation);
	}
}


int MeteoTableModel::rowCount(const QModelIndex &parent) const { return 5; }

int MeteoTableModel::columnCount(const QModelIndex &parent) const { return _forecast.count(); }

QVariant MeteoTableModel::data(const QModelIndex &index, int role) const
{
	QVariant value;
	if(index.isValid()) {
		auto forecast = _forecast.value(index.column());
		if(role == Qt::DisplayRole) {
			switch(index.row()) {
				case 0:
					if(forecast.m_timeOffset == 0) {
						value = "Now";
					} else {
						value = QString("+%1 min").arg(forecast.m_timeOffset);
					}
					break;
				case 2:
					value = QString("Air: %1°C").arg(forecast.m_airTemperature);
					break;
				case 3:
					value = QString("Track: %1°C").arg(forecast.m_trackTemperature);
					break;
				case 4:
					value = QString("Rain: %1%").arg(forecast.m_rainPercentage);
					break;
			}
		} else if(role == Qt::TextAlignmentRole) {
			return Qt::AlignCenter;
		} else if(role == Qt::DecorationRole && index.row() == 1) {
			return pixmaps.value(forecast.m_weather);
		} else if(role == Qt::SizeHintRole && index.row() == 1) {
			return DEFAULT_IMAGE_SIZE;
		}
	}
	return value;
}

void MeteoTableModel::setForecast(const QVector<WeatherForecastSample> &forecast)
{
	if(forecast != _forecast) {
		beginResetModel();
		_forecast = forecast;
		endResetModel();
	}
}
