#include "TelemetryTableWidget.h"
#include "ui_TelemetryTableWidget.h"

#include <Core/TelemetryData.h>
#include <Core/TelemetryDefinitions.h>

#include <QClipboard>
#include <QHeaderView>
#include <QScrollBar>
#include <QTimer>

constexpr int MAX_VISIBLE_ROWS = 8;

TelemetryTableModel::TelemetryTableModel(QObject *parent) : QAbstractTableModel(parent) {}

void TelemetryTableModel::setTelemetryData(QVector<TelemetryData *> telemetry)
{
    beginResetModel();
    _telemetry = telemetry;
    endResetModel();
}

void TelemetryTableModel::clearTelemetryData()
{
    beginResetModel();
    _telemetry.clear();
    _dataIndexes.clear();
    _reference = nullptr;
    endResetModel();
}

void TelemetryTableModel::setDisplayIndexes(const QList<int> &indexes)
{
    beginResetModel();
    _dataIndexes = indexes;
    endResetModel();
}

void TelemetryTableModel::setCursorValue(double value)
{
    _cursorValue = value;
    emit dataChanged(QModelIndex(), QModelIndex());
}

void TelemetryTableModel::setColors(const QList<QColor> &colors)
{
    _colors = colors;
    emit dataChanged(QModelIndex(), QModelIndex());
}

int TelemetryTableModel::rowCount(const QModelIndex &parent) const
{
    Q_UNUSED(parent)
    return _dataIndexes.count();
}

int TelemetryTableModel::columnCount(const QModelIndex &parent) const
{
    Q_UNUSED(parent)
    return _telemetry.count();
}

QVariant TelemetryTableModel::data(const QModelIndex &index, int role) const
{
    if(!index.isValid()) {
        return QVariant();
    }

    auto dataIndex = _dataIndexes.value(index.row());
    auto data = _telemetry.value(index.column());
    auto info = data->availableData().value(dataIndex);
    auto staticInfo = TelemetryDefinitions::staticInfo(data->availableData().value(dataIndex).name);

    if(!info.isValid()) {
        return QVariant();
    }

    switch(role) {
        case Qt::DisplayRole: {
            auto value = data->data(_cursorValue, dataIndex);
            if(std::isnan(value)) {
                return QString();
            }
            auto diff = _diffs.value(dataIndex);
            if(diff && _reference) {
                auto refValue = _reference->data(_cursorValue, dataIndex);
                value -= refValue;
            }
            if(staticInfo.customValues.isEmpty()) {
                return QString::number(value).append(info.unit);
            } else {
                return staticInfo.customValues.value(int(value));
            }
        }
        case Qt::TextAlignmentRole:
            return Qt::AlignCenter;
        case Qt::ForegroundRole:
            return _colors.value(index.column());
        default:
            break;
    }

    return QVariant();
}

QVariant TelemetryTableModel::headerData(int section, Qt::Orientation orientation, int role) const
{
    if(role == Qt::DisplayRole) {
        if(orientation == Qt::Horizontal) {
            auto data = _telemetry.value(section);
            return data->description();
        } else {
            auto dataIndex = _dataIndexes.value(section);
            auto data = _telemetry.value(0);
            if(data) {
                auto info = data->availableData().value(dataIndex);
                return info.name;
            }
        }
    } else if(role == Qt::FontRole && orientation == Qt::Horizontal) {
        auto font = QFont();
        font.setPointSize(9);
        return font;
    }

    return QVariant();
}

Qt::ItemFlags TelemetryTableModel::flags(const QModelIndex &index) const
{
    Q_UNUSED(index)
    return Qt::ItemIsEnabled | Qt::ItemIsSelectable;
}

void TelemetryTableModel::setTelemetryDataDiff(int index, bool diff) { _diffs[index] = diff; }

void TelemetryTableModel::setReference(const TelemetryData *data) { _reference = data; }

// ####################################################################

TelemetryTableWidget::TelemetryTableWidget(QWidget *parent) : QWidget(parent), ui(new Ui::TelemetryTableWidget)
{
    ui->setupUi(this);
    _model = new TelemetryTableModel(this);
    ui->tableView->setModel(_model);

    ui->tableView->horizontalHeader()->setSectionResizeMode(QHeaderView::ResizeToContents);
    ui->tableView->verticalHeader()->setSectionResizeMode(QHeaderView::Fixed);

    auto fm = QFontMetrics(font());
    ui->tableView->verticalHeader()->setDefaultSectionSize(fm.height() + 2);

    auto copyAction = new QAction();
    copyAction->setShortcut(QKeySequence::Copy);
    copyAction->setText(QString("Copy Selection (%1)").arg(copyAction->shortcut().toString()));
    connect(copyAction, &QAction::triggered, this, &TelemetryTableWidget::copySelection);
    ui->tableView->addAction(copyAction);
    ui->tableView->setContextMenuPolicy(Qt::ActionsContextMenu);
}

TelemetryTableWidget::~TelemetryTableWidget() { delete ui; }

void TelemetryTableWidget::setDisplayIndex(int index, bool visible)
{
    if(visible) {
        _displayedIndexes.append(index);
    } else {
        _displayedIndexes.removeAll(index);
    }

    _model->setDisplayIndexes(_displayedIndexes);
    QTimer::singleShot(0, this, &TelemetryTableWidget::updateHeight);
}

QString TelemetryTableWidget::name() const { return "Table"; }

QWidget *TelemetryTableWidget::widget() { return this; }

void TelemetryTableWidget::home()
{
    // Pass : nothing to do
}

void TelemetryTableWidget::addTelemetryData(const QVector<TelemetryData *> &telemetry)
{
    _allTelemetry.append(telemetry);
    updateTelemetryModel();
}

void TelemetryTableWidget::removeTelemetryData(int index)
{
    _allTelemetry.removeAt(index);
    updateTelemetryModel();
}

void TelemetryTableWidget::clear()
{
    _allTelemetry.clear();
    _visibilities.clear();
    _displayedIndexes.clear();
    _model->clearTelemetryData();
}

void TelemetryTableWidget::setTelemetryVisibility(const QVector<bool> &visibility)
{
    _visibilities = visibility;
    updateTelemetryModel();
}

void TelemetryTableWidget::setColors(const QList<QColor> &colors) { _model->setColors(colors); }

void TelemetryTableWidget::setTheme(QChart::ChartTheme theme)
{
    Q_UNUSED(theme)
    // Pass : nothing to do
}

void TelemetryTableWidget::setCustomTheme(const CustomTheme &theme)
{
    Q_UNUSED(theme)
    // Pass : nothing to do
}

void TelemetryTableWidget::setCursor(double value) { _model->setCursorValue(value); }

void TelemetryTableWidget::setCursorColor(const QColor &color)
{
    Q_UNUSED(color)
    // Pass : nothing to do
}

void TelemetryTableWidget::setCursorVisible(bool value) { setVisible(value); }

void TelemetryTableWidget::setTelemetryDataDiff(int index, bool diff) { _model->setTelemetryDataDiff(index, diff); }

void TelemetryTableWidget::updateTelemetryModel()
{
    QVector<TelemetryData *> visibleData;
    auto itVisibility = _visibilities.constBegin();
    for(const auto &data : std::as_const(_allTelemetry)) {
        if(*itVisibility) {
            visibleData << data;
        }
        ++itVisibility;
    }

    _model->setTelemetryData(visibleData);
    QTimer::singleShot(0, this, &TelemetryTableWidget::updateHeight);
}

void TelemetryTableWidget::updateHeight()
{
    if(_limitedViewMode) {
        auto minRowsSize =
            qMin(_model->rowCount(), MAX_VISIBLE_ROWS) * ui->tableView->verticalHeader()->defaultSectionSize();
        auto totalMinSize = minRowsSize + ui->tableView->horizontalHeader()->size().height() + 5;
        if(ui->tableView->horizontalScrollBar()->isVisible())
            totalMinSize += ui->tableView->horizontalScrollBar()->height();
        setMaximumHeight(totalMinSize);
        setMinimumHeight(totalMinSize);
    } else if(minimumHeight() > 0) {
        setMaximumHeight(std::numeric_limits<int>::max());
        setMinimumHeight(0);
    }
}

void TelemetryTableWidget::copySelection()
{
    QString text;
    for(int col = 0; col < _model->columnCount(); ++col) {
        text += _model->headerData(col, Qt::Horizontal, Qt::DisplayRole).toString();
        if(col < _model->columnCount())
            text += '\t';
    }

    auto selectedIndexes = ui->tableView->selectionModel()->selectedIndexes();
    for(int row = 0; row < _model->rowCount(); ++row) {
        text += _model->headerData(row, Qt::Vertical, Qt::DisplayRole).toString();
        text += '\t';
        for(int col = 0; col < _model->columnCount(); ++col) {
            auto index = _model->index(row, col);
            if(selectedIndexes.contains(index)) {
                text += _model->data(index, Qt::DisplayRole).toString();
            }
            if(col < _model->columnCount())
                text += '\t';
        }

        if(row < _model->columnCount())
            text += '\n';
    }

    qApp->clipboard()->setText(text);
}


void TelemetryTableWidget::setReference(const TelemetryData *data) { _model->setReference(data); }

void TelemetryTableWidget::setTrackIndex(int trackIndex) { Q_UNUSED(trackIndex) }

int TelemetryTableWidget::leftMargin() { return 0; }

void TelemetryTableWidget::setLeftMargin(int value) { Q_UNUSED(value) }

int TelemetryTableWidget::strecthFactor() { return 1; }

void TelemetryTableWidget::highlight(int index) { Q_UNUSED(index) }

void TelemetryTableWidget::setZoomMode(QChartView::RubberBand zoomMode) { Q_UNUSED(zoomMode) }

void TelemetryTableWidget::setLimitedViewMode(bool limitedViewMode)
{
    _limitedViewMode = limitedViewMode;
    updateHeight();
}
