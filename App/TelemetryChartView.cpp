#include "TelemetryChartView.h"
#include "Core/TelemetryDefinitions.h"
#include "Core/TrackInfo.h"
#include "Core/Units.h"
#include "TelemetryChart.h"
#include <Core/UdpDataCatalog.h>

#include <CustomTheme.h>
#include <F1TelemetrySettings.h>
#include <QButtonGroup>
#include <QGestureEvent>
#include <QGraphicsProxyWidget>
#include <QMenu>
#include <QToolButton>
#include <QToolTip>
#include <QValueAxis>
#include <QtDebug>
#include <SideToolBar.h>
#include <QGraphicsLayout>
#include <QCategoryAxis>
#include <QApplication>
#include <QBarCategoryAxis>
#include <QLineSeries>
#include <QBoxPlotSeries>

const constexpr double FACTOR = 1.1;

const int TOP_WIDGET_Y = 2;

TelemetryChartView::TelemetryChartView(QChart *chart, const QString &varName, bool showTrackTurns, QWidget *parent)
: QChartView(chart, parent), _varName(varName), _showTrackTurns(showTrackTurns)
{
	chart->layout()->setContentsMargins(0, 0, 0, 0);


	chart->setBackgroundRoundness(0);
	setRubberBand(QChartView::RectangleRubberBand);
	_posLabel = new QLabel(this);
	_posLabel->setStyleSheet("color: black");
	_posLabel->hide();
	initMenuButton();

	updateLabelsPosition();

	grabGesture(Qt::PinchGesture);
	grabGesture(Qt::TapAndHoldGesture);

	_displayModesGroup = new QButtonGroup(this);
	connect(_displayModesGroup, qOverload<QAbstractButton *>(&QButtonGroup::buttonClicked), this,
			&TelemetryChartView::configChanged);
	_displayModesGroup->setExclusive(false);

	addDisplayModeConfig(DisplayMode::Diff, "Diff with reference lap");
	addDisplayModeConfig(DisplayMode::Stats, "Distribution");
	addDisplayModeConfig(DisplayMode::Smoothing, "Smoothing");

	_displayModes.value(DisplayMode::Smoothing)->hide();
}

void TelemetryChartView::addDisplayModeConfig(DisplayMode mode, const QString &text)
{
	auto checkbox = new QCheckBox(text);
	connect(checkbox, &QCheckBox::toggled, this,
			[this, mode](auto value) { emit displayModeChanged(mode, value, this); });
	_displayModes[mode] = checkbox;
	_displayModesGroup->addButton(checkbox);
	addConfigurationWidget(checkbox);
}

void TelemetryChartView::setHomeZoom()
{
	auto xAxis = static_cast<QValueAxis *>(chart()->axes(Qt::Horizontal)[0]);
	auto yAxis = static_cast<QValueAxis *>(chart()->axes(Qt::Vertical)[0]);
	xHome = qMakePair(xAxis->min(), xAxis->max());
	yHome = qMakePair(yAxis->min(), yAxis->max());
}

void TelemetryChartView::home()
{
	auto xAxis = static_cast<QValueAxis *>(chart()->axes(Qt::Horizontal)[0]);
	auto yAxis = static_cast<QValueAxis *>(chart()->axes(Qt::Vertical)[0]);
	xAxis->setRange(xHome.first, xHome.second);
	yAxis->setRange(yHome.first, yHome.second);
}

QRect TelemetryChartView::homeView() const
{
	return QRect(xHome.first, yHome.first, xHome.second - xHome.first, yHome.second - yHome.first);
}

void TelemetryChartView::scaleViewBy(double factor, const QPointF &zoomPoint)
{
	if(_zoomEnabled) {
		auto xAxis = static_cast<QValueAxis *>(chart()->axes(Qt::Horizontal)[0]);
		auto yAxis = static_cast<QValueAxis *>(chart()->axes(Qt::Vertical)[0]);

		auto zoomValue = chart()->mapToValue(zoomPoint);

		auto oldRect = QRectF(xAxis->min(), yAxis->min(), xAxis->max() - xAxis->min(), yAxis->max() - yAxis->min());
		auto newRect = oldRect;
		newRect.setWidth(oldRect.width() * factor);
		newRect.setHeight(oldRect.height() * factor);

		auto d = (zoomValue - oldRect.center());
		newRect.moveLeft(oldRect.center().x() - newRect.width() / 2.0);
		newRect.moveTop(oldRect.center().y() - newRect.height() / 2.0);
		newRect.translate(d - d * factor);

		xAxis->setRange(newRect.left(), newRect.right());
		yAxis->setRange(newRect.top(), newRect.bottom());
	}
}

void TelemetryChartView::scaleViewByOnCenter(double factor, bool onX, bool onY)
{
	if(_zoomEnabled) {
		auto xAxis = static_cast<QValueAxis *>(chart()->axes(Qt::Horizontal)[0]);
		auto yAxis = static_cast<QValueAxis *>(chart()->axes(Qt::Vertical)[0]);

		auto oldRect = QRectF(xAxis->min(), yAxis->min(), xAxis->max() - xAxis->min(), yAxis->max() - yAxis->min());
		auto newRect = oldRect;
		if(onX) {
			newRect.setWidth(oldRect.width() * factor);
			newRect.moveLeft(oldRect.center().x() - newRect.width() / 2.0);
		}
		if(onY) {
			newRect.setHeight(oldRect.height() * factor);
			newRect.moveTop(oldRect.center().y() - newRect.height() / 2.0);
		}

		xAxis->setRange(newRect.left(), newRect.right());
		yAxis->setRange(newRect.top(), newRect.bottom());
	}
}

void TelemetryChartView::wheelEvent(QWheelEvent *event)
{
	if(event->source() == Qt::MouseEventNotSynthesized) {
		auto zoomScale = event->angleDelta().y() < 0 ? FACTOR : 1.0 / FACTOR;
		auto zoomPoint = mapFromGlobal(event->globalPosition().toPoint());
		scaleViewBy(zoomScale, zoomPoint);
	}
}

bool TelemetryChartView::event(QEvent *event)
{
	if(event->type() == QEvent::Gesture) {
		auto ge = static_cast<QGestureEvent *>(event);
		if(auto pinch = static_cast<QPinchGesture *>(ge->gesture(Qt::PinchGesture))) {
			scaleViewBy(1.0 / pinch->scaleFactor(), pinch->centerPoint());
		}
	} else
		return QGraphicsView::event(event);

	return true;
}

void TelemetryChartView::mousePressEvent(QMouseEvent *event)
{
	if(event->button() == Qt::MiddleButton) {
        _panStartPos = event->position();

		auto xAxis = static_cast<QValueAxis *>(chart()->axes(Qt::Horizontal)[0]);
		auto yAxis = static_cast<QValueAxis *>(chart()->axes(Qt::Vertical)[0]);

		_panReferenceValue = QPointF(xAxis->min(), yAxis->min());

	} else
		QChartView::mousePressEvent(event);
}

void TelemetryChartView::mouseReleaseEvent(QMouseEvent *event)
{
	if(event->button() == Qt::MiddleButton) {
		_panStartPos = QPoint();
	}

	if(event->button() != Qt::RightButton) {
		QChartView::mouseReleaseEvent(event);
	}
}

void TelemetryChartView::mouseMoveEvent(QMouseEvent *event)
{
	if(!_panStartPos.isNull()) {
        auto panDistance = chart()->mapToValue(_panStartPos) - chart()->mapToValue(event->position());

		auto xAxis = static_cast<QValueAxis *>(chart()->axes(Qt::Horizontal)[0]);
		xAxis->setRange(_panReferenceValue.x() + panDistance.x(),
						_panReferenceValue.x() + panDistance.x() + (xAxis->max() - xAxis->min()));

		auto yAxis = static_cast<QValueAxis *>(chart()->axes(Qt::Vertical)[0]);
		yAxis->setRange(_panReferenceValue.y() + panDistance.y(),
						_panReferenceValue.y() + panDistance.y() + (yAxis->max() - yAxis->min()));
	}

	if(_posLabel->isVisible()) {
        auto chartValue = chart()->mapToValue(event->position());
		_posLabel->setText(QString::number(chartValue.x()) + _unitX + " / " + QString::number(chartValue.y()) + _unitY);
		_posLabel->resize(_posLabel->sizeHint());
		_posLabel->setStyleSheet(QString("QLabel{color: %1}").arg(chart()->titleBrush().color().name()));
		updateLabelsPosition();
		QChartView::mouseMoveEvent(event);
	}

	auto pickerSize = QPointF(10, 10);
    auto tl = chart()->mapToValue(event->position() - pickerSize);
    auto br = chart()->mapToValue(event->position() + pickerSize);
	_pickerRect.setTopLeft(tl);
	_pickerRect.setBottomRight(br);

	int eventListIndex = 0;
	bool found = false;
	for(const auto eventSeries : _eventsSeries) {
		const auto &eventPoints = eventSeries->points();
		int index = 0;
		for(const auto &point : eventPoints) {
			if(_pickerRect.contains(point)) {
				auto tooltip = _eventsTooltips.value(eventListIndex).value(index);
                QToolTip::showText(event->globalPosition().toPoint(), tooltip, this);
				found = true;
				break;
			}
			++index;
		}

		++eventListIndex;
	}

	if(!found) {
        QToolTip::showText(event->globalPosition().toPoint(), "");
	}
}

QRectF TelemetryChartView::visibleSceneRect() const { return mapToScene(viewport()->geometry()).boundingRect(); }

bool TelemetryChartView::isDisplayModeActivated(TelemetryChartView::DisplayMode mode) const
{
	auto checkbox = _displayModes.value(mode);
	if(checkbox) {
		return checkbox->isChecked();
	}

	return false;
}

void TelemetryChartView::resizeEvent(QResizeEvent *event)
{
	updateLabelsPosition();
	QChartView::resizeEvent(event);
}

void TelemetryChartView::updateLabelsPosition()
{
	if(_posLabel->isVisible()) {
		_posLabel->move(width() - _posLabel->sizeHint().width() - 50, TOP_WIDGET_Y);
	}
}

void TelemetryChartView::initMenuButton()
{
	auto barWidget = new SideToolBar(this);
	barWidget->addContextMenuAction(this);
	QSize s(20, 10);
	connect(barWidget->addAction("+", s), &QToolButton::clicked, this, &TelemetryChartView::zoomY);
	connect(barWidget->addAction("-", s, 0), &QToolButton::clicked, this, &TelemetryChartView::unzoomY);

	barWidget->addToWidget(this);
}

void TelemetryChartView::configChanged(QAbstractButton *buttonClicked)
{
	if(buttonClicked == _displayModes.value(DisplayMode::Stats)) {
		auto checked = buttonClicked->isChecked();
		_displayModes.value(DisplayMode::Diff)->setDisabled(checked);
		_displayModes.value(DisplayMode::Smoothing)->setDisabled(checked);
	}
}

void TelemetryChartView::zoomY() { scaleViewByOnCenter(1.0 / FACTOR, false, true); }

void TelemetryChartView::unzoomY() { scaleViewByOnCenter(FACTOR, false, true); }

bool TelemetryChartView::zoomEnabled() const { return _zoomEnabled; }

void TelemetryChartView::setZoomEnabled(bool zoomEnabled) { _zoomEnabled = zoomEnabled; }

void TelemetryChartView::addConfigurationWidget(QWidget *widget)
{
	auto proxy = new QGraphicsProxyWidget(chart());
	proxy->setWidget(widget);
	_configWidgets << widget;

	auto x = 12;
	for(auto i = 1; i < _configWidgets.count(); ++i) {
		x += _configWidgets[i - 1]->sizeHint().width();
	}

	proxy->setPos(QPoint(x, TOP_WIDGET_Y));
}

void TelemetryChartView::setUnitX(const QString &unitX) { _unitX = unitX; }

void TelemetryChartView::setPosLabelVisible(bool value) { _posLabel->setVisible(value); }

void TelemetryChartView::clearEvents()
{
	_eventsSeries.clear();
	_eventsTooltips.clear();
}

void TelemetryChartView::addEventSerie(QScatterSeries *series, const QStringList &tooltips)
{
	_eventsSeries << series;
	_eventsTooltips << tooltips;
}

TelemetryChart *TelemetryChartView::telemetryChart() const { return static_cast<TelemetryChart *>(chart()); }


QString TelemetryChartView::name() const { return _varName; }

QWidget *TelemetryChartView::widget() { return this; }

void TelemetryChartView::addTelemetryData(const QVector<TelemetryData *> &telemetry) {}

void TelemetryChartView::removeTelemetryData(int index) { telemetryChart()->removeTelemetrySeries(index); }

void TelemetryChartView::clear() {}

void TelemetryChartView::setTelemetryVisibility(const QVector<bool> &visibility)
{
	int varIndex = 0;
	for(const auto &visible : visibility) {
		telemetryChart()->setTelemetrySeriesVisible(varIndex, visible);
		++varIndex;
	}
}

void TelemetryChartView::setColors(const QList<QColor> &colors) {}

void TelemetryChartView::setTheme(QChart::ChartTheme theme) { chart()->setTheme(theme); }

void TelemetryChartView::setCustomTheme(const CustomTheme &theme) { theme.apply(chart()); }

void TelemetryChartView::setCursor(double value) { telemetryChart()->setCursor(value); }

void TelemetryChartView::setCursorColor(const QColor &color) { telemetryChart()->setCursorColor(color); }

void TelemetryChartView::setCursorVisible(bool value) { telemetryChart()->setCursorVisible(value); }

void TelemetryChartView::setTelemetryDataDiff(int index, bool diff) {}

void TelemetryChartView::setReference(const TelemetryData *data) { _reference = data; }

void TelemetryChartView::setTrackIndex(int trackIndex) { _trackIndex = trackIndex; }

int TelemetryChartView::leftMargin()
{
	auto yAxis = qobject_cast<QValueAxis *>(chart()->axes(Qt::Vertical)[0]);
	QString maxLabel;
	int maxSize = 0;
	auto catAxis = qobject_cast<QCategoryAxis *>(yAxis);
	if(catAxis) {
		auto fm = QFontMetrics(yAxis->labelsFont());
		const auto &labels = catAxis->categoriesLabels();
		for(const auto &label : labels) {
			auto size = fm.horizontalAdvance(label);
			if(size > maxSize) {
				maxSize = size;
			}
		}
	} else {
		auto interval = double(yAxis->max() - yAxis->min()) / double(yAxis->tickCount() - 1);
		for(auto v = yAxis->min(); v <= yAxis->max(); v += interval) {
			int size = labelSize(Qt::Vertical, v);
			if(size > maxSize) {
				maxSize = size;
			}
		}
	}
	return maxSize;
}

void TelemetryChartView::setLeftMargin(int value)
{
	auto margin = chart()->margins();
	margin.setLeft(value);

	auto lastLabelSize = 0;
	auto xAxis = chart()->axes(Qt::Horizontal)[0];
	auto catAxis = qobject_cast<QBarCategoryAxis *>(xAxis);
	if(catAxis) {
		lastLabelSize = labelSize(Qt::Horizontal, catAxis->max());
	} else {
		auto valueAxis = qobject_cast<QValueAxis *>(xAxis);
		lastLabelSize = labelSize(Qt::Horizontal, valueAxis->max());
	}

	margin.setRight(qAbs(30 - int(ceil(lastLabelSize / 2.0))));


	chart()->setMargins(margin);
}

int TelemetryChartView::strecthFactor() { return 1; }

void TelemetryChartView::highlight(int index)
{
	int serieIndex = 0;
	F1TelemetrySettings settings;
	for(const auto &serie : chart()->series()) {
		if(serie->type() == QAbstractSeries::SeriesTypeLine) {
			auto lineSerie = static_cast<QXYSeries *>(serie);
			auto pen = lineSerie->pen();
			pen.setWidth(serieIndex == index ? settings.selectedLinesWidth() : settings.linesWidth());
			lineSerie->setPen(pen);
		}

		++serieIndex;
	}
}

void TelemetryChartView::setZoomMode(QChartView::RubberBand zoomMode) { setRubberBand(zoomMode); }

void TelemetryChartView::setUnitY(const QString &unit)
{
	_unitY = unit;

	auto title = _varName;
	if(!_unitY.isEmpty()) {
		title += " (";
		title += _unitY;
		title += ')';
	}

	chart()->setTitle(title);
}

void TelemetryChartView::reloadVariableSeries(const QVector<TelemetryData *> &telemetryData,
											  int varIndex,
											  bool diff,
											  bool stats,
											  bool smooth,
											  QList<QColor> colors)
{
	qApp->setOverrideCursor(Qt::WaitCursor);
	qApp->processEvents(QEventLoop::ExcludeUserInputEvents);

	auto chart = telemetryChart();

	chart->clearTelemetrySeries();
	clearEvents();
	for(auto data : telemetryData) {

		auto color = colors.isEmpty() ? QColor() : colors.takeFirst();
		QVector<QAbstractSeries *> series;
		if(!stats) {
			auto lineSeries = createTelemetryLine(data, varIndex, _reference, diff, smooth, color);
			if(lineSeries) {
				series << lineSeries;
			} else {
				series << new QLineSeries();
			}

			auto events = data->telemetryEvents(varIndex, true);
			auto eventSeries = createEventLine(events, data, varIndex, color, chart->titleBrush().color());
			if(eventSeries) {
				series << eventSeries;
				QStringList allTooltips;
				for(const auto &event : std::as_const(events)) {
					allTooltips << event.description;
				}
				addEventSerie(eventSeries, allTooltips);
			}
		} else {
			auto statsSeries = createTelemetryStat(data, varIndex, color);
			if(statsSeries) {
				series << statsSeries;
			}
		}

		chart->addTelemetrySeries(series);
	}

	if(chart->series().isEmpty()) {
		return;
	}


	if(stats) {
		for(int i = telemetryData.count(); i < 20; ++i) {
			auto series = new QBoxPlotSeries();
			chart->addTelemetrySeries({series});
		}
	}

	auto staticInfo = TelemetryDefinitions::staticInfo(telemetryData[0]->availableData().value(varIndex).name);
	createAxis(chart, stats, staticInfo);
	if(!stats) {
		connect(chart->axes(Qt::Horizontal)[0], SIGNAL(rangeChanged(qreal, qreal)), chart,
				SIGNAL(xAxisChanged(qreal, qreal)));
	}

	setZoomEnabled(!stats);

	qApp->restoreOverrideCursor();
}


QAbstractSeries *TelemetryChartView::createTelemetryLine(TelemetryData *data,
														 int varIndex,
														 const TelemetryData *refData,
														 bool diff,
														 bool smooth,
														 const QColor &color)
{
	const auto &values = smooth ? data->smoothData(varIndex) : data->data(varIndex);
	if(values.isEmpty())
		return nullptr;

	auto series = new QLineSeries();
	series->setName(data->description());
	if(color.isValid()) {
		series->setColor(color);
	}

	if(!refData)
		refData = data;

	auto units = F1TelemetrySettings().units();

	const auto &distances = data->xValues();
	const auto &refDist = refData->xValues();
	const auto &ref = smooth ? refData->smoothData(varIndex) : refData->data(varIndex);
	auto itDistance = distances.constBegin();
	auto itValues = values.constBegin();
	auto itRef = ref.constBegin();
	auto itRefDist = refDist.constBegin();
	while(itDistance != distances.constEnd() && itValues != values.constEnd() &&
		  (!diff || (itRef != ref.constEnd() && itRefDist != refDist.constEnd()))) {
		if(!std::isnan(*itValues)) {
			auto value = units.convert(double(*itValues), _unitY);
			auto distance = double(*itDistance);
			if(diff) {
				while(double(*itRefDist) < distance && itRefDist != refDist.constEnd()) {
					++itRef;
					++itRefDist;
				}
				if(double(*itRefDist) < distance)
					break;

				if(!std::isnan(*itRef)) {
					auto refValue = units.convert(double(*itRef), _unitY);
					auto refDist = double(*itRefDist);
					if(refDist > distance && itRef != ref.constBegin()) {
						// Linear interpolation
						auto prevRefValue = double(*(itRef - 1));
						auto prevDistance = double(*(itRefDist - 1));
						refValue = prevRefValue +
								   (distance - prevDistance) * (refValue - prevRefValue) / (refDist - prevDistance);
					} else if(itRef == ref.constBegin()) {
						++itValues;
						++itDistance;
						continue;
					}
					value -= refValue;
				}
			}
			series->append(distance, value);
		}
		++itValues;
		++itDistance;
	}

	return series;
}

QScatterSeries *TelemetryChartView::createEventLine(const QVector<TelemetryEvent> &events,
													TelemetryData *data,
													int varIndex,
													const QColor &color,
													const QColor &borderColor)
{
	if(events.isEmpty()) {
		return nullptr;
	}

	auto series = new QScatterSeries();
	auto p = series->pen();
	p.setWidth(1);
	series->setPen(p);

	if(color.isValid()) {
		series->setColor(color);
		series->setBorderColor(borderColor);
	}
	series->setMarkerSize(8);

	for(const auto &event : events) {
		auto y = event.yValue(varIndex, data);
		series->append(event.x, y);
	}

	return series;
}

QAbstractSeries *TelemetryChartView::createTelemetryStat(TelemetryData *data, int varIndex, const QColor &color)
{
	const auto &values = data->data(varIndex);
	if(values.isEmpty())
		return nullptr;

	auto series = new QBoxPlotSeries();
	auto p = series->pen();
	if(color.isValid()) {
		series->setBrush(color);
	}
	p.setWidth(2);
	series->setPen(p);
	series->setBoxWidth(0.5);

	auto sortedValues = values;
	std::sort(sortedValues.begin(), sortedValues.end());
	int nbValues = sortedValues.count();

	auto box = new QBoxSet(
		sortedValues.first(), findMedian(0, nbValues / 2, sortedValues), findMedian(0, nbValues, sortedValues),
		findMedian(nbValues / 2 + (nbValues % 2), nbValues, sortedValues), sortedValues.last(), QString());

	series->append(box);

	return series;
}


void TelemetryChartView::createAxis(QChart *chart, bool stats, const TelemetryStaticInfo &staticInfo)
{
	chart->createDefaultAxes();

	if(!staticInfo.customValues.isEmpty()) {
		chart->removeAxis(chart->axes(Qt::Vertical)[0]);
		auto yCategoryAxis = new QCategoryAxis();
		int index = 0;
		for(const auto &label : std::as_const(staticInfo.customValues)) {
			yCategoryAxis->append(label, index);
			++index;
		}
		yCategoryAxis->setLabelsPosition(QCategoryAxis::AxisLabelsPositionOnValue);
		chart->addAxis(yCategoryAxis, Qt::AlignLeft);
		for(auto serie : chart->series()) {
			serie->attachAxis(yCategoryAxis);
		}
	} else {
		auto yAxis = static_cast<QValueAxis *>(chart->axes(Qt::Vertical)[0]);
		if(yAxis->min() < 0 && yAxis->max() > 0) {
			auto absMax = qMax(qAbs(yAxis->min()), yAxis->max());
			yAxis->setMin(-absMax);
			yAxis->setMax(absMax);
		} else {
			yAxis->applyNiceNumbers();
		}

		if(!staticInfo.valueFormat.isEmpty()) {
			yAxis->setLabelFormat(staticInfo.valueFormat);
		} else {
			yAxis->setLabelFormat("%.3g");
		}
	}

	if(!stats) {
		auto xAxis = static_cast<QValueAxis *>(chart->axes(Qt::Horizontal)[0]);
		xAxis->setGridLineVisible(false);

		auto trackTurns = UdpDataCatalog::trackInfo(_trackIndex)->turns;
		if(!trackTurns.isEmpty() && _showTrackTurns) {
			auto categoryAxis = new QCategoryAxis();
			categoryAxis->setMin(0);
			categoryAxis->setMax(xAxis->max());
			for(const auto &t : std::as_const(trackTurns)) {
				categoryAxis->append(QString::number(t.first), t.second);
			}
			categoryAxis->setLabelsPosition(QCategoryAxis::AxisLabelsPositionOnValue);
			auto f = categoryAxis->labelsFont();
			f.setBold(true);
			f.setPointSize(12);
			categoryAxis->setLabelsFont(f);
			chart->addAxis(categoryAxis, Qt::AlignTop);
			chart->series()[0]->attachAxis(categoryAxis);
		}
	} else {
		auto xAxis = static_cast<QBarCategoryAxis *>(chart->axes(Qt::Horizontal)[0]);
		xAxis->setLabelsVisible(false);
	}
}

float TelemetryChartView::findMedian(int begin, int end, const QVector<float> &data)
{
	int count = end - begin;
	if(count % 2) {
		return data.at(count / 2 + begin);
	} else {
		qreal right = data.at(count / 2 + begin);
		qreal left = data.at(count / 2 - 1 + begin);
		return (right + left) / 2.0;
	}
}

int TelemetryChartView::labelSize(Qt::Orientation axisOrientation, double value) const
{
	auto axis = static_cast<QValueAxis *>(chart()->axes(axisOrientation)[0]);

	QString label;
	if(!axis->labelFormat().isEmpty()) {
		label = QString::asprintf(axis->labelFormat().toLatin1().data(), value);
	} else {
		label = QString::number(value);
	}

	return labelSize(axisOrientation, label);
}

int TelemetryChartView::labelSize(Qt::Orientation axisOrientation, const QString &value) const
{
	auto axis = static_cast<QValueAxis *>(chart()->axes(axisOrientation)[0]);
	if(!axis->labelsVisible())
		return 0;

	QFontMetrics fm(axis->labelsFont());
	return fm.horizontalAdvance(value);
}
