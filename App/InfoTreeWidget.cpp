#include "InfoTreeWidget.h"

#include <QAction>
#include <QHeaderView>
#include <QToolButton>
#include <Tools.h>

#include <QMenu>
#include <functional>

const int ACTION_ICON_SIZE = 12;


InfoTreeWidget::InfoTreeWidget(QWidget *parent) : QTreeWidget(parent)
{
	setColumnCount(3);

	header()->setStretchLastSection(false);
	header()->setSectionResizeMode(1, QHeaderView::Stretch);
	header()->setDefaultSectionSize(ACTION_ICON_SIZE);
	header()->setSectionResizeMode(2, QHeaderView::Fixed);

	auto color = palette().color(QPalette::Active, QPalette::Text);
	actionIcon = Tools::loadIconWithColor(":/icons/export", color);

	setContextMenuPolicy(Qt::CustomContextMenu);
	connect(this, &QTreeWidget::customContextMenuRequested, this, &InfoTreeWidget::showContextMenu);
}

QTreeWidgetItem *InfoTreeWidget::makeTopLevelItem(const QString &property, const QString &value)
{
	auto item = new QTreeWidgetItem(this, {property, value});
	return item;
}

QTreeWidgetItem *InfoTreeWidget::makeItem(QTreeWidgetItem *parentItem, const QString &property, const QString &value)
{
	auto item = new QTreeWidgetItem(parentItem, {property, value});
	return item;
}

void InfoTreeWidget::setItemAction(QTreeWidgetItem *item, const QString &name, std::function<void(void)> actionCallback)
{
	setColumnCount(3);
	auto button = new QToolButton(this);
	button->setAutoRaise(true);
	button->setIcon(actionIcon);
	button->setIconSize(QSize(ACTION_ICON_SIZE, ACTION_ICON_SIZE));
	button->setToolTip(name);
	connect(button, &QToolButton::clicked, button, actionCallback);
	setItemWidget(item, 2, button);
}

void InfoTreeWidget::showContextMenu(const QPoint &pos)
{
	auto item = currentItem();
	QMenu menu;

	auto widget = itemWidget(item, 2);
	if(widget) {
		auto actionButton = qobject_cast<QToolButton *>(widget);
		menu.addAction(actionButton->toolTip(), actionButton, &QToolButton::click);
	}

	if(!menu.isEmpty()) {
		menu.exec(mapToGlobal(pos));
	}
}
